# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.0-dev.201](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.200...v1.0.0-dev.201) (2025-03-07)

### Features

* remove permission classes in metadata field API ([1467cf6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1467cf644b1a8a9cc92b11ab2d2f6cd5c18a86e3))

## [1.0.0-dev.200](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.199...v1.0.0-dev.200) (2025-03-07)

### Bug Fixes

* `candidate_value_fields` referenced before assignment when saving a submission metadata without candidate values ([7aa589e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7aa589ee5e6dba74fd225d758ea72805765540bf))

## [1.0.0-dev.199](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.198...v1.0.0-dev.199) (2025-03-06)

### Bug Fixes

* fix the double creation of the mapping ([be22037](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/be22037a1b3684e50bc299343a034d26fe278587))

## [1.0.0-dev.198](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.197...v1.0.0-dev.198) (2025-03-06)

### Bug Fixes

* correct the check submission readiness ([077a3fa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/077a3fab2f4bdda6af1d532ab23a253716a6878a))

## [1.0.0-dev.197](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.196...v1.0.0-dev.197) (2025-03-06)

### Bug Fixes

* add missing "/connector" to the url of the representation ([64a5e57](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/64a5e57ea2641853be62f8b351770f6d596940f1))

## [1.0.0-dev.196](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.195...v1.0.0-dev.196) (2025-03-06)

### Features

* handle the mapping following the new structure ([3073887](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/307388773d3748e5b489ca729b7744ccd1740606))

## [1.0.0-dev.195](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.194...v1.0.0-dev.195) (2025-03-05)

### Features

* add actions to submissions ("generate metadata" and "submit") ([97af6e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/97af6e8c0b022698938f5d9e2cea1ad15747d399))

## [1.0.0-dev.194](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.193...v1.0.0-dev.194) (2025-03-05)

### Bug Fixes

* duplicate creation of ValueFields ([0476666](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/047666624b5bfeb8e678a0573455c23386ba6114))
* when generating metadata objects, fixed a bug where the value was not set when an MO already existed but with no value ([be1cf8e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/be1cf8ef875dda2ae83c73c724b2cf112cd6e6d8))

## [1.0.0-dev.193](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.192...v1.0.0-dev.193) (2025-03-05)

### Features

* automatically check if the submission is ready ([9f1f052](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9f1f052973adfa34ad497890cd084d6554a96250))

## [1.0.0-dev.192](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.191...v1.0.0-dev.192) (2025-03-05)

### Features

* add connector fields in the API metadata field ([1861435](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/186143598d5ebf9213ca838ae65b01eeb402a746))

## [1.0.0-dev.191](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.190...v1.0.0-dev.191) (2025-03-04)

### Features

* handle errors in metadata generation ([39d87e0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/39d87e020986b0868cf16e3e530595e7197997da))

## [1.0.0-dev.190](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.189...v1.0.0-dev.190) (2025-02-28)

### Features

* consider submission's associated data in the creation of metadata objects ([1b81080](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1b81080b26da9d6922a9fd6a4a7e3e53494658d1))

## [1.0.0-dev.189](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.188...v1.0.0-dev.189) (2025-02-20)

### Features

* replace SQLite with psql ([e8a863e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e8a863e1a3aec1271d7426871abad0fcea16080b))

## [1.0.0-dev.188](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.187...v1.0.0-dev.188) (2025-02-20)

### Features

* change the schema handling between the core and connector ([bbaeb63](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bbaeb6386a9c4a3520140bbbc7b0d5631e82cc81))

## [1.0.0-dev.187](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.186...v1.0.0-dev.187) (2025-02-14)

### Features

* trigger metadata generation on submission patch with empty body ([0a719d0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0a719d0d3c1d6e9d61019858f261b7254e978db5))

## [1.0.0-dev.186](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.185...v1.0.0-dev.186) (2025-02-12)

### Features

* add two functions to export the nodes' description from the editorJS format to plain text or markdown ([a489a07](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a489a07af4d9f488e55624b3eb900924452a1db7))

## [1.0.0-dev.185](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.184...v1.0.0-dev.185) (2025-02-12)

### Features

* create a statistics endpoint ([e012f9e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e012f9ef480425392aca4fbb5fc78d04dcfae25c))

## [1.0.0-dev.184](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.183...v1.0.0-dev.184) (2025-02-12)

### Features

* start the metadata generation asynchronous task when adding data to a submission ([9994da6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9994da6672fbad1be7a09c65d8806ae340b1b98a))

## [1.0.0-dev.183](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.182...v1.0.0-dev.183) (2025-02-12)

### Features

* create function to download data with a data connector ([7d947a9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7d947a9587a9ecf210a07cb40f1e5d6a6aa2a67f))

## [1.0.0-dev.182](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.181...v1.0.0-dev.182) (2025-02-07)

### Bug Fixes

* fix method name in serializer SubmissionMetadataObjectSerializer ([948b21f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/948b21fd2168e39c5f8015f4c7feac03f2c83ddb))

## [1.0.0-dev.181](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.180...v1.0.0-dev.181) (2025-02-06)

### Features

* change node description in json format for editorJS ([7cd6f61](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7cd6f619d37d5ae54f8a771518e3b59aa1f7a7f3))

## [1.0.0-dev.180](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.179...v1.0.0-dev.180) (2025-02-06)

### Features

* separate summary schema and use summary_field as list of str ([89b2a6e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/89b2a6e46e649d5979d6c8ac1edbe81668e8a4dd))

## [1.0.0-dev.179](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.178...v1.0.0-dev.179) (2025-02-06)

### Features

* asynchronously create metadata objects by the submission connectors (without data) ([16cb3c7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/16cb3c72399134bd5761a729b42460c7c7e069a8))

## [1.0.0-dev.178](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.177...v1.0.0-dev.178) (2025-02-05)

### Features

* add upload type as zenodo submission settings ([5fd0ed6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5fd0ed62ae5f442bbb5bbd57f11d24951df3821a))

## [1.0.0-dev.177](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.176...v1.0.0-dev.177) (2025-02-05)

### Features

* add autoreload capabilities to celery ([fe45489](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fe4548901e62ddcb5b1404bb849cf365a8d159ec))
* create new schemas ([970725b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/970725b89e761704280c650a3b2d187411a0905d))

## 1.0.0-dev.1 (2025-02-05)

### Features

* activate autorelease ([d944251](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d9442512c4b49fb21848fcd64a600966dd4b09d4))
* Adapt endpoints to workspaces through a X-Workspace parameter ([b0a9834](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b0a9834f144332744f16eb37d455c54b00a3977b))
* Adapt the endpoint for workspaces and nodes roles ([5cce2b4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5cce2b4e621dd34bf9d991f5f63be3ad69e898fe))
* adapt the notifications to the workspaces ([e061c38](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e061c3876bf5b647f16f44e22223d319bc3220c4))
* adapted notification tests to workspaces and corrected the implementation ([612fb88](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/612fb888474803140d77d270ee1811d07b61b328))
* add a command to create a demo workspace ([f5444f0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f5444f0b55a3bfb1db516b54d3deb8a924efd905))
* add a condition for leave on the delete member ([5bccd41](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5bccd413e04c5bfc8fe5320dab6be3a74773b966))
* add a function to submission connectors to return metadata object types ([eb05a85](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/eb05a85b94930388ad7ac05c12b874725c780a27))
* add a schema for the sample ([689d6fb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/689d6fb43a190409469645b4bf6ab57e04090af2))
* Add a slug in workspace ([f95eb29](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f95eb291c78e4552a124dd0ee3780e5862e6fa98))
* add a slug property to node types and create NodeTypeCollection model ([1f687f2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1f687f209c0e33e37ab470ce38692c4486024bf2))
* add abstract class for mapping publication metadata fields ([60809f2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/60809f2242e364b86ea81338210464b0f8831335))
* add abstract class for mapping publication metadata fields ([9f042d3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9f042d3b4e34c258d2c87840782bcb83f32b2ad3))
* add alias support for sample ([1ae2bdb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1ae2bdbddf930f54f8e369fba37ef25fd70ab6b2))
* Add all members of the parent node when creating a node ([068375f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/068375f7f06b82970e32ab15ea9f7aefbdbd196d))
* add all metadata schemas for zenodo ([4ee6b98](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4ee6b98bd2f850e8e6511fd49c9f08b57617364e))
* add allauth package and new authentication providers ([540039b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/540039b6889222c91666ed9f43cf932a6c7b0943))
* add argument parsing for schema processing command ([784e552](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/784e552b41a63e2a016e01526e77dc3a0d9a0a3c))
* add async connection verification on access ([a01c91d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a01c91d6e1bfbbccc730e3ce1d197299c78da90d))
* add autoreload capabilities to celery ([fe45489](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fe4548901e62ddcb5b1404bb849cf365a8d159ec))
* add better support for production settings and Uvicorn worker ([39260d6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/39260d647599253fe612ed044bd643275ba562ef))
* add descendants samples to the API return ([ad158dd](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ad158dd1f481938969701918a2aa4c00b443c3a2))
* add filters on workspace members ([3462ad9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3462ad95b7064e26d039fc927e29f53103b55cbe))
* add format orcid ([1fb1454](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1fb1454f2bfef16be718f9fec476ae45e2f19f4f))
* add GET API for listing publications ([d25819b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d25819b5664d5835df506c64d14a321c9f20f07d))
* add investigation to a Tool creation ([f8c8af6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f8c8af679e1b547264fcbe8d444c7d127ee71440))
* add members permissions for the PUT ([525bc5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/525bc5a46e037d780d3f194519b08c351d9bdf78))
* add metadafield mapping section in readme ([d29a898](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d29a89825c82784630ad453671050fe6ea35f707))
* add metadata groups ([0965ce8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0965ce8a4414aac1f40a39a3bbfed08b1049e2e8))
* add metadata schema view ([09f1f51](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/09f1f51201a15221c9d48dab4d200e56e5ce9404))
* Add MetadataMapping ([ad456c1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ad456c16d4de9504b9c61d57d58cd77beaf74d40))
* Add MetadataSchema and MetadataField ([29981a7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/29981a72dddce213fdadb730064301d3ad42dfd0))
* add model and endpoints for data associations ([4202299](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/420229901cd7f1f7450e9c626021c9b64ab0d106))
* add new metadata for data objects ([0125b74](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0125b74ab1ece02b5ae370432dc4ff3d3825af76))
* add node title to submission ([41963b2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/41963b28f2c1b75a2679ae38705c0a91d6b5dead))
* add notification triggers for the members ([b0b9280](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b0b9280051d5597de7616fd59d20119dd806182d))
* add permissions for the delete ([3b19b15](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3b19b158a1308fa20152407a228e899f4e3fdb11))
* add permissions in the creation of a member ([e917f1a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e917f1ab000390313f48a6dd7d77ee23d2ba65b1))
* add permissions on the GETs members ([5bcb15c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5bcb15cf2dd32f0bc8d144968dbc71aee999f48a))
* Add Publication, PublicationMember, and PublicationElement models ([f05a003](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f05a0032b97944258e2a35886eee1032807455dc))
* Add PublicationSerializer for serializing Publication model ([b5a4105](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b5a41058f4ab03d83d840575aa234f5dfb088f50))
* add restriction - you can only add Node members if they already are members of the workspace ([7cad253](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7cad25398b40a288a5000b1bcb25388e8b9577b1))
* add Sample bound data endpoint ([b06e796](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b06e7962f1d183ca335cc5e16c7a44a8917b4a0f))
* add schemas for zenodo's submission metadata ([25bd2dd](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/25bd2dd3f81b09a913d7143c877527fa47dab451))
* add study & reads metadata for ENA ([8961a99](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8961a991b66557d24b710ee41f4e6448c250fc2d))
* Add SubmissionSettings creation during submission creation ([71b498d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/71b498d99ec0bbacd6b18b393a536a0c1cb222e1))
* add tests for bound samples endpoint ([c0ca128](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c0ca128ba5ddfe882e0f6e050cebea092af6ef97))
* add tests samples ([00fa818](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/00fa8188d0a424c658de68a3d9c18b632280f317))
* add the "additional metadata" metadata ([fd7b180](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fd7b180b236fa722649c54fc949ce26305a0b144))
* add the contributing organization ([afead15](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/afead159cd5cde77dc7bedc2c5e4ae9bf1e3fc9b))
* add the members restriction on the publication endpoints ([510a8c9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/510a8c9afd6db855997323a8ce905e2ea947363b))
* Add the Sample endpoint ([4fb91eb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4fb91eb60957dceb72cf1269ff738c6d98dd7aaf))
* Add the serializer and endpoints for the bound samples ([ac2050f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ac2050f159b92934e6b10ea48b1fb26578528e39))
* add the submissionMetadata endpoints ([5256b5b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5256b5b4b387890d5ed046a207f69a25ec60cbf2))
* add the submissionMetadataObject endpoints ([bef2cd8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bef2cd8b0f5ac962a7990b82aaa7835f320e8332))
* add websocket for the workspaces ([b4cbd3b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b4cbd3b3047f4b1128b57f6dcda0d1b65aad0f22))
* add wokspace member model ([396a4c9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/396a4c9b4d42535ce202e461e64df05b4f2d2669))
* Add workspace linkage to Connection, Data, and Node models ([c553dc1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c553dc1a0aec39f440b4fe687d290be13f8e4295))
* Add workspace model ([a2aa0f7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a2aa0f7708cc61a12355f91b1acda2fc0d0c9f76))
* added a general class method on connectors to return their types ([ef73b65](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ef73b652040f5b0ed7034d8018cf5e49b8c5158f))
* added a related names for datalinks on data ([b2ca427](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b2ca427aae55a5f2a5d1c4d23d13fbe5e80e48fd))
* added a search filter and filters for data types and related types ([b325fbf](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b325fbf1c6e083963cf67eb5efc8d08ea8500ee8))
* added loggers for API errors ([911770a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/911770ae5904498eebd73edaf6376d44b8011e71))
* Adjust the submission mixin to handle submission role ([afbeae9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/afbeae9f2733d12e4f4ab34bc3f29dc36e1486b6))
* allow the association of all metadata field to nodes ([ec921d8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ec921d8291c256dc3b51f1ab24a37df326cded9a))
* better support dataobject descriptions and links when creating a datalink ([c5cf9a3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c5cf9a330272d4bfc0334b6686b24b281a113635))
* Block submission endpoints during metadata generation ([92ab67a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/92ab67a01dd05ef655409b941d1370315595493b))
* change django-allauth version ([15592e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/15592e81078f7114e8be125d5bc27abe1f89ddcb))
* check data size with async task ([bf8e7d0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bf8e7d098148eacf59ddd190cf723c926c7575d0))
* check related type on metadata save ([abb6152](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/abb61520c6fa8aa091d6eb7bcd109ede84db7d43))
* **connection:** Handle the creation of empty connections for publication connectors ([7a460b6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7a460b6108942f5554aa8b269df9bc0ae855e8e8))
* **connector:** List and configure ENA settings fields ([e3ff5d2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e3ff5d21df7da6f8d44778b577a8a7b13266b90c))
* **connector:** List and configure Zenodo settings fields ([9f32097](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9f320977a5bab095df97fc5e5e9d2a9650d0ab75))
* create a ApplicationUserParam model ([e604342](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e6043423a5cf58d70b488f551ab4ffdfb8b4b7fb))
* create a datalink to Galaxy ([e3eab41](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e3eab41f8770e376d695708b35f81268a4b3b316))
* create a history field to keep track of the last_edited_by and last_edited_time in Node objects ([1833a27](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1833a279ddf457d869af92f357f87adf0af66f78))
* create a management command to create the metadata mapping files ([c6234c7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c6234c78eccede024d5147d9c9ce40bb0bd14ee1))
* create a new endpoint to get metadata mapping ([4252346](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4252346019d59da09f9439c573a9ab3604582d37))
* create a release and CI config files ([6f76428](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6f764285d68250d7db1187c59a7aebc3ca4a08a9))
* create a submission metadata object table and modify the submission metadata one ([291d6fa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/291d6fae2d88ee23abc2944189889928baff46d3))
* create a Swagger api ([4caa885](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4caa8858298bf1b988f107a17da2b5cb1b75c4d9))
* create a Tree node ([e121c1f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e121c1ff0270336a696ebf7eb933c2b06b0412b0))
* create a websocket for members ([cd5522b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cd5522b4fade6b2197b6443e750a2b6748237970))
* create an endpoint to get api version ([7557ffe](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7557ffe674654507c203ab76b6ca98f0cf3bd5b2))
* Create an util to generilize the trigger of the notification ([2b9c71a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2b9c71a317c8a7e2bedb43f0cf719bab7091e83d))
* create CI pipeline to check quality ([100c6a2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/100c6a2d128881e8b3a188a710aafef101361dae))
* create DTOs for submission connectors ([88867f8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/88867f85b6106bf0c6bd9afa4690c5351ae95cf1))
* create endpoints for WS and notifications ([8d3f74b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8d3f74b0b867fa4bf232750e3a2fbfbbc4e9d654))
* create fixtures for Node type ([bcc1942](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bcc194282091105a6cbf6360e5dde64f84243df3))
* create function to get metadata object type and get json schema from connector ([b898726](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b898726fbf5e32cf76f66da1927162453e83c844))
* create get list endpoint for Data ([ad1623f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ad1623f22795de149a16bfcdbec8e8a8ed665f25))
* create get_metadata_field function for the submission connectors ([43e350a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/43e350a0124fadd80a995acadd2b1a8eaa680ef0))
* Create new filters in connections ([7275eee](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7275eeead22e5f3f40fb2a3a9693a3bef9936c7a))
* create node and node type models ([774c6d1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/774c6d1ea9e52be91464e1d292ffd07ca7608d1b))
* create node type serializer and endpoints ([dda3946](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/dda3946e43e22d68d9f006972044ef3eb34674a9))
* Create one member at a time ([7aeeaa8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7aeeaa8399b52d1ebf50c0fa956dc166a0b965f1))
* create periodic async tasks ([0b053eb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0b053ebfb3af3600de61eccfe48d7e4896f18d87))
* create the metadata model ([5b50f60](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5b50f604cf5c2d0dfe95f5df238c8662c3271b05))
* create the model related to sample ([44d6ac1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/44d6ac12f5a979b5fba26657caf54090faf41de3))
* create the node serializers and endpoints ([392384e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/392384e79d552ecced5ccb28b073a9a4a83e5acc))
* create the sample serializer ([c7cab19](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c7cab193846c0e461332218bd3592dd436e2d40c))
* Create the SubmissionMetadata model ([29eeb14](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/29eeb1473a66dcd0c0abda5bf5888a9d33433077))
* created a GET detail, PATCH and DELETE endpoint for publications ([3bed472](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3bed472302621d19f0d49cd481188251cf24ecca))
* created a management command to import the metadata mappings of publication connectors ([f7abdcf](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f7abdcf430157abf658fc543266ff0365fb6aa18))
* created the view for metadata fields ([cd5b031](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cd5b031ec9b293722ea5283b8f8b1f36b1875250))
* creating a test architecture ([f5f84d6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f5f84d6436a9db78267c990d389c4adf5f21be11))
* data access right ([75648cb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/75648cb882f6242798539ea1e6da59d76f4f908f))
* Define permissions and add them to a custom decorators in the APIs of members management ([af32dfc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/af32dfcf0c9b465c7353200941f075cb0788f26e))
* delete an investigation by only the owners ([ecd8f0c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ecd8f0c42ffdae81231985c1cc2b79ff75ce5b75))
* **dto:** improve structure and validation for submission DTOs ([a89c0cb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a89c0cb400bdd623618c9753e4e8ca25abf82636))
* edits on metadata inheritance ([4148066](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/41480663e93ab8de2b3fdfdf559572df8c3736d6))
* ENA publication connector ([309cac3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/309cac3862cb0b9a73d5b63197a5c13a5c0150c6))
* Endpoints for the data submission ([bc1aaef](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bc1aaef306537f04df5ae0b15146c394fd42500b))
* Enrish the demo with a submission and data association ([1464422](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1464422bcda4ee4860a699974457979bdbe219e2))
* error refactoring ([5f871fe](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5f871fe2868b8a5afa98dc313f711d429f79cfbf))
* establish the ENA connection ([d90a5f4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d90a5f40bc331d408a3ebb797cac637b6764f632))
* filter get connections ([6a5e1e3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6a5e1e37a4fbf3d40ff625b30d2f3a794cbe75cb))
* filter users by fullname, email or username ([711fcf4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/711fcf4d2c2d036ba9be94ccddc0b7639fa036f1))
* generate a demo ISA objects tree ([4edaceb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4edacebddf6e8dd67bb55fca01ef3c8c74bca51e))
* Generate all json schema for ENA ([2a641be](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2a641bea37bf653231cd1d0241504c368021673e))
* handle the documentation of the publication connector ([722eb5d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/722eb5d11a8fba54ae9757ea66a865dcbf92178b))
* health check ([2cce3bb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2cce3bb8d87b3f3fe3daea7845a19a3ee77f1936))
* implement celery task management ([5130af0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5130af061e217d3842b7d4b5bad3345b8c77f4d0))
* Implement the serializer and endpoints for Workspace ([a35f17c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a35f17c280a9d4503590d1950a87d0762a15e258))
* Implement the serializer and endpoints for WorkspaceMember with their tests ([84aac8f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/84aac8f797d4128a7434be8757e3d61914db654f))
* improve data icon ([ca56f5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ca56f5a65281ab2e943906dfcbd8f0cf4f3678bf))
* improve exception error for connector ([0d2c0d9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0d2c0d92d67deb2c52858736d386c9aa2ff7b702))
* improved the usage of connector errors ([d28f8ec](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d28f8ec83fd47418564dbf68f52c7c9a30e0adba))
* improved user account creation ([38d9daa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/38d9daaa9b4915613d636f638428eb463b970980))
* Include an inherited sample property in the endpoints responses and adjust the functionnal tests ([a765c85](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a765c8540c1f0545f7cf385c6f7a875909dd37fd))
* include metadata heritage in the metadata endpoints ([6bdbe87](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6bdbe87688916c59c8734857b36ca38f73cabeca))
* Initiate madbot referentiel ([01a5891](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/01a589151ddb3c7cef9722ddbed9e36073f767fb))
* introduced metadata related to data association along with the first metadata, raw sequencing paired ([9561fe6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9561fe6fa1f35a20c4c6a7eb101209419eebcd27))
* introduces new ways to handle error uniformly ([9fdc98e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9fdc98e4c74791961e0667a73776d2e77167e314))
* let workspaces' owners access nodes (and their underlying resources) even if they are not members of theses nodes ([b987478](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b9874789e2b8c3684d0e7011a94485f7f8dd40f7))
* license changed to BS3 ([7dbca7b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7dbca7b38efa4be5f99158826b2bf630135caf46))
* make CORS allow a certain list of headers ([2c94fa8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2c94fa8d37757ddc16068e24074ca9c74ce2f8ab))
* Manage investigation members ([7a6b47f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7a6b47fb2c2e6f3bd535ddf8bda8e54fb3492f7f))
* manage POST/PUT/DELETE of a member with a role below or equal to theirs ([00044db](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/00044db9542c0efe491cb3a4318ecafd2699df98))
* manage the creation of multiple members ([d1d074f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d1d074fe56d9f1472d53e81d71e3d534f43bffd1))
* metadata endpoints ([8bcdcb6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8bcdcb6a620b108e10bf565fd3a9e8a607f6b4f5))
* Metadata inheritance management ([4b70c9c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4b70c9c4551efbdbddb211b10e8c4654ab449cbb))
* metadata validation before saving ([a03ecfa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a03ecfa6fd2567386f36f24e00f818b064ccd78f))
* Model for the submission data ([866a813](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/866a813682961ff3b0c76522510026a9cbe824fc))
* new CI pipeline including better code audit, python package release and docker image release ([a6f38e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a6f38e8c2e8fdf9be3e6bbfdb94c260a5ce9f756))
* new schemas data (orientation, publication, ...) ([afda468](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/afda46881deabf0bd3929c30527332b77ecdd38f))
* new standard pagination ([d54c8fc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d54c8fc1dea911b3225f30a010c39dec8ec656cd))
* **node:** add sample, datalink, and metadata objects with related URL and count ([4061a47](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4061a47b1cd267b755656b37c6331a51704af93a))
* Oauth toolkit ([7dbe796](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7dbe7960e27094fcaeb0ae17b91e8566728744d5))
* only show tools from a given investigation ([0b51191](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0b511911b2f0578387e0d8acac8d60771e6b1ecc))
* paginate study list ([ba45659](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ba456590e8806643e100d23710829afbdad6cb7f))
* pagination of investigation members ([b216c91](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b216c917aea3831500173be4d53d44ec7b6b4017))
* prerelease ([335defa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/335defa84f02589743fc9ead067872e5ae87a11f))
* publication api post ([26856c2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/26856c22eb8579dc3dd203f43a112ad092c76039))
* publication member endpoints ([3d103c6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3d103c6bca0bbe89f6d598eff1266effca543056))
* re-adatpt the SSHFS connector ([b9015a9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b9015a9cf09a2cf92096426874fea85c47195f01))
* Readme initialise madbotfields ([e8e8cd9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e8e8cd9217dbe2a1529d30147ab05f908558fc12))
* recover the media type and type from the connectors and use it in a data object ([8170408](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/81704085088d7a43fb220c0a6d3a56555fa27d7b))
* refresh Connector API and Galaxy connector ([84a40b1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/84a40b101a32ecedc366d82f55f65d6c912950bb))
* Remove all connector schemas in RawSchema table ([6dfd0f0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6dfd0f0584aec2e1b7003ce2f8045580323fe225))
* remove owner ([67592de](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/67592de344facadcc28402ae573c0c2525af0f44))
* return connectors, tools and dataobject icons image as an url and add support for FontAwesome icons ([9c5ec62](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9c5ec62038e39ee984d8efe43850d0b5ee941a36))
* return datalink(s) used to access a data when retrieving it ([15bbd3b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/15bbd3bd10e7f50990f692528890ce67485a0e32))
* return the member properties in the investigation API ([d4fde96](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d4fde9609541a5aca1c6c804800afb691661244c))
* reworked error messages ([4ce64aa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4ce64aac80e83e3f419b88362f7b2396ae91adb3))
* **schema:** update JSON schema using oneOf instead of if conditions ([81c04e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/81c04e802b9e14e8d8bfed5fe0565f8343c4b186))
* search filter in study ([e83a015](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e83a015fdabc06a224b67c2d191ff0585f7baa7b))
* set status as generating metadata at submission creation ([c6c5edc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c6c5edc81de16c5195df73968cdb1ed975214574))
* settings schema for publication connectors ([980a0f8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/980a0f8935f558679d8bd2c6ea886b1efbf244a3))
* **settings:** add DTO for submission settings with mandatory field support ([675e029](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/675e0299455299cdef4984e15520e7ad581080af))
* show all connection of the workspace in the get list ([1037136](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/10371368e0f184622a78689a0babac0bdd15d289))
* submission connectors should return supported files ([c334ef4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c334ef4f9675b80f68ec86485583b0b16d2f2f58))
* Submission settings endpoint ([383568d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/383568d0e1328728fbbed7e0826278e6f3799139))
* **submission:** add SubmissionSettings model and update submission relations ([45c7336](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/45c7336f209f355561db0dd3991c9f0319493e4f))
* test assays ([788d32b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/788d32b88f93d16474c7f5d0623f143013999fe1))
* test notary functions ([34ed816](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/34ed81690cb1b99bb49dee825703be4510e1b6ef))
* testing for Members APIs ([cbebe77](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cbebe77423d4a278ae687f44ad2afde960ea007a))
* tests on /tools with a fake connector ([44c7b91](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/44c7b91de16c71ac9484965ad0a5deba661d5bee))
* update README and add Flower basic auth restriction ([f476ea1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f476ea1c158fde4152241b93977fa652730bc487))
* validation of taxon schema ([517b33e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/517b33ec9d10983bb7cd92365209a5728c9e3bef))
* When bounding a sample to a data, the sample and datalink node must have a direct parent-child relationship. ([b173d56](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b173d56c17a8b48919c3a90816bade52739b52e8))
* Zenodo publication connector ([120b132](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/120b1323e5d9193a99fee29c649ae6a19b4118df))

### Bug Fixes

* "blake" corrected to "black" in ci ([881b01d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/881b01d95acaff2fa48c9016df252c75b1926776))
* access datalinks from the related property and add annotations for member role ([bc694a8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bc694a8d9339aa78aad9232208573e9dba60edd3))
* activate study in demo ([f1d02a1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f1d02a16bb593b477d95799f7bcfe47929273ad4))
* add a condition on the members put ([b32004d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b32004d34defcb68549d63f62d4c97bddc4c1151))
* add a decoretor for the investigation delete ([94f27e0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/94f27e0f4638b6b787a7e83ded2b02ff1e5bb37f))
* add a restriction on connector type when requesting external data for a connection ([94b0456](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/94b0456dd612a951c4076fe3875ea07e691694cf))
* add api to the url of the schema ([7344d79](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7344d79c5bb7f978b79a72f985b966f038bc531f))
* add labguru project url ([d25462f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d25462f0b6f0c6f18ff7caf6855b41ae77576dc6))
* add missing items in the date schemas ([bffdcad](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bffdcad4e9fb53c6b19ece0695ca2375d18a70f1))
* add permission on the delete investigation ([7ac24e7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7ac24e7c45790deeeaec9259701a20f4041daae9))
* add the `type` property to json schema with array items that were missing it ([c0ad052](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c0ad05271556b14cf5566e4a6cebd1a75f53a951))
* add the field property to the return properties of the get metadata endpoint ([b0dd9dc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b0dd9dc15778bb750783b41278549009e9ac8812))
* add the workspace to read only fields in the submission update serializer ([2b7d5aa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2b7d5aac55114cd102ca44ad0143797e0b3a5b6b))
* Adjust Ruff Configuration and Codebase Compliance ([6fa17fe](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6fa17fecbde6f46b2b47cfd3c91b1438dc8b86e4))
* bypass python 3.11 warning concerning system package break when using pip outside of venv ([c69d59f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c69d59fd722b57db425f08657b95e119cd595a50))
* change member role control during creation ([057cb70](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/057cb707059eb953925e9f8e55161ace9c50da21))
* change the handling of the ci checks and upgrade django for Safety ([2cf0795](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2cf079583d8d4d141b21fd793983e8936243b59e))
* change the oidc_claim_scope ([04afa35](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/04afa35dc9abb6e1cc1d149965c3efaa5546b504))
* change the response status in the post ([ba7e5d5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ba7e5d550f9507a125889289a006b761259ae71f))
* changed status codes in CESR function names ([26716c4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/26716c4b7a86ba7b8c6041d11c1a6602d1a44adf))
* corrects a security vulnerability when parsing some requests parameters ([a2167e4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a2167e4e2cd391014b04c57894ec3ab935c12289))
* creating connections with no private params ([d572125](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d572125606467b5d40647fb47715a6a963efaa34))
* delete all NodeMembers of nodes in the workspace when deleting a WorkspaceMember ([282f74e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/282f74e089862d6a1a2e4b1f98d90b7a4192406b))
* destroy action on submission mixin ([9b562d4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9b562d495b04acca0a1abb465428557418751d0e))
* disable mimetype detection to improve ssh connector speed ([a08f7cf](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a08f7cf30b8dc190a5060f7c2e71711b86979312))
* duplicate search metadatafields ([cb2a58c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cb2a58c2410bc2425f7913d3782ba698017add95))
* error when creating a publication member due to the  data broker restriction ([c68557b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c68557b46ef23c977a8ac6ed7050778337c2e852))
* **filters:** exclude groups field from metadata search ([98a42ad](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/98a42ade6a1de7fea12530a60443db36d715be11))
* fix error when retrieving history ([11b4e9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/11b4e9dd3bacf4d0ce4ce3bebff69ee8bb53affc))
* Fix security breaches and code audit ([82044c1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/82044c1da1d2ebdca895c3ca8cdff764aec2a7b5))
* fixed package discovery in setuptools ([cb12509](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cb125095ecf65e372936731fcccd8544800e7ee9))
* Group all management commands related to schema initialization into a single command ([886e089](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/886e08973655344f8f368af10f3c85b6786ac25b))
* Handle default values from ENA Access Control settings schema ([e3f5f08](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e3f5f08002d0e0686823c04467e12d161a1c5ff9))
* handle the case when no param is passed to the error ([78b57e6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/78b57e618af3088614cfc7093b5fac412df805c8))
* Improve docker image and python package ([009b6ba](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/009b6ba3759422f5fd1f6ffd8eaba0c224d7aae8))
* Improve error handling for node and member endpoints. ([48e799d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/48e799d9dbca5fc67af3d8373a5f4a74a7bce2f3))
* Improve the listing of files and directories in SSHFS ([d398fb2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d398fb2808837d35baa1af6dbc6741a09d34efa2))
* improve ws message ([e47c8ed](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e47c8ed0754d2e333da172a38c04b1070f0064cc))
* improve ws response ([5bef75f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5bef75f2f7b8b73c0537d1c0b3b9108fbe4cb8c1))
* inverted contributor and collaborator roles ([e1e8458](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e1e8458515443579d256e992595da412351474e9))
* issue when selecting a node metadata from a data ([0d0211e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0d0211e6b034f9ff64fa8d2d66399657158d5c12))
* modified the description of the publication members ([5638279](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/56382790ed8ca870a140aab7b8cd04121573975b))
* patch semantic release ([7c36c46](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7c36c460cdac4c08bdea8d1c31101767fc487c12))
* prevents bash code injection when retrieving external data objects ([47d90fc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/47d90fc4e5d5e00996b0aa9677f03947df16c6bb))
* prevents reuse of the same connection across different workspaces ([ac28537](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ac28537e3b8487ece616175d39e79523058b604d))
* python package was not including static files directly under `core/static` ([4493b64](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4493b64fabbdfba4d51ce8129a2349cd25320279))
* reestablished the filter on connector type in the connector view ([ff9cab9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ff9cab902fe0f8a146afdcd6e4fff2d947c77d6a))
* remove site redirection in the admin ([0d1fcdd](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0d1fcdd92d81d890a64cdba26195ececea528296))
* removed outdated CI badges from doc ([5ec05f5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5ec05f52d13d24d60db29e254b7ce960c7e3c153))
* rename openlink to madbot ([b863b9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b863b9dcc76b6aa482dcdcc40c5691b6a9c1cd28))
* Replace 'required' with 'level' for field validation ([fc54218](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fc54218b7d4125840088f6e3b39e1b53e6cda2c8))
* replace the mime_type by a media_type ([680cdc8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/680cdc8e66a18072b8564bc4984b407a18717660))
* resolve the "django.db.utils.IntegrityError UNIQUE constraint failed"... ([c7fe35b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c7fe35b0e23ec99dfab6f7ff463b3f0fc917eabb))
* resolve the `Multiple top-level packages discovered in a flat-layout` error with pip install ([414661c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/414661cb91fb74ce55032b41addbf1413acbeb17))
* restore support for file extension in SSHFS connector ([fa20009](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fa2000991aff6b21dc3aa1d39d78641277ef7caf))
* **schema:** correct 'sex' schema ([d9553f5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d9553f56fdca9b8e6fa544636ab2841b79720f53))
* schemas of connection parameters ([8d681b6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8d681b6039fef5955cb10c51d8edf32b2e3d41f3))
* separate the data and publication connectors ([85f9c9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/85f9c9d26af98850f4e7859df8ffcc1673d79840))
* **sshfs:** enables navigation in SSHFS subfolders again ([c826af9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c826af9892e9bb5a80b844c18ee2cd7bc5c616cf))
* submission settings extract from connector settings path ([b6e4de6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b6e4de6185264393e6e183219fd405bb0e17aa1a))
* the affiliations on person schema should be a list ([d625395](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d62539568773a848d4816b6f052e7c53a5ae00b1))
* unique constraint on data and datalink ([4296528](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4296528d11c282a421ddd8ad68f1acf642bd8810))
* Update tests for member endpoints ([259ca13](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/259ca1310b9a18fc70c66de04be0a92441e399e8))
* Update tests for node endpoints ([6ee7dfc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6ee7dfc751d0858a5563f103480773fd509be88a))
* Update tests for nodetype endpoints ([3bd6ab6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3bd6ab603aa04287d42e5f91f42c1a0cb8c885a4))
* url galaxy schema ([65d6384](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/65d63842358ba067a0b269f2e59925d1cb455a11))
* use 403 Forbidden instead of 401 unauthorized for API calls with forbidden access ([2fe6315](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2fe6315fd828e2807e25381338dbb5c0e57f8908))
* when creating a new bound sample, check if there is already an existing... ([f2cfc9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f2cfc9da9b9ff297d4142ca0037213142708adbf))
* when deleting a connection check that no publication with that connection exist ([f9d0c5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f9d0c5a7442762d38531c367d6b870f672d8ca9a))
* when retrieving a data, ensure that the user has access to it ([c6e220c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c6e220c3181cf0d4fce1ccf521786ca12c7a53e1))

### Performance Improvements

* reduce the number of queries to fetch collections and datalinks ([0ef3eb2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0ef3eb2cf155b2e40fc86c0c4d260b0406c4b22d))

## [1.0.0-dev.176](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.175...v1.0.0-dev.176) (2025-02-05)

### Features

* change django-allauth version ([15592e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/15592e81078f7114e8be125d5bc27abe1f89ddcb))

## [1.0.0-dev.175](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.174...v1.0.0-dev.175) (2025-01-22)

### Features

* Block submission endpoints during metadata generation ([92ab67a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/92ab67a01dd05ef655409b941d1370315595493b))

## [1.0.0-dev.174](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.173...v1.0.0-dev.174) (2025-01-20)

### Features

* **dto:** improve structure and validation for submission DTOs ([a89c0cb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a89c0cb400bdd623618c9753e4e8ca25abf82636))

## [1.0.0-dev.173](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.172...v1.0.0-dev.173) (2025-01-20)

### Features

* set status as generating metadata at submission creation ([c6c5edc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c6c5edc81de16c5195df73968cdb1ed975214574))

## [1.0.0-dev.172](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.171...v1.0.0-dev.172) (2025-01-20)

### Bug Fixes

* change the handling of the ci checks and upgrade django for Safety ([2cf0795](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2cf079583d8d4d141b21fd793983e8936243b59e))

## [1.0.0-dev.171](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.170...v1.0.0-dev.171) (2025-01-06)

### Bug Fixes

* python package was not including static files directly under `core/static` ([4493b64](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4493b64fabbdfba4d51ce8129a2349cd25320279))

## [1.0.0-dev.170](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.169...v1.0.0-dev.170) (2024-12-24)

### Bug Fixes

* the affiliations on person schema should be a list ([d625395](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d62539568773a848d4816b6f052e7c53a5ae00b1))

## [1.0.0-dev.169](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.168...v1.0.0-dev.169) (2024-12-24)

### Bug Fixes

* Handle default values from ENA Access Control settings schema ([e3f5f08](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e3f5f08002d0e0686823c04467e12d161a1c5ff9))

## [1.0.0-dev.168](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.167...v1.0.0-dev.168) (2024-12-23)

### Features

* Submission settings endpoint ([383568d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/383568d0e1328728fbbed7e0826278e6f3799139))

## [1.0.0-dev.167](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.166...v1.0.0-dev.167) (2024-12-23)

### Features

* add the submissionMetadata endpoints ([5256b5b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5256b5b4b387890d5ed046a207f69a25ec60cbf2))

## [1.0.0-dev.166](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.165...v1.0.0-dev.166) (2024-12-23)

### Bug Fixes

* destroy action on submission mixin ([9b562d4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9b562d495b04acca0a1abb465428557418751d0e))

## [1.0.0-dev.165](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.164...v1.0.0-dev.165) (2024-12-20)

### Features

* Adjust the submission mixin to handle submission role ([afbeae9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/afbeae9f2733d12e4f4ab34bc3f29dc36e1486b6))

## [1.0.0-dev.164](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.163...v1.0.0-dev.164) (2024-12-19)

### Features

* add the submissionMetadataObject endpoints ([bef2cd8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bef2cd8b0f5ac962a7990b82aaa7835f320e8332))

## [1.0.0-dev.163](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.162...v1.0.0-dev.163) (2024-12-19)

### Features

* create get_metadata_field function for the submission connectors ([43e350a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/43e350a0124fadd80a995acadd2b1a8eaa680ef0))

## [1.0.0-dev.162](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.161...v1.0.0-dev.162) (2024-12-18)

### Features

* Remove all connector schemas in RawSchema table ([6dfd0f0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6dfd0f0584aec2e1b7003ce2f8045580323fe225))

## [1.0.0-dev.161](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.160...v1.0.0-dev.161) (2024-12-18)

### Features

* create function to get metadata object type and get json schema from connector ([b898726](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b898726fbf5e32cf76f66da1927162453e83c844))

## [1.0.0-dev.160](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.159...v1.0.0-dev.160) (2024-12-16)

### Bug Fixes

* submission settings extract from connector settings path ([b6e4de6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b6e4de6185264393e6e183219fd405bb0e17aa1a))

## [1.0.0-dev.159](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.158...v1.0.0-dev.159) (2024-12-11)

### Features

* add a function to submission connectors to return metadata object types ([eb05a85](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/eb05a85b94930388ad7ac05c12b874725c780a27))

## [1.0.0-dev.158](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.157...v1.0.0-dev.158) (2024-12-10)

### Features

* **settings:** add DTO for submission settings with mandatory field support ([675e029](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/675e0299455299cdef4984e15520e7ad581080af))

## [1.0.0-dev.157](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.156...v1.0.0-dev.157) (2024-12-10)

### Features

* add all metadata schemas for zenodo ([4ee6b98](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4ee6b98bd2f850e8e6511fd49c9f08b57617364e))

## [1.0.0-dev.156](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.155...v1.0.0-dev.156) (2024-12-10)

### Features

* add study & reads metadata for ENA ([8961a99](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8961a991b66557d24b710ee41f4e6448c250fc2d))

## [1.0.0-dev.155](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.154...v1.0.0-dev.155) (2024-12-10)

### Features

* Generate all json schema for ENA ([2a641be](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2a641bea37bf653231cd1d0241504c368021673e))

## [1.0.0-dev.154](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.153...v1.0.0-dev.154) (2024-12-10)

### Features

* improved user account creation ([38d9daa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/38d9daaa9b4915613d636f638428eb463b970980))

## [1.0.0-dev.153](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.152...v1.0.0-dev.153) (2024-12-06)

### Features

* create a submission metadata object table and modify the submission metadata one ([291d6fa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/291d6fae2d88ee23abc2944189889928baff46d3))

## [1.0.0-dev.152](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.151...v1.0.0-dev.152) (2024-12-06)

### Features

* Add SubmissionSettings creation during submission creation ([71b498d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/71b498d99ec0bbacd6b18b393a536a0c1cb222e1))

## [1.0.0-dev.151](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.150...v1.0.0-dev.151) (2024-12-06)

### Bug Fixes

* Replace 'required' with 'level' for field validation ([fc54218](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fc54218b7d4125840088f6e3b39e1b53e6cda2c8))

## [1.0.0-dev.150](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.149...v1.0.0-dev.150) (2024-12-06)

### Features

* **connector:** List and configure ENA settings fields ([e3ff5d2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e3ff5d21df7da6f8d44778b577a8a7b13266b90c))

## [1.0.0-dev.149](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.148...v1.0.0-dev.149) (2024-12-06)

### Features

* **connector:** List and configure Zenodo settings fields ([9f32097](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9f320977a5bab095df97fc5e5e9d2a9650d0ab75))

## [1.0.0-dev.148](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.147...v1.0.0-dev.148) (2024-12-05)

### Features

* **submission:** add SubmissionSettings model and update submission relations ([45c7336](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/45c7336f209f355561db0dd3991c9f0319493e4f))

## [1.0.0-dev.147](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.146...v1.0.0-dev.147) (2024-12-04)

### Features

* create DTOs for submission connectors ([88867f8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/88867f85b6106bf0c6bd9afa4690c5351ae95cf1))

## [1.0.0-dev.146](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.145...v1.0.0-dev.146) (2024-12-03)

### Features

* submission connectors should return supported files ([c334ef4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c334ef4f9675b80f68ec86485583b0b16d2f2f58))

## [1.0.0-dev.145](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.144...v1.0.0-dev.145) (2024-12-03)

### Features

* add the contributing organization ([afead15](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/afead159cd5cde77dc7bedc2c5e4ae9bf1e3fc9b))

## [1.0.0-dev.144](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.143...v1.0.0-dev.144) (2024-12-02)

### Bug Fixes

* add the field property to the return properties of the get metadata endpoint ([b0dd9dc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b0dd9dc15778bb750783b41278549009e9ac8812))

## [1.0.0-dev.143](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.142...v1.0.0-dev.143) (2024-12-02)

### Bug Fixes

* add the `type` property to json schema with array items that were missing it ([c0ad052](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c0ad05271556b14cf5566e4a6cebd1a75f53a951))

## [1.0.0-dev.142](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.141...v1.0.0-dev.142) (2024-11-26)

### Features

* add format orcid ([1fb1454](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1fb1454f2bfef16be718f9fec476ae45e2f19f4f))

## [1.0.0-dev.141](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.140...v1.0.0-dev.141) (2024-11-25)

### Features

* new schemas data (orientation, publication, ...) ([afda468](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/afda46881deabf0bd3929c30527332b77ecdd38f))

## [1.0.0-dev.140](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.139...v1.0.0-dev.140) (2024-11-22)

### Features

* **schema:** update JSON schema using oneOf instead of if conditions ([81c04e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/81c04e802b9e14e8d8bfed5fe0565f8343c4b186))

## [1.0.0-dev.139](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.138...v1.0.0-dev.139) (2024-11-21)

### Features

* add schemas for zenodo's submission metadata ([25bd2dd](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/25bd2dd3f81b09a913d7143c877527fa47dab451))

## [1.0.0-dev.138](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.137...v1.0.0-dev.138) (2024-11-20)

### Features

* add node title to submission ([41963b2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/41963b28f2c1b75a2679ae38705c0a91d6b5dead))

## [1.0.0-dev.137](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.136...v1.0.0-dev.137) (2024-11-18)

### Features

* Create the SubmissionMetadata model ([29eeb14](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/29eeb1473a66dcd0c0abda5bf5888a9d33433077))

## [1.0.0-dev.136](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.135...v1.0.0-dev.136) (2024-11-18)

### Features

* Endpoints for the data submission ([bc1aaef](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bc1aaef306537f04df5ae0b15146c394fd42500b))

## [1.0.0-dev.135](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.134...v1.0.0-dev.135) (2024-11-06)

### Features

* Model for the submission data ([866a813](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/866a813682961ff3b0c76522510026a9cbe824fc))

## [1.0.0-dev.134](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.133...v1.0.0-dev.134) (2024-11-06)

### Features

* add the "additional metadata" metadata ([fd7b180](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fd7b180b236fa722649c54fc949ce26305a0b144))

## [1.0.0-dev.133](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.132...v1.0.0-dev.133) (2024-11-06)

### Features

* Enrish the demo with a submission and data association ([1464422](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1464422bcda4ee4860a699974457979bdbe219e2))

## [1.0.0-dev.132](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.131...v1.0.0-dev.132) (2024-11-06)

### Features

* add new metadata for data objects ([0125b74](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0125b74ab1ece02b5ae370432dc4ff3d3825af76))

## [1.0.0-dev.131](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.130...v1.0.0-dev.131) (2024-11-05)

### Features

* introduced metadata related to data association along with the first metadata, raw sequencing paired ([9561fe6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9561fe6fa1f35a20c4c6a7eb101209419eebcd27))

## [1.0.0-dev.130](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.129...v1.0.0-dev.130) (2024-10-30)

### Features

* return datalink(s) used to access a data when retrieving it ([15bbd3b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/15bbd3bd10e7f50990f692528890ce67485a0e32))

## [1.0.0-dev.129](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.128...v1.0.0-dev.129) (2024-10-30)

### Bug Fixes

* access datalinks from the related property and add annotations for member role ([bc694a8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bc694a8d9339aa78aad9232208573e9dba60edd3))

## [1.0.0-dev.128](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.127...v1.0.0-dev.128) (2024-10-28)

### Bug Fixes

* removed outdated CI badges from doc ([5ec05f5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5ec05f52d13d24d60db29e254b7ce960c7e3c153))

## [1.0.0-dev.127](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.126...v1.0.0-dev.127) (2024-10-25)

### Bug Fixes

* Improve docker image and python package ([009b6ba](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/009b6ba3759422f5fd1f6ffd8eaba0c224d7aae8))

## [1.0.0-dev.126](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.125...v1.0.0-dev.126) (2024-10-18)

### Features

* **node:** add sample, datalink, and metadata objects with related URL and count ([4061a47](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4061a47b1cd267b755656b37c6331a51704af93a))

## [1.0.0-dev.125](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.124...v1.0.0-dev.125) (2024-10-18)

### Features

* add Sample bound data endpoint ([b06e796](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b06e7962f1d183ca335cc5e16c7a44a8917b4a0f))

## [1.0.0-dev.124](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.123...v1.0.0-dev.124) (2024-10-18)

### Features

* update README and add Flower basic auth restriction ([f476ea1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f476ea1c158fde4152241b93977fa652730bc487))

## [1.0.0-dev.123](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.122...v1.0.0-dev.123) (2024-10-17)

### Features

* check data size with async task ([bf8e7d0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bf8e7d098148eacf59ddd190cf723c926c7575d0))

## [1.0.0-dev.122](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.121...v1.0.0-dev.122) (2024-10-17)

### Bug Fixes

* **filters:** exclude groups field from metadata search ([98a42ad](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/98a42ade6a1de7fea12530a60443db36d715be11))

## [1.0.0-dev.121](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.120...v1.0.0-dev.121) (2024-10-17)

### Features

* add model and endpoints for data associations ([4202299](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/420229901cd7f1f7450e9c626021c9b64ab0d106))

## [1.0.0-dev.120](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.119...v1.0.0-dev.120) (2024-10-17)

### Features

* add async connection verification on access ([a01c91d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a01c91d6e1bfbbccc730e3ce1d197299c78da90d))

## [1.0.0-dev.119](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.118...v1.0.0-dev.119) (2024-10-17)

### Features

* data access right ([75648cb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/75648cb882f6242798539ea1e6da59d76f4f908f))

## [1.0.0-dev.118](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.117...v1.0.0-dev.118) (2024-10-16)

### Bug Fixes

* changed status codes in CESR function names ([26716c4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/26716c4b7a86ba7b8c6041d11c1a6602d1a44adf))

## [1.0.0-dev.117](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.116...v1.0.0-dev.117) (2024-10-15)

### Features

* create periodic async tasks ([0b053eb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0b053ebfb3af3600de61eccfe48d7e4896f18d87))

## [1.0.0-dev.116](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.115...v1.0.0-dev.116) (2024-10-15)

### Bug Fixes

* add the workspace to read only fields in the submission update serializer ([2b7d5aa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2b7d5aac55114cd102ca44ad0143797e0b3a5b6b))

## [1.0.0-dev.115](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.114...v1.0.0-dev.115) (2024-10-15)

### Bug Fixes

* inverted contributor and collaborator roles ([e1e8458](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e1e8458515443579d256e992595da412351474e9))

## [1.0.0-dev.114](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.113...v1.0.0-dev.114) (2024-10-14)

### Bug Fixes

* Adjust Ruff Configuration and Codebase Compliance ([6fa17fe](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6fa17fecbde6f46b2b47cfd3c91b1438dc8b86e4))

## [1.0.0-dev.113](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.112...v1.0.0-dev.113) (2024-10-05)

### Bug Fixes

* schemas of connection parameters ([8d681b6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8d681b6039fef5955cb10c51d8edf32b2e3d41f3))

## [1.0.0-dev.112](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.111...v1.0.0-dev.112) (2024-10-04)

### Features

* implement celery task management ([5130af0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5130af061e217d3842b7d4b5bad3345b8c77f4d0))

## [1.0.0-dev.111](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.110...v1.0.0-dev.111) (2024-10-04)

### Bug Fixes

* creating connections with no private params ([d572125](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d572125606467b5d40647fb47715a6a963efaa34))

## [1.0.0-dev.110](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.109...v1.0.0-dev.110) (2024-10-04)

### Bug Fixes

* add a restriction on connector type when requesting external data for a connection ([94b0456](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/94b0456dd612a951c4076fe3875ea07e691694cf))

## [1.0.0-dev.109](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.108...v1.0.0-dev.109) (2024-10-03)

### Bug Fixes

* error when creating a publication member due to the  data broker restriction ([c68557b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c68557b46ef23c977a8ac6ed7050778337c2e852))

## [1.0.0-dev.108](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.107...v1.0.0-dev.108) (2024-10-02)

### Bug Fixes

* **schema:** correct 'sex' schema ([d9553f5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d9553f56fdca9b8e6fa544636ab2841b79720f53))

## [1.0.0-dev.107](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.106...v1.0.0-dev.107) (2024-10-01)

### Features

* health check ([2cce3bb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2cce3bb8d87b3f3fe3daea7845a19a3ee77f1936))

## [1.0.0-dev.106](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.105...v1.0.0-dev.106) (2024-09-30)

### Features

* add the members restriction on the publication endpoints ([510a8c9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/510a8c9afd6db855997323a8ce905e2ea947363b))

## [1.0.0-dev.105](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.104...v1.0.0-dev.105) (2024-09-30)

### Bug Fixes

* url galaxy schema ([65d6384](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/65d63842358ba067a0b269f2e59925d1cb455a11))

## [1.0.0-dev.104](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.103...v1.0.0-dev.104) (2024-09-27)

### Bug Fixes

* modified the description of the publication members ([5638279](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/56382790ed8ca870a140aab7b8cd04121573975b))

## [1.0.0-dev.103](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.102...v1.0.0-dev.103) (2024-09-27)

### Features

* add argument parsing for schema processing command ([784e552](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/784e552b41a63e2a016e01526e77dc3a0d9a0a3c))

## [1.0.0-dev.102](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.101...v1.0.0-dev.102) (2024-09-27)

### Features

* **connection:** Handle the creation of empty connections for publication connectors ([7a460b6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7a460b6108942f5554aa8b269df9bc0ae855e8e8))

## [1.0.0-dev.101](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.100...v1.0.0-dev.101) (2024-09-27)

### Bug Fixes

* Group all management commands related to schema initialization into a single command ([886e089](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/886e08973655344f8f368af10f3c85b6786ac25b))

## [1.0.0-dev.100](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.99...v1.0.0-dev.100) (2024-09-26)

### Bug Fixes

* when deleting a connection check that no publication with that connection exist ([f9d0c5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f9d0c5a7442762d38531c367d6b870f672d8ca9a))

## [1.0.0-dev.99](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.98...v1.0.0-dev.99) (2024-09-26)

### Features

* publication api post ([26856c2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/26856c22eb8579dc3dd203f43a112ad092c76039))

## [1.0.0-dev.98](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.97...v1.0.0-dev.98) (2024-09-25)

### Features

* publication member endpoints ([3d103c6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3d103c6bca0bbe89f6d598eff1266effca543056))

## [1.0.0-dev.97](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.96...v1.0.0-dev.97) (2024-09-24)

### Features

* settings schema for publication connectors ([980a0f8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/980a0f8935f558679d8bd2c6ea886b1efbf244a3))

## [1.0.0-dev.96](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.95...v1.0.0-dev.96) (2024-09-24)

### Features

* created a GET detail, PATCH and DELETE endpoint for publications ([3bed472](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3bed472302621d19f0d49cd481188251cf24ecca))

## [1.0.0-dev.95](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.94...v1.0.0-dev.95) (2024-09-20)

### Features

* handle the documentation of the publication connector ([722eb5d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/722eb5d11a8fba54ae9757ea66a865dcbf92178b))

## [1.0.0-dev.94](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.93...v1.0.0-dev.94) (2024-09-20)

### Features

* improve data icon ([ca56f5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ca56f5a65281ab2e943906dfcbd8f0cf4f3678bf))

## [1.0.0-dev.93](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.92...v1.0.0-dev.93) (2024-09-20)

### Bug Fixes

* prevents bash code injection when retrieving external data objects ([47d90fc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/47d90fc4e5d5e00996b0aa9677f03947df16c6bb))
* prevents reuse of the same connection across different workspaces ([ac28537](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ac28537e3b8487ece616175d39e79523058b604d))
* restore support for file extension in SSHFS connector ([fa20009](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/fa2000991aff6b21dc3aa1d39d78641277ef7caf))

## [1.0.0-dev.92](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.91...v1.0.0-dev.92) (2024-09-20)

### Features

* establish the ENA connection ([d90a5f4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d90a5f40bc331d408a3ebb797cac637b6764f632))

## [1.0.0-dev.91](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.90...v1.0.0-dev.91) (2024-09-18)

### Bug Fixes

* remove site redirection in the admin ([0d1fcdd](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0d1fcdd92d81d890a64cdba26195ececea528296))

## [1.0.0-dev.90](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.89...v1.0.0-dev.90) (2024-09-18)

### Features

* filter get connections ([6a5e1e3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6a5e1e37a4fbf3d40ff625b30d2f3a794cbe75cb))

## [1.0.0-dev.89](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.88...v1.0.0-dev.89) (2024-09-16)

### Features

* add GET API for listing publications ([d25819b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d25819b5664d5835df506c64d14a321c9f20f07d))

## [1.0.0-dev.88](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.87...v1.0.0-dev.88) (2024-09-16)

### Features

* Add PublicationSerializer for serializing Publication model ([b5a4105](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b5a41058f4ab03d83d840575aa234f5dfb088f50))

## [1.0.0-dev.87](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.86...v1.0.0-dev.87) (2024-09-16)

### Features

* Add Publication, PublicationMember, and PublicationElement models ([f05a003](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f05a0032b97944258e2a35886eee1032807455dc))

## [1.0.0-dev.86](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.85...v1.0.0-dev.86) (2024-09-12)

### Bug Fixes

* issue when selecting a node metadata from a data ([0d0211e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0d0211e6b034f9ff64fa8d2d66399657158d5c12))

## [1.0.0-dev.85](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.84...v1.0.0-dev.85) (2024-09-12)

### Features

* allow the association of all metadata field to nodes ([ec921d8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ec921d8291c256dc3b51f1ab24a37df326cded9a))

## [1.0.0-dev.84](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.83...v1.0.0-dev.84) (2024-09-04)

### Features

* edits on metadata inheritance ([4148066](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/41480663e93ab8de2b3fdfdf559572df8c3736d6))

## [1.0.0-dev.83](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.82...v1.0.0-dev.83) (2024-09-03)

### Features

* When bounding a sample to a data, the sample and datalink node must have a direct parent-child relationship. ([b173d56](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b173d56c17a8b48919c3a90816bade52739b52e8))

## [1.0.0-dev.82](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.81...v1.0.0-dev.82) (2024-09-02)

### Bug Fixes

* add missing items in the date schemas ([bffdcad](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bffdcad4e9fb53c6b19ece0695ca2375d18a70f1))

## [1.0.0-dev.81](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.80...v1.0.0-dev.81) (2024-08-22)

### Bug Fixes

* patch semantic release ([7c36c46](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7c36c460cdac4c08bdea8d1c31101767fc487c12))

## [1.0.0-dev.80](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.79...v1.0.0-dev.80) (2024-08-20)

### Features

* test notary functions ([34ed816](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/34ed81690cb1b99bb49dee825703be4510e1b6ef))

## [1.0.0-dev.79](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.78...v1.0.0-dev.79) (2024-08-19)

### Features

* add better support for production settings and Uvicorn worker ([39260d6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/39260d647599253fe612ed044bd643275ba562ef))

## [1.0.0-dev.78](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.77...v1.0.0-dev.78) (2024-08-09)

### Features

* create a new endpoint to get metadata mapping ([4252346](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4252346019d59da09f9439c573a9ab3604582d37))

## [1.0.0-dev.77](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.76...v1.0.0-dev.77) (2024-08-06)

### Features

* add descendants samples to the API return ([ad158dd](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ad158dd1f481938969701918a2aa4c00b443c3a2))

## [1.0.0-dev.76](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.75...v1.0.0-dev.76) (2024-08-02)

### Features

* validation of taxon schema ([517b33e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/517b33ec9d10983bb7cd92365209a5728c9e3bef))

## [1.0.0-dev.75](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.74...v1.0.0-dev.75) (2024-08-02)

### Features

* check related type on metadata save ([abb6152](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/abb61520c6fa8aa091d6eb7bcd109ede84db7d43))

## [1.0.0-dev.74](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.73...v1.0.0-dev.74) (2024-08-01)

### Features

* Readme initialise madbotfields ([e8e8cd9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e8e8cd9217dbe2a1529d30147ab05f908558fc12))

## [1.0.0-dev.73](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.72...v1.0.0-dev.73) (2024-08-01)

### Features

* add metadafield mapping section in readme ([d29a898](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d29a89825c82784630ad453671050fe6ea35f707))

## [1.0.0-dev.72](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.71...v1.0.0-dev.72) (2024-07-29)

### Features

* metadata validation before saving ([a03ecfa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a03ecfa6fd2567386f36f24e00f818b064ccd78f))

## [1.0.0-dev.71](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.70...v1.0.0-dev.71) (2024-07-25)

### Features

* include metadata heritage in the metadata endpoints ([6bdbe87](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6bdbe87688916c59c8734857b36ca38f73cabeca))

## [1.0.0-dev.70](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.69...v1.0.0-dev.70) (2024-07-19)

### Bug Fixes

* duplicate search metadatafields ([cb2a58c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cb2a58c2410bc2425f7913d3782ba698017add95))

## [1.0.0-dev.69](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.68...v1.0.0-dev.69) (2024-07-19)

### Features

* add a schema for the sample ([689d6fb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/689d6fb43a190409469645b4bf6ab57e04090af2))

## [1.0.0-dev.68](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.67...v1.0.0-dev.68) (2024-07-18)

### Bug Fixes

* add api to the url of the schema ([7344d79](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7344d79c5bb7f978b79a72f985b966f038bc531f))

## [1.0.0-dev.67](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.66...v1.0.0-dev.67) (2024-07-17)

### Features

* Metadata inheritance management ([4b70c9c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4b70c9c4551efbdbddb211b10e8c4654ab449cbb))

## [1.0.0-dev.66](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.65...v1.0.0-dev.66) (2024-07-17)

### Features

* metadata endpoints ([8bcdcb6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8bcdcb6a620b108e10bf565fd3a9e8a607f6b4f5))

## [1.0.0-dev.65](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.64...v1.0.0-dev.65) (2024-07-12)

### Bug Fixes

* disable mimetype detection to improve ssh connector speed ([a08f7cf](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a08f7cf30b8dc190a5060f7c2e71711b86979312))

## [1.0.0-dev.64](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.63...v1.0.0-dev.64) (2024-07-11)

### Features

* add a command to create a demo workspace ([f5444f0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f5444f0b55a3bfb1db516b54d3deb8a924efd905))

## [1.0.0-dev.63](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.62...v1.0.0-dev.63) (2024-07-05)

### Features

* add metadata groups ([0965ce8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0965ce8a4414aac1f40a39a3bbfed08b1049e2e8))

## [1.0.0-dev.62](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.61...v1.0.0-dev.62) (2024-07-02)

### Features

* add alias support for sample ([1ae2bdb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1ae2bdbddf930f54f8e369fba37ef25fd70ab6b2))

## [1.0.0-dev.61](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.60...v1.0.0-dev.61) (2024-06-25)

### Bug Fixes

* separate the data and publication connectors ([85f9c9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/85f9c9d26af98850f4e7859df8ffcc1673d79840))

## [1.0.0-dev.60](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.59...v1.0.0-dev.60) (2024-06-25)

### Features

* create the metadata model ([5b50f60](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5b50f604cf5c2d0dfe95f5df238c8662c3271b05))

## [1.0.0-dev.59](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.58...v1.0.0-dev.59) (2024-06-25)

### Features

* add metadata schema view ([09f1f51](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/09f1f51201a15221c9d48dab4d200e56e5ce9404))

## [1.0.0-dev.58](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.57...v1.0.0-dev.58) (2024-06-25)

### Features

* Adapt the endpoint for workspaces and nodes roles ([5cce2b4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5cce2b4e621dd34bf9d991f5f63be3ad69e898fe))
* adapted notification tests to workspaces and corrected the implementation ([612fb88](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/612fb888474803140d77d270ee1811d07b61b328))
* add abstract class for mapping publication metadata fields ([60809f2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/60809f2242e364b86ea81338210464b0f8831335))
* add abstract class for mapping publication metadata fields ([9f042d3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9f042d3b4e34c258d2c87840782bcb83f32b2ad3))
* add allauth package and new authentication providers ([540039b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/540039b6889222c91666ed9f43cf932a6c7b0943))
* Add MetadataMapping ([ad456c1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ad456c16d4de9504b9c61d57d58cd77beaf74d40))
* Add MetadataSchema and MetadataField ([29981a7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/29981a72dddce213fdadb730064301d3ad42dfd0))
* add tests for bound samples endpoint ([c0ca128](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c0ca128ba5ddfe882e0f6e050cebea092af6ef97))
* add tests samples ([00fa818](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/00fa8188d0a424c658de68a3d9c18b632280f317))
* Add the Sample endpoint ([4fb91eb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4fb91eb60957dceb72cf1269ff738c6d98dd7aaf))
* Add the serializer and endpoints for the bound samples ([ac2050f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ac2050f159b92934e6b10ea48b1fb26578528e39))
* added a general class method on connectors to return their types ([ef73b65](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ef73b652040f5b0ed7034d8018cf5e49b8c5158f))
* added a related names for datalinks on data ([b2ca427](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b2ca427aae55a5f2a5d1c4d23d13fbe5e80e48fd))
* added a search filter and filters for data types and related types ([b325fbf](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b325fbf1c6e083963cf67eb5efc8d08ea8500ee8))
* added loggers for API errors ([911770a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/911770ae5904498eebd73edaf6376d44b8011e71))
* create a management command to create the metadata mapping files ([c6234c7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c6234c78eccede024d5147d9c9ce40bb0bd14ee1))
* create the model related to sample ([44d6ac1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/44d6ac12f5a979b5fba26657caf54090faf41de3))
* create the sample serializer ([c7cab19](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c7cab193846c0e461332218bd3592dd436e2d40c))
* created a management command to import the metadata mappings of publication connectors ([f7abdcf](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f7abdcf430157abf658fc543266ff0365fb6aa18))
* created the view for metadata fields ([cd5b031](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cd5b031ec9b293722ea5283b8f8b1f36b1875250))
* ENA publication connector ([309cac3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/309cac3862cb0b9a73d5b63197a5c13a5c0150c6))
* error refactoring ([5f871fe](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5f871fe2868b8a5afa98dc313f711d429f79cfbf))
* improved the usage of connector errors ([d28f8ec](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d28f8ec83fd47418564dbf68f52c7c9a30e0adba))
* Include an inherited sample property in the endpoints responses and adjust the functionnal tests ([a765c85](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a765c8540c1f0545f7cf385c6f7a875909dd37fd))
* Initiate madbot referentiel ([01a5891](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/01a589151ddb3c7cef9722ddbed9e36073f767fb))
* recover the media type and type from the connectors and use it in a data object ([8170408](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/81704085088d7a43fb220c0a6d3a56555fa27d7b))
* Zenodo publication connector ([120b132](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/120b1323e5d9193a99fee29c649ae6a19b4118df))

### Bug Fixes

* Improve the listing of files and directories in SSHFS ([d398fb2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d398fb2808837d35baa1af6dbc6741a09d34efa2))
* reestablished the filter on connector type in the connector view ([ff9cab9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ff9cab902fe0f8a146afdcd6e4fff2d947c77d6a))
* replace the mime_type by a media_type ([680cdc8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/680cdc8e66a18072b8564bc4984b407a18717660))
* resolve the "django.db.utils.IntegrityError UNIQUE constraint failed"... ([c7fe35b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c7fe35b0e23ec99dfab6f7ff463b3f0fc917eabb))
* resolve the `Multiple top-level packages discovered in a flat-layout` error with pip install ([414661c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/414661cb91fb74ce55032b41addbf1413acbeb17))
* when creating a new bound sample, check if there is already an existing... ([f2cfc9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f2cfc9da9b9ff297d4142ca0037213142708adbf))
* when retrieving a data, ensure that the user has access to it ([c6e220c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c6e220c3181cf0d4fce1ccf521786ca12c7a53e1))

## [1.0.0-dev.57](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.56...v1.0.0-dev.57) (2024-04-23)


### Features

* add websocket for the workspaces ([b4cbd3b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b4cbd3b3047f4b1128b57f6dcda0d1b65aad0f22))

## [1.0.0-dev.56](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.55...v1.0.0-dev.56) (2024-04-23)


### Features

* add restriction - you can only add Node members if they already are members of the workspace ([7cad253](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7cad25398b40a288a5000b1bcb25388e8b9577b1))
* filter users by fullname, email or username ([711fcf4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/711fcf4d2c2d036ba9be94ccddc0b7639fa036f1))

## [1.0.0-dev.55](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.54...v1.0.0-dev.55) (2024-04-19)


### Features

* add filters on workspace members ([3462ad9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3462ad95b7064e26d039fc927e29f53103b55cbe))

## [1.0.0-dev.54](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.53...v1.0.0-dev.54) (2024-04-19)


### Bug Fixes

* delete all NodeMembers of nodes in the workspace when deleting a WorkspaceMember ([282f74e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/282f74e089862d6a1a2e4b1f98d90b7a4192406b))

## [1.0.0-dev.53](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.52...v1.0.0-dev.53) (2024-04-19)


### Features

* create get list endpoint for Data ([ad1623f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ad1623f22795de149a16bfcdbec8e8a8ed665f25))

## [1.0.0-dev.52](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.51...v1.0.0-dev.52) (2024-04-18)


### Features

* let workspaces' owners access nodes (and their underlying resources) even if they are not members of theses nodes ([b987478](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b9874789e2b8c3684d0e7011a94485f7f8dd40f7))

## [1.0.0-dev.51](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.50...v1.0.0-dev.51) (2024-04-17)


### Features

* adapt the notifications to the workspaces ([e061c38](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e061c3876bf5b647f16f44e22223d319bc3220c4))

## [1.0.0-dev.50](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.49...v1.0.0-dev.50) (2024-04-16)


### Features

* make CORS allow a certain list of headers ([2c94fa8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2c94fa8d37757ddc16068e24074ca9c74ce2f8ab))

## [1.0.0-dev.49](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.48...v1.0.0-dev.49) (2024-04-16)


### Features

* create an endpoint to get api version ([7557ffe](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7557ffe674654507c203ab76b6ca98f0cf3bd5b2))

## [1.0.0-dev.48](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.47...v1.0.0-dev.48) (2024-04-15)


### Features

* show all connection of the workspace in the get list ([1037136](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/10371368e0f184622a78689a0babac0bdd15d289))

## [1.0.0-dev.47](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.46...v1.0.0-dev.47) (2024-04-15)


### Features

* Add a slug in workspace ([f95eb29](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f95eb291c78e4552a124dd0ee3780e5862e6fa98))

## [1.0.0-dev.46](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.45...v1.0.0-dev.46) (2024-04-15)


### Features

* Adapt endpoints to workspaces through a X-Workspace parameter ([b0a9834](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b0a9834f144332744f16eb37d455c54b00a3977b))

## [1.0.0-dev.45](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.44...v1.0.0-dev.45) (2024-04-10)


### Features

* Add workspace linkage to Connection, Data, and Node models ([c553dc1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c553dc1a0aec39f440b4fe687d290be13f8e4295))

## [1.0.0-dev.44](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.43...v1.0.0-dev.44) (2024-04-10)


### Features

* Implement the serializer and endpoints for WorkspaceMember with their tests ([84aac8f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/84aac8f797d4128a7434be8757e3d61914db654f))

## [1.0.0-dev.43](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.42...v1.0.0-dev.43) (2024-04-08)


### Features

* Add all members of the parent node when creating a node ([068375f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/068375f7f06b82970e32ab15ea9f7aefbdbd196d))

## [1.0.0-dev.42](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.41...v1.0.0-dev.42) (2024-04-08)


### Features

* Implement the serializer and endpoints for Workspace ([a35f17c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a35f17c280a9d4503590d1950a87d0762a15e258))

## [1.0.0-dev.41](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.40...v1.0.0-dev.41) (2024-04-08)


### Features

* add wokspace member model ([396a4c9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/396a4c9b4d42535ce202e461e64df05b4f2d2669))

## [1.0.0-dev.40](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.39...v1.0.0-dev.40) (2024-04-08)


### Features

* Add workspace model ([a2aa0f7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a2aa0f7708cc61a12355f91b1acda2fc0d0c9f76))

## [1.0.0-dev.39](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.38...v1.0.0-dev.39) (2024-03-29)


### Features

* re-adatpt the SSHFS connector ([b9015a9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b9015a9cf09a2cf92096426874fea85c47195f01))

## [1.0.0-dev.38](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.37...v1.0.0-dev.38) (2024-03-29)


### Bug Fixes

* fix error when retrieving history ([11b4e9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/11b4e9dd3bacf4d0ce4ce3bebff69ee8bb53affc))

## [1.0.0-dev.37](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.36...v1.0.0-dev.37) (2024-03-29)


### Features

* Create new filters in connections ([7275eee](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7275eeead22e5f3f40fb2a3a9693a3bef9936c7a))

## [1.0.0-dev.36](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.35...v1.0.0-dev.36) (2024-03-22)


### Bug Fixes

* improve ws response ([5bef75f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5bef75f2f7b8b73c0537d1c0b3b9108fbe4cb8c1))

## [1.0.0-dev.35](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.34...v1.0.0-dev.35) (2024-03-22)


### Bug Fixes

* Update tests for nodetype endpoints ([3bd6ab6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3bd6ab603aa04287d42e5f91f42c1a0cb8c885a4))

## [1.0.0-dev.34](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.33...v1.0.0-dev.34) (2024-03-21)


### Bug Fixes

* Update tests for node endpoints ([6ee7dfc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6ee7dfc751d0858a5563f103480773fd509be88a))

## [1.0.0-dev.33](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.32...v1.0.0-dev.33) (2024-03-21)


### Bug Fixes

* improve ws message ([e47c8ed](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e47c8ed0754d2e333da172a38c04b1070f0064cc))

## [1.0.0-dev.32](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.31...v1.0.0-dev.32) (2024-03-21)


### Bug Fixes

* Update tests for member endpoints ([259ca13](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/259ca1310b9a18fc70c66de04be0a92441e399e8))

## [1.0.0-dev.31](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.30...v1.0.0-dev.31) (2024-03-20)


### Features

* add notification triggers for the members ([b0b9280](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b0b9280051d5597de7616fd59d20119dd806182d))

## [1.0.0-dev.30](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.29...v1.0.0-dev.30) (2024-03-20)


### Bug Fixes

* Improve error handling for node and member endpoints. ([48e799d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/48e799d9dbca5fc67af3d8373a5f4a74a7bce2f3))

## [1.0.0-dev.29](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.28...v1.0.0-dev.29) (2024-03-20)


### Features

* Create an util to generilize the trigger of the notification ([2b9c71a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2b9c71a317c8a7e2bedb43f0cf719bab7091e83d))

## [1.0.0-dev.28](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.27...v1.0.0-dev.28) (2024-03-20)


### Features

* create endpoints for WS and notifications ([8d3f74b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/8d3f74b0b867fa4bf232750e3a2fbfbbc4e9d654))

## [1.0.0-dev.27](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.26...v1.0.0-dev.27) (2024-03-01)


### Bug Fixes

* change member role control during creation ([057cb70](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/057cb707059eb953925e9f8e55161ace9c50da21))

## [1.0.0-dev.26](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.25...v1.0.0-dev.26) (2024-03-01)


### Bug Fixes

* unique constraint on data and datalink ([4296528](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4296528d11c282a421ddd8ad68f1acf642bd8810))

## [1.0.0-dev.25](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.24...v1.0.0-dev.25) (2024-02-27)


### Performance Improvements

* reduce the number of queries to fetch collections and datalinks ([0ef3eb2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0ef3eb2cf155b2e40fc86c0c4d260b0406c4b22d))

## [1.0.0-dev.24](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.23...v1.0.0-dev.24) (2024-02-23)


### Features

* create a datalink to Galaxy ([e3eab41](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e3eab41f8770e376d695708b35f81268a4b3b316))

## [1.0.0-dev.23](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.22...v1.0.0-dev.23) (2024-02-20)


### Features

* refresh Connector API and Galaxy connector ([84a40b1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/84a40b101a32ecedc366d82f55f65d6c912950bb))

## [1.0.0-dev.22](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.21...v1.0.0-dev.22) (2024-02-14)


### Features

* create a Tree node ([e121c1f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e121c1ff0270336a696ebf7eb933c2b06b0412b0))

## [1.0.0-dev.21](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.20...v1.0.0-dev.21) (2024-02-02)


### Features

* add a slug property to node types and create NodeTypeCollection model ([1f687f2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1f687f209c0e33e37ab470ce38692c4486024bf2))
* create a history field to keep track of the last_edited_by and last_edited_time in Node objects ([1833a27](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/1833a279ddf457d869af92f357f87adf0af66f78))
* create fixtures for Node type ([bcc1942](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/bcc194282091105a6cbf6360e5dde64f84243df3))
* create node and node type models ([774c6d1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/774c6d1ea9e52be91464e1d292ffd07ca7608d1b))
* create node type serializer and endpoints ([dda3946](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/dda3946e43e22d68d9f006972044ef3eb34674a9))
* create the node serializers and endpoints ([392384e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/392384e79d552ecced5ccb28b073a9a4a83e5acc))


### Bug Fixes

* bypass python 3.11 warning concerning system package break when using pip outside of venv ([c69d59f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c69d59fd722b57db425f08657b95e119cd595a50))

## [1.0.0-dev.20](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.19...v1.0.0-dev.20) (2023-12-20)


### Features

* create a ApplicationUserParam model ([e604342](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e6043423a5cf58d70b488f551ab4ffdfb8b4b7fb))

## [1.0.0-dev.19](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.18...v1.0.0-dev.19) (2023-12-14)


### Features

* create a websocket for members ([cd5522b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cd5522b4fade6b2197b6443e750a2b6748237970))

## [1.0.0-dev.18](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.17...v1.0.0-dev.18) (2023-12-11)


### Bug Fixes

* change the oidc_claim_scope ([04afa35](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/04afa35dc9abb6e1cc1d149965c3efaa5546b504))

## [1.0.0-dev.17](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.16...v1.0.0-dev.17) (2023-12-06)


### Features

* remove owner ([67592de](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/67592de344facadcc28402ae573c0c2525af0f44))

## [1.0.0-dev.16](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.15...v1.0.0-dev.16) (2023-11-27)


### Features

* reworked error messages ([4ce64aa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4ce64aac80e83e3f419b88362f7b2396ae91adb3))

## [1.0.0-dev.15](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.14...v1.0.0-dev.15) (2023-11-22)


### Features

* testing for Members APIs ([cbebe77](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cbebe77423d4a278ae687f44ad2afde960ea007a))

## [1.0.0-dev.14](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.13...v1.0.0-dev.14) (2023-11-22)


### Features

* test assays ([788d32b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/788d32b88f93d16474c7f5d0623f143013999fe1))

## [1.0.0-dev.13](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.12...v1.0.0-dev.13) (2023-11-21)


### Features

* tests on /tools with a fake connector ([44c7b91](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/44c7b91de16c71ac9484965ad0a5deba661d5bee))

## [1.0.0-dev.12](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.11...v1.0.0-dev.12) (2023-11-20)


### Bug Fixes

* handle the case when no param is passed to the error ([78b57e6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/78b57e618af3088614cfc7093b5fac412df805c8))

## [1.0.0-dev.11](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.10...v1.0.0-dev.11) (2023-11-20)


### Features

* introduces new ways to handle error uniformly ([9fdc98e](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9fdc98e4c74791961e0667a73776d2e77167e314))

## [1.0.0-dev.10](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.9...v1.0.0-dev.10) (2023-11-16)


### Features

* creating a test architecture ([f5f84d6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f5f84d6436a9db78267c990d389c4adf5f21be11))

## [1.0.0-dev.9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.8...v1.0.0-dev.9) (2023-11-13)


### Bug Fixes

* Fix security breaches and code audit ([82044c1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/82044c1da1d2ebdca895c3ca8cdff764aec2a7b5))

## [1.0.0-dev.8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.7...v1.0.0-dev.8) (2023-11-08)


### Bug Fixes

* corrects a security vulnerability when parsing some requests parameters ([a2167e4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a2167e4e2cd391014b04c57894ec3ab935c12289))

## [1.0.0-dev.7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.6...v1.0.0-dev.7) (2023-11-07)


### Features

* Oauth toolkit ([7dbe796](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7dbe7960e27094fcaeb0ae17b91e8566728744d5))

## [1.0.0-dev.6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.5...v1.0.0-dev.6) (2023-11-06)


### Features

* new standard pagination ([d54c8fc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d54c8fc1dea911b3225f30a010c39dec8ec656cd))
* paginate study list ([ba45659](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ba456590e8806643e100d23710829afbdad6cb7f))
* pagination of investigation members ([b216c91](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b216c917aea3831500173be4d53d44ec7b6b4017))
* search filter in study ([e83a015](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e83a015fdabc06a224b67c2d191ff0585f7baa7b))


### Bug Fixes

* activate study in demo ([f1d02a1](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f1d02a16bb593b477d95799f7bcfe47929273ad4))

## [1.0.0-dev.5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.4...v1.0.0-dev.5) (2023-11-03)


### Features

* add a condition for leave on the delete member ([5bccd41](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5bccd413e04c5bfc8fe5320dab6be3a74773b966))

## [1.0.0-dev.4](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.3...v1.0.0-dev.4) (2023-11-03)


### Bug Fixes

* add permission on the delete investigation ([7ac24e7](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7ac24e7c45790deeeaec9259701a20f4041daae9))

## [1.0.0-dev.3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.2...v1.0.0-dev.3) (2023-11-03)


### Bug Fixes

* fixed package discovery in setuptools ([cb12509](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/cb125095ecf65e372936731fcccd8544800e7ee9))

## [1.0.0-dev.2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/compare/v1.0.0-dev.1...v1.0.0-dev.2) (2023-11-03)


### Bug Fixes

* change the response status in the post ([ba7e5d5](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ba7e5d550f9507a125889289a006b761259ae71f))

## 1.0.0-dev.1 (2023-11-02)


### Features

* activate autorelease ([d944251](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d9442512c4b49fb21848fcd64a600966dd4b09d4))
* add investigation to a Tool creation ([f8c8af6](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/f8c8af679e1b547264fcbe8d444c7d127ee71440))
* add members permissions for the PUT ([525bc5a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/525bc5a46e037d780d3f194519b08c351d9bdf78))
* add permissions for the delete ([3b19b15](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/3b19b158a1308fa20152407a228e899f4e3fdb11))
* add permissions in the creation of a member ([e917f1a](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/e917f1ab000390313f48a6dd7d77ee23d2ba65b1))
* add permissions on the GETs members ([5bcb15c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/5bcb15cf2dd32f0bc8d144968dbc71aee999f48a))
* better support dataobject descriptions and links when creating a datalink ([c5cf9a3](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c5cf9a330272d4bfc0334b6686b24b281a113635))
* create a release and CI config files ([6f76428](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/6f764285d68250d7db1187c59a7aebc3ca4a08a9))
* create a Swagger api ([4caa885](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4caa8858298bf1b988f107a17da2b5cb1b75c4d9))
* create CI pipeline to check quality ([100c6a2](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/100c6a2d128881e8b3a188a710aafef101361dae))
* Create one member at a time ([7aeeaa8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7aeeaa8399b52d1ebf50c0fa956dc166a0b965f1))
* Define permissions and add them to a custom decorators in the APIs of members management ([af32dfc](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/af32dfcf0c9b465c7353200941f075cb0788f26e))
* delete an investigation by only the owners ([ecd8f0c](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/ecd8f0c42ffdae81231985c1cc2b79ff75ce5b75))
* generate a demo ISA objects tree ([4edaceb](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/4edacebddf6e8dd67bb55fca01ef3c8c74bca51e))
* improve exception error for connector ([0d2c0d9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0d2c0d92d67deb2c52858736d386c9aa2ff7b702))
* license changed to BS3 ([7dbca7b](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7dbca7b38efa4be5f99158826b2bf630135caf46))
* Manage investigation members ([7a6b47f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/7a6b47fb2c2e6f3bd535ddf8bda8e54fb3492f7f))
* manage POST/PUT/DELETE of a member with a role below or equal to theirs ([00044db](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/00044db9542c0efe491cb3a4318ecafd2699df98))
* manage the creation of multiple members ([d1d074f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d1d074fe56d9f1472d53e81d71e3d534f43bffd1))
* new CI pipeline including better code audit, python package release and docker image release ([a6f38e8](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/a6f38e8c2e8fdf9be3e6bbfdb94c260a5ce9f756))
* only show tools from a given investigation ([0b51191](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/0b511911b2f0578387e0d8acac8d60771e6b1ecc))
* prerelease ([335defa](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/335defa84f02589743fc9ead067872e5ae87a11f))
* return connectors, tools and dataobject icons image as an url and add support for FontAwesome icons ([9c5ec62](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/9c5ec62038e39ee984d8efe43850d0b5ee941a36))
* return the member properties in the investigation API ([d4fde96](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d4fde9609541a5aca1c6c804800afb691661244c))


### Bug Fixes

* "blake" corrected to "black" in ci ([881b01d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/881b01d95acaff2fa48c9016df252c75b1926776))
* add a condition on the members put ([b32004d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b32004d34defcb68549d63f62d4c97bddc4c1151))
* add a decoretor for the investigation delete ([94f27e0](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/94f27e0f4638b6b787a7e83ded2b02ff1e5bb37f))
* add labguru project url ([d25462f](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/d25462f0b6f0c6f18ff7caf6855b41ae77576dc6))
* rename openlink to madbot ([b863b9d](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/b863b9dcc76b6aa482dcdcc40c5691b6a9c1cd28))
* **sshfs:** enables navigation in SSHFS subfolders again ([c826af9](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/c826af9892e9bb5a80b844c18ee2cd7bc5c616cf))
* use 403 Forbidden instead of 401 unauthorized for API calls with forbidden access ([2fe6315](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/commit/2fe6315fd828e2807e25381338dbb5c0e57f8908))
