FROM mambaorg/micromamba:alpine

COPY pyproject.toml .
COPY --chmod=755 django_entrypoint.sh /opt/
COPY --chmod=755 celery_entrypoint.sh /opt/

# Install some madbot-api dependencies
RUN micromamba install -y -n base -c conda-forge \
    python=3.10 \
    zeroc-ice=3.6.5 \
    compilers \
    openldap \
    psycopg2 \
    gunicorn gevent && \
    micromamba clean --all --yes

# Install madbot-api from python package
ARG MAMBA_DOCKERFILE_ACTIVATE=1
# ADD --chown=mambauser . /tmp/madbot
# RUN cd /tmp/madbot && pip install -e ".[all]"
RUN pip install madbot-api[all]==$(grep -m 1 version pyproject.toml | tr -s ' ' | tr -d '"' | tr -d "'" | cut -d' ' -f3) \
   --index-url https://gitlab.com/api/v4/projects/50871918/packages/pypi/simple
RUN pip install uvicorn

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/_entrypoint.sh", "/opt/django_entrypoint.sh"]
