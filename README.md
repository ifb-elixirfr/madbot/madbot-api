# Madbot API

## Overview

### Description

API of Madbot, the Metadata And Data Brokering Online Tool

### Language

[![Made with Django](https://img.shields.io/badge/Made%20with-Django-blue)](https://www.djangoproject.com/)
[![Made with Django REST framework](https://img.shields.io/badge/Made%20with-Django%20REST%20framework-blue)](https://www.django-rest-framework.org/)

**Questions**

[:speech_balloon: Ask a question/suggestions or report a bug](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/issues/new)
[:book: List of questions/suggestions and bugs](https://gitlab.com/ifb-elixirfr/madbot/madbot-api/-/issues)
[:book: General questions](mailto:ifb-madbot@groupes.france-bioinformatique.fr)

**Contribution**

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

## Requirements

To run Madbot locally, ensure you have the following tools installed:

- [python](https://www.python.org/)
- [docker](https://docs.docker.com/get-docker/)

## Installation procedure for developers

Follow the above steps to set up the development environment for Madbot.

**Clone the Repository :**

```bash
git clone git@gitlab.com:ifb-elixirfr/madbot/madbot-api.git
or
git clone https://gitlab.com/ifb-elixirfr/madbot/madbot-api.git
```

Navigate to the API's working directory

```bash
cd madbot-api
```

**Create and activate your development environment :**

Create a conda environment with Python and the zeroc-ice library

```bash
micromamba env create -p ./env -f dev-env.yml
```

If you are working on an Apple silicon computer and face issues resolving some packages like `vault`, you can force conda to search for x86 packages using the following command :

```bash
CONDA_SUBDIR=osx-64 micromamba env create -p ./env -f dev-env.yml
```

**Install Madbot**

Activate your dev environment

```bash
micromamba activate ./env
```

Install Madbot and its python dependencies

```bash
pip install -e ".[dev,galaxy,sshfs]"
```

**Create Madbot local configuration**

Create a configuration file `.env` for your local Madbot instance :

```bash
DEBUG=True
ALLOWED_HOSTS=127.0.0.1,localhost
MADBOT_VAULT_URL=http://localhost:8200
MADBOT_OIDC_RSA_PRIVATE_KEY=<rsa_key>
```

Replace `rsa_key` with an RSA key obtained as follows:

- Ubuntu:

  ```sh
  openssl genrsa -out oidc.key 4096
  awk -v ORS='\\n' '1' oidc.key | xclip -selection clipboard
  ```

- MacOs:

  ```sh
  openssl genrsa -out oidc.key 4096
  awk -v ORS='\\n' '1' oidc.key | pbcopy
  ```

**Launch a local Vault server**

Open a new Terminal

Navigate to your Madbot API working directory

Activate your dev environment

```bash
micromamba activate ./env
```

Launch Vault, with the turnkey script :

```bash
./run_local_vault.sh
```

When the script is running, it will give you the corresponding settings for Madbot :

```plaintext
MADBOT_VAULT_URL=http://127.0.0.1:8200
MADBOT_VAULT_MOUNT_POINT=madbot
MADBOT_VAULT_ROLEID=c142bb99-7d0e-3993-0d70-e60a69156fdf
MADBOT_VAULT_SECRETID=eb33b039-900b-b114-f6e6-410a4f3558e7
```

Add these lines at the end of your Madbot configuration (`.env` file)

**Launch a local Redis server**

Open a new Terminal

Navigate to your Madbot API working directory

Activate your dev environment

```bash
micromamba activate ./env
```

Start Redis

```bash
redis-server
```

**Initialize Madbot database**

Go back to your first terminal

```bash
# launch the database creation
python manage.py makemigrations core
python manage.py migrate
# collect the static files
python manage.py collectstatic
# import isa types
madbot collection_isa
# initialise madbot metadata schema and fields 
madbot initialise_madbot_metadata_fields
# create periodic async tasks
madbot create_periodic_tasks
```

**Create an admin user**

```bash
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@madbot.fr', '<YourSuperAdminPassword>', id='9da4931b-e89e-48f1-988d-0aeb6a2ea1bd', first_name='Madbot', last_name='Admin', is_approved=True)" | madbot shell
```

**Create a OIDC client for the Madbot Nuxt Client**

```bash
madbot createapplication --client-id=madbot --client-secret="madbot" --name="madbot" --algorithm=RS256 --redirect-uris=http://localhost:3000/api/auth/callback/madbot --user="9da4931b-e89e-48f1-988d-0aeb6a2ea1bd" confidential authorization-code
```

**Optionnal: Create a demo in Madbot**

```bash
madbot create_demo_workspace --username <your_username>
```

**Start the API server**

```bash
madbot runserver
```

**Start the celery worker**

Open a new Terminal

Activate your dev environment

```bash
micromamba activate ./env
```

Start Celery

```bash
madbot celery
```

This command starts a Celery worker that will execute the tasks defined in your Django application.

**(Optional) Start Flower for Monitoring:**

```bash
celery --broker=redis://localhost flower --port=5555 --basic_auth=$CELERY_FLOWER_OPTIONS
```

This command starts Flower, a web-based tool for monitoring your Celery tasks. You can access it at <http://localhost:5555>.

**Start Celery beat**

```bash
celery -A madbot_api beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler
```

## Verification of the dependencies and code quality

Before every push in GitLab, it is recommended to run the following commands:

**Code audit :**

```bash
ruff check .
```

**Verification of the code quality :**

```bash
ruff format --check .
```

**Dependencies vulnerability :**

```bash
safety check .
```

**Security breach :**

```bash
bandit -c pyproject.toml -r madbot_api
```

⚠️ Note : In order not to fail at the time of continuous integration, no errors should be detected.

## Developpement Best Practices

### Conventional Commit Messages

We follow the [Conventional Commits](https://www.conventionalcommits.org/) convention for our commit messages. Please adhere to this format for all commits in this project to maintain a coherent and readable code history.

**Format of the commit message :**

```html
<type>(<scope>): <subject>

<body>

<footer>
```

**Example of a commit message :**

```sh
git commit -m "feat: add new API endpoint"
```

**Allowed 'type' values :**

- **build** : Builds - changes that affect the build system or external dependencies
- **chores** : Chores - updating grunt tasks etc; no production code change
- **ci** : Continous Integration - chanhes to our CI configuration files and scripts
- **docs** : Documentation - changes to the documentation
- **feat** : Features - new feature for the user, not a new feature for build script
- **fix** : Bug fixes - bug fix for the user, not a fix to a build script
- **perf** : Performance Improvements - a code change that improves performance
- **refactor** : Code Refactoring - a code change that neither fixes a bug or adds a feature
- **reverts** : Reverts - reverts a previous commit
- **style** : Style - formatting, missing semi colons, etc; no production code change
- **test** : Tests - adding missing tests, refactoring tests; no production code change

**Example 'scope' values :**

- init
- runner
- watcher
- config
- web-server
- proxy
- etc.
The "scope" can be empty (e.g. if the change is a global or difficult to assign to a single component), in which case the parentheses are omitted. In smaller projects such as Karma plugins, the "scope" is empty.

**Message body :**

- Uses the imperative, present tense: “change” not “changed” nor “changes”
- Includes motivation for the change and contrasts with previous behavior

**Message footer :**

- **Referencing issues** : Closed issues should be listed on a separate line in the footer prefixed with "Closes" keyword like this :

```sh
Closes#234
```

or in case of multiple issues :

```sh
Closes#123, #245, #992
```

- **Breaking changes** : a commit that has a footer BREAKING CHANGE:, or appends a ! after the type/scope, introduces a breaking API change (correlating with MAJOR in Semantic Versioning). A BREAKING CHANGE can be part of commits of any type.

## Initialise Metadata Reference Fields

Metadata reference fields are crucial for defining and standardizing the metada fields used across Madbot. If needed, you can configure these fields by editing a CSV file named `madbot-metadata-fields.csv` in the static folder. Then, Import this Metadata reference fields by running the command `initialise_madbot_metadata_fields.`

#### CSV Structure

The `madbot-metadata-fields.csv` file describes the Metadata reference fields with the following columns:

- **Slug**:
  - **Type**: `string`
  - **Description**: A unique identifier for the metadata field. It is used to reference the field within the system.
- **Name**:
  - **Type**: `string`
  - **Description**: The display name of the metadata field. This is a human-readable name that describes the field.
- **Description**:
  - **Type**: `string`
  - **Description**: A brief description of what the metadata field represents or its purpose.
- **Objects**:
  - **Type**: `string`
  - **Description**: Specifies the types of objects that the field is associated with, such as node, sample and data. Multiple object types can be separated by commas.
- **Data types**:
  - **Type**: `string`
  - **Description**: The type of data the metadata field holds. This field can be left empty if not applicable.
- **Schema**:
  - **Type**: `string`
  - **Description**: Indicates the schema with which the field corresponds. This should match one of the schemas contained in `static/json/Schema`.

#### Example

Here is an example entry in the CSV file:

```csv
Slug;Name;Description;Objects;Data types;Schema
cell_line;Sample cell line;Cell line from which the sample was obtained;node, sample;;text
```

### Steps

#### 1/ Editing the CSV File

If necessary, you can edit the `madbot-metadata-fields.csv` file to add or update metadata reference fields. Ensure that each field is properly defined according to the structure above.

#### 2/ Importing Metadata Reference Fields

Run the following command to import the metadata reference fields into MadBot:

```bash
# run import command 
python manage.py initialise_madbot_metadata_fields
```

## Contributors

- [Bouri Laurent](https://gitlab.com/lbouri1) <a itemprop="sameAs" content="https://orcid.org/0000-0002-2297-1559" href="https://orcid.org/0000-0002-2297-1559" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Denecker Thomas](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Messak Imane](https://gitlab.com/imanemessak) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1654-6652" href="https://orcid.org/0000-0002-1654-6652" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Mohamed Anliat](https://gitlab.com/anliatm) <a itemprop="sameAs" content="https://orcid.org/0000-0002-1105-8262" href="https://orcid.org/0000-0002-1105-8262" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Rousseau Baptiste](https://gitlab.com/nobabar) <a itemprop="sameAs" content="https://orcid.org/0009-0002-1723-2732" href="https://orcid.org/0009-0002-1723-2732" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>
- [Seiler Julien](https://gitlab.com/julozi) <a itemprop="sameAs" content="https://orcid.org/0000-0002-4549-5188" href="https://orcid.org/0000-0002-4549-5188" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>

## Contribution

Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of conduct

Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## Licence

[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Madbot API is licensed under a [The 3-Clause BSD License License](https://opensource.org/license/bsd-3-clause/).

## Acknowledgement

- Nadia GOUE and Matéo HIRIART for their contribution to openLink ;
- The working group for its advisory role ;
- All application testers.

## Citation

If you use Matbot API project, please cite us :

IFB-ElixirFr, Matbot API, (2023), GitLab repository, <https://gitlab.com/ifb-elixirfr/madbot/madbot-api.git>
