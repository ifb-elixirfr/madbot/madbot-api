madbot collectstatic --noinput
madbot makemigrations core
madbot migrate
madbot collection_isa
madbot initialise_madbot_metadata_fields
# create oauth client if env settings defined
gunicorn madbot_api.asgi --bind 0.0.0.0:8000 --timeout 420 -k uvicorn.workers.UvicornWorker
