from __future__ import absolute_import, unicode_literals

import os
import sys

from .celery import app as celery_app

__version__ = "1.0.0-dev.201"
VERSION = __version__

__all__ = ("celery_app",)


def manage():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "madbot_api.settings")
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
