from django.apps import AppConfig


class MadbotENAConfig(AppConfig):
    """Config for the MadbotENA connector."""

    name = "madbot_api.connectors.madbot_ena"
