import json
import logging
import os.path
from io import BytesIO
from urllib.parse import urlparse

import requests
from django.utils.text import slugify
from requests.auth import HTTPBasicAuth

import madbot_api
from madbot_api import settings
from madbot_api.core.connector import (
    AuthenticationError,
    ConnectionField,
    FieldRepresentation,
    MetadataObject,
    MetadataObjectType,
    SchemaRepresentation,
    SettingField,
    SubmissionConnector,
    SubmissionMetadata,
    UnreachableError,
)

logger = logging.getLogger(__name__)


def generate_slug(path):
    """
    Generate a slug from a file path.

    Args:
    ----
        path (str): The file path from which to generate the slug.

    Returns:
    -------
        str: A slug created by joining relevant parts of the path with underscores.

    """
    parts = path.split(os.sep)

    if "." in parts[-1]:
        parts[-1] = os.path.splitext(parts[-1])[0]

    return slugify("_".join(parts))


def add_fields(fields_dir):
    """Add fields in different objects."""
    mandatory_fields = []
    recommended_fields = []
    optional_fields = []

    for root, _, files in os.walk(dir_path_schema):
        for file_name in files:
            if file_name.endswith(".json"):
                file_path = os.path.join(root, file_name)
                with open(file_path, "r") as f:
                    # Load the JSON schema using Schema DTO
                    metadata_field = json.load(f)
                    metadata_slug = generate_slug(
                        os.path.relpath(file_path, dir_path_schema)
                    )

                    # Create Schema DTO for each field
                    schema_field = SchemaRepresentation(
                        slug=metadata_slug,
                        schema_content=metadata_field,
                        source="ena",
                    )

                    # create the Field DTO
                    field_dto = FieldRepresentation(
                        slug=metadata_slug,
                        schema_rep=schema_field,
                        title=metadata_field.get("title", ""),
                        description=metadata_field.get("description", ""),
                        URL_external="",  # TODO: ENA fields have an external url don't forget to handle them
                        madbot_fields=[],  # TODO: add the mapping
                        hidden=metadata_field.get("hidden", False),
                    )

                    ENA_FIELDS[metadata_slug] = field_dto

                    # Classify fields based on their level
                    if metadata_field.get("level") == "mandatory":
                        mandatory_fields.append(ENA_FIELDS[metadata_slug])
                    elif metadata_field.get("level") == "recommended":
                        recommended_fields.append(ENA_FIELDS[metadata_slug])
                    elif metadata_field.get("level") == "optional":
                        optional_fields.append(ENA_FIELDS[metadata_slug])
    return mandatory_fields, recommended_fields, optional_fields


ENA_FIELDS = {}

study_mandatory_fields = []
study_recommended_fields = []
study_optional_fields = []

sample_mandatory_fields = []
sample_recommended_fields = []
sample_optional_fields = []

raw_reads_mandatory_fields = []
raw_reads_recommended_fields = []
raw_reads_optional_fields = []

app_path = os.path.dirname(madbot_api.__file__)

dir_path_schema = os.path.join(
    app_path,
    "connectors",
    "madbot_ena",
    "static",
    "json",
    "schema",
)

for category in ["study", "sample", "raw_reads"]:
    dir_path_sub = os.path.join(dir_path_schema, category)
    ENA_FIELDS[category] = {}

    # Load fields for the current category
    mandatory_fields, recommended_fields, optional_fields = add_fields(dir_path_sub)
    # TODO: this part will be reworked with the goal of reorganizing the metadata fields into metadata object types
    ENA_FIELDS[category]["mandatory"] = mandatory_fields
    ENA_FIELDS[category]["recommended"] = recommended_fields
    ENA_FIELDS[category]["optional"] = optional_fields


METADATA_OBJECT_TYPE = {
    "study": MetadataObjectType(
        name="Study",
        slug="study",
        summary_fields=["title", "alias", "description"],
        priority=["title", "alias", "description"],
        description=(
            "A study groups together data submitted to the archive and controls its release date."
            "A study accession is typically used when citing data submitted to ENA."
            "Note that all associated data and other objects are made public when the study is released."
        ),
        minimal_occurrence=1,
        maximal_occurrence=1,
        mandatory_fields_schemas=study_mandatory_fields,
        recommended_fields_schemas=study_recommended_fields,
        optional_fields_schemas=study_optional_fields,
    ),
    "sample": MetadataObjectType(
        name="Sample",
        slug="sample",
        summary_fields=["title", "alias"],
        priority=["title", "alias"],
        description=(
            "A sample contains information about the sequenced source material."
            "Samples are associated with checklists, which define the fields used to annotate the samples."
            "Samples are always associated with a taxon."
        ),
        minimal_occurrence=1,
        mandatory_fields_schemas=sample_mandatory_fields,
        recommended_fields_schemas=sample_recommended_fields,
        optional_fields_schemas=sample_optional_fields,
    ),
    "raw_reads": MetadataObjectType(
        name="Raw reads",
        slug="raw_reads",
        summary_fields=["title", "alias"],
        priority=["title", "alias"],
        description=(
            "A raw reads object contains the raw sequencing data and information about the sequencing experiment including library and instrument details."
        ),
        minimal_occurrence=1,
        mandatory_fields_schemas=raw_reads_mandatory_fields,
        recommended_fields_schemas=raw_reads_recommended_fields,
        optional_fields_schemas=raw_reads_optional_fields,
    ),
}


class ENAConnector(SubmissionConnector):
    """Connector for interacting with the European Nucleotide Archive (ENA)."""

    METADATA_GROUPS_PATH = (
        "madbot_api/connectors/madbot_ena/static/madbot_ena/metadata_groups.json"
    )
    groups = None

    def __init__(self, parameters):
        """
        Initialize the ENAConnector with the provided parameters.

        :param parameters: A dictionary containing the connection and submission parameters,
                          including 'url_ena', 'webin', 'password', 'submission' and 'settings'.
        """
        self.url = parameters["url_ena"].lower()
        if "webin" and "password" in parameters:
            self.webin = parameters["webin"]
            self.password = parameters["password"]
            self._validate_connection()
        if "submission" in parameters:
            self.submission = parameters["submission"]
        if "settings" in parameters:
            self.settings = parameters["settings"]

    def _validate_connection(self):
        """
        Validate the connection to the ENA server using the provided credentials.

        Raises
        ------
            UnreachableError: If the connection to the ENA server fails.
            AuthenticationError: If the authentication with the server fails.

        """
        try:
            response = requests.post(
                self.url + "/submit/drop-box/submit",
                files=[
                    ("SUBMISSION", ("submission.xml", BytesIO(b""))),
                ],
                auth=HTTPBasicAuth(self.webin, self.password),
                timeout=20,
            )

            # Raise an error if the response indicates a failure
            response.raise_for_status()
        except requests.exceptions.ConnectionError as conn_err:
            # Raised if the server is unreachable
            logger.error("Connection error: ", {conn_err})
            raise UnreachableError(f"Unable to reach {self.url}")

        except requests.exceptions.Timeout as timeout_err:
            # Raised when the request times out
            logger.error("Request timed out: ", {timeout_err})
            raise UnreachableError(f"Request to {self.url} timed out")

        except requests.RequestException as req_err:
            # General requests exceptions (e.g., invalid URL)
            logger.error("Request error occurred: ", {req_err})
            raise AuthenticationError()

    @classmethod
    def get_name(cls):
        """
        Return the name of the connector.

        :return: The name of the connector.
        """
        return "ENA"

    @classmethod
    def get_description(cls):
        """
        Return a description of the ENA connector.

        :return: A string describing the ENA connector.
        """
        return (
            "The European Nucleotide Archive (ENA) provides a comprehensive record "
            "of the world's nucleotide sequencing information, covering raw sequencing "
            "data, sequence assembly information and functional annotation."
        )

    @classmethod
    def get_connection_documentation(cls):
        """
        Return the URL for connection documentation.

        :return: A string containing the connection documentation URL.
        """
        return "https://www.ebi.ac.uk/ena/submit/webin/"

    @classmethod
    def get_author(cls):
        """
        Return the author of the connector.

        :return: The author's name.
        """
        return "Madbot team"

    @classmethod
    def get_connection_fields(cls):
        """
        Retrieve the connection fields for the ENA connector.

        Returns
        -------
            tuple[list[ConnectionField], list[ConnectionField]]: A tuple containing:
                - A list of shared `ConnectionField` DTOs.
                - A list of private `ConnectionField` DTOs.

        """
        shared = [
            ConnectionField(
                slug="url_ena",
                schema_content=ENA_FIELDS["url_ena"].schema_rep.schema_content
                if "url_ena" in ENA_FIELDS
                else {},
            ),
        ]

        private = [
            ConnectionField(
                slug="webin",
                schema_content=ENA_FIELDS["webin"].schema_rep.schema_content
                if "webin" in ENA_FIELDS
                else {},
            ),
            ConnectionField(
                slug="password",
                schema_content=ENA_FIELDS["password"].schema_rep.schema_content
                if "password" in ENA_FIELDS
                else {},
            ),
        ]

        return shared, private

    @classmethod
    def get_logo(cls):
        """
        Return the logo path for the ENA connector.

        :return: A string representing the logo path.
        """
        return "madbot_ena/ena.png"

    @classmethod
    def get_color(cls):
        """
        Return the color associated with the ENA connector.

        :return: A string representing the color in hex format.
        """
        return "#70BDBD"

    def get_instance_name(self):
        """
        Return the instance name for the connector.

        :return: A string representing the instance name.
        """
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return f"ENA ({str(host)})"

    def get_shared_parameters(self):
        """
        Return shared parameters for the connector.

        :return: A dictionary containing shared parameters.
        """
        return {"url": self.url}

    @classmethod
    def get_metadata_fields_groups(cls, parent=None):
        """
        Return metadata field groups, optionally filtered by a parent group.

        :param parent: The parent group slug to filter the results (optional).
        :return: A list of metadata field groups.
        """
        if cls.groups is None:
            # Read the JSON file
            with open(
                os.path.join(settings.BASE_DIR, cls.METADATA_GROUPS_PATH), "r"
            ) as f:
                cls.groups = json.load(f)

        if parent:
            for group in cls.groups:
                if group["slug"] == parent.lower():
                    return group["groups"]
            return []
        return cls.groups

    @classmethod
    def get_settings_fields(cls):
        """
        Retrieve the settings fields for the ENA connector.

        Returns
        -------
            list[SettingField]: A list of SettingField DTOs, each containing a `slug`
            and its corresponding `schema_content`.

        """
        return [
            SettingField(
                slug="checklist",
                schema_content=ENA_FIELDS["checklist"].schema_rep.schema_content
                if "checklist" in ENA_FIELDS
                else {},
            ),
            SettingField(
                slug="access_control",
                schema_content=ENA_FIELDS["access_control"].schema_rep.schema_content
                if "access_control" in ENA_FIELDS
                else {},
            ),
        ]

    @classmethod
    def get_supported_file_extensions(cls):
        """
        Return a list of supported file extensions for the ENA connector.

        Returns
        -------
            list: list of file extensions supported by ENA.

        """
        return ["fastq", "fq"]

    @classmethod
    def get_all_metadata_object_types(cls):
        """
        Return a dictionary of metadata object types supported by the connector.

        Returns
        -------
            Dict[str, MetadataObjectType]: Dictionary of metadata object types supported by the connector.

        """
        return METADATA_OBJECT_TYPE

    @classmethod
    def create_metadata_objects(cls, metadataobjects=[], data=[]):
        """
        Create metadata objects for a submission.

        Args:
        ----
            metadataobjects (List[MetadataObject]): List of metadataObjects.
            data (List[Data]): List of data objects to create metadata objects for.

        Returns:
        -------
            List[MetadataObject]: List of metadata objects to create.

        """
        metadata_object_types = cls.get_all_metadata_object_types()

        metadata_objects_dto = []

        study_metadata_object = MetadataObject(
            type=metadata_object_types.get("study"),
            metadata=[],
            completeness={
                "mandatory": {
                    "complete": 0,
                    "total": 0,
                },
                "recommended": {
                    "complete": 0,
                    "total": 0,
                },
                "optional": {
                    "complete": 0,
                    "total": 0,
                },
            },
            cross_validation={},
        )

        # add metadata to the study metadata object
        for metadata_level in ["mandatory", "recommended", "optional"]:
            for metadata_field in getattr(
                metadata_object_types.get("study"), f"{metadata_level}_fields_schemas"
            ):
                # create empty metadata for now
                # later on if a value for this field is provided, use it for the metadata
                study_metadata_object.metadata.append(
                    SubmissionMetadata(
                        field=metadata_field,
                        value=None,
                        candidate_values=[],
                        is_valid=False,  # TODO: validate against the schema
                    )
                )

                # update completeness total values
                study_metadata_object.completeness[metadata_level]["total"] += 1
                # TODO: if a value is provided, update completeness complete values

        metadata_objects_dto.append(study_metadata_object)

        # TODO: create raw reads metadata objects
        # for data_object in data:
        #     metadata_objects_dto.append(
        #         MetadataObject(
        #             name=data_object.name,
        #             type=metadata_object_types.get("raw_reads"),
        #             metadata=[],  # TODO: add metadata
        #             linked_metadata_objects={},
        #         )
        #     )
        # TODO: infer sample metadata objects from the data objects architecture

        return metadata_objects_dto
