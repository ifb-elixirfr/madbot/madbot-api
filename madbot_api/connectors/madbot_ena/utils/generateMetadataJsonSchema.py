# ----------------------------------------------------------------
# Import
# ----------------------------------------------------------------
import json
import os
import re
import sys

import requests
from defusedxml import ElementTree
from django.utils.text import slugify

import madbot_api

# ----------------------------------------------------------------
# Field objects
# ----------------------------------------------------------------


class Field:
    """Represents a metadata field with its properties and validation rules for ENA schema generation."""

    REQUIREMENT_CHOICES = (
        ("recommended", "recommended"),
        ("optional", "optional"),
        ("mandatory", "mandatory"),
    )

    def __init__(
        self,
        name,
        description=None,
        format=None,
        restriction=None,
        level="mandatory",
        unit=None,
        group=None,
        example=None,
        checklists=None,
    ):
        """
        Initialize a Field instance with metadata properties.

        Args:
        ----
            name (str): Field name
            description (str, optional): Field description. Defaults to None.
            format (str, optional): Field format type. Defaults to None.
            restriction (str, optional): Field value restrictions. Defaults to None.
            level (str, optional): Requirement level. Defaults to "mandatory".
            unit (str, optional): Field unit. Defaults to None.
            group (str, optional): Field group. Defaults to None.
            example (str, optional): Example value. Defaults to None.
            checklists (str, optional): Associated checklists. Defaults to None.

        """
        self.name = name
        self.description = description
        self.format = format
        self.restriction = restriction
        self.level = level
        self.unit = unit
        self.group = group
        self.example = example
        self.checklists = checklists if checklists is not None else ""

    def __str__(self):
        """Return a string representation of the Field instance."""
        return f"{self.name} - {self.description}"


# ----------------------------------------------------------------
# Function
# ----------------------------------------------------------------
def sanitize_filename(filename):
    """Sanitize a filename to make it safe for file system operations."""
    sanitized = re.sub(r'[<>:"/\\|?*]', "_", filename)

    sanitized = re.sub(r"\s+", "_", sanitized)

    if os.name == "nt":
        sanitized = re.sub(r"[^\w.-]", "", sanitized)

    return slugify(sanitized)


# ----------------------------------------------------------------
# Variables
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Imports
# ----------------------------------------------------------------
for i in range(11, 60):
    response = requests.get(
        "https://www.ebi.ac.uk/ena/browser/api/xml/ERC0000" + str(i), timeout=10
    )
    if response.ok:
        checklist_name = "ERC0000" + str(i)
        sys.stdout.write(checklist_name + "\n")
        app_path = os.path.dirname(madbot_api.__file__)
        dir_path = os.path.join(
            app_path,
            "connectors",
            "madbot_ena",
            "static",
            "json",
            "schema",
            "metadata",
            "sample",
        )
        os.makedirs(dir_path, exist_ok=True)
        root = ElementTree.fromstring(response.content)
        cl = root.findall("./CHECKLIST/DESCRIPTOR/LABEL")[0].text
        for fg in root.findall("./CHECKLIST/DESCRIPTOR/FIELD_GROUP"):
            for inter_field in fg.findall("FIELD"):
                f = Field(name=inter_field.find("NAME").text, checklists=cl)
                if inter_field.find("DESCRIPTION") is not None:
                    inter_split = inter_field.find("DESCRIPTION").text.split(
                        " Example: "
                    )
                    f.description = inter_split[0]
                    if len(inter_split) == 2:
                        f.example = inter_split[1]
                f.format = inter_field.find("FIELD_TYPE")[0].tag
                f.level = inter_field.find("MANDATORY").text
                if len(inter_field.findall("./FIELD_TYPE/TEXT_FIELD/REGEX_VALUE")) != 0:
                    f.restriction = inter_field.find(
                        "./FIELD_TYPE/TEXT_FIELD/REGEX_VALUE"
                    ).text
                elif (
                    len(
                        inter_field.findall("./FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE")
                    )
                    != 0
                ):
                    items = []
                    for i in inter_field.findall(
                        "./FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE"
                    ):
                        items.append(i.find("VALUE").text)
                    f.restriction = items
                if len(inter_field.findall("./UNITS/UNIT")) != 0:
                    items = []
                    for i in inter_field.findall("./UNITS/UNIT"):
                        items.append(i.text)
                    f.unit = items
                f.group = fg.find("NAME").text
                json_schema = {
                    "title": f.name,
                    "type": "object",
                    "level": f.level,
                    "properties": {
                        f.name: {
                            "description": f.description,
                        }
                    },
                }

                if f.format == "TEXT_FIELD":
                    json_schema["properties"][f.name]["type"] = "string"

                    if hasattr(f, "restriction") and f.restriction:
                        json_schema["properties"][f.name]["pattern"] = f.restriction

                elif f.format == "DATE_FIELD":
                    json_schema["properties"][f.name]["type"] = "string"
                    json_schema["properties"][f.name]["format"] = "date"
                elif f.format == "ONTOLOGY_FIELD":
                    json_schema["properties"][f.name]["type"] = "string"
                    # TO DO: to improve with a new format and a renderer !
                elif f.format == "TEXT_AREA_FIELD":
                    pass
                elif f.format == "TAXON_FIELD":
                    json_schema["properties"][f.name]["type"] = "string"
                    # TO DO: to improve with a new format and a renderer !
                elif f.format == "TEXT_CHOICE_FIELD":
                    json_schema["properties"][f.name]["type"] = "string"
                    json_schema["properties"][f.name]["enum"] = f.restriction

                if f.level == "mandatory":
                    json_schema["required"] = [f.name]

                if hasattr(f, "unit") and f.unit:
                    json_schema["properties"]["unit"] = {
                        "type": "string",
                        "enum": f.unit,
                    }

                if hasattr(f, "example") and f.example:
                    json_schema["properties"][f.name]["example"] = f.restriction
                temp_name = sanitize_filename(checklist_name + "_" + f.name)
                with open(dir_path + "/" + temp_name + ".json", "w") as file:
                    json.dump(json_schema, file, indent=4)
