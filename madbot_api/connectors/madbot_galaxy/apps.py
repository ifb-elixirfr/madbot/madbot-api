from django.apps import AppConfig


class MadbotGalaxyConfig(AppConfig):
    """Config for the Madbot Galaxy connector."""

    name = "madbot_api.connectors.madbot_galaxy"
