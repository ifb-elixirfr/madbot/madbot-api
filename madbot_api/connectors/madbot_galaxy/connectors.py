import json
import logging
import mimetypes
import os
import posixpath
from urllib.parse import urlparse

import requests
from bioblend import ConnectionError, galaxy
from bioblend.galaxy.datasets import DatasetStateException
from django.utils.text import slugify

import madbot_api
from madbot_api.core.connector import (
    AuthenticationError,
    ConnectionField,
    DataConnector,
    DataInvalidError,
    DataNotFoundError,
    DownloadDataError,
    ExternalDataObject,
    SchemaRepresentation,
    UnreachableError,
)
from madbot_api.core.views.utils.icons import get_icon_nerd

logger = logging.getLogger(__name__)


def generate_slug(path):
    """
    Generate a slug from a file path.

    Args:
    ----
        path (str): The file path from which to generate the slug.

    Returns:
    -------
        str: A slug created by joining relevant parts of the path with underscores.

    """
    parts = path.split(os.sep)

    if "." in parts[-1]:
        parts[-1] = os.path.splitext(parts[-1])[0]

    return slugify("_".join(parts))


GALAXY_SCHEMA = []

app_path = os.path.dirname(madbot_api.__file__)

dir_path_schema = os.path.join(
    app_path,
    "connectors",
    "madbot_galaxy",
    "static",
    "json",
    "schema",
)

# Iterate over all JSON files in the schema directory
for root, _, files in os.walk(dir_path_schema):
    for file_name in files:
        if file_name.endswith(".json"):
            file_path = os.path.join(root, file_name)
            with open(file_path, "r") as f:
                schema_content = json.load(f)

                # Generate a slug based on the file path
                slug = generate_slug(os.path.relpath(file_path, dir_path_schema))

                # Create the Schema DTO for each field file
                schema_field = SchemaRepresentation(
                    slug=slug,
                    schema_content=schema_content,
                    source="galaxy",
                )

                GALAXY_SCHEMA.append(schema_field)


class GalaxyConnector(DataConnector):
    """Establish connection to the Galaxy instance for data operations."""

    def __init__(self, parameters):
        """
        Initialize the GalaxyConnector with connection parameters.

        Args:
        ----
            parameters (dict): A dictionary containing connection details.
                - url_galaxy: The URL of the Galaxy instance.
                - login: User login for Galaxy.
                - password: User password for Galaxy.

        Raises:
        ------
            AuthenticationError: If authentication fails.
            UnreachableError: If the Galaxy instance is unreachable.

        """
        url_parsed = urlparse(parameters["url_galaxy"].lower())
        self.url = "%s://%s" % (
            url_parsed.scheme,
            url_parsed.netloc,
        )
        self.login = parameters["login"]
        self.password = parameters["password"]

        try:
            self.gi = galaxy.GalaxyInstance(
                url=self.url,
                email=self.login,
                password=self.password,
                verify=True,
            )
        except Exception as e:
            if str(e) == "Failed to authenticate user.":
                raise AuthenticationError()
            raise UnreachableError(host=self.url)

        status_code = self.gi.make_get_request(url=self.gi.base_url).status_code
        if status_code != requests.codes.ok:
            if status_code == 401:
                raise AuthenticationError()
            raise UnreachableError(host=self.url)

    #
    # Default fonction
    #

    @classmethod
    def get_name(cls):
        """
        Return the name of the connector.

        Returns
        -------
            str: The name of the connector.

        """
        return "Galaxy"

    @classmethod
    def get_description(cls):
        """
        Return a description of the Galaxy connector.

        Returns
        -------
            str: A brief description of Galaxy's purpose.

        """
        return "Galaxy is an open source, web-based platform for data intensive biomedical research."

    @classmethod
    def get_author(cls):
        """
        Return the author of the connector.

        Returns
        -------
            str: The name of the author.

        """
        return "Madbot team"

    @classmethod
    def get_connection_fields(cls):
        """
        Retrieve the connection fields for the Galaxy connector.

        Returns
        -------
            tuple[list[ConnectionField], list[ConnectionField]]: A tuple containing:
                - A list of shared `ConnectionField` DTOs.
                - A list of private `ConnectionField` DTOs.

        """

        def get_schema_content_for_slug(slug):
            # Search for the SchemaRepresentation with the given slug
            for schema_field in GALAXY_SCHEMA:
                if schema_field.slug == slug:
                    return schema_field.schema_content
            return {}

        shared = [
            ConnectionField(
                slug="url_galaxy",
                schema_content=get_schema_content_for_slug("url_galaxy"),
            ),
        ]

        private = [
            ConnectionField(
                slug="login",
                schema_content=get_schema_content_for_slug("login"),
            ),
            ConnectionField(
                slug="password",
                schema_content=get_schema_content_for_slug("password"),
            ),
        ]

        return shared, private

    @classmethod
    def get_logo(cls):
        """
        Return the logo path for the connector.

        Returns
        -------
            str: The path to the connector's logo.

        """
        return "madbot_galaxy/galaxy.png"

    @classmethod
    def get_color(cls):
        """
        Return the color associated with the connector.

        Returns
        -------
            str: The color code for the connector.

        """
        return "#7FAEE5"

    @staticmethod
    def get_root_name():
        """
        Return the root name for Galaxy data.

        Returns
        -------
            str: The root name for Galaxy data.

        """
        return "All histories"

    @staticmethod
    def get_root_id():
        """
        Return the root ID for Galaxy data.

        Returns
        -------
            None: Always returns None for the root ID.

        """
        return

    def get_instance_name(self):
        """
        Return a user-friendly name for the Galaxy instance.

        Returns
        -------
            str: The name of the Galaxy instance.

        """
        name = self.gi.config.get_config()["brand"]
        if name:
            return "Galaxy " + str(name)
        url_parse = urlparse(self.gi.base_url)
        host = url_parse.hostname
        return str(host)

    def get_shared_parameters(self):
        """
        Return shared parameters for the Galaxy connection.

        Returns
        -------
            dict: A dictionary containing shared parameters.

        """
        return {"url": self.url}

    # def get_url_link_to_an_object(self, object_type, object_id):
    #     if object_type == "history":
    #         url = os.path.join(
    #             self.url + f"/history/switch_to_history?hist_id={object_id}"
    #         )
    #     elif object_type == "datasets":
    #         url = os.path.join(self.url + f"/datasets/{object_id}/details")
    #     return url

    def get_mime_type(self, file_extension):
        """
        Get the MIME type for a given file extension.

        Args:
        ----
            file_extension (str): The file extension to lookup.

        Returns:
        -------
            str: The corresponding MIME type, defaults to 'application/octet-stream'.

        """
        mime_type, _ = mimetypes.guess_type(file_extension)
        if mime_type:
            return mime_type
        return "application/octet-stream"

    def get_data_objects(self, parent_id):
        """
        Retrieve data objects associated with a given parent ID.

        Args:
        ----
            parent_id (str): The ID of the parent object.

        Returns:
        -------
            list: A list of data objects associated with the parent ID.

        Raises:
        ------
            DataNotFoundError: If the data object cannot be found.
            DataInvalidError: If the parent ID is invalid.

        """
        if parent_id is None:
            # root is requested, getting all histories
            histories = []
            hi = self.gi.histories.get_histories()
            for h in hi:
                histories.append(self.__history_to_dataobject(h))
            return histories

        if parent_id.startswith("hi"):
            history_id = parent_id[2:]
            data_objects = []
            try:
                di = self.gi.datasets.get_datasets(
                    history_id=history_id,
                    visible=True,
                    deleted=False,
                    purged=False,
                )
            except ConnectionError:
                raise DataNotFoundError(dataobject=parent_id)
            for d in di:
                try:
                    dataset = self.gi.datasets.show_dataset(dataset_id=str(d["id"]))
                except ConnectionError:
                    raise DataNotFoundError(dataobject=d["id"])
                data_objects.append(self.__dataset_to_dataobject(dataset))
            return data_objects
        raise DataInvalidError(dataobject=parent_id)

    def get_data_object(self, object_id):
        """
        Retrieve a data object by its ID.

        Args:
        ----
            object_id (str): The ID of the data object.

        Returns:
        -------
            ExternalDataObject: The requested data object.

        Raises:
        ------
            DataNotFoundError: If the data object cannot be found.

        """
        if object_id.startswith("hi"):
            history_id = object_id[2:]
            try:
                history = self.gi.histories.show_history(history_id=history_id)
            except ConnectionError:
                raise DataNotFoundError(dataobject=object_id)
            return self.__history_to_dataobject(history)
        dataset_id = object_id[2:]
        try:
            dataset = self.gi.datasets.show_dataset(
                dataset_id=dataset_id,
                deleted=False,
            )
        except ConnectionError:
            raise DataNotFoundError(dataobject=object_id)
        return self.__dataset_to_dataobject(dataset)

    def __history_to_dataobject(self, history):
        """
        Convert a Galaxy history object to an ExternalDataObject.

        Args:
        ----
            history: The Galaxy history object.

        Returns:
        -------
            ExternalDataObject: The corresponding external data object.

        """
        content = self.gi.histories.show_history(history_id=history["id"])
        data_object = ExternalDataObject(
            id=f"hi{history['id']}",
            name=history["name"],
            size=0,
            type="Galaxy History",
            media_type=None,
            file_extension=None,
            description=history["annotation"],
            icon=get_icon_nerd("history", history["name"]),
            has_children=content["state_details"]["ok"] != 0,
            linkable=True,
        )
        data_object.add_external_access(
            ExternalDataObject.ACCESS_BY_URL,
            os.path.join(
                self.url + f"/history/switch_to_history?hist_id={history['id']}",
            ),
        )
        return data_object

    def __dataset_to_dataobject(self, dataset):
        """
        Convert a Galaxy dataset object to an ExternalDataObject.

        Args:
        ----
            dataset: The Galaxy dataset object.

        Returns:
        -------
            ExternalDataObject: The corresponding external data object.

        """
        data_object = ExternalDataObject(
            id=f"ds{dataset['id']}",
            name=dataset["name"],
            description=dataset["misc_info"],
            file_extension=dataset["file_ext"],
            media_type=self.get_mime_type(dataset["data_type"]),
            type="Galaxy Dataset",
            size=dataset["file_size"],
            icon=get_icon_nerd("file", dataset["name"]),
            has_children=False,
            linkable=True,
        )
        data_object.add_external_access(
            ExternalDataObject.ACCESS_BY_URL,
            os.path.join(self.url + f"/datasets/{dataset['id']}/details"),
        )
        return data_object

    def download_data(self, external_dataobject_id, destination_path):
        """
        Import data object in local storage and return a import report.

        Args:
        ----
            external_dataobject_id (str): ID of the data object.
            destination_path (str): path to download file.

        Returns:
        -------
            data_download_report: An DTO containing detailed information about the import.

        Raises:
        ------
            DataNotFoundError: EException raised when a data object is not found.
            DownloadDataError: Exception raised for error during dowload data.

        """
        dataset_id = external_dataobject_id[2:]

        # STEP 1: Generate local path
        # Why this step ? Because use_default_filename=True Doesn't work very well and
        # produces final names like : Galaxy21-[FILENAME.xx].xx and not just FILENAME.xx
        try:
            dataset = self.gi.datasets.show_dataset(
                dataset_id=dataset_id,
                deleted=False,
            )
        except ConnectionError:
            raise DataNotFoundError(dataobject=external_dataobject_id)

        local_path = posixpath.join(
            destination_path, posixpath.basename(dataset["name"])
        )

        try:
            local_file_path = self.gi.datasets.download_dataset(
                dataset_id,
                file_path=local_path,
                use_default_filename=False,
                require_ok_state=True,
            )
        except DatasetStateException as e:
            raise DownloadDataError(reason="Error downloading: %s" % str(e))
        except ConnectionError:
            raise DataNotFoundError(dataobject=external_dataobject_id)
        except Exception as e:
            raise DownloadDataError(reason="Error downloading: %s" % str(e))

        # STEP 2: As there is no checksum returned for the moment, we propose a basic
        # check of the file weight
        local_file_size = os.path.getsize(local_file_path)

        # STEP 3: Return the import report
        if local_file_size != dataset["file_size"]:
            if os.path.exists(local_path):
                os.remove(local_path)
            raise DownloadDataError(reason="Integrity error: size comparison failed")

        return local_file_path
