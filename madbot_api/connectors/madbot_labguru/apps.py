from django.apps import AppConfig


class MadbotLabGuruConfig(AppConfig):
    """Config for the MadbotLabGuru connector."""

    name = "madbot_api.connectors.madbot_labguru"
