from django.apps import AppConfig


class MadbotOmeroConfig(AppConfig):
    """Config for the MadbotOMERO connector."""

    name = "madbot_api.connectors.madbot_omero"
