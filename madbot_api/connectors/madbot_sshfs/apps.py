from django.apps import AppConfig


class MadbotSSHFSConfig(AppConfig):
    """Config for the MadbotSSHFS connector."""

    name = "madbot_api.connectors.madbot_sshfs"
