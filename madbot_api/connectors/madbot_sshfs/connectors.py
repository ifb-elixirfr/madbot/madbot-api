import json
import mimetypes
import os.path
import posixpath
import stat
from pathlib import Path

from django.utils.text import slugify
from paramiko import MissingHostKeyPolicy
from paramiko.client import SSHClient
from paramiko.ssh_exception import (
    AuthenticationException,
    NoValidConnectionsError,
)

import madbot_api
from madbot_api.core.connector import (
    AuthenticationError,
    ConnectionField,
    DataConnector,
    DataNotFoundError,
    DownloadDataError,
    ExternalDataObject,
    SchemaRepresentation,
    UnreachableError,
)
from madbot_api.core.views.utils.icons import get_icon_nerd

SCRIPT_TEMPLATE = """/bin/bash <<"EOT"
for item in '{directory}'/*; do
        if [[ -r $item ]]; then
                if [[ -f $item ]]; then
                        item_type="file"
                        size=$(stat -L -c %s "$item")
                else
                        item_type="directory"
                        size=0
                fi
                echo -e "$item|$item_type|$size"
        fi
done
EOT"""


def generate_slug(path):
    """
    Generate a slug from a file path by extracting relevant parts.

    Args:
    ----
        path (str): The file path from which to generate the slug.

    Returns:
    -------
        str: A slug created by joining relevant parts of the path with underscores.

    """
    parts = path.split(os.sep)

    if "." in parts[-1]:
        parts[-1] = os.path.splitext(parts[-1])[0]

    return slugify("_".join(parts))


SSHFS_SCHEMA = []

app_path = os.path.dirname(madbot_api.__file__)

dir_path_schema = os.path.join(
    app_path,
    "connectors",
    "madbot_sshfs",
    "static",
    "json",
    "schema",
)

# Iterate over all JSON files in the schema directory
for root, _, files in os.walk(dir_path_schema):
    for file_name in files:
        if file_name.endswith(".json"):
            file_path = os.path.join(root, file_name)
            with open(file_path, "r") as f:
                schema_content = json.load(f)

                # Generate a slug based on the file path
                slug = generate_slug(os.path.relpath(file_path, dir_path_schema))

                # Create the Schema DTO for each field file
                schema_field = SchemaRepresentation(
                    slug=slug,
                    schema_content=schema_content,
                    source="sshfs",
                )

                SSHFS_SCHEMA.append(schema_field)


class SSHFSConnector(DataConnector):
    """Establish an SSHFS connection to a remote filesystem."""

    def __init__(self, parameters):
        """
        Initialize the SSHFSConnector with connection parameters.

        Args:
        ----
            parameters (dict): A dictionary containing connection parameters including
                               'host_sshfs', 'username', and 'password'.

        Raises:
        ------
            AuthenticationError: If authentication fails.
            UnreachableError: If unable to connect to the host.

        """
        self.host = parameters["host_sshfs"]
        self.username = parameters["username"]
        self.password = parameters["password"]

        self.client = SSHClient()
        self.client.set_missing_host_key_policy(MissingHostKeyPolicy())

        try:
            self.client.connect(
                self.host,
                username=self.username,
                password=self.password,
            )
        except AuthenticationException:
            raise AuthenticationError()
        except NoValidConnectionsError:
            raise UnreachableError()

    @classmethod
    def get_name(cls):
        """
        Return the name of the connector.

        Returns
        -------
            str: The name of the connector.

        """
        return "SSHFS"

    @classmethod
    def get_description(cls):
        """
        Return the description of the connector.

        Returns
        -------
            str: The description of the connector.

        """
        return "Connect to any NAS or cluster through SSH"

    @classmethod
    def get_author(cls):
        """
        Return the author of the connector.

        Returns
        -------
            str: The author's name.

        """
        return "Madbot Team"

    @classmethod
    def get_connection_fields(cls):
        """
        Retrieve the connection fields for the Galaxy connector.

        Returns
        -------
            tuple[list[ConnectionField], list[ConnectionField]]: A tuple containing:
                - A list of shared `ConnectionField` DTOs.
                - A list of private `ConnectionField` DTOs.

        """

        def get_schema_content_for_slug(slug):
            # Search for the SchemaRepresentation with the given slug
            for schema_field in SSHFS_SCHEMA:
                if schema_field.slug == slug:
                    return schema_field.schema_content
            return {}

        shared = [
            ConnectionField(
                slug="host_sshfs",
                schema_content=get_schema_content_for_slug("host_sshfs"),
            ),
        ]

        private = [
            ConnectionField(
                slug="username",
                schema_content=get_schema_content_for_slug("username"),
            ),
            ConnectionField(
                slug="password",
                schema_content=get_schema_content_for_slug("password"),
            ),
        ]

        return shared, private

    @classmethod
    def get_logo(cls):
        """
        Return the logo path for the connector.

        Returns
        -------
            str: The logo path.

        """
        return "madbot_sshfs/sshfs.png"

    @classmethod
    def get_color(cls):
        """
        Return the color associated with the connector.

        Returns
        -------
            str: The color code.

        """
        return "#FF0000"

    @staticmethod
    def get_root_name():
        """
        Return the name of the root directory.

        Returns
        -------
            str: The root name.

        """
        return "Filesystem root"

    @staticmethod
    def get_root_id():
        """
        Return the ID of the root directory.

        Returns
        -------
            str: The root ID.

        """
        return "/"

    def get_instance_name(self):
        """
        Return the instance name of the connector.

        Returns
        -------
            str: The host name.

        """
        return self.host

    def get_shared_parameters(self):
        """
        Return shared parameters for the connector.

        Returns
        -------
            dict: A dictionary of shared parameters.

        """
        return {
            "host": self.host,
        }

    def get_data_objects(self, parent_id):
        """
        Return a list of data objects for the given parent ID.

        Args:
        ----
            parent_id (str): The parent directory ID.

        Returns:
        -------
            list: A sorted list of ExternalDataObject instances.

        """
        parent_id = "/" if parent_id is None else parent_id
        list_objects = []
        objects = self.get_readable_items(parent_id)
        for obj in objects:
            id, object_type, size = obj.split("|")
            is_dir = object_type == "directory"
            is_link = object_type == "link"
            name = posixpath.basename(id)
            list_objects.append(
                ExternalDataObject(
                    id=id,
                    name=id.rsplit("/", 1)[-1],
                    type=object_type,
                    size=size,
                    icon=get_icon_nerd("dir" if is_dir or is_link else "file", name),
                    media_type=self.get_mime_type(id),
                    file_extension=Path(id).suffix.lstrip("."),
                    has_children=is_dir,
                ),
            )

        return sorted(list_objects, key=lambda x: x.name)

    def get_data_object(self, object_id):
        """
        Return a data object for the specified object ID.

        Args:
        ----
            object_id (str): The ID of the object.

        Returns:
        -------
            ExternalDataObject: The data object instance.

        Raises:
        ------
            DataNotFoundError: If the object does not exist.

        """
        sftp = self.client.open_sftp()
        try:
            object_stat = sftp.stat(object_id)
            is_dir = stat.S_ISDIR(object_stat.st_mode)
            name = posixpath.basename(object_id)

            return ExternalDataObject(
                id=object_id,
                name=name,
                type="directory" if is_dir else "file",
                size=object_stat.st_size,
                icon=get_icon_nerd("dir" if is_dir else "file", name),
                media_type=self.get_mime_type(name),
                file_extension=Path(object_id).suffix.lstrip("."),
                has_children=is_dir,
            )

        except FileNotFoundError:
            raise DataNotFoundError(object_id)

    def get_mime_type(self, file_name):
        """
        Return the MIME type of a file based on its name.

        Args:
        ----
            file_name (str): The name of the file.

        Returns:
        -------
            str: The MIME type of the file. If the MIME type cannot be determined, "application/octet-stream" is returned.

        """
        return mimetypes.guess_type(file_name)[0] or None

    def get_readable_items(self, path):
        """
        Return the list of files and directory contained in path that a readable for the current user.

        Args:
        ----
            path (str): The path read items from

        Returns:
        -------
            list: A list of permitted files for the given path, if successful.

        Raises:
        ------
            Exception: If an error occurs while checking the ACL permissions.

        """
        script = SCRIPT_TEMPLATE.format(directory=path)
        stdin, stdout, stderr = self.client.exec_command(script)  # nosec B601
        errors = stderr.readlines()
        if len(errors) > 0:
            return []  # we might should raise an exception here

        return [line.strip("\n") for line in stdout.readlines()]

    def download_data(self, external_dataobject_id, destination_path):
        """
        Import data object in local storage and return a import report.

        Args:
        ----
            external_dataobject_id (str): ID of the data object.
            destination_path (str): path to download file.

        Returns:
        -------
            data_download_report: An DTO containing detailed information about the import.

        Raises:
        ------
            DataNotFoundError: EException raised when a data object is not found.
            DownloadDataError: Exception raised for error during dowload data.

        """
        sftp = self.client.open_sftp()

        # STEP 1: Download the data
        local_path = posixpath.join(
            destination_path, posixpath.basename(external_dataobject_id)
        )

        try:
            sftp.get(
                external_dataobject_id,
                local_path,
                # You can also add a callback to follow the download. This callback can be used in asynchronous tasks.
                # callback=self.__progress_callback
            )

        except FileNotFoundError:
            raise DataNotFoundError(external_dataobject_id)
        except Exception as e:
            raise DownloadDataError(f"Failed to download data: {str(e)}")

        # STEP 2: Check data integrity

        self.__verify_file_integrity(external_dataobject_id, local_path)
        # STEP 3: Return path of the downloaded file

        return local_path

    # @staticmethod
    # def __progress_callback(transferred, total):
    #     percent = (transferred / total) * 100 if total else 100
    #     print(f"Downloaded: {transferred}/{total} bytes ({percent:.2f}%)", end="\r")

    def __is_command_available(self, command):
        """
        Check if a given command is available on the remote server.

        This method executes a shell command to determine whether the specified
        command exists and is executable.

        Args:
        ----
            command (str): The name of the command to check.

        Returns:
        -------
            bool: True if the command is available, False otherwise.

        """
        stdin, stdout, stderr = self.client.exec_command(
            f"command -v {command} >/dev/null 2>&1 && echo OK || echo NO"
        )  # nosec B601
        return stdout.read().decode().strip() == "OK"

    def __get_remote_checksum(self, remote_path):
        """
        Compute the checksum of a remote file using the best available method.

        This method attempts to use `md5sum`, `sha256sum`, or a Python script executed remotely
        to calculate the file's hash.

        Args:
        ----
            remote_path (str): The path to the remote file.

        Returns:
        -------
            dict or None: A dictionary containing the method used and the computed hash value.
                        Returns None if no suitable method is available.

        """
        if self.__is_command_available("md5sum"):
            cmd = f"md5sum {remote_path} | awk '{{print $1}}'"
            method = "md5sum"
        if self.__is_command_available("sha256sum"):
            cmd = f"sha256sum {remote_path} | awk '{{print $1}}'"
            method = "sha256sum"
        elif self.__is_command_available("python3"):
            cmd = f'python3 -c \'import hashlib; print(hashlib.md5(open("{remote_path}", "rb").read()).hexdigest())\''
            method = "python3"
        else:
            return None

        stdin, stdout, stderr = self.client.exec_command(cmd)  # nosec B601
        return {"method": method, "value": stdout.read().decode().strip()}

    def __get_remote_filesize(self, remote_path):
        """
        Retrieve the size of a remote file in bytes.

        Args:
        ----
            remote_path (str): The path to the remote file.

        Returns:
        -------
            int: The file size in bytes.

        """
        sftp = self.client.open_sftp()
        object_stat = sftp.stat(remote_path)
        return object_stat.st_size

    @staticmethod
    def __get_local_checksum(local_file_path, remote_method):
        """
        Compute the checksum of a local file using the same method as the remote system.

        Args:
        ----
            local_file_path (str): Path to the local file.
            remote_method (str): The checksum method used remotely (e.g., 'md5sum', 'sha256sum', 'python3').

        Returns:
        -------
            str: The computed hash value.

        """
        import hashlib

        hash_algorithms = {
            "md5sum": hashlib.md5,
            "sha256sum": hashlib.sha256,
            "python3": hashlib.md5,  # Python3 command uses MD5 in the remote method
        }

        hasher = hash_algorithms[remote_method]()

        with open(local_file_path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hasher.update(chunk)

        return hasher.hexdigest()

    @staticmethod
    def __get_local_filesize(local_file_path):
        """
        Get the size of a local file in bytes.

        This function uses the `os.path.getsize()` method to retrieve the size of the
        file specified by the `local_file_path`.

        Args:
        ----
            local_file_path (str): The path of the local file whose size is to be retrieved.

        Returns:
        -------
            int: The size of the file in bytes.

        """
        return os.path.getsize(local_file_path)

    def __verify_file_integrity(self, remote_path, local_path):
        """
        Checkthe file intergrity.

        Args:
        ----
            remote_path (str): The path to the remote file.
            local_path (str): The path to the local file.


        """
        remote_checksum = self.__get_remote_checksum(remote_path)

        if remote_checksum:
            local_checksum = self.__get_local_checksum(
                local_path, remote_checksum["method"]
            )
            if remote_checksum["value"] != local_checksum:
                if os.path.exists(local_path):
                    os.remove(local_path)
                raise DownloadDataError(
                    reason="Integrity error : checksum comparison failed"
                )

        # Fallback
        remote_size = self.__get_remote_filesize(remote_path)
        local_size = self.__get_local_filesize(local_path)

        if remote_size != local_size:
            if os.path.exists(local_path):
                os.remove(local_path)
            raise DownloadDataError(reason="Integrity error: size comparison failed")
