import json
import logging
import os
from urllib.parse import urlparse

import requests
from django.utils.text import slugify

import madbot_api
import madbot_api.core.views
from madbot_api.connectors.madbot_zenodo.mapping_treatment import (
    treatment_contributors,
    treatment_creators,
    treatment_description,
    treatment_protocols,
    treatment_related_identifiers,
)
from madbot_api.core.connector import (
    AuthenticationError,
    ConnectionField,
    Data,
    FieldRepresentation,
    MetadataObject,
    MetadataObjectType,
    SchemaRepresentation,
    SettingField,
    SubmissionConnector,
    SubmissionMetadata,
    UnreachableError,
    ValueField,
)

logger = logging.getLogger(__name__)


def generate_slug(path):
    """
    Generate a slug from a file path.

    Args:
    ----
        path (str): The file path from which to generate the slug.

    Returns:
    -------
        str: A slug created by joining relevant parts of the path with underscores.

    """
    parts = path.split(os.sep)

    if "." in parts[-1]:
        parts[-1] = os.path.splitext(parts[-1])[0]

    return slugify("_".join(parts))


class ConnectorMappingField:
    """Class representing a Connector mapping field for the mapping."""

    def __init__(self, referential, treatment_method):
        """
        Initialize the ConnectorMappingField with the referential and treatment method.

        Args:
        ----
            referential (list): A list of referential fields.
            treatment_method (callable): A function to process the referential fields.

        """
        self.referential = referential
        self.treatment_method = treatment_method

    def get_referential(self):
        """Return the referential of the ConnectorMappingField."""
        return self.referential

    def process(self, *args, **kwargs):
        """Apply the treatment method on the provided value."""
        try:
            return self.treatment_method(*args, **kwargs)
        except Exception as e:
            raise ValueError(
                f"Error applying treatment method for {self.referential}: {str(e)}"
            )


MAPPING_ZENODO = {
    "contributors-zenodo": ConnectorMappingField(
        ["contributors"], treatment_contributors
    ),
    "dates": ConnectorMappingField(["key_dates"], lambda x: x),
    "grants": ConnectorMappingField(["grants"], lambda x: x),
    "keywords": ConnectorMappingField(["keywords"], lambda x: x),
    "creators": ConnectorMappingField(["contributors"], treatment_creators),
    "language": ConnectorMappingField(["language"], lambda x: x),
    "locations": ConnectorMappingField(["collection_location"], lambda x: x),
    "method": ConnectorMappingField(["protocol"], treatment_protocols),
    "references": ConnectorMappingField(["identifiers"], lambda x: x),
    "related-identifiers-zenodo": ConnectorMappingField(
        ["related-identifiers"], treatment_related_identifiers
    ),
    "subjects": ConnectorMappingField(["ncbi-taxon"], lambda x: x),
    "description": ConnectorMappingField(["description"], treatment_description),
}


ZENODO_FIELDS = {}
general_mandatory_fields = []
general_recommended_fields = []
general_optional_fields = []

app_path = os.path.dirname(madbot_api.__file__)
dir_path_schema = os.path.join(
    app_path,
    "connectors",
    "madbot_zenodo",
    "static",
    "json",
    "schema",
)


# Load the schemas and create necessary field representations
for root, _, files in os.walk(dir_path_schema):
    for file_name in files:
        if file_name.endswith(".json"):
            file_path = os.path.join(root, file_name)
            with open(file_path, "r") as f:
                # Add the fields to the appropriate list
                metadata_field = json.load(f)
                metadata_slug = generate_slug(
                    os.path.relpath(file_path, dir_path_schema)
                )
                mapping_field = MAPPING_ZENODO.get(metadata_slug)

                # Create the Schema DTO for each field file
                schema_field = SchemaRepresentation(
                    slug=metadata_slug,
                    schema_content=metadata_field,
                    source="zenodo",
                )
                # create the Field DTO
                field_dto = FieldRepresentation(
                    slug=metadata_slug,
                    schema_rep=schema_field,
                    title=metadata_field.get("title", ""),
                    description=metadata_field.get("description", ""),
                    URL_external="",
                    madbot_fields=mapping_field.get_referential()
                    if mapping_field
                    else [],
                    hidden=metadata_field.get("hidden", False),
                )
                ZENODO_FIELDS[metadata_slug] = field_dto

                if metadata_field.get("level") == "mandatory":
                    general_mandatory_fields.append(ZENODO_FIELDS[metadata_slug])
                elif metadata_field.get("level") == "recommended":
                    general_recommended_fields.append(ZENODO_FIELDS[metadata_slug])
                elif metadata_field.get("level") == "optional":
                    general_optional_fields.append(ZENODO_FIELDS[metadata_slug])


# Define MetadataObjectType using the loaded fields
METADATA_OBJECT_TYPE = {
    "general": MetadataObjectType(
        name="General",
        slug="general",
        summary_fields=["title", "description"],
        priority=["title", "description"],
        description="Deposition metadata used for uploading and editing records on Zenodo.",
        minimal_occurrence=1,
        maximal_occurrence=1,
        mandatory_fields_schemas=general_mandatory_fields,
        recommended_fields_schemas=general_recommended_fields,
        optional_fields_schemas=general_optional_fields,
    ),
}


class ZenodoConnector(SubmissionConnector):
    """Establish connection to the Zenodo instance for submission operations."""

    def __init__(self, parameters):
        """
        Initialize the ZenodoConnector with connection parameters.

        Args:
        ----
            parameters (dict): A dictionary containing connection and submission details.
                - url_zenodo: The URL of the Zenodo instance.
                - token: The API token for accessing Zenodo.
                - settings: Structured settings for the submission

        Raises:
        ------
            AuthenticationError: If authentication fails.
            UnreachableError: If the Zenodo instance is unreachable.

        """
        url_parsed = urlparse(parameters["url_zenodo"].lower())
        self.url = "%s://%s" % (url_parsed.scheme, url_parsed.netloc)
        if "token" in parameters:
            self.token = parameters["token"]
            self.headers = {
                "Authorization": f"Bearer {self.token}",
                "Content-Type": "application/json",
            }
            self._validate_connection()
        if "settings" in parameters:
            self.settings = parameters["settings"]

    def _validate_connection(self):
        """
        Attempt to connect to the Zenodo server with the provided credentials.

        Raises
        ------
            UnreachableError: If the Zenodo server cannot be reached
                or the request times out.
            AuthenticationError: If authentication fails.

        """
        try:
            response = requests.get(
                f"{self.url}/api/deposit/depositions",
                headers=self.headers,
                timeout=30,
            )

            # Raise an error if the response indicates a failure
            response.raise_for_status()
        except requests.exceptions.ConnectionError as conn_err:
            # Raised if the server is unreachable
            logger.error("Connection error: ", {conn_err})
            raise UnreachableError(f"Unable to reach {self.url}")

        except requests.exceptions.Timeout as timeout_err:
            # Raised when the request times out
            logger.error("Request timed out: ", {timeout_err})
            raise UnreachableError(f"Request to {self.url} timed out")

        except requests.RequestException as req_err:
            # General requests exceptions (e.g., invalid URL)
            logger.error("Request error occurred: ", {req_err})
            raise AuthenticationError()

    #
    # Default functions
    #

    @classmethod
    def get_name(cls):
        """
        Return the name of the connector.

        Returns
        -------
            str: The name of the connector.

        """
        return "Zenodo"

    @classmethod
    def get_description(cls):
        """
        Return a description of the Zenodo connector.

        Returns
        -------
            str: A brief description of Zenodo's purpose.

        """
        return (
            "Zenodo is an open source, web-based platform for research data sharing and preservation, "
            "developed under the European OpenAIRE program and operated by CERN. "
        )

    @classmethod
    def get_connection_documentation(cls):
        """
        Return the URL for the Zenodo API connection documentation.

        Returns
        -------
            str: The URL for the Zenodo API connection documentation.

        """
        return "https://developers.zenodo.org/#authentication"

    @classmethod
    def get_author(cls):
        """
        Return the author of the connector.

        Returns
        -------
            str: The name of the author.

        """
        return "Madbot team"

    @classmethod
    def get_connection_fields(cls):
        """
        Retrieve the connection fields for the Zenodo connector.

        Returns
        -------
            tuple[list[ConnectionField], list[ConnectionField]]: A tuple containing:
                - A list of shared `ConnectionField` DTOs.
                - A list of private `ConnectionField` DTOs.

        """
        shared = [
            ConnectionField(
                slug="url_zenodo",
                schema_content=ZENODO_FIELDS["url_zenodo"].schema_rep.schema_content
                if "url_zenodo" in ZENODO_FIELDS
                else {},
            ),
        ]

        private = [
            ConnectionField(
                slug="token",
                schema_content=ZENODO_FIELDS["token"].schema_rep.schema_content
                if "token" in ZENODO_FIELDS
                else {},
            ),
        ]

        return shared, private

    @classmethod
    def get_logo(cls):
        """
        Return the logo path for the connector.

        Returns
        -------
            str: The path to the connector's logo.

        """
        return "madbot_zenodo/zenodo.png"

    @classmethod
    def get_color(cls):
        """
        Return the color associated with the connector.

        Returns
        -------
            str: The color code for the connector.

        """
        return "#7CD0DD"

    def get_instance_name(self):
        """
        Return a user-friendly name for the Zenodo instance.

        Returns
        -------
            str: The name of the Zenodo instance.

        """
        url_parse = urlparse(self.url)
        host = url_parse.hostname
        return f"Zenodo ({str(host)})"

    def get_shared_parameters(self):
        """
        Return shared parameters for the Zenodo connection.

        Returns
        -------
            dict: A dictionary containing shared parameters.

        """
        return {"url": self.url}

    @classmethod
    def get_metadata_fields_groups(cls, parent=None):
        """
        Return metadata field groups for the Zenodo connector.

        Args:
        ----
            parent: An optional parent field to group the metadata fields.

        Returns:
        -------
            list: An empty list as no specific groups are defined.

        """
        return []

    @classmethod
    def get_settings_fields(cls):
        """
        Retrieve the settings fields for the Zenodo connector.

        Returns
        -------
            list[SettingField]: A list of SettingField DTOs, each containing a `slug`
            and its corresponding `schema_content`.

        """
        return [
            SettingField(
                slug="zenodo_access-right",
                schema_content=ZENODO_FIELDS[
                    "zenodo_access-right"
                ].schema_rep.schema_content
                if "zenodo_access-right" in ZENODO_FIELDS
                else {},
            ),
            SettingField(
                slug="upload-type",
                schema_content=ZENODO_FIELDS["upload-type"].schema_rep.schema_content
                if "upload-type" in ZENODO_FIELDS
                else {},
            ),
        ]

    @classmethod
    def get_all_metadata_object_types(cls):
        """
        Return a dictionary of metadata object types supported by the connector.

        Returns
        -------
            Dict[str, MetadataObjectType]: Dictionary of metadata object types supported by the connector.

        """
        return METADATA_OBJECT_TYPE

    @classmethod
    def get_supported_file_extensions(cls):
        """
        Return a list of supported file extensions for the Zenodo connector.

        Returns
        -------
            list: empty list as all file extensions are supported.

        """
        return []

    def create_metadata_objects(self, submission, metadata_objects=[], data=[]):
        """
        Create metadata objects for a submission.

        This function takes a submission object, a list of already existing metadata objects
        and a list of data objects (as `Data`) and return a list of `MetadataObject` for
        the core to create the corresponding metadata objects.

        Args:
        ----
            submission (Submission): The submission object.
            metadata_objects (List[MetadataObject]): List of already existing metadataObjects.
            data (List[Data]): List of data objects to create metadata objects for.

        Returns:
        -------
            List[MetadataObject]: List of metadata objects to create.

        """
        from pathlib import Path

        from django.core.exceptions import ValidationError
        from referencing import Registry, Resource

        from madbot_api.core.views.utils.schema_validation import MadbotValidator

        metadata_object_types = self.get_all_metadata_object_types()

        # Find the last common ancestor(s) for all data objects
        common_ancestor = Data.find_last_common_ancestor(data)

        general_metadata_object = MetadataObject(
            id=metadata_objects[0].id if metadata_objects else None,
            type=metadata_object_types.get("general"),
            metadata=[],
            completeness={
                "mandatory": {"complete": 0, "total": 0},
                "recommended": {"complete": 0, "total": 0},
                "optional": {"complete": 0, "total": 0},
            },
            cross_validation={},
        )

        # Set up schema validation
        schema_path = Path(dir_path_schema)

        def retrieve_from_filesystem(uri: str):
            path = schema_path / Path(
                uri.removeprefix("/api/schemas/connectors/madbot_zenodo/") + ".json"
            )
            contents = json.loads(path.read_text())
            contents["$schema"] = "https://json-schema.org/draft/2019-09/schema"
            return Resource.from_contents(contents)

        registry = Registry(retrieve=retrieve_from_filesystem)

        # Create field values mapping
        field_values = {}
        field_candidate_values = {
            "title": [ValueField(value=ancestor.title) for ancestor in common_ancestor]
            if common_ancestor
            else [ValueField(value=submission.title)],
            "description": [
                ValueField(value=ancestor.description) for ancestor in common_ancestor
            ]
            if common_ancestor
            else [],
            "creators": [
                ValueField(value=[{"name": f"{member.last_name}, {member.first_name}"}])
                for member in submission.members
            ],
        }

        # add metadata to the metadata object of type general
        for metadata_level in ["mandatory", "recommended", "optional"]:
            field_schemas = getattr(
                metadata_object_types.get("general"), f"{metadata_level}_fields_schemas"
            )
            for metadata_field in field_schemas:
                candidate_values = field_candidate_values.get(metadata_field.slug, [])
                value = field_values.get(metadata_field.slug, None)

                # if there are only one candidate value, and no value, we can use the candidate value
                if len(candidate_values) == 1 and not value:
                    value = candidate_values[0]
                    candidate_values = []

                # Validate value if present
                is_valid = False
                if value is not None:
                    try:
                        validator = MadbotValidator(
                            {
                                "$ref": "/api/schemas/connectors/madbot_zenodo/"
                                + metadata_field.slug
                            },
                            registry=registry,
                        )
                        validator.validate(value.value)
                        is_valid = True
                    except ValidationError:
                        is_valid = False

                general_metadata_object.metadata.append(
                    SubmissionMetadata(
                        field=metadata_field.slug,
                        value=value,
                        candidate_values=candidate_values,
                        is_valid=is_valid,
                    )
                )

                # update completeness
                general_metadata_object.completeness[metadata_level]["total"] += 1
                if is_valid:
                    general_metadata_object.completeness[metadata_level][
                        "complete"
                    ] += 1

        return [general_metadata_object]
