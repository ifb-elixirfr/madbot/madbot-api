import bleach
import markdown


def treatment_contributors(contributor_list, submission_members):
    """
    Standardize and concatenate contributors from the list and submission members.

    Args:
    ----
        contributor_list (list): List of contributors (dicts with name/affiliation).
        submission_members (list): List of submission members (strings or dicts).

    Returns:
    -------
        list: Merged list of contributors in a standardized format.

    """
    standardized_contributors = [
        contributor if isinstance(contributor, dict) else {"name": contributor}
        for contributor in contributor_list
    ]

    standardized_submission_members = [
        {"name": member} if isinstance(member, str) else member
        for member in submission_members
    ]

    return standardized_contributors + standardized_submission_members


def treatment_creators(submission_members):
    """
    Standardize and return submission members as creators.

    Args:
    ----
        submission_members (list): List of submission members (strings or dicts).

    Returns:
    -------
        list: Standardized list of creators.

    """
    return [
        {"name": member} if isinstance(member, str) else member
        for member in submission_members
    ]


def treatment_protocols(protocols):
    """
    Convert protocol steps into an HTML-formatted string.

    Args:
    ----
        protocols (list): List of protocol steps.

    Returns:
    -------
        str: Concatenated and HTML-formatted protocol steps.

    """
    return convert_protocols_to_markdown(protocols)


def treatment_related_identifiers(identifiers):
    """
    Enrich identifiers with relationships.

    Args:
    ----
        identifiers (list): List of identifiers.

    Returns:
    -------
        list: Enriched list of identifiers with relations.

    """
    return [{"identifier": id, "relation": "related"} for id in identifiers]


def treatment_description(node):
    """
    Standardize and return the description of a node.

    Args:
    ----
        node: The node object which includes 'description'.

    Returns:
    -------
        str: The standardized description value.

    """
    return node.get("description")


def convert_protocols_to_markdown(protocols):
    """
    Convert a list of protocol steps into a Markdown-formatted string.

    Args:
    ----
        protocols (list): List of protocol steps (dicts with 'step' and 'description').

    Returns:
    -------
        str: Protocol steps formatted in Markdown.

    Example:
    -------
        protocols = [
            {"step": "1", "description": "Prepare the samples"},
            {"step": "2", "description": "Load into the sequencer"}
        ]
        Output:
        ## Protocol Steps

        1. **Prepare the samples**
        2. **Load into the sequencer**

    """
    markdown_text = "## Protocol Steps\n\n"
    for protocol in protocols:
        step = protocol.get("step")
        description = protocol.get("description")
        markdown_text += f"{step}. **{description}**\n"

    return markdown_text.strip()


# Allowed HTML tags for sanitization
ALLOWED_TAGS = {
    "a",
    "abbr",
    "acronym",
    "b",
    "blockquote",
    "br",
    "code",
    "caption",
    "div",
    "em",
    "i",
    "li",
    "ol",
    "p",
    "pre",
    "span",
    "strike",
    "strong",
    "sub",
    "table",
    "tbody",
    "thead",
    "th",
    "td",
    "tr",
    "u",
    "ul",
}


def convert_markdown_to_html(markdown_text):
    """
    Convert Markdown text to sanitized HTML.

    Args:
    ----
        markdown_text (str): The Markdown string.

    Returns:
    -------
        str: Converted HTML string, sanitized to allow only specified tags.

    """
    # Convert Markdown to HTML
    html = markdown.markdown(markdown_text)

    # Sanitize the HTML to allow only specified tags
    return bleach.clean(html, tags=ALLOWED_TAGS, strip=True)
