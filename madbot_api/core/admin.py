from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.urls import reverse
from django.utils.html import format_html, format_html_join
from simple_history.admin import SimpleHistoryAdmin

from madbot_api.core.models import (
    ApplicationUserParam,
    Connection,
    ConnectorMetadataField,
    ConnectorRawSchema,
    Data,
    DataAccessRight,
    DataAssociation,
    DataLink,
    DataType,
    MadbotMetadataField,
    MadbotRawSchema,
    MadbotUser,
    Metadata,
    MetadataMapping,
    Node,
    NodeMember,
    NodeType,
    Sample,
    SampleBoundData,
    SharedConnectionParam,
    Submission,
    SubmissionData,
    SubmissionMember,
    SubmissionMetadata,
    SubmissionMetadataObject,
    SubmissionSettings,
    Workspace,
    WorkspaceMember,
)

User = get_user_model()


# Re-register UserAdmin
# admin.site.unregister(User)
class CustomUserAdmin(UserAdmin):
    """Admin panel for managing custom user model."""

    model = MadbotUser
    list_display = ["username", "first_name", "last_name", "email", "is_approved"]
    list_filter = ("is_approved", "is_active")
    search_fields = ("username", "first_name", "last_name", "email")
    actions = ["approve_users"]

    def approve_users(self, request, queryset):
        """
        Approve selected users.

        This action iterates through the selected users in the queryset,
        sets their "is_approved" field to True, and saves the changes to
        ensure the `post_save` signal is triggered. It also avoids
        unnecessary operations by skipping users who are already approved.

        Args:
        ----
            request: The current HTTP request object.
            queryset: The queryset of selected users to approve.

        Side Effects:
            - Updates the "is_approved" status of each user in the queryset.
            - Triggers the `post_save` signal for each approved user.
            - Displays a success message in the admin interface with the
            count of users approved.

        Returns:
        -------
            None

        """
        count = 0

        # We are obliged to go through a loop because the update approach does not
        # trigger a signal (necessary to send the email to indicate that the user has
        # been approved).

        for user in queryset:
            if not user.is_approved:
                user.is_approved = True
                user.save()
                count += 1

        self.message_user(request, f"{count} user(s) have been successfully approved.")

    approve_users.short_description = "Approve selected users"


class ViewOnSiteModelAdmin(admin.ModelAdmin):
    """Base model admin class that includes media customizations."""

    class Media:
        """Media customization for the admin panel."""

        css = {
            "all": (
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
            ),
        }


class DataLinkAdmin(admin.ModelAdmin):
    """Admin panel for managing DataLink objects."""

    def data_name(self, obj):
        """Return the name of the associated data."""
        return obj.data.name

    list_display = ["id", "data_name", "node"]


class DataAdmin(admin.ModelAdmin):
    """Admin panel for managing Data objects."""

    def connection_name(self, obj):
        """Return the name of the associated connection."""
        return obj.connection.name

    list_display = ["id", "name", "connection_name"]


class DataAssociationObjectsInline(admin.TabularInline):
    """Tabular Inline to display ManyToMany relation with Data for the DataAssociation model."""

    model = DataAssociation.data_objects.through
    extra = 0


class DataAssociationAdmin(admin.ModelAdmin):
    """Admin panel for managing Data Association objects."""

    @admin.display
    def display_data_objects(self, obj):
        """Create hyperlinks for each associated data objects for display."""
        return format_html_join(
            format_html("<br>"),
            '<a href="{}">{}</a>',
            (
                (
                    reverse("admin:core_data_change", args=[data.id]),
                    " - ".join([str(data.id), data.name]),
                )
                for data in obj.data_objects.all()
            ),
        )

    list_display = ["id", "display_data_objects"]
    inlines = [DataAssociationObjectsInline]


class NodeMemberAdmin(admin.ModelAdmin):
    """Admin panel for managing NodeMember objects."""

    list_display = [
        "user",
        "node",
        "role",
    ]

    search_fields = ["user", "node"]


class NodeTypeAdmin(admin.ModelAdmin):
    """Admin panel for managing NodeType objects."""

    list_display = ["id", "name", "description"]


class NodeAdmin(admin.ModelAdmin):
    """Admin panel for managing Node objects."""

    list_display = [
        "id",
        "title",
        "workspace",
        "parent",
        "last_edited_by",
        "created_by",
        "history",
    ]


class WorkspaceAdmin(admin.ModelAdmin):
    """Admin panel for managing Workspace objects."""

    list_display = [
        "id",
        "slug",
        "name",
        "last_edited_by",
        "created_by",
        "history",
    ]


class WorkspaceMemberAdmin(admin.ModelAdmin):
    """Admin panel for managing WorkspaceMember objects."""

    list_display = [
        "user",
        "workspace",
        "role",
    ]

    search_fields = ["user", "workspace"]


class SampleAdmin(admin.ModelAdmin):
    """Admin panel for managing Sample objects."""

    list_display = [
        "id",
        "title",
        "node",
        "last_edited_by",
        "created_by",
    ]


class SampleBoundDataAdmin(admin.ModelAdmin):
    """Admin panel for managing SampleBoundData objects."""

    list_display = [
        "id",
        "sample",
        "data",
    ]


class MadbotMetadataFieldAdmin(admin.ModelAdmin):
    """Admin panel for managing MadbotMetadataField objects."""

    list_display = [
        "name",
        "slug",
        "data_related",
        "sample_related",
        "node_related",
        "data_association_related",
    ]
    search_fields = ["slug", "name"]


class ConnectorMetadataFieldAdmin(admin.ModelAdmin):
    """Admin panel for managing ConnectorMetadataField objects."""

    list_display = [
        "name",
        "slug",
        "source",
        "data_related",
        "sample_related",
        "node_related",
        "data_association_related",
        "level",
    ]
    search_fields = ["slug", "name", "level"]


class MadbotRawSchemaAdmin(admin.ModelAdmin):
    """Admin panel for managing RawSchema objects."""

    list_display = ["slug", "source", "version"]
    history_list_display = ["version"]
    search_fields = ["slug", "source"]


class ConnectorRawSchemaAdmin(admin.ModelAdmin):
    """Admin panel for managing RawSchema objects."""

    list_display = ["slug", "source", "version"]
    history_list_display = ["version"]
    search_fields = ["slug", "source"]


class MetadataMappingAdmin(admin.ModelAdmin):
    """Admin panel for managing MetadataMapping objects."""

    list_display = [
        "get_madbot_metadata_field_name",
        "get_connector_metadata_field_name",
        "connector",
    ]

    def get_madbot_metadata_field_name(self, obj):
        """Retrieve the 'name' field from the related Madbot metadata field."""
        return obj.madbot_metadata_field.name if obj.madbot_metadata_field else None

    def get_connector_metadata_field_name(self, obj):
        """Retrieve the 'name' field from the related Connector metadata field."""
        return (
            obj.connector_metadata_field.name if obj.connector_metadata_field else None
        )


class MetadataAdmin(SimpleHistoryAdmin):
    """Admin panel for managing Metadata objects with history tracking."""

    def metadata_field_slug(self, obj):
        """Return the slug of the associated metadata field."""
        return obj.field.slug

    @admin.display(boolean=True)
    def data_related(self, obj):
        """Indicate if the metadata is related to data."""
        return obj.data is not None

    @admin.display(boolean=True)
    def sample_related(self, obj):
        """Indicate if the metadata is related to a sample."""
        return obj.sample is not None

    @admin.display(boolean=True)
    def node_related(self, obj):
        """Indicate if the metadata is related to a node."""
        return obj.node is not None

    @admin.display(boolean=True)
    def data_association_related(self, obj):
        """Indicate if the metadata is related to data association."""
        return obj.data is not None

    list_display = [
        "id",
        "metadata_field_slug",
        "value",
        "selected_value",
        "data_related",
        "sample_related",
        "node_related",
        "data_association_related",
    ]


class SubmissionAdmin(admin.ModelAdmin):
    """Admin panel for managing Submission objects."""

    list_display = [
        "title",
        "status",
        "connection",
    ]


class SubmissionSettingAdmin(admin.ModelAdmin):
    """Admin panel for managing Submission objects."""

    list_display = [
        "submission",
        "field",
        "value",
    ]


class SubmissionDataAdmin(admin.ModelAdmin):
    """Admin panel for managing SubmissionData objects."""

    list_display = [
        "id",
        "submission",
        "data",
        "datalink",
    ]


class SubmissionMetadataObjectAdmin(admin.ModelAdmin):
    """Admin panel for managing SubmissionMetadataObject objects."""

    def connector(self, obj):
        """Return the slug of the associated metadata field."""
        return obj.submission.connection.connector.split(".")[-1]

    list_display = ["id", "submission", "type", "connector"]


class SubmissionMetadataAdmin(admin.ModelAdmin):
    """Admin panel for managing SubmissionMetadata objects."""

    @admin.display
    def metadata_object(self, obj):
        """Create hyperlinks for metadata object for display."""
        return format_html(
            '<a href="{}">[{}] - {}</a>',
            reverse(
                "admin:core_submissionmetadataobject_change",
                args=[obj.submission_metadata_object.id],
            ),
            obj.submission_metadata_object.type,
            str(obj.submission_metadata_object.id),
        )

    @admin.display
    def metadata_field(self, obj):
        """Display the field."""
        return format_html(
            '<a href="{}">[{}] - {}</a>',
            reverse(
                "admin:core_connectormetadatafield_change",
                args=[obj.field.id],
            ),
            obj.field.slug,
            str(obj.field.id),
        )

    @admin.display
    def value_content(self, obj):
        """Display the value content from ValueField."""
        return obj.value.value if obj.value else None

    @admin.display
    def source_metadata(self, obj):
        """Display the source metadata from ValueField."""
        if not obj.value or not obj.value.source_metadata:
            return None
        return format_html(
            '<a href="{}">{}</a>',
            reverse(
                "admin:core_metadata_change",
                args=[obj.value.source_metadata.id],
            ),
            str(obj.value.source_metadata),
        )

    @admin.display
    def candidate_values_content(self, obj):
        """Display the candidate values content."""
        return (
            ", ".join(
                str(value_field.value) for value_field in obj.candidate_values.all()
            )
            if obj.candidate_values.exists()
            else None
        )

    list_display = [
        "id",
        "submission",
        "metadata_object",
        "metadata_field",
        "value_content",
        "source_metadata",
        "candidate_values_content",
        "is_valid",
    ]


admin.site.register(MadbotUser, CustomUserAdmin)
admin.site.register(Workspace, WorkspaceAdmin)
admin.site.register(WorkspaceMember, WorkspaceMemberAdmin)
admin.site.register(NodeMember, NodeMemberAdmin)
admin.site.register(NodeType, NodeTypeAdmin)
admin.site.register(Node, NodeAdmin)
admin.site.register(Sample, SampleAdmin)
admin.site.register(SampleBoundData, SampleBoundDataAdmin)
admin.site.register(Connection)
admin.site.register(SharedConnectionParam)
admin.site.register(DataLink, DataLinkAdmin)
admin.site.register(Data, DataAdmin)
admin.site.register(DataAssociation, DataAssociationAdmin)
admin.site.register(DataType)
admin.site.register(DataAccessRight)
admin.site.register(MetadataMapping, MetadataMappingAdmin)
admin.site.register(ConnectorRawSchema, ConnectorRawSchemaAdmin)
admin.site.register(MadbotRawSchema, MadbotRawSchemaAdmin)
admin.site.register(MadbotMetadataField, MadbotMetadataFieldAdmin)
admin.site.register(ConnectorMetadataField, ConnectorMetadataFieldAdmin)
admin.site.register(Metadata, MetadataAdmin)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(SubmissionData, SubmissionDataAdmin)
admin.site.register(SubmissionSettings, SubmissionSettingAdmin)
admin.site.register(SubmissionMember)
admin.site.register(SubmissionMetadataObject, SubmissionMetadataObjectAdmin)
admin.site.register(SubmissionMetadata, SubmissionMetadataAdmin)
admin.site.register(ApplicationUserParam)

# remove the "View site" link in the admin page
admin.site.site_url = ""
