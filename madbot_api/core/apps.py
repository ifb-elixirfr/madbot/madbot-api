import logging

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.dispatch import receiver

from .connector import reload_connectors

logger = logging.getLogger(__name__)

CONNECTORS_LOADED = False


class ApiConfig(AppConfig):
    """Configuration class for the Madbot API core application."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "madbot_api.core"
    label = "core"

    def ready(self):
        """
        Perform startup actions when the application is ready.

        This method is called when the application is fully loaded. It loads connectors
        and registers the Vault health check plugin.
        """
        from health_check.plugins import plugin_dir

        from madbot_api.core.health_check.vault import VaultHealthCheck

        plugin_dir.register(VaultHealthCheck)

        from celery import current_app

        current_app.autodiscover_tasks(["madbot_api.core"], force=True)
        post_migrate.connect(run_get_connectors, sender=self)


@receiver(post_migrate)
def run_get_connectors(sender, **kwargs):
    """
    Execute the reload_connectors function after database migrations.

    This function is a signal receiver that runs after the post_migrate signal
    is sent. It ensures that the reload_connectors function is called to update
    or initialize connector data in the database once all migrations have been applied.

    Args:
    ----
        sender: The sender of the signal (automatically provided by Django).
        **kwargs: Additional keyword arguments provided by the signal.

    """
    global CONNECTORS_LOADED
    if CONNECTORS_LOADED:
        return  # Skip if connectors are already loaded

    # Only execute if this is the first time
    reload_connectors()
    CONNECTORS_LOADED = True
