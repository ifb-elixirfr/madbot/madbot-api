import inspect
import logging
import sys
from abc import ABC, abstractmethod
from importlib import import_module
from typing import Any, Dict, List, Optional
from uuid import UUID, uuid4

from django.conf import settings
from django.utils import timezone
from pydantic import BaseModel, Field, Json, model_validator

from madbot_api.core.views.utils import camelcase_to_snakecase

logger = logging.getLogger(__name__)


## -----------------------------------------------------------------------------
## Connector exceptions
## -----------------------------------------------------------------------------


class ConnectorError(Exception):
    """Base class for exceptions related to connectors."""

    pass


class ConnectorNotFoundError(ConnectorError):
    """Exception raised when a connector is not found."""

    def __init__(self, connector):
        """
        Initialize ConnectorNotFoundError.

        Args:
        ----
            connector (str): The name of the connector that was not found.

        """
        self.code = "connector_not_found"
        self.parameter = "connector"
        self.parameter_value = connector


class AuthenticationError(ConnectorError):
    """Exception raised for authentication errors."""

    def __init__(self):
        """Initialize AuthenticationError."""
        self.code = "invalid_credentials"
        self.parameter = "credentials"


class UnreachableError(ConnectorError):
    """Exception raised when a host is unreachable."""

    def __init__(self, host):
        """
        Initialize UnreachableError.

        Args:
        ----
            host (str): The hostname that is unreachable.

        """
        self.code = "hostname_unreachable"
        self.parameter = "hostname"
        self.parameter_value = host


class DataNotFoundError(ConnectorError):
    """Exception raised when a data object is not found."""

    def __init__(self, dataobject):
        """
        Initialize DataNotFoundError.

        Args:
        ----
            dataobject (str): The name of the data object that was not found.

        """
        self.code = "dataobject_not_found"
        self.parameter = "dataobject"
        self.parameter_value = dataobject


class DataInvalidError(ConnectorError):
    """Exception raised for invalid data objects."""

    def __init__(self, dataobject):
        """
        Initialize DataInvalidError.

        Args:
        ----
            dataobject (str): The name of the invalid data object.

        """
        self.code = "invalid_dataobject"
        self.parameter = "dataobject"
        self.parameter_value = dataobject


class ConnectorPermissionError(ConnectorError):
    """Exception raised for permission errors related to connectors."""

    def __init__(self, reason):
        """
        Initialize ConnectorPermissionError.

        Args:
        ----
            reason (str): The reason for the permission error.

        """
        self.code = "permission_error"
        self.parameter = "connector"
        self.parameter_value = reason


class DownloadDataError(ConnectorError):
    """Exception raised for error during dowload data."""

    def __init__(self, reason):
        """
        Initialize DownloadDataError.

        Args:
        ----
            reason (str): The reason for the permission error.

        """
        self.code = "download_data_error"
        self.parameter = "dataobject"
        self.parameter_value = reason


## -----------------------------------------------------------------------------
## Connector classes
## -----------------------------------------------------------------------------


class Connector(ABC):
    """Abstract base class for all connectors, declaring basic functions of a connector."""

    @classmethod
    def get_id(cls):
        """
        Return the unique identifier of the connector.

        Returns
        -------
            str: Unique identifier in the format 'module.class'.

        """
        return f"{cls.__module__}.{cls.__name__}"

    @classmethod
    @abstractmethod
    def get_name(cls):
        """
        Return the connector name.

        Returns
        -------
            str: Connector name.

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_description(cls):
        """
        Return connector description.

        Returns
        -------
            str: Connector description

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_author(cls):
        """
        Return connector author.

        Returns
        -------
            str: Connector author

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_connection_fields(cls):
        """
        Retrieve the connection fields for the connector.

        Returns
        -------
            tuple[list[ConnectionField], list[ConnectionField]]: A tuple containing:
                - A list of shared `ConnectionField` DTOs.
                - A list of private `ConnectionField` DTOs.

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_logo(cls):
        """
        Return the static path of the connector logo.

        Returns
        -------
            str: Connector logo static path.

        """
        raise NotImplementedError()

    @abstractmethod
    def get_instance_name(self):
        """
        Return the instance name of the connector.

        Returns
        -------
            str: Instance name.

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_color(cls):
        """
        Return the color of the tool.

        Returns
        -------
            str: Connector color.

        """
        raise NotImplementedError()

    def get_shared_parameters(self):
        """
        Return all shared parameters in a cleaned form.

        Returns
        -------
            dict: Shared parameters cleaned.

        """
        raise NotImplementedError()

    @classmethod
    def get_types(cls):
        """
        Return the list of the connector types in snake_case.

        Returns
        -------
            list: list of connector types.

        """
        return [
            camelcase_to_snakecase(parent_class.__name__)
            for parent_class in cls.__bases__
        ]


class SubmissionConnector(Connector):
    """Abstract class defining the minimum API for a submission connector."""

    @classmethod
    @abstractmethod
    def get_connection_documentation(cls):
        """
        Retrieve the documentation URL for the connector.

        This method should be implemented by subclasses to provide
        the appropriate URL that points to the documentation for
        the specific connector.

        Returns
        -------
            str: A string containing the documentation URL.

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_metadata_fields_groups(cls, parent=None):
        """
        Return a list of metadata fields groups.

        This function returns a list of metadata fields groups. Each group is represented as a dictionary with the following keys:
        - "slug": The unique identifier for the group.
        - "name": The name of the group.
        - "description": A description of the group.
        - "groups": A list of child groups, if any.

        If the 'parent' parameter is provided, only the groups that are children of the specified parent group will be returned.

        Returns:
        -------
            list: A list of metadata fields groups.

        Example:
        -------
        [
            {
                "slug": "study",
                "name": "Study",
                "description": "Study metadata field group"
            },
            {
                "slug": "sample",
                "name": "Sample",
                "description": "Sample metadata field group",
                "groups": [
                    {
                        "slug": "erc000010",
                        "name": "ERC000010",
                        "description": "Default checklist"
                    },
                    {
                        "slug": "erc000028",
                        "name": "ERC000028",
                        "description": "Minimum information required for a prokaryotic pathogen sample"
                    }
                ]
            },
            {
                "slug": "experiment",
                "name": "Experiment",
                "description": "Experiment metadata field group"
            },
            {
                "slug": "run",
                "name": "Run",
                "description": "Run metadata field group"
            },
            {
                "slug": "analysis",
                "name": "Analysis",
                "description": "Analysis metadata field group"
            }
        ]

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_settings_fields(cls):
        """
        Retrieve the settings fields for the connector.

        Returns
        -------
            list[SettingField]: A list of SettingField DTOs, each containing a `slug`
            and its corresponding `schema_content`.

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_supported_file_extensions(cls):
        """
        Return a list of supported file extensions for the connector.

        Returning an empty list implies that all file extensions are supported.

        Returns:
        -------
            list: list of file extensions supported by the connector.

        Example:
        -------
            supported fastq files
            ["fastq"]
            All files supported
            []

        """
        raise NotImplementedError()

    @classmethod
    @abstractmethod
    def get_all_metadata_object_types(cls):
        """
        Return a dictionary of metadata object types supported by the connector.

        Returns
        -------
            Dict[str, MetadataObjectType]: Dictionary of metadata object types supported by the connector.

        """
        raise NotImplementedError()

    @abstractmethod
    def create_metadata_objects(self, submission, metadata_objects, data):
        """
        Create metadata objects for a submission.

        This function takes a submission object, a list of already existing metadata objects
        and a list of data objects (as `Data`) and return a list of `MetadataObject` for
        the core to create the corresponding metadata objects.

        Args:
        ----
            submission (Submission): The submission object.
            metadata_objects (List[MetadataObject]): List of already existing metadataObjects.
            data (List[Data]): List of data objects to create metadata objects for.

        Returns:
        -------
            List[MetadataObject]: List of metadata objects to create.

        """
        raise NotImplementedError()


class DataConnector(Connector):
    """Parent class for data management connector, declares the minimum functions to map a data to madbot."""

    @staticmethod
    @abstractmethod
    def get_root_name():
        """
        Return the name of the root data object.

        Returns
        -------
            str: Name of the root data object.

        """
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def get_root_id():
        """
        Return the unique identifier of the root data object.

        Returns
        -------
            str: Unique identifier of the root data object.

        """
        raise NotImplementedError()

    @abstractmethod
    def get_data_objects(self, parent_id):
        """
        Return list of all mappable objects from a given parent.

        Args:
        ----
            parent_id (str): ID of the parent object.

        Returns:
        -------
            list: List of all data objects.

        """
        raise NotImplementedError()

    @abstractmethod
    def get_data_object(self, object_id):
        """
        Return the detailed information of a data object.

        Args:
        ----
            object_id (str): ID of the data object.

        Returns:
        -------
            ExternalDataObject: An object containing detailed information about the data object.

        """
        raise NotImplementedError()

    @abstractmethod
    def download_data(self, external_dataobject_id, destination_path):
        """
        Download a data object to local storage and returns an download report.

        It is **strongly recommended** to implement data integrity verification in this method.
        This may include, but is not limited to:
        - Checksum validation (e.g., MD5, SHA256) if available.
        - File size comparison with the source.
        - Any other consistency checks relevant to the data source.

        Implementing integrity checks helps prevent data corruption or incomplete downloads,
        ensuring reliable processing of the imported data.

        Args:
        ----
            external_dataobject_id (str): Unique identifier of the data object in the external system.
            destination_path (str): Local path where the file should be saved.

        Returns:
        -------
            local_file_path (str): Path to the local file.

        Raises:
        ------
            NotImplementedError: This method must be implemented in a subclass.

        """
        raise NotImplementedError()

    # class ISAImporter(DataConnector):
    #     """Parent class for data management connector, declares the minimum
    #     functions to map a data to madbot.
    #     """


## -----------------------------------------------------------------------------
## Settings DTO
## -----------------------------------------------------------------------------


class Settings(BaseModel):
    """DTO for settings validation based on the schema."""

    field: str
    value: Any
    schema_content: Optional[Dict[str, Any]] = None

    @classmethod
    def initialize_setting(cls, field_schema: Dict[str, Any], value: Any):
        """
        Create a Settings from the schema.

        Args:
        ----
          field_schema: A dictionary containing information about a field schema.
          value: The value of the setting.

        Returns:
        -------
          A Settings object is being returned with the specified field, value,
            schema_setting, and level attributes initialized based on the provided
            field_schema dictionary.

        """
        return cls(
            field=field_schema["slug"],
            value=value,
            schema_content=field_schema,
        )


## -----------------------------------------------------------------------------
## Submission Member DTO
## -----------------------------------------------------------------------------


class SubmissionMember(BaseModel):
    """DTO for submission member."""

    first_name: str
    last_name: str
    role: str


## -----------------------------------------------------------------------------
## Submission DTO
## -----------------------------------------------------------------------------


class Submission(BaseModel):
    """DTO for submission."""

    title: str
    status: str
    members: List[SubmissionMember]


## -----------------------------------------------------------------------------
## Submission connector input DTOs
## -----------------------------------------------------------------------------


class Source(BaseModel):
    """Base class for source DTOs."""

    madbot_id: UUID = Field(default_factory=uuid4)


class SourceData(Source):
    """DTO for Madbot source data."""

    name: str


class SourceNode(Source):
    """DTO for Madbot source nodes."""

    madbot_id: UUID = Field(default_factory=uuid4)
    title: str
    description: Optional[str] = None
    parent: Optional["SourceNode"] = None

    @model_validator(mode="before")
    @classmethod
    def from_node(cls, values):
        """Convert a generic node object into a SourceNode."""
        node = values.get("node")
        if node:
            return {
                "madbot_id": node.id,
                "title": node.title,
                "description": node.description,
                "parent": (
                    SourceNode(node=node.parent)
                    if getattr(node, "parent", None)
                    else None
                ),
            }
        return values

    def get_ancestors(self) -> List["SourceNode"]:
        """
        Get all ancestors of this node, from root to the node itself.

        Returns:
            List[SourceNode]: List of ancestors, ordered from root to this node

        """
        ancestors = []
        current = self
        while current:
            ancestors.append(current)
            current = current.parent
        return list(reversed(ancestors))  # Root to node order


class SourceSample(Source):
    """DTO for Madbot source samples."""

    title: str
    alias: str


class MadbotMetadata(BaseModel):
    """DTO for Madbot metadata."""

    field: str
    value: Json[Any]
    source: Source
    madbot_id: UUID = Field(default_factory=uuid4)


class MadbotMetadataObject(BaseModel):
    """DTO representing a metadata object instance."""

    id: UUID = Field(default_factory=uuid4)
    type: str
    metadata: List[MadbotMetadata]


class SourceAssociationData(Source):
    """DTO for Madbot source data associations."""

    source_data: List[SourceData]
    metadata: List[MadbotMetadata] = []


class Data(BaseModel):
    """DTO for Madbot data objects."""

    filename: str
    size: int
    madbot_id: UUID = Field(default_factory=uuid4)
    parent_node: SourceNode
    metadata: List[MadbotMetadata] = []
    data_association: Optional[SourceAssociationData] = None

    @staticmethod
    def find_last_common_ancestor(data_objects: List["Data"]) -> Optional[SourceNode]:
        """
        Find the last common ancestor of multiple data objects.

        Args:
            data_objects: List of Data objects to find common ancestor for

        Returns:
            Optional[SourceNode]: The last common ancestor node, or None if no common ancestor exists

        """
        if not data_objects:
            return None

        # If only one data object, return the parent node
        if len(data_objects) == 1:
            return [data_objects[0].parent_node]

        # group data objects by their root node
        data_objects_by_root = {}
        for data in data_objects:
            ancestors = data.parent_node.get_ancestors()

            root_node = ancestors[0]
            # if the root node is not in the data_objects_by_root, add it
            if str(root_node.madbot_id) not in data_objects_by_root:
                data_objects_by_root[str(root_node.madbot_id)] = {
                    str(node.madbot_id): node for node in ancestors
                }
            # otherwise, intersect the previous ancestors with the new ones
            else:
                previous_ancestors = data_objects_by_root[str(root_node.madbot_id)]
                data_objects_by_root[str(root_node.madbot_id)] = {
                    k: previous_ancestors[k]
                    for k in set.intersection(
                        set(previous_ancestors.keys()),
                        {str(ancestor.madbot_id) for ancestor in ancestors},
                    )
                }

        # Find LCAs across all paths
        lca = []
        for node_tree in data_objects_by_root.values():
            if node_tree:
                # Get the deepest node by comparing the length of ancestors for each node value
                lca.append(
                    max(node_tree.values(), key=lambda node: len(node.get_ancestors()))
                )

        if not lca:
            return None

        return lca


## -----------------------------------------------------------------------------
## Submission connector output DTOs
## -----------------------------------------------------------------------------


class ValueField(BaseModel):
    """DTO for Madbot value fields."""

    value: Optional[Any] = None
    source_metadata_id: Optional[UUID] = None


class SubmissionMetadata(BaseModel):
    """DTO for connector metadata fields."""

    field: str
    value: Optional[ValueField] = None
    candidate_values: List[ValueField]
    is_valid: bool


## -----------------------------------------------------------------------------
## schema connector DTOs
## -----------------------------------------------------------------------------


class SchemaRepresentation(BaseModel):
    """DTO representing a schema."""

    slug: str
    schema_content: dict
    source: str


## -----------------------------------------------------------------------------
## Field connector DTOs
## -----------------------------------------------------------------------------
class FieldRepresentation(BaseModel):
    """DTO representing a metadata field."""

    slug: str
    schema_rep: SchemaRepresentation
    title: str
    description: str
    URL_external: str
    madbot_fields: List[str]
    hidden: bool


class ConnectionField(BaseModel):
    """DTO representing a connection field."""

    slug: str
    schema_content: dict


class SettingField(ConnectionField):
    """DTO representing a connection setting field."""

    pass


class MetadataObjectType(BaseModel):
    """DTO representing a type of metadata object in the submission connector."""

    name: str
    slug: str
    summary_fields: List[str]
    priority: List[str]
    description: str
    minimal_occurrence: int  # minimal number of occurrences required in the submission
    maximal_occurrence: Optional[int] = None  # maximal number of occurrences allowed
    mandatory_fields_schemas: list[FieldRepresentation]
    recommended_fields_schemas: list[FieldRepresentation]
    optional_fields_schemas: list[FieldRepresentation]


class MetadataObject(BaseModel):
    """DTO representing a metadata object instance."""

    id: Optional[UUID] = Field(default_factory=uuid4)
    type: MetadataObjectType
    metadata: List[SubmissionMetadata]
    completeness: Dict[str, Any]
    cross_validation: Dict[str, Any]


class ExternalDataObject:
    """Representation of an external data accessible through a connector."""

    ACCESS_BY_URL = "url"
    ACCESS_BY_SHELL = "shell"
    SUPPORTED_ACCESS_TYPES = [ACCESS_BY_URL, ACCESS_BY_SHELL]

    def __init__(
        self,
        id,
        name,
        type=None,
        url=None,
        description=None,
        media_type=None,
        file_extension=None,
        size=None,
        icon=None,
        has_children=False,
        linkable=False,
        children=[],
    ):
        """
        Initialize ExternalDataObject.

        Args:
        ----
            id (str): Unique identifier for the external data object.
            name (str): Name of the external data object.
            type (str, optional): Type of the external data object. Defaults to None.
            url (str, optional): URL for accessing the external data object. Defaults to None.
            description (str, optional): Description of the external data object. Defaults to None.
            media_type (str, optional): Media type of the external data object. Defaults to None.
            file_extension (str, optional): File extension of the external data object. Defaults to None.
            size (int, optional): Size of the external data object. Defaults to None.
            icon (str, optional): Icon for the external data object. Defaults to None.
            has_children (bool, optional): Indicates if the object has children. Defaults to False.
            linkable (bool, optional): Indicates if the object can be linked. Defaults to False.
            children (list, optional): List of child external data objects. Defaults to None.

        """
        self.id = id
        self.name = name
        self.description = description
        self.type = type
        self.url = url
        self.media_type = media_type
        self.file_extension = file_extension
        self.size = size
        self.icon = icon
        self.has_children = has_children
        self.linkable = linkable
        self.children = children
        self.accesses = {}

    def add_external_access(self, access_type, access):
        """
        Add external access information to the data object.

        Args:
        ----
            access_type (str): The type of access being added (must be one of the supported access types).
            access (dict): The access details to be associated with the specified access type.

        Raises:
        ------
            ValueError: If the provided access_type is not supported.

        """
        if access_type not in ExternalDataObject.SUPPORTED_ACCESS_TYPES:
            raise ValueError("unknown access type")
        self.accesses[access_type] = access


## -----------------------------------------------------------------------------
## Connector utilities
## -----------------------------------------------------------------------------


def find_connectors_in_module(module):
    """
    Find and return all connector classes defined in the specified module.

    Args:
    ----
        module: The module to inspect for connector classes.

    Returns:
    -------
        list: A list of connector classes found in the module.

    """
    connectors = []
    for name, obj in inspect.getmembers(module):
        if (
            inspect.isclass(obj)
            and issubclass(obj, Connector)
            and obj
            not in [
                Connector,
                DataConnector,
                SubmissionConnector,
            ]
        ):
            connectors.append(obj)
        if inspect.ismodule(obj) and module.__name__ in obj.__name__:
            connectors.extend(find_connectors_in_module(obj))

    return connectors


connectors = []


def get_connectors(connector_type=None):
    """
    Retrieve dynamically all implementations of connectors in apps, optionally filtered by connector type.

    Args:
    ----
        connector_type: Optional; the type of connectors to filter
            (DataConnector or SubmissionConnector).

    Returns:
    -------
        list: A list of connectors.

    """
    global connectors

    if len(connectors) == 0:
        for app_module_name in settings.MADBOT_INSTALLED_CONNECTORS:
            connectors_module = None
            connectors_module_name = f"{app_module_name}.connectors"
            if connectors_module_name in sys.modules:
                connectors_module = sys.modules[connectors_module_name]
            else:
                try:
                    connectors_module = import_module(connectors_module_name)
                except ModuleNotFoundError:
                    pass

            if connectors_module:
                connectors.extend(find_connectors_in_module(connectors_module))

    if connector_type is None:
        return connectors
    return [
        connector for connector in connectors if issubclass(connector, connector_type)
    ]


def reload_connectors():
    """Reload all connectors and stores their raw schemas."""
    from madbot_api.core.models import (
        ConnectorMetadataField,
        ConnectorRawSchema,
        MadbotMetadataField,
        MetadataMapping,
    )

    # Recover all available connectors
    connectors = get_connectors()

    for connector in connectors:
        source = connector.get_name().lower()
        if issubclass(connector, SubmissionConnector):
            # For the submission connectors, call the necessary methods
            connection_fields = connector.get_connection_fields()
            setting_fields = connector.get_settings_fields()
            metadata_object_types = connector.get_all_metadata_object_types()

            # Handle connection fields (shared + private)
            for field in connection_fields[0] + connection_fields[1]:
                # Check if the field exists in the database
                try:
                    # Try to fetch the existing schema
                    existing_schema = ConnectorRawSchema.objects.get(
                        slug=field.slug, source=source
                    )
                    # If the schema exists, check if it has changed
                    if existing_schema.schema != field.schema_content:
                        # Just save the updated schema (Simple History will track the versioning)
                        existing_schema.schema = field.schema_content
                        existing_schema.save()

                    # Mark as obsolete if the field is not present anymore
                    if (
                        field not in connection_fields[0]
                        and field not in connection_fields[1]
                    ):
                        existing_schema.obsolete = True
                        existing_schema.save()
                except ConnectorRawSchema.DoesNotExist:
                    # If the schema doesn't exist, create it
                    ConnectorRawSchema.objects.create(
                        slug=field.slug,
                        source=source,
                        schema=field.schema_content,
                        version=timezone.now(),
                    )

            # Handle setting fields similarly
            for field in setting_fields:
                try:
                    existing_schema = ConnectorRawSchema.objects.get(
                        slug=field.slug, source=source
                    )
                    # If the schema exists, check if it has changed
                    if existing_schema.schema != field.schema_content:
                        # Just save the updated schema (Simple History will track the versioning)
                        existing_schema.schema = field.schema_content
                        existing_schema.save()

                    # Mark as obsolete if the field is not present anymore
                    if field not in setting_fields:
                        existing_schema.obsolete = True
                        existing_schema.save()
                except ConnectorRawSchema.DoesNotExist:
                    # If the schema doesn't exist, create it
                    ConnectorRawSchema.objects.create(
                        slug=field.slug,
                        source=source,
                        schema=field.schema_content,
                        version=timezone.now(),
                    )

            # Handle metadata object types
            for metadata_type in metadata_object_types.values():
                all_fields = (
                    metadata_type.mandatory_fields_schemas
                    + metadata_type.recommended_fields_schemas
                    + metadata_type.optional_fields_schemas
                )
                all_fields_slugs = []
                for field in all_fields:
                    all_fields_slugs.append(field.slug)
                    try:
                        existing_schema = ConnectorRawSchema.objects.get(
                            slug=field.slug, source=source
                        )
                        # If the schema exists, check if it has changed
                        if existing_schema.schema != field.schema_rep.schema_content:
                            # Just save the updated schema (Simple History will track the versioning)
                            existing_schema.schema = field.schema_rep.schema_content
                            existing_schema.save()

                        # Mark as obsolete if the field is not present anymore
                        if field not in all_fields:
                            existing_schema.obsolete = True
                            existing_schema.save()
                    except ConnectorRawSchema.DoesNotExist:
                        # If the schema doesn't exist, create it
                        ConnectorRawSchema.objects.create(
                            slug=field.slug,
                            source=source,
                            schema=field.schema_rep.schema_content,
                            version=timezone.now(),
                        )

                    # handle metadata fields
                    try:
                        ConnectorMetadataField.objects.get(
                            slug=field.slug, source=source
                        )
                    except ConnectorMetadataField.DoesNotExist:
                        # If the metadata field doesn't exist, create it
                        ConnectorMetadataField.objects.create(
                            slug=field.slug,
                            source=source,
                            name=field.title,
                            description=field.description,
                            level=field.schema_rep.schema_content.get(
                                "level", "optional"
                            ),
                            schema=ConnectorRawSchema.objects.get(
                                slug=field.slug,
                                source=source,
                            ),
                            data_related=True,
                            sample_related=True,
                            node_related=True,
                            data_association_related=True,
                        )
                    for madbot_field in field.madbot_fields:
                        try:
                            madbot_metadata_field = MadbotMetadataField.objects.get(
                                slug=madbot_field
                            )
                            connector_metadata_field = (
                                ConnectorMetadataField.objects.get(
                                    slug=field.slug, source=source
                                )
                            )
                            # Check if the mapping already exists
                            existing_mapping = MetadataMapping.objects.filter(
                                madbot_metadata_field=madbot_metadata_field,
                                connector_metadata_field=connector_metadata_field,
                                connector=connector.get_name(),
                            ).exists()

                            if not existing_mapping:
                                metadata_mapping_objects = []
                                # Append the MetadataMapping object
                                metadata_mapping_objects.append(
                                    MetadataMapping(
                                        madbot_metadata_field=madbot_metadata_field,
                                        connector_metadata_field=connector_metadata_field,
                                        connector=connector.get_name(),
                                    ),
                                )
                                if metadata_mapping_objects:
                                    MetadataMapping.objects.bulk_create(
                                        metadata_mapping_objects
                                    )

                        except MadbotMetadataField.DoesNotExist:
                            logger.info(
                                "MadbotMetadataField %s not found, skipping...",
                                madbot_field,
                            )

                # Mark obsolete
                ConnectorRawSchema.objects.filter(source=source).exclude(
                    slug__in=all_fields_slugs
                ).update(obsolete=True)

        elif issubclass(connector, DataConnector):
            # For the data connectors, only call necessary methods
            connection_fields = connector.get_connection_fields()

            for field in connection_fields[0] + connection_fields[1]:
                try:
                    # Check if the field exists in the database
                    existing_schema = ConnectorRawSchema.objects.get(
                        slug=field.slug, source=source
                    )
                    # If the schema exists, check if it has changed
                    if existing_schema.schema != field.schema_content:
                        # Just save the updated schema (Simple History will track the versioning)
                        existing_schema.schema = field.schema_content
                        existing_schema.save()
                except ConnectorRawSchema.DoesNotExist:
                    # If the schema doesn't exist, create it
                    ConnectorRawSchema.objects.create(
                        slug=field.slug,
                        source=source,
                        schema=field.schema_content,
                        version=timezone.now(),
                    )


def get_connector_class(connector_id):
    """
    Return the class object of a connector based on its ID.

    Args:
    ----
        connector_id: The unique ID of the connector in the module.class format.

    Returns:
    -------
        class: The class object of the connector.

    Raises:
    ------
        ConnectorNotFoundError: If the connector cannot be found.

    """
    (module_name, class_name) = connector_id.rsplit(".", 1)
    try:
        module = import_module(module_name)
    except ModuleNotFoundError:
        raise ConnectorNotFoundError(module_name)
    try:
        class_ = getattr(module, class_name)
    except AttributeError:
        raise ConnectorNotFoundError(module_name)
    return class_


def get_connector_instance(connector_id, parameters):
    """
    Return a connector instance based on its ID and parameters.

    Args:
    ----
        connector_id: The unique ID of the connector in the module.class format.
        parameters: A dict of shared and private parameters together.

    Returns:
    -------
        Connector: The instance of the connector.

    """
    connector = get_connector_class(connector_id)
    return connector(parameters)


# def get_connector(parameters, tool):
#     """
#     Return instance of the connector corresponding to the given tool instance
#     """
#     connector_class = get_connector_class(tool.connector)
#     return connector_class(parameters)


# def set_connector(self, tool, user):
#     """
#     The function sets the connector for a given tool and user.

#     :param tool: The "tool" parameter is the name or identifier of the tool or software that the user
#     wants to connect to
#     :param user: The "user" parameter refers to the user who is using the tool. It could be the username
#     or any other identifier that uniquely identifies the user
#     """
#     parameters = {}
#     connector_class = self.get_connector_class(tool.connector)
#     connector_param = connector_class.get_connection_param()
#     for param in connector_param:
#         if param["access"] == "public":
#             parameters[param["id"]] = tool.get_public_param(param["id"])
#         elif param["access"] == "private":
#             parameters[param["id"]] = tool.get_private_param(key=param["id"], user=user)
#     return self.get_connector(parameters, tool)
