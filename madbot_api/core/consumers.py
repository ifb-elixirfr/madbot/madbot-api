import json

from channels.exceptions import StopConsumer
from channels.generic.websocket import AsyncWebsocketConsumer


class NotificationConsumer(AsyncWebsocketConsumer):
    """Handle WebSocket connections for notifications."""

    async def connect(self):
        """Check user permissions and adds the user to a specific group for notifications."""
        # Check Auth user permission
        if self.scope["user"].is_anonymous:
            await self.channel_layer.group_discard(
                "notifications_anonymous",
                self.channel_name,
            )
        # Join investigation group
        else:
            self.username = self.scope["user"].username
            self.notifications_identifier = f"notifications_{self.username}"
            await self.channel_layer.group_add(
                self.notifications_identifier,
                self.channel_name,
            )
            await self.accept()

    async def disconnect(self, close_code):
        """
        Remove the current channel from the investigation group.

        This method is called when a WebSocket connection is closed, allowing
        the current channel to leave the associated investigation group.

        Args:
        ----
            close_code: The reason for the disconnection, typically an error
            code or message indicating why the connection is being closed.

        """
        # Leave investigation group
        await self.channel_layer.group_discard(
            self.notifications_identifier,
            self.channel_name,
        )
        raise StopConsumer()

    async def notification_message(self, event):
        """
        Send a message to a WebSocket.

        This method handles sending messages to the WebSocket when an event
        is triggered.

        Args:
        ----
            event: A dictionary containing event information, expected to include
            a "message" key with the message data and a "source" key indicating
            the source of the message.

        """
        message = event["message"]
        source = event["source"]
        # Send message to WebSocket
        await self.send(text_data=json.dumps({"source": source, "data": message}))


class WorkspaceConsumer(AsyncWebsocketConsumer):
    """Handle WebSocket connections for workspaces."""

    async def connect(self):
        """Check the authentication and membership status of a user and add them to a workspace group if they meet the requirements."""
        # Check Auth user permission
        if self.scope["user"].is_anonymous:
            await self.close()  # Close connection for anonymous users
        else:
            self.username = self.scope["user"].username
            self.workspace_identifier = f"workspace_{self.username}"
            await self.channel_layer.group_add(
                self.workspace_identifier,
                self.channel_name,
            )
            await self.accept()

    async def disconnect(self, close_code):
        """
        Remove the current channel from the workspace group.

        This method is called when a WebSocket connection is closed, allowing
        the current channel to leave the associated workspace group.

        Args:
        ----
            close_code: The reason for the disconnection, typically an error
            code or message indicating why the connection is being closed.

        """
        # Leave workspace group
        await self.channel_layer.group_discard(
            self.workspace_identifier,
            self.channel_name,
        )
        raise StopConsumer()

    async def workspace_message(self, event):
        """
        Send a message to a WebSocket.

        This method handles sending messages to the WebSocket when an event
        is triggered.

        Args:
        ----
            event: A dictionary containing event information, expected to include
            a "message" key with the message data and a "source" key indicating
            the source of the message.

        """
        message = event["message"]
        source = event["source"]
        # Send message to WebSocket
        await self.send(text_data=json.dumps({"source": source, "data": message}))


class SubmissionConsumer(AsyncWebsocketConsumer):
    """Handle WebSocket connections for submissions."""

    async def connect(self):
        """Check the authentication and membership status of a user and add them to a submission group if they meet the requirements."""
        # Check Auth user permission
        if self.scope["user"].is_anonymous:
            await self.close()  # Close connection for anonymous users
        else:
            self.username = self.scope["user"].username
            self.submission_id = self.scope["url_route"]["kwargs"]["submission_id"]
            self.submission_identifier = f"submission_{self.submission_id}"
            await self.channel_layer.group_add(
                self.submission_identifier,
                self.channel_name,
            )
            await self.accept()

    async def disconnect(self, close_code):
        """Remove the current channel from the submission group."""
        await self.channel_layer.group_discard(
            self.submission_identifier,
            self.channel_name,
        )
        raise StopConsumer()

    async def submission_message(self, event):
        """Send a message to a WebSocket."""
        message = event["message"]
        source = event.get("source", "submission")
        await self.send(text_data=json.dumps({"source": source, "data": message}))
