from madbot_api.core.connector import ConnectorError
from madbot_api.core.views.utils.error import parse_serializer_error


class CustomException(Exception):
    """Base class for exceptions related to custom exceptions."""

    pass


class ValidationException(CustomException):
    """
    Exception class for validation errors.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception.
        code_message_mapping (dict): A mapping of error codes to error messages.
        details_mapping (dict): A mapping of details codes to details messages.

    Methods
    -------
        __init__: Initializes a new instance of the ValidationException class.
        get_details: Returns the details information for the exception.
        get_message: Returns the error message for the exception.

    """

    status = 400
    code_message_mapping = {
        "validation_error": "Validation error occurred. Check the `details` property for more details.",
        "header_validation_error": "Invalid header. Check the `details` property for more details.",
        "uri_validation_error": "Invalid URI parameters. Check the `details` property for more details.",
        "query_validation_error": "Invalid query parameters. Check the `details` property for more details.",
        "body_validation_error": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
    }

    details_mapping = {
        "not_supported": "This field is not supported.",
        "required": "This field is required.",
        "does_not_exist": '"{parameter_value}" does not exist.',
        "invalid": 'Invalid value "{parameter_value}".',
        "invalid_format": "Invalid format, expected {parameter_value}.",
        "invalid_uuid": '"{parameter_value}" is not a valid UUID.',
        "invalid_choice": '"{parameter_value}" is not a valid choice.',
        "invalid_value_on_schema": '"{parameter_value}" value does not match the schema.',
        "schema_not_found": 'No available schema for "{parameter_value}".',
    }

    def __init__(
        self,
        code=None,
        details_code=None,
        parameter=None,
        parameter_value=None,
        details=None,
        initial_data=None,
        *args,
    ):
        """
        Initialize a new instance of the ValidationException class.

        Args:
        ----
        code (str): The error code associated with the exception.
        details_code (str): The details code associated with the exception.
            This will automatically fill the details field with the appropriate message.
        parameter (str): The parameter associated with the exception.
        parameter_value (str): The value of the parameter associated with the exception.
        details (list/dict/ErrorDetail): The detailed information associated with the exception.
            Either provide a `details_code` for the details to be automatically filled
            or a complete `details` that will be parsed to fit into the specifications.
        initial_data (dict): The initial data passed to the serializer, used for error parsing.
            This can be helpful for providing context about the input that caused the validation error.
        *args: Additional arguments to be passed to the base Exception class.

        """
        self.code = code if code else "validation_error"
        self.message = self.get_message()

        self.details_code = details_code
        self.parameter = parameter
        self.parameter_value = parameter_value

        if details:
            self.details = parse_serializer_error(
                details=details,
                initial_data=initial_data,
                details_mapping=self.details_mapping,
            )
        else:
            self.details = self.get_details()

        super().__init__(self.message, self.details, *args)

    def get_details(self):
        """
        Return the details information for the exception.

        Returns
        -------
            dict: A dictionary containing the details' code, parameter, and message.

        """
        details_code = self.details_code

        details_message = self.details_mapping.get(details_code, "")
        details_message = details_message.format(
            parameter=self.parameter,
            parameter_value=self.parameter_value,
        )

        if details_code in ["invalid_uuid", "invalid_format"]:
            details_code = "invalid"

        return [
            {
                "parameter": self.parameter,
                "code": details_code,
                "message": details_message,
            },
        ]

    def get_message(self):
        """
        Return the error message for the exception.

        Returns
        -------
            str: The error message.

        """
        return self.code_message_mapping.get(self.code, "Unknown error")


class SaveException(CustomException):
    """
    Exception class for error while saving data.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception.
        code (str): The error code associated with the exception.
        message (str): The error message associated with the exception.
        details (dict): The detailed error information associated with the exception.

    """

    status = 400
    code = "save_validation_error"
    message = "The request body data are incorrect. Check the `details` property for more details."
    details_code = "invalid"

    def __init__(self, details, *args):
        """
        Initialize a new instance of the SaveException class.

        Args:
        ----
            details (dict): A dictionary containing the details parameter and message.
            *args: Additional arguments to be passed to the base Exception class.

        Raises:
        ------
            ValueError: If the details dictionary does not contain the required keys.

        """
        # assert the details dictionary contains all needed keys
        if not all([key in details for key in ["message", "parameter"]]):
            raise ValueError(
                "details dictionary must contain keys message and parameter",
            )
        self.parameter = details.get("parameter")
        self.details_message = details.get("message")

        self.details = self.get_details()

        super().__init__(self.message, self.details, *args)

    def get_details(self):
        """
        Return the details information for the exception.

        Returns
        -------
            dict: A dictionary containing the details code, parameter, and message.

        """
        return [
            {
                "parameter": self.parameter,
                "code": self.details_code,
                "message": self.details_message,
            },
        ]


class UnauthorizedException(CustomException):
    """
    Exception raised when an API token is invalid.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception (401).
        code (str): The error code associated with the exception ("unauthorized").
        message (str): The error message associated with the exception ("API token is invalid.").
        details (list): Additional details about the exception (empty list by default).

    """

    status = 401
    code = "unauthorized"
    message = "Access to the requested resource requires valid authentication. Check the `details` property for more details."

    def __init__(self, details=None, *args):
        """
        Initialize a new instance of the UnauthorizedException class.

        Args:
        ----
            details (dict): Additional details about the error. Defaults to None.
            *args: Variable length argument list.

        """
        self.details = [details]
        super().__init__(self.message, self.details, *args)


class ForbiddenException(CustomException):
    """
    Exception raised when a user does not have permission to perform a certain operation.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception (403).
        code (str): The error code associated with the exception ("forbidden").
        message (str): The error message associated with the exception.
        parameter (str): The parameter associated with the exception.
        details_code (str): The details code associated with the exception.
        details_message (str): The details message associated with the exception.
        details (dict): A dictionary containing the details code, parameter, and message.

    """

    status = 403
    code = "forbidden"
    message = "You do not have permission to perform this operation. Check the `details` property for more details."

    def __init__(self, details, *args):
        """
        Initialize a new instance of the ForbiddenException class.

        Args:
        ----
            details (dict): A dictionary containing the details code, parameter, and message.
            *args: Additional arguments to be passed to the base Exception class.

        Raises:
        ------
            ValueError: If the details dictionary does not contain the required keys.

        """
        # assert the details dictionary contains all needed keys
        if not all([key in details for key in ["code", "message", "parameter"]]):
            raise ValueError(
                "details dictionary must contain keys code, message and parameter",
            )
        self.parameter = details.get("parameter")
        self.details_code = details.get("code")
        self.details_message = details.get("message")

        self.details = self.get_details()

        super().__init__(self.message, self.details, *args)

    def get_details(self):
        """
        Return the details information for the exception.

        Returns
        -------
            dict: A dictionary containing the details code, parameter, and message.

        Raises
        ------
            ValueError: If the details code is not one of ["not_allowed", "permission_denied"].

        """
        if self.details_code not in ["not_allowed", "permission_denied"]:
            raise ValueError(
                "details_code must be one of [not_allowed, permission_denied]",
            )

        return [
            {
                "parameter": self.parameter,
                "code": self.details_code,
                "message": self.details_message,
            },
        ]


class ConnectorException(CustomException):
    """
    Exception class for errors related to connectors.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception.
        code (str): The error code associated with the exception.
        message (str): The error message associated with the exception.
        details_mapping (dict): A mapping of details codes to details messages.
        details_code (str): The details code associated with the exception.
        parameter (str): The parameter associated with the exception.
        parameter_value (str): The value of the parameter associated with the exception.
        details (dict): A dictionary containing the details code, parameter, and message.

    """

    status = 400
    code = "connector_error"
    message = "An error occurred in the connector. Check the `details` property for more details."

    details_mapping = {
        "connector_not_found": "Connector `{parameter_value}` not found.",
        "hostname_unreachable": "Hostname ({parameter_value}) is unreachable.",
        "invalid_credentials": "Invalid username, password or token.",
        "expired_connection": "Authentication parameters must be updated.",
        "dataobject_not_found": "Could not find dataobject: {parameter_value}.",
        "invalid_dataobject": "Invalid dataobject `{parameter_value}`.",
        "permission_error": "Permission error: {parameter_value}.",
        "invalid_connector_type": "Invalid connector type: {parameter_value}.",
    }

    def __init__(
        self,
        error=None,
        details_code=None,
        parameter=None,
        parameter_value=None,
        *args,
    ):
        """
        Initialize a new instance of the ConnectorException class.

        Args:
        ----
            error (ConnectorError): The error object associated with the exception.
            details_code (str): The code representing the specific details of the exception.
            parameter (str): The parameter associated with the exception.
            parameter_value (str): The value of the parameter associated with the exception.
            *args: Additional arguments passed to the base Exception class.

        """
        if error and isinstance(error, ConnectorError):
            self.details_code = error.code
            self.parameter = error.parameter
            if getattr(error, "parameter_value", None):
                self.parameter_value = error.parameter_value
            else:
                self.parameter_value = parameter_value
        else:
            self.details_code = details_code
            self.parameter = parameter
            self.parameter_value = parameter_value

        self.details = self.get_details()

        super().__init__(self.message, self.details, *args)

    def get_details(self):
        """
        Return the details information for the exception.

        Returns
        -------
            dict: A dictionary containing the details' code, parameter, and message.

        """
        details_message = self.details_mapping.get(self.details_code, "")
        details_message = details_message.format(
            parameter=self.parameter,
            parameter_value=self.parameter_value,
        )

        return [
            {
                "parameter": self.parameter,
                "code": self.details_code,
                "message": details_message,
            },
        ]


class ObjectNotFoundException(CustomException):
    """
    Exception raised when an object is not found.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception.
        code (str): The error code associated with the exception.
        message (str): The error message associated with the exception.
        details_mapping (dict): A mapping of details codes to details messages.
        parameter (str): The parameter associated with the exception.
        parameter_value (str): The value of the parameter associated with the exception.
        details (dict): A dictionary containing the details code, parameter, and message.

    """

    status = 404
    code = "object_not_found"
    message = "Object not found. Check the `details` property for more details."
    details_mapping = {
        "does_not_exist": '"{parameter_value}" does not exist.',
    }

    def __init__(self, parameter, parameter_value, *args):
        """
        Initialize a new instance of the ObjectNotFoundException class.

        Args:
        ----
            parameter (str): The parameter associated with the exception.
            parameter_value (str): The value of the parameter associated with the exception.
            *args: Additional arguments to be passed to the base Exception class.

        """
        self.parameter = parameter
        self.parameter_value = parameter_value
        self.details = self.get_details()

        super().__init__(self.message, self.details, *args)

    def get_details(self):
        """
        Return the details information for the exception.

        Returns
        -------
            dict: A dictionary containing the details code, parameter, and message.

        """
        details_message = self.details_mapping.get("does_not_exist", "")
        details_message = details_message.format(parameter_value=self.parameter_value)
        return [
            {
                "parameter": self.parameter,
                "code": "does_not_exist",
                "message": details_message,
            },
        ]


class MethodNotAllowedException(CustomException):
    """
    Exception raised when an unsupported request is encountered.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception.
        code (str): The error code associated with the exception.
        message (str): The error message associated with the exception.
        details (list): Additional details about the exception.

    """

    status = 405
    code = "unsupported_method"
    message = "Method `{method}` not allowed."
    details = []

    def __init__(self, method, *args):
        """
        Initialize a new instance of the MethodNotAllowedException class.

        Args:
        ----
            method (str): The method associated with the exception.
            *args: Additional arguments to be passed to the base Exception class.

        """
        self.message = self.message.format(method=method)
        super().__init__(self.message, *args)


class InternalServerErrorException(CustomException):
    """
    Exception class for internal server errors.

    Attributes
    ----------
        status (int): The HTTP status code for the error (default: 500).
        code (str): The error code for the exception (default: "internal_server_error").
        message (str): The error message for the exception (default: "Unexpected error occurred.").
        details (list): Additional details about the error (default: []).

    """

    status = 500
    code = "internal_server_error"
    message = (
        "An unexpected error occurred. Check the `details` property for more details."
    )
    details_message = "The unique identifier for the error is {id}. Contact your system administrator with this identifier for more information."

    def __init__(self, id=None, *args):
        """
        Initialize a new instance of the InternalServerErrorException class.

        Args:
        ----
            id (str): The unique identifier for the error.
            *args: Variable length argument list.

        """
        self.details = [{"message": self.details_message.format(id=id)} if id else None]
        super().__init__(self.message, self.details, *args)


class ServiceUnavailableException(CustomException):
    """
    Exception raised when the Madbot service is unavailable.

    Attributes
    ----------
        status (int): The HTTP status code associated with the exception (503).
        code (str): The error code for the exception ("service_unavailable").
        message (str): The error message for the exception ("Madbot is unavailable, please try again later.").
        details (list): Additional details about the exception (empty list by default).

    """

    status = 503
    code = "service_unavailable"
    message = "Madbot is unavailable, please try again later."
    details = []


class GatewayTimeoutException(CustomException):
    """
    Class representing a Gateway Timeout Exception.

    Attributes
    ----------
        status (int): HTTP status code for Gateway Timeout error, which is 504.
        code (str): Error code for Gateway Timeout exception, which is 'gateway_timeout'.
        message (str): Error message for Gateway Timeout exception, which is 'Gateway Timeout'.
        details (list): An empty list to store additional error details if any.

    """

    status = 504
    code = "gateway_timeout"
    message = "Gateway Timeout"
    details = []
