import logging

import hvac
from health_check.backends import BaseHealthCheckBackend
from hvac.exceptions import VaultDown, VaultError
from requests.exceptions import ConnectionError

from madbot_api import settings

logger = logging.getLogger(__name__)


class VaultHealthCheck(BaseHealthCheckBackend):
    """
    A health check backend to verify the status of a Vault instance using the hvac client.

    This health check verifies whether the Vault is sealed, unsealed, or uninitialized. If Vault is sealed,
    uninitialized, or if there is a failure to connect, it marks the check as failed. Otherwise,
    it marks the check as successful.
    """

    def check_status(self):
        """Check the status of Vault by retrieving the seal status."""
        client = hvac.Client(url=settings.MADBOT_VAULT_URL)

        try:
            # Use read_seal_status to get the seal state of Vault
            seal_status = client.sys.read_seal_status()

            if seal_status["sealed"]:
                self.fail("Vault is sealed.")
            elif not seal_status["initialized"]:
                self.fail("Vault is not initialized.")
            else:
                self.mark_success()

        except VaultError as e:
            self.fail(f"Vault error: {str(e)}")

        except (VaultDown, ConnectionError):
            self.fail("Connection error: Vault is down.")

        except Exception as e:
            self.fail(f"Unexpected error: {str(e)}")

    def mark_success(self):
        """Mark the health check as successful and logs the success."""
        logger.info("Vault health check passed.")

    def fail(self, message):
        """
        Mark the health check as failed, logs the failure, and adds a filtered error message.

        Args:
        ----
            message (str): A description of the failure reason.

        """
        logger.error("Vault health check failed: ", {message})
        self.add_filtered_error(message)

    def add_filtered_error(self, message):
        """
        Filter out 'unknown error' text from the error message, if present, and adds the error.

        Args:
        ----
            message (str): The error message to filter and add.

        """
        # Remove any occurrence of "unknown error" from the message if it exists
        filtered_message = (
            message.replace("unknown error: ", "")
            if "unknown error" in message.lower()
            else message
        )
        logger.error(filtered_message)
        self.add_error(filtered_message)

    def add_error(self, message):
        """
        Fully overrides the parent class's add_error method to prevent 'unknown error' from being appended.

        Args:
        ----
            message (str): The filtered error message to add.

        """
        # Directly handle adding the error without calling the parent class's method
        self.errors.append(message)
