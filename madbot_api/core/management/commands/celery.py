import subprocess  # nosec B404

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import autoreload


def restart_celery():
    """Restart the celery worker."""
    subprocess.run(["pkill", "-f", "celery -A madbot_api worker"], check=False)  # nosec B603 B607 # noqa: S603 S607
    subprocess.run(["celery", "-A", "madbot_api", "worker", "-l", "INFO"], check=False)  # nosec B603 B607 # noqa: S603 S607


class Command(BaseCommand):
    """Call celery worker with autoreload capability."""

    def handle(self, *args, **options):
        """Handle the command."""
        if settings.DEBUG:
            self.stdout.write(
                "Starting celery worker with autoreload for development..."
            )
            autoreload.run_with_reloader(restart_celery)
        else:
            self.stdout.write("Starting celery worker...")
            restart_celery()
