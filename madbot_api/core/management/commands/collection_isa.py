from django.core.management.base import BaseCommand

from madbot_api.core.models import NodeType, NodeTypeCollection


# The below class is a Django management command that creates and configures
# different node types for an ISA metadata framework used in life science,
# environmental, and biomedical experiments.
class Command(BaseCommand):
    """
    Import ISA objects and configure node types for the ISA metadata framework.

    This command sets up node types like Investigation, Study, and Assay for managing
    metadata in life science, environmental, and biomedical experiments.
    """

    help = "Import ISA objects"

    def handle(self, *args, **options):
        """
        Create and configure different node types for an ISA metadata framework.

        This framework is used in life science, environmental, and biomedical experiments.
        """
        (isa_collection, created) = NodeTypeCollection.objects.get_or_create(
            name="isa",
            description="ISA is a metadata framework to manage an increasingly diverse set of life science, environmental and biomedical experiments that employ one or a combination of technologies. Built around the Investigation (the project context), Study (a unit of research) and Assay (analytical measurements) concepts, ISA helps you to provide rich descriptions of experimental metadata (i.e. sample characteristics, technology and measurement types, sample-to-data relationships) so that the resulting data and discoveries are reproducible and reusable.",
        )

        (investigation, created) = NodeType.objects.get_or_create(
            name="investigation",
            description="An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions.",
            icon="mdi-feather",
            collection=isa_collection,
        )

        (study, created) = NodeType.objects.get_or_create(
            name="study",
            description="Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
            icon="mdi-notebook-outline",
            collection=isa_collection,
        )
        study.parent.add(investigation)

        (assay, created) = NodeType.objects.get_or_create(
            name="assay",
            description="An Assay represents a test performed either on material taken from a subject or on a whole initial subject, producing qualitative or quantitative measurements.",
            icon="mdi-test-tube",
            collection=isa_collection,
        )
        assay.parent.add(study)

        self.stdout.write("ISA import completed successfully")
