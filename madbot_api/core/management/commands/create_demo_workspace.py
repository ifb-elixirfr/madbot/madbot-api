import json
import os

from django.core.management.base import BaseCommand
from django.utils.text import slugify

import madbot_api
from madbot_api.core.models import (
    Connection,
    Data,
    DataAssociation,
    DataLink,
    MadbotMetadataField,
    MadbotUser,
    Metadata,
    Node,
    NodeType,
    Sample,
    SampleBoundData,
    SharedConnectionParam,
    Submission,
    SubmissionSettings,
    Workspace,
)


class Command(BaseCommand):
    """
    Create a demo workspace with an investigation, study, assay, samples, and data.

    This command sets up a workspace with an ISA structure, including the creation
    of an investigation, study, assay, and associated samples and data for the given user.
    """

    help = "create a demo that add a workspace, an investigation, a study and an assay with samples and data for the given user"

    def add_arguments(self, parser):
        """
        Add custom arguments to the command parser.

        This method adds the 'username' argument to specify the user for whom the
        demo workspace and related nodes (investigation, study, assay, samples, and data)
        will be created.

        Args:
        ----
        parser (CommandParser): The parser to which the argument is added.

        """
        parser.add_argument(
            "-u",
            "--username",
            type=str,
            required=True,
            help="The username of the main user",
        )

    def handle(self, *args, **options):
        """
        Create a demo workspace with an associated investigation, study, assay, samples, and data.

        This method retrieves demo data, checks if a workspace with the specified name already exists, and if not,
        creates a workspace for the given user. It proceeds to create an investigation, study, assay, and links samples
        and data to the nodes, adding metadata to each where applicable.

        Args:
        ----
        *args (Any): Variable length argument list.
        **options (Any): Arbitrary keyword arguments including the username of the main user.

        """
        self.username = options["username"]

        ## Get demo data
        app_path = os.path.dirname(madbot_api.__file__)
        file_path = os.path.join(
            app_path,
            "core",
            "static",
            "json",
            "demo",
            "BII-I-1.json",
        )

        with open(file_path, "r", encoding="utf-8") as file:
            data = json.load(file)

        request_user = MadbotUser.objects.get(username=self.username)

        # setup the name of the workspace
        workspace_name = "Demo - Yeast Growth Control Systems Biology"

        workspace_slug = slugify(workspace_name)

        # Check if a workspace with the given slug already exists
        workspace_exists = Workspace.objects.filter(
            slug=workspace_slug,
            created_by=request_user,
        ).exists()

        if not workspace_exists:
            # Create a workspace for the provided user
            workspace = Workspace.objects.create(
                slug=workspace_slug,
                created_by=request_user,
                name=workspace_name,
                description="This workspace is dedicated to testing through the study of eukaryote cell growth regulation using a systems biology approach, focusing on yeast as a model organism.",
            )

            self.stdout.write(f"Workspace {workspace_name} created")

            # Create a Node Investigation for the provided user
            investigation = Node.objects.create(
                title=data["title"],
                workspace=workspace,
                type=NodeType.objects.get(name="investigation"),
                description=data["description"],
                created_by=request_user,
                status="active",
            )

            self.stdout.write(f"Investigation '{investigation.title}' created")

            # Retrieve metadata field library name
            library_name_field = MadbotMetadataField.objects.get(
                slug="library_name",
                node_related=True,
            )

            # Add metadata value for library name field in the investigation
            Metadata.objects.create(
                node=investigation,
                field=library_name_field,
                value="Growth Control Systems Biology Study Yeast Library",
                created_by=request_user,
            )

            # Retrieve metadata field Sample cell type
            cell_type_field = MadbotMetadataField.objects.get(
                slug="cell_type",
                node_related=True,
            )

            # Add metadata value for Sample cell type field in the investigation
            Metadata.objects.create(
                node=investigation,
                field=cell_type_field,
                value="eukaryote",
                created_by=request_user,
            )

            # Retrieve metadata field environmental sample
            environmental_sample_field = MadbotMetadataField.objects.get(
                slug="environmental_sample",
                sample_related=True,
                node_related=True,
            )

            # Add metadata value for environmental sample field in the investigation
            Metadata.objects.create(
                node=investigation,
                field=environmental_sample_field,
                value=True,
                created_by=request_user,
            )

            # Create the study
            study = Node.objects.create(
                title=data["studies"][0]["title"],
                parent=investigation,
                workspace=workspace,
                type=NodeType.objects.get(name="study"),
                description=data["studies"][0]["description"],
                created_by=request_user,
            )
            self.stdout.write(f"Study '{study.title}' created")

            # Add metadata value for environmental sample field in the study
            Metadata.objects.create(
                node=study,
                field=environmental_sample_field,
                value=False,
                created_by=request_user,
            )

            # Create the assay
            assay = Node.objects.create(
                title=data["studies"][0]["assays"][0]["measurementType"][
                    "annotationValue"
                ],
                parent=study,
                workspace=workspace,
                type=NodeType.objects.get(name="assay"),
                created_by=request_user,
            )
            self.stdout.write(f"Assay '{assay.title}' created")

            # Retrieve metadata field Library construction protocol
            library_construction_protocol_field = MadbotMetadataField.objects.get(
                slug="library_construction_protocol",
                node_related=True,
            )

            # Add metadata value for Library construction protocol field in the assay
            Metadata.objects.create(
                node=assay,
                field=library_construction_protocol_field,
                value=data["studies"][0]["assays"][0]["measurementType"][
                    "annotationValue"
                ],
                created_by=request_user,
            )

            # Create the connection to Galaxy France
            connection1 = Connection.objects.create(
                name="Galaxy France Demo",
                workspace=workspace,
                connector="madbot_api.connectors.madbot_galaxy.connectors.GalaxyConnector",
                created_by=request_user,
            )

            shared_params = {"url": "https://usegalaxy.fr/"}
            for key, value in shared_params.items():
                SharedConnectionParam.objects.create(
                    connection=connection1,
                    key=key,
                    value=value,
                )

            # Create the first data
            data1 = Data.objects.create(
                name=data["studies"][0]["assays"][0]["dataFiles"][0]["name"],
                connection=connection1,
                workspace=workspace,
                icon="fa-solid fa-file",
                created_by=request_user,
            )

            # Create the datalink to link data to a node study
            datalink1 = DataLink.objects.create(
                node=study,
                data=data1,
                created_by=request_user,
            )
            self.stdout.write(f"Datalink for data {data1.name} created")

            # Create the second data
            data2 = Data.objects.create(
                name=data["studies"][0]["assays"][0]["dataFiles"][6]["name"],
                connection=connection1,
                workspace=workspace,
                icon="fa-solid fa-file",
                created_by=request_user,
            )

            DataLink.objects.create(
                node=assay,
                data=data2,
                created_by=request_user,
            )
            self.stdout.write(f"Datalink for data {data2.name} created")

            # Create a DataAssociation between the two data objects
            data_association = DataAssociation.objects.create(created_by=request_user)
            data_association.data_objects.set([data1, data2])
            self.stdout.write(
                f"Data association between '{data1.name}' and '{data2.name}' created"
            )

            # Create sample in the node study
            sample1 = Sample.objects.create(
                title="sample-E-0.07-aliquot1",
                node=study,
                alias="aliquot1",
                created_by=request_user,
            )
            self.stdout.write(f"Sample {sample1.title} created")

            # Retrieve metadata field Mating type
            mating_type_field = MadbotMetadataField.objects.get(
                slug="mating_type",
                sample_related=True,
            )

            # Add metadata value for Mating type field in the sample1
            Metadata.objects.create(
                sample=sample1,
                field=mating_type_field,
                value="alpha",
                created_by=request_user,
            )

            # Create sample in the node assay
            sample2 = Sample.objects.create(
                title="sample-E-0.07-aliquot2",
                node=investigation,
                alias="aliquot2",
                created_by=request_user,
            )
            self.stdout.write(f"Sample {sample2.title} created")

            # Add metadata value for environmental sample field in the sample2
            Metadata.objects.create(
                sample=sample2,
                field=environmental_sample_field,
                value=True,
                created_by=request_user,
            )

            # Retrieve metadata field collection date
            sample_germline_field = MadbotMetadataField.objects.get(
                slug="germline",
                sample_related=True,
            )

            # Add metadata value for collectin date field in the sample2
            Metadata.objects.create(
                sample=sample2,
                field=sample_germline_field,
                value=True,
                created_by=request_user,
            )

            # Bind sample1 to data1
            SampleBoundData.objects.create(
                sample=sample1,
                data=data1,
                datalink=datalink1,
                created_by=request_user,
            )

            self.stdout.write(
                f"Bond between '{sample1.title}' and '{data1.name}' created",
            )

            # Bind sample2 to data1
            SampleBoundData.objects.create(
                sample=sample2,
                data=data1,
                datalink=datalink1,
                created_by=request_user,
            )
            self.stdout.write(
                f"Bond between '{sample2.title}' and '{data1.name}' created",
            )

            # Create a connection to the ENA Connector
            ena_connection = Connection.objects.create(
                name="ENA Demo",
                workspace=workspace,
                connector="madbot_api.connectors.madbot_ena.connectors.ENAConnector",
                created_by=request_user,
            )
            # Set the shared parameters for ENA connection
            ena_shared_params = {"url": "https://www.ebi.ac.uk/ena"}
            for key, value in ena_shared_params.items():
                SharedConnectionParam.objects.create(
                    connection=ena_connection,
                    key=key,
                    value=value,
                )

            self.stdout.write("ENA connection created.")

            # Create a submission using the ENA connection
            submission = Submission.objects.create(
                title="ENA Submission for Yeast Study",
                status=Submission.STATUS_DRAFT,
                connection=ena_connection,
                created_by=request_user,
                workspace=workspace,
            )

            self.stdout.write(
                f"Submission '{submission.title}' created with ENA connection"
            )

            # Create default SubmissionSettings for the submission
            default_settings = [
                {"field": "checklist", "value": "ERC000011"},
                # Add other default settings here if needed
            ]

            for setting in default_settings:
                SubmissionSettings.objects.create(
                    submission=submission,
                    field=setting["field"],
                    value=setting["value"],
                    created_by=request_user,
                )

            self.stdout.write("Submission settings created.")
            self.stdout.write(f"Successfully created Demo for the user {request_user}")

        else:
            self.stdout.write(
                f"Workspace with slug '{workspace_slug}' already exists. No actions taken.",
            )
