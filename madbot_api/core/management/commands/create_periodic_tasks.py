from django.core.management.base import BaseCommand
from django_celery_beat.models import IntervalSchedule, PeriodicTask


class Command(BaseCommand):
    """
    Command to create periodics task in celery.

    This will delete existing periodic tasks and create new ones.
    """

    help = "Import the metadata mappings from the submission connectors."

    def handle(self, *args, **options):
        """Handle the command execution."""
        PeriodicTask.objects.all().delete()

        ########
        ## Clear all TaskResult objects with a name containing "health_check"
        ########
        # Define Interval
        schedule, created = IntervalSchedule.objects.get_or_create(
            every=1,
            period=IntervalSchedule.HOURS,
        )
        # Create a periodic task
        PeriodicTask.objects.create(
            interval=schedule,
            name="Clear health checks task results",
            task="madbot_api.core.tasks.clear_health_check_results",
        )

        self.stdout.write(
            "Successfully create periodic tasks",
        )
