import csv
import json
import os.path

from django.core.management.base import BaseCommand
from django.utils.timezone import now

import madbot_api
from madbot_api.core.models import (
    MadbotMetadataField,
    MadbotRawSchema,
)


class Command(BaseCommand):
    """
    Create Metadata reference fields and schemas.

    This management command processes metadata schema files and metadata fields,
    updating or creating them in the database as necessary.

    Usage:
        To process all connectors:
        python manage.py initialise_madbot_metadata_fields
    """

    help = "Create Metadata reference fields and schemas"

    def handle(self, *args, **options):
        """
        Execute the main logic for processing metadata schemas and fields.

        This method defines the directory for metadata schemas, processes the schemas,
        and then processes the metadata fields from a CSV file.
        """
        # Define the directory for metadata schemas
        app_path = os.path.dirname(madbot_api.__file__)
        metadata_schema_dir = os.path.join(
            app_path,
            "core",
            "static",
            "json",
            "schemas",
            "referential",
        )

        # Process metadata schemas
        self.process_schemas(metadata_schema_dir)

        # Process metadata fields
        self.process_metadata_fields()

    def process_schemas(self, schema_dir):
        """
        Process metadata schema files from the given directory.

        This method reads JSON files containing metadata schemas, validates them,
        and either updates or creates the corresponding `MadbotRawSchema` entries in the database.

        Args:
        ----
            schema_dir (str): Directory containing the metadata schema files.

        """
        if not os.path.exists(schema_dir):
            self.stderr.write(f"Directory {schema_dir} does not exist.")
            return

        for file_name in os.listdir(schema_dir):
            if file_name.endswith(".json"):
                file_path = os.path.join(schema_dir, file_name)

                with open(file_path, "r", encoding="utf-8") as json_file:
                    try:
                        schema = json.load(json_file)
                        schema_name = file_name[:-5]  # Remove .json extension

                        try:
                            # Try to get the schema if it exists
                            existing_schema = MadbotRawSchema.objects.get(
                                slug=schema_name, source="referential"
                            )
                            # If it exists, check if it needs updating
                            if existing_schema.schema != schema:
                                existing_schema.schema = schema
                                existing_schema.version = now()
                                existing_schema.save()
                                self.stdout.write(
                                    f"Schema {schema_name} (referential) updated."
                                )
                            else:
                                self.stdout.write(
                                    f"Schema {schema_name} (referential) is already up-to-date."
                                )
                        except MadbotRawSchema.DoesNotExist:
                            # If it doesn't exist, create the schema
                            MadbotRawSchema.objects.create(
                                slug=schema_name,
                                source="referential",
                                schema=schema,
                                version=now(),
                            )
                            self.stdout.write(
                                f"Schema {schema_name} (referential) created."
                            )
                    except json.JSONDecodeError as e:
                        self.stderr.write(
                            f"JSON decoding error for file {file_name}: {e}"
                        )

    def process_metadata_fields(self):
        """
        Process metadata fields from the CSV file.

        This method reads metadata field information from a CSV file, looks up the corresponding
        schema, and either updates or creates `MetadataField` entries in the database.
        """
        csv_path = os.path.join(
            os.path.dirname(madbot_api.__file__),
            "core",
            "static",
            "madbot-metadata-fields.csv",
        )

        if not os.path.exists(csv_path):
            self.stderr.write(f"CSV file {csv_path} does not exist.")
            return

        with open(csv_path, mode="r", encoding="utf-8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=";")

            # Skip the header line
            next(csv_reader)

            # Process each row in the CSV file
            for row in csv_reader:
                slug = row[0]
                name = row[1]
                description = row[2]
                related = [item.strip() for item in row[3].split(",")]
                data_related = "data" in related
                node_related = "node" in related
                sample_related = "sample" in related
                data_association_related = "data_association" in related

                try:
                    schema = MadbotRawSchema.objects.get(
                        slug=row[5], source="referential"
                    )
                except MadbotRawSchema.DoesNotExist as e:
                    self.stderr.write(f"Schema not found: {row[5]}")
                    raise e

                lookup_criteria = {"slug": slug}
                update_values = {
                    "name": name,
                    "description": description,
                    "data_related": data_related,
                    "node_related": node_related,
                    "sample_related": sample_related,
                    "data_association_related": data_association_related,
                    "schema": schema,
                    "source": "referential",
                }

                field, created = MadbotMetadataField.objects.update_or_create(
                    defaults=update_values,
                    **lookup_criteria,
                )
                self.stdout.write(f"Field {slug} {'created' if created else 'updated'}")
