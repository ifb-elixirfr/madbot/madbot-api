import json
import os
import sys
from pathlib import Path

import pandas as pd
from django.core.management.base import BaseCommand

BASE_URL = "https://www.ebi.ac.uk/ena/browser/view/"


# that contains the metadata mapping of madbot metadata fields to the metadata fields of a particular
# submission database. It then reads and formats the csv file before and converting it to a json, file.
class Command(BaseCommand):
    """
    Management command to convert a CSV or TSV file containing metadata mappings from Notion to a JSON file.

    This program takes as input a CSV (or TSV) file exported from Notion that contains
    the metadata mapping of madbot metadata fields to the metadata fields of a particular
    submission database. It reads and formats the CSV file before converting it to
    a JSON file.

    Usage:
    -i, --input_file: The input CSV (or TSV) file.
    -o, --output: The output file. If a connector is specified, the output file will be saved in the connector's folder.
    -d, --database: The name of the submission database.
    -c, --connector: The name of the madbot connector which will use the metadata mapping.
                      Must be in the format 'madbot_connectorname'.
    --overwrite: Should the metadata mapping file be overwritten.
    """

    help = (
        "This program take as input a csv (or tsv) file exported from Notion that contains "
        "the metadata mapping of madbot metadata fields to the metadata fields of a particular "
        "submission database. It then reads and formats the csv file before converting it to "
        "a json file."
    )

    def add_arguments(self, parser):
        """
        Add command-line arguments to the parser.

        Args:
        ----
            parser: The argument parser to which the arguments will be added.

        The following arguments are added to the parser:
            - `-i`, `--input_file`: The input CSV or TSV file (required).
            - `-o`, `--output`: The output file. If a connector is specified, the output will be saved in the connector's folder.
            - `-d`, `--database`: The name of the submission database (required).
            - `-c`, `--connector`: The name of the madbot connector to use for metadata mapping.
            - `--overwrite`: A flag to determine if the metadata mapping file should be overwritten.

        """
        parser.add_argument(
            "-i",
            "--input_file",
            type=str,
            required=True,
            help="The input csv (or tsv) file.",
        )
        parser.add_argument(
            "-o",
            "--output",
            type=str,
            help="The output file. If a connector is specified, the output file will be saved in the connector's folder.",
        )
        parser.add_argument(
            "-d",
            "--database",
            type=str,
            required=True,
            help="The name of the submission database.",
        )
        parser.add_argument(
            "-c",
            "--connector",
            type=str,
            help="The name of the madbot connector which will use the metadata mapping. Must be in the format 'madbot_connectorname'",
        )
        parser.add_argument(
            "--overwrite",
            action="store_true",
            default=False,
            help="Should the metadata mapping file be overwritten",
        )

    def handle(self, *args, **options):
        """Handle the command execution."""
        self.input_file = options["input_file"]
        self.output_file = options["output"]
        self.database = options["database"]
        self.connector = options["connector"]
        self.overwrite = options["overwrite"]

        if not os.path.exists(self.input_file):
            self.stdout.write(f"Input file {self.input_file} does not exist")
            sys.exit(0)

        if not (self.input_file.endswith(".csv") or self.input_file.endswith(".tsv")):
            self.stdout.write("Input file must be a csv (or tsv) file")
            sys.exit(0)

        # if a connector is specified
        if self.connector:
            # check that the connector exists
            if not os.path.exists(
                "madbot_api/connectors/" + self.connector + "/connectors.py",
            ):
                self.stdout.write(f"Connector {self.connector} does not exist")
                sys.exit(0)

            if self.output_file and self.output_file.endswith(".json"):
                self.stdout.write(
                    "A connector was provider, output file will be saved in the connector's folder",
                )
                self.output_file = f"madbot_api/connectors/{self.connector}/static/{self.connector}/.{self.output_file}"
            else:
                self.output_file = f"madbot_api/connectors/{self.connector}/static/{self.connector}/metadata_mapping.json"

        elif self.output_file:
            if not self.output_file.endswith(".json"):
                self.stdout.write("Output file must be a json file")
                sys.exit(0)
        else:
            self.output_file = Path(self.input_file).stem.rsplit(".", 1)[0] + ".json"

        if not self.overwrite and os.path.exists(self.output_file):
            self.stdout.write(f"Output file {self.output_file} already exists")
            sys.exit(0)

        self.csv_converter()

        self.stdout.write(
            f"{self.output_file.split('.')[-1].capitalize()} file {self.output_file} created successfully",
        )

    def csv_converter(self):
        """Convert the input CSV file to a json file."""
        # Read the csv file
        df = pd.read_csv(self.input_file)

        # Format the dataframe to a python list of dictionaries
        df, mapping = self.format_df(df)

        # Save the mapping as a json file
        with open(self.output_file, "w") as f:
            json.dump(mapping, f, indent=4)

    def format_df(self, df):
        """
        Format the input DataFrame to a Python list of dictionaries.

        Args:
        ----
            df : pandas.DataFrame
                The input DataFrame containing the metadata mappings.

        Returns:
        -------
            tuple
                A tuple containing:
                    - pandas.DataFrame: The formatted DataFrame.
                    - list: The mapping as a list of dictionaries.

        """
        # see if the database row is present
        if self.database not in df.columns:
            self.stdout.write(
                "Database column not found, available columns are:"
                + ", ".join(df.columns),
            )
            sys.exit(0)

        # drop rows where there is no mapping to the selected database
        df = df[df[self.database].notnull()]

        # drop rows that are not MVP
        df = df[df["MVP"] == "Yes"]

        # if the selected database is ENA, select only the slug, database name, database description and database checklist columns
        if self.database == "ENA":
            df = df[
                [
                    "Slug",
                    self.database,
                    f"{self.database} description",
                    f"{self.database} checklist",
                ]
            ]
            # transform the database checklist column to a database_url column
            # the url should be constructed from the checklist name(s) and the base url
            # the database_checklist column can hold multiple values separated by a comma
            df[f"{self.database} url"] = df[f"{self.database} checklist"].apply(
                self.checklist_to_url,
            )
            # drop the checklist column
            df.drop(columns=[f"{self.database} checklist"], inplace=True)
        # for other databases select only the slug, database name, database description and database url columns
        else:
            df = df[
                [
                    "Slug",
                    self.database,
                    f"{self.database} description",
                    f"{self.database} url",
                ]
            ]

        # format the content of the database column to remove the notion links
        df[self.database] = df[self.database].str.replace(
            r"\s\\(((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z]){2,6}([a-zA-Z0-9\.\&\/\?\:@\-_=#])*\\)",
            "",
            regex=True,
        )

        # convert the dataframe to a list of dictionaries
        mapping = df.to_dict(orient="records")

        json_mapping = []
        for metadata in mapping:
            # if there are mutliple metadata, split them and iterate
            if "," in metadata[self.database]:
                names = metadata[self.database].split(",")

                descriptions = None
                urls = None
                if (
                    not pd.isna(metadata[f"{self.database} description"])
                    and metadata[f"{self.database} description"]
                ):
                    descriptions = metadata[f"{self.database} description"].split(",")
                if (
                    not pd.isna(metadata[f"{self.database} url"])
                    and metadata[f"{self.database} url"]
                ):
                    urls = metadata[f"{self.database} url"].split(",")

                for i, name in enumerate(names):
                    json_mapping.append(
                        {
                            "madbot_field": metadata["Slug"],
                            "external_field": {
                                "name": name.strip(),
                                "description": descriptions[i].strip()
                                if descriptions
                                else "",
                                "url": urls[i].strip() if urls else "",
                            },
                        },
                    )
            else:
                json_mapping.append(
                    {
                        "madbot_field": metadata["Slug"],
                        "external_field": {
                            "name": metadata[self.database],
                            "description": metadata[f"{self.database} description"]
                            if not pd.isna(metadata[f"{self.database} description"])
                            else "",
                            "url": metadata[f"{self.database} url"]
                            if not pd.isna(metadata[f"{self.database} url"])
                            else "",
                        },
                    },
                )

        return df, json_mapping

    def checklist_to_url(self, checklists):
        """
        Convert checklist name(s) into a URL.

        Args:
        ----
            checklists : str
                The checklist name(s), separated by commas.

        Returns:
        -------
            str
                URL(s) constructed from the checklist name(s) and the base URL.

        """
        # check type of the checklists object
        if isinstance(checklists, str):
            # split the checklists string by comma and create a url for each checklist
            return ",".join(
                [
                    f"{BASE_URL}{checklist.strip()}"
                    for checklist in checklists.split(",")
                ],
            )
        return ""
