import json
import logging
import uuid
from pathlib import Path

import jsonschema
import markdown
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.utils.text import slugify
from django_celery_results.models import TaskResult
from referencing import Registry, Resource
from simple_history.models import HistoricalRecords
from tree_queries.models import TreeNode

from madbot_api.core import connector as connectors
from madbot_api.core.errors import SaveException
from madbot_api.core.views.utils.schema_validation import MadbotValidator

# , is_ncbi_taxon
from madbot_api.core.views.utils.vault import Vault

logger = logging.getLogger(__name__)

vault_url = settings.MADBOT_VAULT_URL


class MadbotUser(AbstractUser):
    """Model representing a user in the Madbot system."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    is_approved = models.BooleanField(default=False)
    terms_approved = models.BooleanField(default=False)

    def __str__(self):
        """Return the username of the user."""
        return str(self.username)


class ApplicationUserParam(models.Model):
    """Model representing parameters associated with a user for a specific application object."""

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")
    user = models.ForeignKey(MadbotUser, on_delete=models.CASCADE)
    params = models.JSONField(default=dict, blank=True, null=True)

    class Meta:
        """Meta class for ApplicationUserParam."""

        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]


class NodeTypeCollection(models.Model):
    """Model representing a collection of node types."""

    id = models.SlugField(primary_key=True)
    name = models.CharField(max_length=50, editable=False)
    description = models.TextField(blank=True, null=True)

    def save(self, *args, **kwargs):
        """
        Save the node type collection instance, setting the ID based on the name.

        This method automatically generates the ID for the collection by slugifying
        the name, replacing underscores with dashes.

        :param args: Positional arguments to be passed to the superclass's save method.
        :param kwargs: Keyword arguments to be passed to the superclass's save method.
        """
        self.id = slugify(self.name.replace("_", "-"))
        super(NodeTypeCollection, self).save(*args, **kwargs)


class NodeType(models.Model):
    """Model representing a type of node within a collection."""

    id = models.SlugField(primary_key=True)
    name = models.CharField(max_length=50, editable=False)
    description = models.TextField(blank=True, null=True)
    icon = models.TextField(blank=True, null=True)
    parent = models.ManyToManyField(
        "self",
        symmetrical=False,
        related_name="children",
        blank=True,
        db_index=True,
    )
    collection = models.ForeignKey(
        NodeTypeCollection,
        on_delete=models.CASCADE,
        related_name="types",
        blank=True,
        null=True,
        db_index=True,
        editable=False,
    )

    def save(self, *args, **kwargs):
        """
        Set the ID based on the collection name and node name.

        This method constructs the node ID by slugifying the combination of
        the collection's primary key (if present) and the node name, replacing
        underscores with hyphens. Save the instance after setting the ID.
        """
        collection_name = self.collection.pk + "_" if self.collection else ""
        self.id = slugify(collection_name + self.name.replace("_", "-"))
        super(NodeType, self).save(*args, **kwargs)


class Workspace(models.Model):
    """Model representing a workspace that contains nodes and connections."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    slug = models.SlugField(unique=True, editable=False)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="ws_last_edited_by",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "name",
            "description",
            "created_time",
            "created_by",
        ],
    )

    def save(self, *args, **kwargs):
        """
        Set the slug based on the workspace name.

        If the workspace is being created, generate a slug from the name,
        ensuring its uniqueness by adding a random suffix if needed. If
        the workspace is updated, update the slug if the name has changed.
        Save the instance after setting the slug.
        """
        # Check if the workspace is being created for the first time
        if self._state.adding:
            self.slug = slugify(self.name)
            if Workspace.objects.filter(slug=self.slug).exists():
                # If the generated slug already exists, add a random suffix
                self.slug += "-" + str(self.id)[:8]

            super().save(*args, **kwargs)

            # Add the user as a workspace member with the role "owner"
            WorkspaceMember.objects.create(
                user=self.created_by,
                workspace=self,
                role="owner",
                created_by=self.created_by,
                last_edited_by=self.last_edited_by,
            )
        else:
            # Get the original instance
            original_instance = Workspace.objects.get(pk=self.pk)
            # If the name is changed
            if self.name != original_instance.name:
                self.slug = slugify(self.name)

                if (
                    Workspace.objects.filter(slug=self.slug)
                    .exclude(id=self.id)
                    .exists()
                ):
                    # If the generated slug already exists, add a random suffix
                    self.slug += "-" + str(self.id)[:8]
            super().save(*args, **kwargs)


class WorkspaceMember(models.Model):
    """Model representing a member of a workspace."""

    ROLE_WORKSPACE_MEMBER = (
        ("owner", "owner"),
        ("member", "member"),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(MadbotUser, on_delete=models.CASCADE)
    workspace = models.ForeignKey(
        Workspace,
        on_delete=models.CASCADE,
        blank=True,
        related_name="workspace_members",
    )
    role = models.CharField(max_length=100, choices=ROLE_WORKSPACE_MEMBER)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="workspace_member_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="workspace_member_last_edited_by",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "user",
            "workspace",
            "role",
            "created_time",
            "created_by",
        ],
    )

    def save(self, *args, **kwargs):
        """
        Create or update a workspace member.

        Check if the member is being added to a workspace for the first time.
        If so, ensure the user is not already a member. If the member already
        exists, update their role if it has changed.
        Raise a SaveException if the user is already a member of the workspace.
        """
        if self._state.adding:
            if WorkspaceMember.objects.filter(
                workspace=self.workspace,
                user=self.user,
            ).exists():
                raise SaveException(
                    details={
                        "parameter": "workspace",
                        "message": "User is already a member of this workspace.",
                    },
                )
            # Save the WorkspaceMember first
            super(WorkspaceMember, self).save(*args, **kwargs)

        else:
            # Call the method to update roles when the role is changed
            # Check if the 'role' field has not changed
            if (
                self.pk
                and self.__class__.objects.filter(
                    pk=self.pk,
                    role__exact=self.role,
                ).exists()
            ):
                # Role has not changed, no need to update
                super(WorkspaceMember, self).save(*args, **kwargs)
            else:
                # Role has changed, update the role member
                existing_member = WorkspaceMember.objects.filter(
                    workspace=self.workspace,
                    user=self.user,
                )
                existing_member.update(role=self.role)
                super(WorkspaceMember, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """
        Remove the member from the workspace and all associated nodes.

        Delete the user from all nodes within the workspace before removing
        the workspace member instance.
        """
        # Removes the member from all nodes of the workspace
        nodes_members = NodeMember.objects.filter(
            node__workspace=self.workspace,
            user=self.user,
        )
        nodes_members.delete()
        super(WorkspaceMember, self).delete(*args, **kwargs)


class Node(TreeNode):
    """Model representing a node in a workspace."""

    NODE_STATUS = (
        ("pending", "pending"),
        ("active", "active"),
        ("archived", "archived"),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.ForeignKey(NodeType, on_delete=models.PROTECT)
    title = models.CharField(max_length=1000, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=100, choices=NODE_STATUS, blank=True)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="node_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="node_last_edited_by",
    )
    workspace = models.ForeignKey(
        Workspace,
        on_delete=models.CASCADE,
        blank=True,
        related_name="nodes",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "type",
            "title",
            "description",
            "status",
            "created_time",
            "created_by",
            "parent",
        ],
    )

    def __str__(self):
        """Return the title of the node as a string."""
        return str(self.title)

    def save(self, *args, **kwargs):
        """
        Save the node, ensuring valid parent-child relationships and member assignments.

        This method checks if the node's type is a valid child of its parent type.
        If the node is being created, it also adds the user who created the node
        as an owner and adds owners from the parent node if it exists.
        """
        if self.parent and self.type:
            parent_type = self.parent.type
            if parent_type and self.type not in parent_type.children.all():
                raise SaveException(
                    details={
                        "parameter": "type",
                        "message": "The node type must be a child of the parent type.",
                    },
                )
        # Checks if the node is being created
        if self._state.adding:
            # Save the node first
            super(Node, self).save(*args, **kwargs)

            # Call the method to add a member with the "owner" role
            NodeMember.objects.create(
                user=self.created_by,
                node=self,
                role="owner",
                created_by=self.created_by,
                last_edited_by=self.created_by,
            )

            # If the node has a parent, add owner members from the parent
            if self.parent:
                parent_members = NodeMember.objects.filter(node=self.parent).exclude(
                    user=self.created_by.id,
                )
                parent_members_to_create = [
                    NodeMember(
                        user=member.user,
                        node=self,
                        role=member.role,
                        created_by=self.created_by,
                        last_edited_by=self.created_by,
                    )
                    for member in parent_members
                ]
                # Bulk create the owner members
                NodeMember.objects.bulk_create(parent_members_to_create)
        else:
            super(Node, self).save(*args, **kwargs)

    def get_ancestors(self, include_self=False):
        """
        Return a queryset of the node's ancestors, with an option to include the node itself.

        Args:
        ----
            include_self (bool): Determines whether to include the node itself in the queryset of ancestors.
                                Defaults to False.

        Returns:
        -------
            QuerySet: A queryset of the node's ancestors, including the node itself if specified.

        """
        ancestors = []
        node = self if include_self else self.parent
        while node:
            ancestors.append(node)
            node = node.parent
        return Node.objects.filter(id__in=[ancestor.id for ancestor in ancestors])

    def get_html_description(self):
        """
        Convert the description to HTML format.

        Returns
        -------
            str: HTML formatted description.

        """
        if not self.description:
            return ""
        return markdown.markdown(
            self.description, extensions=["extra", "tables", "fenced_code"]
        )


class NodeMember(models.Model):
    """Model representing a member of a node."""

    ROLE_MEMBER = (
        ("owner", "owner"),
        ("manager", "manager"),
        ("contributor", "contributor"),
        ("collaborator", "collaborator"),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(MadbotUser, on_delete=models.CASCADE)
    node = models.ForeignKey(
        Node,
        on_delete=models.CASCADE,
        related_name="node_members",
    )
    role = models.CharField(max_length=100, choices=ROLE_MEMBER)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="node_member_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="node_member_last_edited_by",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "user",
            "node",
            "role",
            "created_time",
            "created_by",
        ],
    )

    # class Meta:
    #     unique_together = ("user", "node")

    def save(self, *args, **kwargs):
        """
        Save the node member, ensuring unique membership per node and user.

        This method checks if the user is already a member of the node before saving.
        If the user is a new member, it will also add the member to all descendant nodes.
        If the member's role is updated, it updates the roles for all descendant nodes as well.
        """
        if self._state.adding:
            # raise an exception if the user already exists in the node
            if NodeMember.objects.filter(node=self.node, user=self.user).exists():
                raise SaveException(
                    details={
                        "parameter": "node",
                        "message": "User is already a member of this node.",
                    },
                )
            # otherwise, save the member
            # Save the nodeMember first
            super(NodeMember, self).save(*args, **kwargs)
            descendants_nodes = self.node.descendants()
            for node in descendants_nodes:
                existing_member = NodeMember.objects.filter(
                    node=node,
                    user=self.user,
                ).first()
                if not existing_member:
                    NodeMember.objects.create(
                        user=self.user,
                        node=node,
                        role=self.role,
                        created_by=self.created_by,
                        last_edited_by=self.last_edited_by,
                    )
        else:
            # Call the method to update roles for children when the role is changed
            # Check if the 'role' field has not changed
            if (
                self.pk
                and self.__class__.objects.filter(
                    pk=self.pk,
                    role__exact=self.role,
                ).exists()
            ):
                # Role has not changed, no need to update descendants
                super(NodeMember, self).save(*args, **kwargs)
            else:
                # Role has changed, update the role member from all descendant nodes.
                descendants_nodes = self.node.descendants()
                descendants_members = NodeMember.objects.filter(
                    node__in=descendants_nodes,
                    user=self.user,
                )
                descendants_members.update(role=self.role)
                super(NodeMember, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """Delete the node member and remove them from all descendant nodes."""
        # Removes the member from all descendant nodes.
        descendants_nodes = self.node.descendants()
        descendants_members = NodeMember.objects.filter(
            node__in=descendants_nodes,
            user=self.user,
        )
        descendants_members.delete()
        super(NodeMember, self).delete(*args, **kwargs)


class Connection(models.Model):
    """Model representing a connection to an external service."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1000, blank=True, null=True)
    connector = models.CharField(max_length=100, blank=True, null=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    created_time = models.DateTimeField(auto_now_add=True)
    workspace = models.ForeignKey(
        Workspace,
        on_delete=models.CASCADE,
        blank=True,
        related_name="connections",
    )

    def save(self, *args, **kwargs):
        """Save the connection and validate the connector."""
        connectors.get_connector_class(self.connector)
        super().save(*args, **kwargs)

    def get_all_public_param(self):
        """
        Retrieve all public connection parameters.

        Returns
        -------
            QuerySet: A QuerySet of SharedConnectionParam objects associated with the connection.

        """
        return SharedConnectionParam.objects.filter(connection=self.id)

    def get_public_param(self):
        """
        Retrieve public parameter values as a dictionary.

        Returns
        -------
            dict: A dictionary of public parameters where keys are parameter names and values are their values.

        """
        params = SharedConnectionParam.objects.select_related(
            "connection",
            "connection__workspace",
        ).filter(connection_id=str(self.id))
        return {param.key: param.value for param in params}

    def get_private_param(self, user, credential):
        """
        Retrieve a private parameter based on a user and credential.

        Args:
        ----
            user: The user object representing the user making the request. Used to check permissions.
            credential: The credential object used to access the private parameter.

        Returns:
        -------
            dict: The response containing the private parameter values.

        """
        vault = Vault()
        return vault.read_secret(user, credential)

    def get_current_tasks(self):
        """
        Retrieve the list of current active tasks for a given connection.

        Returns
        -------
        list: A list of task IDs (strings) representing the currently active
        tasks for the given connection.

        """
        # Combine both conditions using Q objects
        task_filter = Q(task_kwargs__contains=f"'connection_id': '{str(self.id)}'") & Q(
            task_kwargs__contains=f"'user_id': '{str(self.created_by.id)}'"
        )

        # Filter the TaskResult based on the combined condition
        return list(
            TaskResult.objects.filter(task_filter).values_list("task_id", flat=True)
        )


class SharedConnectionParam(models.Model):
    """Model representing a shared connection parameter."""

    key = models.CharField(max_length=100, null=True)
    value = models.CharField(max_length=100, null=True)
    connection = models.ForeignKey(
        Connection,
        on_delete=models.CASCADE,
        related_name="shared_params",
    )

    def to_dict(self):
        """
        Convert the shared connection parameter to a dictionary.

        Returns
        -------
            dict: A dictionary representation of the shared connection parameter.

        """
        return {self.key: self.value}

    class Meta:
        """Meta class for SharedConnectionParam."""

        constraints = [
            models.UniqueConstraint(
                fields=["connection", "key", "value"],
                name="unique_connection_key_value",
            ),
        ]


class Credential(models.Model):
    """Model representing credentials for a connection."""

    STATUS = (
        ("connected", "connected"),
        ("not_connected", "not_connected"),
        ("failed", "failed"),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    connection = models.ForeignKey(
        Connection,
        on_delete=models.CASCADE,
        related_name="credential",
    )
    user = models.ForeignKey(MadbotUser, on_delete=models.CASCADE)
    created_time = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100, blank=False, null=False, choices=STATUS)
    last_edited_time = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta class for Credential."""

        unique_together = ("connection", "user")


class Data(TreeNode):
    """Model representing data associated with a connection."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    connection = models.ForeignKey(
        Connection,
        on_delete=models.CASCADE,
        related_name="data",
    )
    name = models.CharField(max_length=1000)
    description = models.TextField(blank=True, null=True)
    has_children = models.BooleanField(default=False)
    type = models.CharField(max_length=1000, blank=True, null=True)
    icon = models.CharField(max_length=10000, blank=True, null=True)
    size = models.BigIntegerField(blank=True, null=True)
    media_type = models.CharField(max_length=1000, blank=True, null=True)
    file_extension = models.CharField(max_length=1000, blank=True, null=True)
    external_id = models.CharField(max_length=1000)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    last_update_time = models.DateTimeField(auto_now=True)
    workspace = models.ForeignKey(
        Workspace,
        on_delete=models.CASCADE,
        blank=True,
        related_name="data_objects",
    )
    history = HistoricalRecords(
        excluded_fields=[
            "type",
            "name",
            "connection",
            "media_type",
            "created_time",
            "created_by",
            "parent",
            "data_object_id",
            "external_id",
        ],
    )

    class Meta:
        """Meta class for Data."""

        verbose_name_plural = "Data"


class ExternalAccess(models.Model):
    """Model representing external access permissions for data."""

    type = models.CharField(max_length=1000, blank=True, null=True)
    access = models.CharField(max_length=1000, blank=True, null=True)
    data = models.ForeignKey(
        Data,
        on_delete=models.CASCADE,
        null=True,
        related_name="external_accesses",
    )


class DataAccessRight(models.Model):
    """Model representing data access right."""

    ACCESS_RIGHT = (
        ("accessible", "accessible"),
        ("unaccessible", "unaccessible"),
        ("unreachable", "unreachable"),
        ("unverified", "unverified"),
    )

    user = models.ForeignKey(
        MadbotUser,
        on_delete=models.CASCADE,
    )

    data = models.ForeignKey(
        Data,
        on_delete=models.CASCADE,
        related_name="access_right",
    )

    access_right = models.CharField(max_length=50, choices=ACCESS_RIGHT)
    last_updated_time = models.DateTimeField(auto_now_add=True)
    last_verification_time = models.DateTimeField(auto_now=True)


class DataLink(models.Model):
    """Model representing a link between a node and data."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    node = models.ForeignKey(
        Node,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        db_index=True,
        editable=False,
        related_name="datalinks",
    )

    data = models.ForeignKey(
        Data,
        on_delete=models.CASCADE,
        db_index=True,
        editable=False,
        blank=True,
        null=True,
        related_name="datalinks",
    )

    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="dl_last_edited_by",
    )

    class Meta:
        """Meta class for DataLink."""

        unique_together = ["data", "node"]


class DataAssociation(models.Model):
    """Model representing an association between two data."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data_objects = models.ManyToManyField(Data, related_name="associations")
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="associated_data_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        db_index=True,
        blank=True,
        null=True,
        related_name="associated_data_last_edited_by",
    )


class Sample(models.Model):
    """Model representing a sample associated with a node."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    alias = models.SlugField(blank=False, null=False)
    title = models.CharField(max_length=1000, blank=True, null=True)
    node = models.ForeignKey(
        Node,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        db_index=True,
        editable=False,
        related_name="samples",
    )

    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        db_index=True,
        blank=True,
        null=True,
        related_name="sample_last_edited_by",
    )
    history = HistoricalRecords(
        excluded_fields=[
            "title",
            "node",
            "datalink",
            "created_time",
            "created_by",
        ],
    )

    def save(self, *args, **kwargs):
        """
        Save the sample instance, ensuring the alias is unique within the parent node's tree.

        Raises:
        ------
            SaveException: If the sample alias already exists in the parent node's tree.

        Args:
        ----
            *args: Positional arguments to be passed to the superclass's save method.
            **kwargs: Keyword arguments to be passed to the superclass's save method.

        """
        # Check if alias already exists
        duplicated_samples = Sample.objects.filter(
            alias=self.alias,
            node__workspace=self.node.workspace,
        ).exclude(id=self.id)
        if duplicated_samples.exists():
            # Retrieve root node from the sample object to save
            root_node = self.node.ancestors(include_self=True)[0]
            for duplicated_sample in duplicated_samples:
                # Compare node root of each duplicated sample to node root of the sample to save
                if duplicated_sample.node.ancestors(include_self=True)[0] == root_node:
                    raise SaveException(
                        details={
                            "parameter": "alias",
                            "message": "The sample alias already exists in the parent node's tree.",
                        },
                    )
        super().save(*args, **kwargs)


class SampleBoundData(models.Model):
    """Model representing the binding of sample to data and datalink."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sample = models.ForeignKey(
        Sample,
        on_delete=models.CASCADE,
        related_name="bound_data",
        editable=False,
    )
    data = models.ForeignKey(
        Data,
        on_delete=models.CASCADE,
        related_name="bound_samples",
        editable=False,
    )
    datalink = models.ForeignKey(
        DataLink,
        on_delete=models.CASCADE,
        related_name="bound_samples",
        editable=False,
    )
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        db_index=True,
        blank=True,
        null=True,
        related_name="bound_sample_last_edited_by",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "sample",
            "data",
            "datalink",
            "created_time",
            "created_by",
        ],
    )

    class Meta:
        """Meta class for SampleBoundData."""

        verbose_name_plural = "Sample bound Data"

    def save(self, *args, **kwargs):
        """
        Save the sample bound data instance.

        It ensures that the sample and datalink nodes have a direct parent-child
        relationship and that the data is part of the datalink's hierarchy.

        Raises:
        ------
            SaveException: If the sample and datalink nodes do not have a direct
                            parent-child relationship or if the data is not part
                            of the datalink's hierarchy.

        Args:
        ----
            *args: Positional arguments to be passed to the superclass's save method.
            **kwargs: Keyword arguments to be passed to the superclass's save method.

        """
        if not (
            self.datalink.node.ancestors(include_self=True)
            .filter(id=self.sample.node.id)
            .exists()
        ):
            raise SaveException(
                details={
                    "parameter": "node",
                    "message": "The sample and datalink node must have a direct parent-child relationship.",
                },
            )

        # Check if the data is part of the datalink's data hierarchy
        if (
            not self.datalink.data.descendants(include_self=True)
            .filter(id=self.data.id)
            .exists()
            and not self.data.descendants(include_self=True)
            .filter(id=self.datalink.data.id)
            .exists()
        ):
            raise SaveException(
                details={
                    "parameter": "data",
                    "message": "The data must be part of the datalink's data hierarchy, and vice versa.",
                },
            )

        super().save(*args, **kwargs)


class DataType(models.Model):
    """Model representing data types with associated media types and extensions."""

    name = models.CharField(max_length=255)
    media_types = models.JSONField()
    extensions = models.JSONField()


"""
    Example DataType values:

    - media_types: This should be a list of strings where each string represents a Media type.
      Example: ['image/jpeg', 'image/png', 'application/pdf']

    - extensions: This should be a list of strings where each string represents a file extension.
      Example: ['.jpg', '.jpeg', '.png', '.pdf']

    Example usage:

    Creating a new DataType instance:
    data_type = DataType(
        media_types=['image/jpeg', 'image/png', 'application/pdf'],
        extensions=['.jpg', '.jpeg', '.png', '.pdf'],
        name='Document'
    )
    data_type.save()

    Retrieving and displaying all DataType instances:
    data_types = DataType.objects.all()
    for dt in data_types:
        print(dt.name, dt.media_types, dt.extensions)
    Output:
    Document ['image/jpeg', 'image/png', 'application/pdf'] ['.jpg', '.jpeg', '.png', '.pdf']
    """


class RawSchema(models.Model):
    """Model representing raw schema information."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    slug = models.SlugField(editable=False)
    schema = models.JSONField()
    source = models.CharField(max_length=50)
    version = models.DateTimeField(auto_now_add=True)
    obsolete = models.BooleanField(default=False)

    history = HistoricalRecords(inherit=True)

    class Meta:
        """Meta class for RawSchema."""

        abstract = True
        unique_together = ("slug", "source")

    def __str__(self):
        """Return the slug of the raw schema."""
        return self.slug

    def is_valid_schema(self):
        """Check if the schema is non-empty and well-formatted."""
        if isinstance(self.schema, dict) and self.schema:
            return True

        try:
            jsonschema.Draft201909Validator.check_schema(self.schema)
            return True  # Schema is valid
        except jsonschema.exceptions.SchemaError:
            return False  # Schema is invalid

    def clean(self):
        """Validate schema before saving."""
        if not self.is_valid_schema():
            raise ValidationError(
                f"Invalid JSON schema in '{self.source}' for the field '{self.slug}'"
            )

    def save(self, *args, **kwargs):
        """Override save to ensure schema validation before saving."""
        self.clean()
        super().save(*args, **kwargs)


class ConnectorRawSchema(RawSchema):
    """Model representing raw schemas specific to connectors."""

    pass  # Inherits all fields from RawSchema

    class Meta:
        """Meta class for ConnectorRawSchema."""

        unique_together = ("slug", "source")


class MadbotRawSchema(RawSchema):
    """Model representing raw schemas specific to metadata fields."""

    pass  # Inherits all fields from RawSchema

    class Meta:
        """Meta class for MadbotRawSchema."""

        unique_together = ("slug", "source")


class MetadataField(models.Model):
    """Model representing metadata fields related to raw schemas."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    slug = models.SlugField(editable=False)
    source = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=3000)
    data_related = models.BooleanField(default=False)
    sample_related = models.BooleanField(default=False)
    node_related = models.BooleanField(default=False)
    data_association_related = models.BooleanField(default=False)
    hidden = models.BooleanField(default=False)

    class Meta:
        """Meta class for metadata fields."""

        abstract = True  # make this class abstract (no table in the db)
        unique_together = ("slug", "source")


class ConnectorMetadataField(MetadataField):
    """Metadata field specific to a connector, with field mapping."""

    LEVEL_MANDATORY = "mandatory"
    LEVEL_RECOMMENDED = "recommended"
    LEVEL_OPTIONAL = "optional"
    LEVEL_CHOICES = (
        (LEVEL_MANDATORY, "mandatory"),
        (LEVEL_RECOMMENDED, "recommended"),
        (LEVEL_OPTIONAL, "optional"),
    )

    level = models.CharField(max_length=20, choices=LEVEL_CHOICES, default="optional")
    mapping = models.ManyToManyField(
        "MetadataMapping", related_name="connector_metadata_fields", blank=True
    )
    data_types = models.ManyToManyField(
        DataType,
        related_name="connector_metadata_fields",
        blank=True,
    )
    schema = models.ForeignKey(
        ConnectorRawSchema,
        on_delete=models.CASCADE,
    )


class MadbotMetadataField(MetadataField):
    """Metadata fields managed directly by Madbot (core fields)."""

    data_types = models.ManyToManyField(
        DataType,
        related_name="core_metadata_fields",
        blank=True,
    )
    schema = models.ForeignKey(
        MadbotRawSchema,
        on_delete=models.CASCADE,
    )


class MetadataMapping(models.Model):
    """Model representing mapping of metadata fields to external fields."""

    connector_metadata_field = models.ForeignKey(
        ConnectorMetadataField,
        on_delete=models.CASCADE,
        related_name="metadata_mapping",
        blank=True,
        null=True,
    )
    madbot_metadata_field = models.ForeignKey(
        MadbotMetadataField,
        on_delete=models.CASCADE,
        related_name="metadata_mapping",
        blank=True,
        null=True,
    )
    connector = models.CharField(max_length=100, blank=True, null=True)


class Metadata(models.Model):
    """Model representing metadata associated with data, samples, or nodes."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data = models.ForeignKey(
        Data,
        on_delete=models.CASCADE,
        related_name="metadata",
        blank=True,
        null=True,
    )
    sample = models.ForeignKey(
        Sample,
        on_delete=models.CASCADE,
        related_name="metadata",
        blank=True,
        null=True,
    )
    node = models.ForeignKey(
        Node,
        on_delete=models.CASCADE,
        related_name="metadata",
        blank=True,
        null=True,
    )
    data_association = models.ForeignKey(
        DataAssociation,
        on_delete=models.CASCADE,
        related_name="metadata",
        blank=True,
        null=True,
    )
    field = models.ForeignKey(
        MadbotMetadataField,
        on_delete=models.CASCADE,
        related_name="metadata",
    )
    value = models.JSONField(blank=True, null=True)
    selected_value = models.ForeignKey(
        "self",
        related_name="metadata_selected_value",
        blank=True,
        db_index=True,
        on_delete=models.CASCADE,
        null=True,
    )
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        db_index=True,
        blank=True,
        null=True,
        related_name="metadata_last_edited_by",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "node",
            "sample",
            "data",
            "created_time",
            "created_by",
        ],
    )

    class Meta:
        """Meta class for Metadata."""

        verbose_name_plural = "Metadata"

    def save(self, *args, **kwargs):
        """
        Save the metadata instance, validating associations and values.

        Raises:
        ------
            SaveException: If the metadata does not relate to exactly one of data, sample, or node,
            or if both value and selected_value are None or filled.

        Args:
        ----
            *args: Positional arguments to be passed to the superclass's save method.
            **kwargs: Keyword arguments to be passed to the superclass's save method.

        """
        # Counts the number of non-None values among data, sample, and node
        associated_objects_count = sum(
            bool(x) for x in [self.data, self.sample, self.node, self.data_association]
        )

        # Check if the metadata is associated with no objects or more than one object
        if associated_objects_count == 0:
            raise SaveException(
                details={
                    "parameter": "associated_object",
                    "message": "Metadata must be related to a data, sample, node or data_association.",
                },
            )
        if associated_objects_count > 1:
            raise SaveException(
                details={
                    "parameter": "associated_object",
                    "message": "Metadata can only be related to one object: data, sample, node or data_association.",
                },
            )

        if self.value is None and self.selected_value is None:
            raise SaveException(
                details={
                    "parameter": "value",
                    "message": "Only one of the 'value' or 'selected_value' can be None, not both.",
                },
            )
        if self.value and self.selected_value:
            raise SaveException(
                details={
                    "parameter": "value",
                    "message": "Only one of the 'value' or 'selected_value' must be filled.",
                },
            )
        if self.value:
            # Type_related validation based on MetadataField attributes
            if self.data and not self.field.data_related:
                raise SaveException(
                    details={
                        "parameter": "associated_object",
                        "message": "This Metadata cannot not be related to a Data object. Please check the Metadata field configuration.",
                    },
                )
            if self.sample and not self.field.sample_related:
                raise SaveException(
                    details={
                        "parameter": "associated_object",
                        "message": "This Metadata cannot not be related to a Sample object. Please check the Metadata field configuration.",
                    },
                )
            if self.node and not self.field.node_related:
                raise SaveException(
                    details={
                        "parameter": "associated_object",
                        "message": "This Metadata cannot not be related to a Node object. Please check the Metadata field configuration.",
                    },
                )
            if self.data_association and not self.field.data_association_related:
                raise SaveException(
                    details={
                        "parameter": "associated_object",
                        "message": "This Metadata cannot not be related to a Data Association object. Please check the Metadata field configuration.",
                    },
                )

            schema = Path("madbot_api/core/static/json/schemas/referential")

            def retrieve_from_filesystem(uri: str):
                path = schema / Path(
                    uri.removeprefix("/api/schemas/referential/") + ".json",
                )
                contents = json.loads(path.read_text())
                contents["$schema"] = "https://json-schema.org/draft/2019-09/schema"
                return Resource.from_contents(contents)

            registry = Registry(retrieve=retrieve_from_filesystem)

            try:
                # format_checker = FormatChecker()
                # format_checker.checkers["ncbi-taxon"] = (is_ncbi_taxon, ValueError)

                MadbotValidator(
                    {"$ref": "/api/schemas/referential/" + self.field.schema.slug},
                    registry=registry,
                    # format_checker=format_checker,
                ).validate(self.value)
            except ValidationError as e:
                raise SaveException(
                    details={
                        "parameter": "value",
                        "message": "Validation error: %s" % e.message,
                    },
                )

        super().save(*args, **kwargs)

    def get_associated_object(self):
        """Get the associated object for this metadata."""
        return self.data or self.sample or self.node or self.data_association


class Submission(models.Model):
    """Model representing a submission with associated data and settings."""

    STATUS_DRAFT = "draft"
    STATUS_GENERATING_METADATA = "generating metadata"
    STATUS_READY = "ready"
    STATUS_SUBMISSION_IN_PROGRESS = "submission in progress"
    STATUS_FAILED = "failed"
    STATUS_SUBMITTED = "submitted"
    STATUS_UPDATE_AVAILABLE = "update available"

    SUBMISSION_STATUS = (
        (STATUS_DRAFT, "draft"),
        (STATUS_GENERATING_METADATA, "generating metadata"),
        (STATUS_READY, "ready"),
        (STATUS_SUBMISSION_IN_PROGRESS, "submission in progress"),
        (STATUS_FAILED, "failed"),
        (STATUS_SUBMITTED, "submitted"),
        (STATUS_UPDATE_AVAILABLE, "update available"),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=500, blank=False, null=False)
    status = models.CharField(max_length=100, choices=SUBMISSION_STATUS)
    connection = models.ForeignKey(Connection, on_delete=models.CASCADE)
    _last_failed_async_task = models.JSONField(default=dict, blank=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submissions_created",
    )
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submissions_last_edited",
    )
    created_time = models.DateTimeField(auto_now_add=True)
    last_edited_time = models.DateTimeField(auto_now=True)

    workspace = models.ForeignKey(
        Workspace,
        on_delete=models.CASCADE,
        blank=True,
        related_name="submissions",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "status",
            "title",
            "connection",
            "created_time",
            "created_by",
            "last_failed_async_task",
        ],
    )

    class Meta:
        """Meta class for Submission."""

        verbose_name_plural = "Submissions"

    def __str__(self):
        """Return the title of the submission as its string representation."""
        return self.title

    def check_submission_readiness(self):
        """Check if the submission meets all requirements to be marked as ready. Update the status if so."""
        metadata_objects = self.metadata_objects.all()

        if metadata_objects.exists():
            is_ready = True
            for metadata_object in metadata_objects:
                # Check mandatory completeness
                if (
                    metadata_object.completeness
                    and "mandatory" in metadata_object.completeness
                ):
                    mandatory_data = metadata_object.completeness["mandatory"]
                    if mandatory_data.get("total", 0) != mandatory_data.get(
                        "complete", 0
                    ):
                        is_ready = False
                        break

                # Check cross validation
                if (
                    metadata_object.cross_validation
                    and len(metadata_object.cross_validation) > 0
                ):
                    is_ready = False
                    break

            if is_ready:
                self.status = self.STATUS_READY
            else:
                self.status = self.STATUS_DRAFT
            super(Submission, self).save()

    def save(self, *args, **kwargs):
        """
        Save the submission instance, validating settings and managing statuses.

        If all mandatory metadata fields are complete and have no cross-validation issues,
        the submission status will be updated to 'ready'.

        Raises
        ------
            SaveException: If any setting is invalid or does not match the schema.

        """
        # Check if the submission is being created for the first time
        if self._state.adding:
            super(Submission, self).save(*args, **kwargs)
            SubmissionMember.objects.create(
                user=self.created_by,
                submission=self,
                role="owner",
                created_by=self.created_by,
                last_edited_by=self.created_by,
            )
        else:
            super(Submission, self).save(*args, **kwargs)

        self.check_submission_readiness()

    @property
    def last_failed_async_task(self):
        """
        Get the last failed async task information.

        Returns:
            dict: A dictionary containing task_id and error message, or None if not set

        """
        return self._last_failed_async_task

    @last_failed_async_task.setter
    def last_failed_async_task(self, value):
        """
        Set the last failed async task information.

        Args:
            value: A dictionary containing task_id and error message

        """
        if not isinstance(value, dict):
            raise ValueError("last_failed_async_task must be a dictionary")

        # if the dictionary is empty, reset the property
        if not value:
            self._last_failed_async_task = {}
            return

        if (
            "task_name" not in value
            or "task_id" not in value
            or "error_message" not in value
            or "timestamp" not in value
        ):
            raise ValueError(
                "last_failed_async_task must contain 'task_name', 'task_id', 'error_message' and 'timestamp' keys"
            )

        self._last_failed_async_task = {
            "task_name": value["task_name"],
            "task_id": value["task_id"],
            "error_message": value["error_message"],
            "timestamp": value["timestamp"],
        }


class SubmissionMember(models.Model):
    """Model representing a member of a submission with a specific role."""

    ROLE_SUBMISSION_MEMBER = (
        ("owner", "owner"),
        ("manager", "manager"),
        ("data broker", "data broker"),
        ("contributor", "contributor"),
        ("collaborator", "collaborator"),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    submission = models.ForeignKey(
        Submission,
        on_delete=models.CASCADE,
        related_name="submission_members",
    )
    user = models.ForeignKey(MadbotUser, on_delete=models.CASCADE)
    role = models.CharField(max_length=100, choices=ROLE_SUBMISSION_MEMBER)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submission_members_created",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submission_members_last_edited",
    )

    history = HistoricalRecords(
        excluded_fields=[
            "user",
            "submission",
            "role",
            "created_time",
            "created_by",
            "created_time",
        ],
    )

    class Meta:
        """Meta class for SubmissionMember."""

        verbose_name_plural = "Submission Members"

    def save(self, *args, **kwargs):
        """
        Save the submission member instance, ensuring the user is not already a member.

        Raises:
        ------
            SaveException: If the user is already a member of the submission.

        Args:
        ----
            *args: Positional arguments to be passed to the superclass's save method.
            **kwargs: Keyword arguments to be passed to the superclass's save method.

        """
        # Checks if the submission is being created
        if self._state.adding:
            if SubmissionMember.objects.filter(
                submission=self.submission,
                user=self.user,
            ).exists():
                raise SaveException(
                    details={
                        "parameter": "submission",
                        "message": "User is already a member of this submission.",
                    },
                )
            super(SubmissionMember, self).save(*args, **kwargs)
        else:
            super(SubmissionMember, self).save(*args, **kwargs)


class SubmissionData(models.Model):
    """Model representing data to submit in a submission along with their metadata."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    submission = models.ForeignKey(
        Submission, on_delete=models.CASCADE, related_name="data"
    )
    data = models.ForeignKey(Data, on_delete=models.CASCADE)
    datalink = models.ForeignKey(DataLink, on_delete=models.CASCADE)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="submission_data_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="submission_data_last_edited",
    )

    class Meta:
        """Meta class for SubmissionData."""

        verbose_name_plural = "Submission Data"


class SubmissionMetadataObject(models.Model):
    """Model representing a metadata object in a submission."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    submission = models.ForeignKey(
        Submission, on_delete=models.CASCADE, related_name="metadata_objects"
    )
    type = models.CharField(max_length=500, blank=False, null=False)
    completeness = models.JSONField(blank=True, null=True)
    cross_validation = models.JSONField(blank=True, null=True)
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="submission_metadata_object_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="submission_metadata_object_last_edited",
    )

    def save(self, *args, **kwargs):
        """Save the submission metadata object instance."""
        # check that the type is a valid metadata object type for the submission connector
        connector_class = connectors.get_connector_class(
            self.submission.connection.connector
        )
        if self.type not in connector_class.get_all_metadata_object_types().keys():
            raise SaveException(
                details={
                    "parameter": "type",
                    "message": f"Invalid metadata object type: {self.type}. Available types are: [{list(connector_class.get_all_metadata_object_types().keys())}].",
                }
            )
        super(SubmissionMetadataObject, self).save(*args, **kwargs)


class ValueField(models.Model):
    """Model representing a value for a metadata field."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    value = models.JSONField(default=dict, blank=True, null=True)
    source_metadata = models.ForeignKey(
        Metadata,
        on_delete=models.SET_NULL,
        related_name="submission_metadata_value_fields",
        null=True,
        blank=True,
    )
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="submission_metadata_value_fields_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        null=True,
        related_name="submission_metadata_value_fields_last_edited_by",
    )


class SubmissionMetadata(models.Model):
    """Model representing metadata associated with a submission."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    submission = models.ForeignKey(
        Submission, on_delete=models.CASCADE, related_name="metadata"
    )
    submission_metadata_object = models.ForeignKey(
        SubmissionMetadataObject, on_delete=models.CASCADE, related_name="metadata"
    )
    field = models.ForeignKey(
        ConnectorMetadataField, on_delete=models.CASCADE, related_name="metadata_field"
    )
    value = models.ForeignKey(
        ValueField,
        on_delete=models.CASCADE,
        related_name="submission_metadata",
        blank=True,
        null=True,
    )
    candidate_values = models.ManyToManyField(
        ValueField,
        related_name="submission_metadata_candidate_values",
        blank=True,
    )
    is_valid = models.BooleanField(default=True)
    metadata = models.ManyToManyField(
        Metadata, related_name="submission_metadata", blank=True
    )
    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submission_metadata_created_by",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        MadbotUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submission_metadata_last_edited_by",
    )

    class Meta:
        """Meta class for SubmissionMetadata."""

        verbose_name_plural = "Submission Metadata"

    def save(self, *args, **kwargs):
        """
        Save the submission metadata instance, handling ValueField creation/updates automatically.

        This method will:
        1. Validate that the submission data object belongs to the submission
        2. Create or update a ValueField instance if value is provided directly
        3. Validate the value against the field schema

        Raises:
            SaveException: If the submission data object is not a data object of the submission
            SaveException: If the source metadata object does not exist

        """
        # check that the submission data object is a data object of the submission
        if not self.submission_metadata_object.submission == self.submission:
            raise SaveException(
                details={
                    "parameter": "submission_data_object",
                    "message": "Submission data object is not a data object of the submission",
                }
            )

        # If value_data is provided, create or update ValueField instance
        if hasattr(self, "_value_data") and self._value_data is not None:
            value = self._value_data.get("value", None)
            source_metadata_id = self._value_data.get("source_metadata_id", None)

            # Get the source metadata object if an ID is provided
            source_metadata = None
            if source_metadata_id:
                try:
                    source_metadata = Metadata.objects.get(id=source_metadata_id)
                except Metadata.DoesNotExist:
                    raise SaveException(
                        details={
                            "parameter": "source_metadata_id",
                            "message": f"Metadata with id {source_metadata_id} does not exist",
                        }
                    )

            # Update existing ValueField if it exists, otherwise create new one
            if self.value:
                self.value.value = value
                self.value.source_metadata = source_metadata
                self.value.last_edited_by = self.last_edited_by
                self.value.save()
            else:
                value_field = ValueField.objects.create(
                    value=value,
                    source_metadata=source_metadata,
                    created_by=self.created_by,
                    last_edited_by=self.last_edited_by,
                )
                self.value = value_field

        candidate_value_fields = []
        # If candidate_values_data is provided, create/update ValueField instances
        if hasattr(self, "_candidate_values_data") and self._candidate_values_data:
            # Delete existing candidate ValueField instances to avoid orphaned records
            self.candidate_values.all().delete()

            for candidate_data in self._candidate_values_data:
                value = candidate_data.get("value", None)
                source_metadata_id = candidate_data.get("source_metadata_id", None)

                # Get the Metadata object if an ID is provided
                source_metadata = None
                if source_metadata_id:
                    try:
                        source_metadata = Metadata.objects.get(id=source_metadata_id)
                    except Metadata.DoesNotExist:
                        raise SaveException(
                            details={
                                "parameter": "candidate_values_data/source_metadata_id",
                                "message": f"Metadata with id {source_metadata_id} does not exist",
                            }
                        )

                candidate_field = ValueField.objects.create(
                    value=value,
                    source_metadata=source_metadata,
                    created_by=self.created_by,
                    last_edited_by=self.last_edited_by,
                )
                candidate_value_fields.append(candidate_field)

        super().save(*args, **kwargs)

        # Add all candidate values if any were created
        if candidate_value_fields:
            self.candidate_values.add(*candidate_value_fields)

    @property
    def value_data(self):
        """
        Get the value data from the ValueField.

        Returns:
            any: The value of the ValueField.

        Example:
            metadata.value_data    # instead of metadata.value.value

        """
        return self.value.value if self.value else None

    @value_data.setter
    def value_data(self, data):
        """
        Set the value data to be used when creating a ValueField.

        Args:
            data: The value to set.

        Example:
            metadata.value_data = "value"   # instead of metadata.value.value = "value"

        """
        self._value_data = data

    @property
    def candidate_values_data(self):
        """
        Get the candidate values data in a list format.

        Returns:
            list: List of values for each candidate value.

        Example:
            metadata.candidate_values_data    # instead of metadata.candidate_value.all()

        """
        if not self.candidate_value.exists():
            return []

        return [value_field.value for value_field in self.candidate_values.all()]

    @candidate_values_data.setter
    def candidate_values_data(self, data):
        """Store the candidate values data to be used when saving."""
        self._candidate_values_data = data


class SubmissionSettings(models.Model):
    """Model representing settings associated with a submission."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    submission = models.ForeignKey(
        Submission,
        on_delete=models.CASCADE,
        related_name="settings",
    )
    field = models.CharField(max_length=255)
    value = models.TextField(blank=True, null=True)

    created_time = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        "MadbotUser",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submission_settings_created",
    )
    last_edited_time = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        "MadbotUser",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="submission_settings_last_edited",
    )

    class Meta:
        """Meta class for SubmissionSettings."""

        verbose_name_plural = "Submission Settings"
        unique_together = ("submission", "field")

    def clean(self):
        """Check the validity of settings based on the connector."""
        connector = self.submission.connection.connector
        connector_class = connectors.get_connector_class(connector)

        # Retrieve the connector's settings schema
        valid_fields = connector_class.get_settings_fields()
        # Validate if the field is valid in the connector schema
        matching_field = next(
            (field for field in valid_fields if field.slug == self.field), None
        )

        if not matching_field:
            raise ValidationError(
                f"The field '{self.field}' is not valid for the {connector_class.get_name()} connector."
            )
        if isinstance(self.value, str):
            try:
                self.value = json.loads(self.value.replace("'", '"'))
            except json.JSONDecodeError:
                pass

        # Continue with MadbotValidator including DTO validation
        self._validate_value_with_madbot_validator(connector_class)

    def _validate_value_with_madbot_validator(self, connector_class):
        """
        Validate the value using the MadbotValidator and Settings.

        Args:
        ----
            connector_class: The connector class.

        """
        connector_name = connector_class.get_name().lower()
        schema_path = Path(
            f"madbot_api/connectors/madbot_{connector_name}/static/json/schema"
        )

        def retrieve_from_filesystem(uri: str):
            # Modify to read the correct schema file path
            path = schema_path / Path(
                uri.removeprefix(f"/api/schemas/connectors/{connector_name}/") + ".json"
            )
            contents = json.loads(path.read_text())
            contents["$schema"] = "https://json-schema.org/draft/2019-09/schema"
            return Resource.from_contents(contents)

        # Create the registry to load schemas
        registry = Registry(retrieve=retrieve_from_filesystem)

        # Construct the schema reference for this specific field
        schema_ref = f"/api/schemas/connectors/{connector_name}/{self.field}"

        try:
            # Attempt to retrieve schema dynamically
            field_schema_path = schema_path / f"{self.field}.json"
            field_schema = json.loads(field_schema_path.read_text())

            # Create the DTO from the schema
            setting_dto = connectors.Settings.initialize_setting(
                field_schema, self.value
            )

            try:
                setting_dto.dict()  # Triggers Pydantic validation
            except ValidationError as e:
                raise ValidationError(
                    f"Validation error for field '{self.field}': {e.errors()}"
                )

            # Use MadbotValidator for additional validation if necessary
            MadbotValidator(
                {"$ref": schema_ref},
                registry=registry,
            ).validate(self.value)

        except ValidationError as e:
            raise ValidationError(
                f"Validation error for field '{self.field}' using MadbotValidator: {e.message}"
            )

    def save(self, *args, **kwargs):
        """Override the save method to add custom validation."""
        self.clean()
        super().save(*args, **kwargs)
