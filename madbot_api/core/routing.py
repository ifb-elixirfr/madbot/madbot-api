from django.urls import re_path

from . import consumers

websocket_urlpatterns = [
    re_path(r"ws/notifications/$", consumers.NotificationConsumer.as_asgi()),
    re_path(r"ws/workspaces/$", consumers.WorkspaceConsumer.as_asgi()),
    re_path(
        r"ws/submissions/(?P<submission_id>[^/]+)/$",
        consumers.SubmissionConsumer.as_asgi(),
    ),
]
