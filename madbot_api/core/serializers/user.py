from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers

from madbot_api.core.models import ApplicationUserParam, MadbotUser

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """Serializer for the MadbotUser model."""

    queryset = MadbotUser.objects.all()

    class Meta:
        """Meta options for UserSerializer."""

        model = MadbotUser
        fields = ["id", "username", "email", "first_name", "last_name"]
        read_only_fields = ["id"]


class LDAPLoginSerializer(serializers.Serializer):
    """Serializer for LDAP login authentication."""

    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        """Validate username and password against the LDAP backend."""
        user = authenticate(**data)
        if user and user.is_active:
            return user
        raise serializers.ValidationError("Invalid Details.")


class ApplicationUserParamSerializer(serializers.ModelSerializer):
    """Serializer for ApplicationUserParam model."""

    username = serializers.SerializerMethodField(required=False)
    application = serializers.SerializerMethodField(required=False)

    class Meta:
        """Meta options for ApplicationUserParamSerializer."""

        model = ApplicationUserParam
        fields = ("username", "application", "params")

    def get_username(self, obj):
        """Retrieve the username of the associated user."""
        return obj.user.username

    def get_application(self, obj):
        """Retrieve the name of the associated application."""
        return obj.content_object.name
