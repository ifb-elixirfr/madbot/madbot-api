import logging
import time
from datetime import timedelta

from celery import Task, shared_task
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from django_celery_results.models import TaskResult

from madbot_api.core import connector as connectors
from madbot_api.core.connector import ConnectorError, DataConnector
from madbot_api.core.models import (
    Connection,
    ConnectorMetadataField,
    ConnectorRawSchema,
    Credential,
    Data,
    DataAccessRight,
    DataLink,
    MadbotUser,
    Submission,
    SubmissionMetadata,
    SubmissionMetadataObject,
    SubmissionSettings,
    Workspace,
)
from madbot_api.core.utils.websocket import notifier
from madbot_api.core.views.utils.connection import check_connection_state
from madbot_api.core.views.utils.notification import trigger_notification
from madbot_api.core.views.utils.object_handeling import object_to_dictionary
from madbot_api.core.views.utils.vault import Vault

logger = logging.getLogger(__name__)


class LogErrorsTask(Task):
    """Custom Celery task class that logs errors to the logger."""

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        """Log the error and call the parent class's on_failure method."""
        logger.exception("Celery task failure!", exc_info=exc)
        super(LogErrorsTask, self).on_failure(exc, task_id, args, kwargs, einfo)


# The commented code block provided is defining a custom Celery task
# class named `CustomTask`. This custom task class extends the base Celery
# `Task` class and overrides the `on_success` and `on_failure` methods to add
# custom behavior when a task succeeds or fails.
# from celery import Task
# class CustomTask(Task):
#     def on_success(self, retval, task_id, args, kwargs):
#         print(f"Taks {task_id} successed with return : {retval}")
#         super().on_success(retval, task_id, args, kwargs)
#     def on_failure(self, exc, task_id, args, kwargs, einfo):
#         print(f"Task {task_id} failed with exception : {exc}")
#         super().on_failure(exc, task_id, args, kwargs, einfo)
# @shared_task(base=CustomTask, name="submission_submit")
# def submission_submit(id, **kwargs):
#     time.sleep(10)
#     return id


@shared_task(name="check_workspace_connections")
def check_workspace_connections(workspace_id, user_id, connection_id=None, **kwargs):
    """
    Asynchronous task to verify all or one connection for a given workspace.

    Args:
    ----
        workspace_id: The ID of the workspace whose connections are to be verified.
        user_id: The ID of the user who triggered the verification.
        connection_id: The ID of the specific connection to verify. If None, all connections are verified.
        **kwargs: Additional keyword arguments that may be passed to the task.

    """
    # Check if the workspace exists
    try:
        Workspace.objects.get(
            id=workspace_id,
        )
    except Workspace.DoesNotExist:
        raise ValueError(f"Workspace with id {workspace_id} does not exist")

    # Check if the user exists
    try:
        user = MadbotUser.objects.get(id=user_id)
    except MadbotUser.DoesNotExist:
        raise ValueError(f"User with id {user_id} does not exist")

    if connection_id:
        connections = Connection.objects.filter(
            workspace=workspace_id, id=connection_id
        )
    else:
        connections = Connection.objects.filter(workspace=workspace_id)

    for connection in connections:
        try:
            # Gather shared parameters
            shared_params = connection.shared_params.all()
            shared_param_dict = {param.key: param.value for param in shared_params}
            # Retrieve the credential for the connection and user
            credential, created = Credential.objects.get_or_create(
                connection=connection, defaults={"user": user}
            )
            # Access private parameters from the vault
            vault = Vault()
            private_params = vault.read_secret(user, credential)
            # Verify connection state
            check_connection_state(
                connection.connector, shared_param_dict, private_params
            )
            # Update credential status to 'connected' if verification succeeds
            credential.status = "connected"
            credential.user = user
            credential.save()

        except Exception:
            # If verification fails, update credential status to 'failed'
            credential.status = "not_connected"
            credential.user = user
            credential.save()


# @shared_task(name="verify_all_workspaces_connections")
# def verify_all_connections():
#     """Asynchronous task to verify all connections for all workspaces."""
#     workspaces = Workspace.objects.all()
#     for workspace in workspaces:
#         connections = Connection.objects.filter(workspace=workspace)

#         for connection in connections:
#             try:
#                 shared_params = connection.shared_params.all()
#                 shared_param_dict = {param.key: param.value for param in shared_params}

#                 private_params = getattr(connection, "private_params", None)

#                 check_connection_state(
#                     connection.connector, shared_param_dict, private_params
#                 )

#                 Credential.objects.update_or_create(
#                     connection=connection,
#                     defaults={
#                         "status": "connected",
#                         "user": connection.created_by,
#                     },
#                 )

#             except Exception:
#                 Credential.objects.update_or_create(
#                     connection=connection,
#                     defaults={"status": "failed", "user": connection.created_by},
#                 )


# # Define a periodic task
# @current_app.on_after_finalize.connect
# def setup_periodic_tasks(sender, **kwargs):
#     """Create a periodic task that updates the status of the connection."""
#     sender.add_periodic_task(
#         schedule(run_every=settings.VERIFY_CONNECTIONS_FREQUENCY),
#         verify_all_connections.s(),
#         name="verify connections periodically",
#     )


@shared_task(name="submit")
def submit(id, **kwargs):
    """
    Submit a submission by its ID.

    This task simulates a long-running process by sleeping for 10 seconds
    before returning the given submission ID.

    Args:
    ----
        id (int): The ID of the submission to submit.
        **kwargs: Additional keyword arguments.

    Returns:
    -------
        int: The ID of the submit submission.

    """
    time.sleep(10)
    return id


@shared_task
def clear_health_check_results():
    """Clear all TaskResult objects with a name containing "health_check" or status REVOKED."""
    TaskResult.objects.filter(task_name__icontains="health_check").delete()
    TaskResult.objects.filter(status="REVOKED").delete()


def update_data_access_right(object, new_access_right):
    """
    Update the access rights of an object and records the last verification and update times.

    :param object: The `object` parameter is an instance of a data access object or model.
    :param new_access_right: The `new_access_right` parameter represents the updated access rights
    that you want to assign to the object.
    """
    if object.access_right != new_access_right:
        object.access_right = new_access_right
        object.last_updated_time = timezone.now()

    object.save()


@shared_task(name="check_data_access")
def check_data_access(workspace_id, user_id, **kwargs):
    """
    Check data access for a specific user in a specific workspace.

    Args:
    ----
        workspace_id: The ID of the workspace whose connections are to be verified.
        user_id: The ID of the user who triggered the verification.
        **kwargs: Additional keyword arguments that may be passed to the task.

    """
    # Check if the workspace exists
    try:
        workspace = Workspace.objects.get(
            id=workspace_id,
        )
    except Workspace.DoesNotExist:
        raise ValueError(f"Workspace with id {workspace_id} does not exist")

    # Check if the user exists
    try:
        user = MadbotUser.objects.get(id=user_id)
    except MadbotUser.DoesNotExist:
        raise ValueError(f"User with id {user_id} does not exist")

    # Check if the user is part of the workspace
    if (
        not workspace.workspace_members.select_related("user", "workspace")
        .filter(user=user)
        .exists()
    ):
        raise ValueError("You are not a member of this workspace.")

    threshold_time = timezone.now() - timedelta(
        seconds=settings.VERIFY_CONNECTIONS_FREQUENCY
    )

    queryset = (
        Data.objects.filter(workspace=workspace)
        .filter(
            datalinks__in=DataLink.objects.filter(
                node__workspace=workspace,
                node__node_members__user=user,
            ).distinct(),
        )
        .filter(
            Q(access_right__isnull=True)
            | (
                (
                    Q(access_right__last_verification_time__lte=threshold_time)
                    & Q(access_right__user=user)
                )
                | (
                    Q(access_right__user=user)
                    & ~Q(access_right__access_right="accessible")
                )
            )
        )
        .distinct()
    )

    # Verify the data
    for data in queryset:
        da, created = DataAccessRight.objects.get_or_create(
            user=user,
            data=data,
            defaults={"access_right": "unverified"},
        )

        connector_class = connectors.get_connector_class(
            data.connection.connector,
        )
        if not issubclass(connector_class, DataConnector):
            update_data_access_right(da, "unreachable")
            continue

        try:
            credential = Credential.objects.get(connection=data.connection, user=user)
        except Credential.DoesNotExist:
            update_data_access_right(da, "unreachable")
            continue

        if credential.last_edited_time >= threshold_time:
            try:
                # Convert the shared parameters related to the credential's connection to a dictionary representation.
                shared_params_dict = object_to_dictionary(
                    credential.connection.shared_params.all(),
                )

                vault = Vault()
                private_param_data = vault.read_secret(user, credential)

                # Attempt to establish the connection
                connector_instance = check_connection_state(
                    data.connection.connector,
                    shared_params_dict,
                    private_param_data,
                )
                credential.status = "connected"
                credential.user = user
                credential.save()
            except Exception:
                # If verification fails, update credential status to 'not_connected'
                credential.status = "not_connected"
                credential.user = user
                credential.save()

        try:
            external_data = connector_instance.get_data_object(
                data.external_id,
            )
            update_data_access_right(da, "accessible")
            if external_data.size != data.size:
                data.size = external_data.size
                data.save()
            continue
        except ConnectorError:
            update_data_access_right(da, "unaccessible")
            continue
        except Exception:
            update_data_access_right(da, "unreachable")
            continue


@shared_task(base=LogErrorsTask, name="async_create_metadata_objects")
def async_create_metadata_objects(submission_id, user_id):
    """
    Asynchronous task to create metadata objects using a specific connector.

    Args:
    ----
        submission_id (str): The ID of the submission
        user_id (str): The ID of the user

    Raises:
    ------
        ConnectorError: If there are issues with the connector

    """
    logger.debug("Starting submission task for ID: %s", submission_id)

    try:
        # Retrieve user object
        user = MadbotUser.objects.get(id=user_id)

        # Retrieve submission object
        submission = Submission.objects.get(id=submission_id)

        # Get the submission settings
        submission_settings = SubmissionSettings.objects.filter(submission=submission)

        # Initialise the connector
        # This part will change following the new way of storing schema
        connector_class = connectors.get_connector_class(
            submission.connection.connector
        )
        connector_name = connector_class.get_name().lower()

        settings = []
        for setting in submission_settings:
            try:
                # Retrieve schema from database
                schema = ConnectorRawSchema.objects.get(
                    slug=setting.field, source=connector_name
                ).schema

                # Create the DTO from the schema
                setting_dto = connectors.Settings.initialize_setting(
                    schema, setting.value
                )
                settings.append(setting_dto)
            except ConnectorRawSchema.DoesNotExist:
                raise ValueError(
                    f"Schema not found for field '{setting.field}' in connector '{connector_name}'"
                )

        shared_params = object_to_dictionary(submission.connection.shared_params.all())

        # instantiate the connector
        combined_params = {
            **shared_params,
            "settings": settings,
        }
        connector_instance = connector_class(combined_params)

        # Construct the submission member DTO for the specified submission
        members = []
        for member in submission.submission_members.all():
            member = connectors.SubmissionMember(
                first_name=member.user.first_name,
                last_name=member.user.last_name,
                role=member.role,
            )
            members.append(member)

        # Construct the submission DTO
        submission_dto = connectors.Submission(
            title=submission.title,
            status=submission.status,
            members=members,
        )

        # construct the metadata object DTOs
        metadata_object_dto = []
        for metadata_object in submission.metadata_objects.all():
            metadata_object_dto.append(
                connectors.MadbotMetadataObject(
                    id=metadata_object.id,
                    type=metadata_object.type,
                    metadata=[],
                )
            )

        # Construct the data DTOs
        data_dto = []
        for submission_data in submission.data.all():
            data_dto.append(
                connectors.Data(
                    filename=submission_data.data.name,
                    size=submission_data.data.size if submission_data.data.size else 0,
                    madbot_id=submission_data.data.id,
                    parent_node=connectors.SourceNode(
                        node=submission_data.datalink.node
                    ),
                    metadata=[],
                )
            )

        metadata_objects = connector_instance.create_metadata_objects(
            submission_dto,
            metadata_object_dto,
            data_dto,
        )
        # Create/update the metadata objects in the submission
        for metadata_object in metadata_objects:
            # if the metadata object is already in the database, update it
            if metadata_object.id:
                # retrieve the submission metadata object
                submission_metadata_object = SubmissionMetadataObject.objects.get(
                    id=metadata_object.id,
                )

                for metadata in metadata_object.metadata:
                    connector_metadata_field = ConnectorMetadataField.objects.get(
                        slug=metadata.field, source=connector_name
                    )
                    submission_metadata = SubmissionMetadata.objects.get(
                        submission=submission,
                        submission_metadata_object=submission_metadata_object,
                        field=connector_metadata_field,
                    )
                    # only update the value if it was not modified by a Madbot user
                    if not submission_metadata.value or (
                        submission_metadata.value
                        and not submission_metadata.value.last_edited_by
                    ):
                        submission_metadata.value_data = (
                            metadata.value.dict() if metadata.value else None
                        )
                        submission_metadata.is_valid = metadata.is_valid
                    # otherwise, handle the value set manually by a Madbot user
                    else:
                        # if the value set by the user is valid and the value returned by the connector is not, increment the complete count
                        if submission_metadata.is_valid and (
                            not metadata.value or not metadata.is_valid
                        ):
                            # retrieve the completeness level based on the level of the field
                            field_level = connector_metadata_field.level
                            metadata_object.completeness[field_level]["complete"] += 1
                        # if the value set by the user is not valid and the value returned by the connector is valid, decrement the complete count
                        elif (
                            not submission_metadata.is_valid
                            and metadata.value
                            and metadata.is_valid
                        ):
                            field_level = connector_metadata_field.level
                            metadata_object.completeness[field_level]["complete"] -= 1

                        # and if there is a returned value, since it can't be used as a value, append it to the candidate values
                        if metadata.value:
                            metadata.candidate_values.append(metadata.value)

                    # Set new candidate values
                    submission_metadata.candidate_values.all().delete()
                    submission_metadata.candidate_values_data = [
                        value.dict() for value in metadata.candidate_values
                    ]

                    submission_metadata.save()

                # update the completeness of the metadata object
                submission_metadata_object.completeness = metadata_object.completeness
                submission_metadata_object.save()

            # otherwise, create it
            else:
                submission_metadata_object = SubmissionMetadataObject.objects.create(
                    submission=submission,
                    type=metadata_object.type.slug,
                    completeness=metadata_object.completeness,
                    created_by=None,  # leave blank to indicate it was created by the asynchronous task
                    last_edited_by=None,
                )

                for metadata in metadata_object.metadata:
                    connector_metadata_field = ConnectorMetadataField.objects.get(
                        slug=metadata.field, source=connector_name
                    )
                    SubmissionMetadata.objects.create(
                        submission=submission,
                        submission_metadata_object=submission_metadata_object,
                        field=connector_metadata_field,
                        value_data=metadata.value.dict() if metadata.value else None,
                        candidate_values_data=[
                            value.dict() for value in metadata.candidate_values
                        ],
                        created_by=None,  # leave blank to indicate it was created by the asynchronous task
                        last_edited_by=None,
                        is_valid=metadata.is_valid,
                    )

        # sleep for 10 seconds
        time.sleep(10)

        # update the submission status to "draft" and set the last edited by to the user who triggered the task
        submission.status = Submission.STATUS_DRAFT
        submission.last_edited_by = user
        # Reset the last_failed_async_task property if no error occurs
        submission.last_failed_async_task = {}
        submission.save()

        trigger_notification(
            sender=user,
            recipient=user,
            verb="generated metadata",
            action_object=submission,
            target=submission,
            actor=user,
            workspace=submission.workspace.id,
        )

        # Send WebSocket message to notify clients about submission update
        notifier.sync_update(
            model_type="submission",
            object_id=submission_id,
            action="update",
            source="submission",
        )

        logger.debug("Submission task completed")

    except Exception as e:
        # Get the current task ID
        task_id = async_create_metadata_objects.request.id
        error_message = str(e)

        logger.error("Task %s failed with error: %s", task_id, error_message)

        # Retrieve submission object
        submission = Submission.objects.get(id=submission_id)

        submission.status = Submission.STATUS_DRAFT
        # Save the task ID and error reason in the submission properties
        submission.last_failed_async_task = {
            "task_name": "async_create_metadata_objects",
            "task_id": task_id,
            "error_message": error_message,
            "timestamp": timezone.now(),
        }
        submission.save()

        user = MadbotUser.objects.get(id=user_id)

        trigger_notification(
            sender=user,
            recipient=user,
            verb="error generating metadata",
            action_object=submission,
            target=submission,
            actor=user,
            workspace=submission.workspace.id,
        )

        # Send WebSocket message to notify clients about the failure
        notifier.sync_update(
            model_type="submission",
            object_id=submission_id,
            action="error",
            source="submission",
        )

        # Re-raise the exception to ensure the task is marked as failed
        raise e


@shared_task(base=LogErrorsTask, name="async_submit")
def async_submit(submission_id, user_id):
    """
    Asynchronous task to submit a submission.

    Args:
    ----
        submission_id (str): The ID of the submission
        user_id (str): The ID of the user

    """
    pass
