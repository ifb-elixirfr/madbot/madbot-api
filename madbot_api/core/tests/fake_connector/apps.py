from django.apps import AppConfig


class FakeConnectorConfig(AppConfig):
    """Configuration for the FakeConnector application."""

    name = "madbot_api.core.tests.fake_connector"
