import logging

from madbot_api.core.connector import DataConnector, ExternalDataObject

logger = logging.getLogger(__name__)


class FakeConnector(DataConnector):
    """A fake connector for testing purposes, simulating external data objects."""

    def __init__(self, parameters):
        """
        Initialize the FakeConnector with given parameters.

        Args:
        ----
            parameters (dict): Configuration parameters, including 'host'.

        """
        self.host = parameters["host"]
        self.objects = {
            "container_1": ExternalDataObject(
                id="container_1",
                name="Container 1",
                type="container",
                icon="fas fa-folder",
                has_children=False,
                linkable=True,
            ),
            "object_1": ExternalDataObject(
                id="object_1",
                name="Object 1",
                type="object",
                icon="madbot_api/core/tests/fake_connector/static/fake_connector/logo.png",
                has_children=False,
                linkable=True,
            ),
            "object_2": ExternalDataObject(
                id="object_2",
                name="Object 2",
                type="object",
                icon="fas fa-file",
                has_children=False,
                linkable=True,
            ),
            "object_3": ExternalDataObject(
                id="object_3",
                name="Object 3",
                type="object",
                icon="fas fa-file",
                has_children=False,
                linkable=True,
            ),
        }
        self.containers = {
            "root": [self.objects["container_1"], self.objects["object_1"]],
            "container_1": [self.objects["object_2"], self.objects["object_3"]],
        }

    @classmethod
    def get_name(cls):
        """Return the name of the connector."""
        return "FakeConnector"

    @classmethod
    def get_description(cls):
        """Return a description of the connector."""
        return "A fake description"

    @classmethod
    def get_connection_param(cls):
        """Return a list of connection parameters for the connector."""
        return [
            {
                "id": "host",
                "name": "Host",
                "helper": "Example :  fake.host.com",
                "type": "text",
                "access": "public",
            },
            {
                "id": "username",
                "name": "Username",
                "helper": "",
                "type": "text",
                "access": "private",
            },
            {
                "id": "password",
                "name": "Password",
                "helper": "",
                "type": "password",
                "access": "private",
            },
        ]

    @classmethod
    def get_logo(cls):
        """Return the logo path for the connector."""
        return "fake_connector/logo.png"

    @classmethod
    def get_color(cls):
        """Return the color associated with the connector."""
        return "#FFFFFF"

    def get_instance_name(self):
        """Return the name of the connector instance."""
        return self.host

    def get_root_id(self):
        """Return the root ID of the connector."""
        return

    def get_root_name(self):
        """Return the name of the root object."""
        return "root"

    def get_data_objects(self, parent_type, parent_id):
        """
        Return the data objects for the given parent ID.

        Args:
        ----
            parent_type (str): The type of the parent object.
            parent_id (str): The ID of the parent object.

        Returns:
        -------
            list: A list of external data objects.

        """
        return self.containers[parent_id]

    def get_data_object(self, object_type, object_id, depth=0):
        """
        Return a specific data object by its ID.

        Args:
        ----
            object_type (str): The type of the object.
            object_id (str): The ID of the object.
            depth (int, optional): The depth for retrieval. Defaults to 0.

        Returns:
        -------
            ExternalDataObject: The requested external data object.

        """
        return self.objects[object_id]
