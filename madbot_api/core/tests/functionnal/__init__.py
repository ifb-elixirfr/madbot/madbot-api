import secrets
import string
import uuid
from datetime import datetime, timedelta

import factory
import pytz
import responses
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.utils.text import slugify
from notifications.models import Notification
from oauth2_provider.models import AccessToken, Application

from madbot_api.core.models import (
    Connection,
    Data,
    DataLink,
    MadbotMetadataField,
    MadbotRawSchema,
    MadbotUser,
    Metadata,
    Node,
    NodeMember,
    NodeType,
    NodeTypeCollection,
    Sample,
    SampleBoundData,
    Workspace,
    WorkspaceMember,
)


class MadbotTestCase(TestCase):
    """
    Base test case for Madbot API.

    This class provides methods to make HTTP requests with token authentication
    and workspace context in a test environment.
    """

    def post(
        self,
        url,
        token=None,
        workspace=None,
        data={},
        content_type="application/json",
    ):
        """
        Send a POST request to the specified URL.

        This function sends a POST request to a specified URL with optional token
        authentication and data payload.

        :param url: The `url` parameter is the URL of the endpoint where the POST
        request will be sent. It should be a string that specifies the complete URL,
        including the protocol (e.g., "/api/endpoint").
        :param token: The `token` parameter is an optional parameter that represents
        an authentication token. It is used to authorize the request by including it
        in the request headers. If a token is provided, it will be included in the
        "Authorization" header as a bearer token. If no token is provided, the
        "Authorization" header will be omitted.
        :param data: The `data` parameter is a dictionary that contains the data to
        be sent in the request body. It is used to pass any additional information
        or payload that needs to be sent along with the request. The data will be
        serialized and sent in the specified `content_type` format (default is JSON).
        :param content_type: The `content_type` parameter specifies the type of data
        being sent in the request body. In this case, it is set to
        `"application/json"`, indicating that the data being sent is in JSON format.
        Defaults to application/json (optional).
        :return: The `post` method returns the response from the `self.client.post`
        method.
        """
        headers = {}
        if token is not None:
            headers["Authorization"] = f"Bearer {token}"
        if workspace is not None:
            headers["X-Workspace"] = workspace

        return self.client.post(
            url,
            headers=headers,
            data=data,
            content_type=content_type,
        )

    def get(self, url, token=None, workspace=None, content_type="application/json"):
        """
        Send a GET request to the specified URL.

        This function sends a GET request to a specified URL with an optional token
        for authorization.

        :param url: The `url` parameter is the URL of the endpoint you want to make
        a GET request to. It specifies the location of the resource you want to
        retrieve or access.
        :param token: The `token` parameter is used to pass an authentication token
        to the server. It is used to authorize the client's request. If a token is
        provided, it will be included in the request headers as the value of the
        `Authorization` field. If no token is provided, the `Authorization`
        header will be omitted.
        :param content_type: The `content_type` parameter specifies the type of
        content that is expected in the response from the server. In this case, the
        default value is set to `"application/json"`, indicating that the expected
        content type is JSON. Defaults to application/json (optional).
        :return: The `get` method returns the result of the `self.client.get`
        method call.
        """
        headers = {}
        if token is not None:
            headers["Authorization"] = f"Bearer {token}"
        if workspace is not None:
            headers["X-Workspace"] = workspace

        return self.client.get(
            url,
            headers=headers,
            content_type=content_type,
        )

    def put(
        self,
        url,
        token=None,
        workspace=None,
        data={},
        content_type="application/json",
    ):
        """
        Send a PUT request to the specified URL.

        This function sends a PUT request to a specified URL with optional token
        authentication and data payload.

        :param url: The `url` parameter is the endpoint or URL where the PUT request
        will be sent to. It specifies the location where the data should be updated
        or modified.
        :param token: The `token` parameter is used to pass an authentication token
        to the server. It is used to authorize the request and ensure that the user
        has the necessary permissions to perform the requested action. If a token is
        provided, it will be included in the request headers as the value of the
        `Authorization` header.
        :param data: The `data` parameter is a dictionary that contains the data to
        be sent in the request body. It is used to pass any additional information
        or payload that needs to be sent along with the request.
        :param content_type: The `content_type` parameter specifies the type of data
        being sent in the request body. In this case, it is set to
        `"application/json"`, indicating that the data being sent is in JSON format.
        Defaults to application/json (optional).
        :return: The result of the `self.client.put()` method call.
        """
        headers = {}
        if token is not None:
            headers["Authorization"] = f"Bearer {token}"
        if workspace is not None:
            headers["X-Workspace"] = workspace

        return self.client.put(
            url,
            headers=headers,
            data=data,
            content_type=content_type,
        )

    def delete(self, url, token=None, workspace=None, content_type="application/json"):
        """
        Send a DELETE request to the specified URL.

        This function sends a DELETE request to the specified URL with optional token
        and content type headers.

        :param url: The `url` parameter is the endpoint or URL of the resource that
        you want to delete. It specifies the location of the resource that you want
        to remove from the server.
        :param token: The `token` parameter is an optional parameter that represents
        an authentication token. It is used to authorize the request by including it
        in the request headers. If a token is provided, it will be included in the
        request headers as "Authorization: Bearer {token}". If no token is provided,
        the `Authorization` header will be omitted.
        :param content_type: The `content_type` parameter specifies the type of
        content being sent in the request. In this case, the default value is set to
        `"application/json"`, indicating that the request body will be in JSON
        format. However, you can override this value by passing a different content
        type when calling the method. Defaults to application/json (optional).
        :return: The `delete` method returns the result of the
        `self.client.delete` method call.
        """
        headers = {}
        if token is not None:
            headers["Authorization"] = f"Bearer {token}"
        if workspace is not None:
            headers["X-Workspace"] = workspace

        return self.client.delete(
            url,
            headers=headers,
            content_type=content_type,
        )

    def assert_date(self, date):
        """
        Assert that the given date matches the expected ISO 8601 format.

        :param date: The date to validate against the ISO 8601 regex pattern.
        """
        return self.assertRegex(
            date,
            r"^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)?(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])?$",
        )

    def assert_uuid(self, uuid_to_test):
        """
        Assert that the given UUID is valid.

        :param uuid_to_test: The UUID string to validate.
        """
        isvalid = True
        try:
            uuid_obj = uuid.UUID(uuid_to_test, version=4)
        except ValueError:
            isvalid = False
        if not isvalid and str(uuid_obj) != uuid_to_test:
            self.fail("The UUID is not valid")

    def check_pagination(
        self,
        json_response,
        count,
        url_next,
        url_previous,
        current_page,
        total_pages,
    ):
        """
        Check the pagination metadata in the JSON response.

        :param json_response: The JSON response containing pagination data.
        :param count: The expected count of total items.
        :param url_next: The expected URL for the next page.
        :param url_previous: The expected URL for the previous page.
        :param current_page: The expected current page number.
        :param total_pages: The expected total number of pages.
        """
        self.assertEqual(json_response["count"], count)
        self.assertEqual(json_response["next"], url_next)
        self.assertEqual(json_response["previous"], url_previous)
        self.assertEqual(json_response["current_page"], current_page)
        self.assertEqual(json_response["total_pages"], total_pages)


def make_random_string(size):
    """Generate a random string of the specified size."""
    return "".join(
        secrets.choice(string.ascii_lowercase + string.digits) for _ in range(size)
    )


class UserFactory(factory.django.DjangoModelFactory):
    """Create a user instance with random attributes."""

    class Meta:
        """Meta class for UserFactory."""

        model = MadbotUser
        django_get_or_create = ["username"]

    # id = factory.LazyAttribute(lambda _: uuid.uuid4())
    # id = factory.Sequence(lambda n: uuid.uuid4())
    id = factory.Faker("uuid4")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    username = factory.LazyAttribute(
        lambda a: "{}{}".format(a.first_name[0], a.last_name).lower(),
    )
    email = factory.LazyAttribute(
        lambda a: "{}.{}@example.com".format(a.first_name, a.last_name).lower(),
    )
    # TODO: create test to check this field
    is_approved = True

    @factory.post_generation
    def set_uuid(self, create, extracted, **kwargs):
        """Set a UUID for the user if not already set."""
        if not self.id:
            self.id = uuid.uuid4()


class ApplicationFactory(factory.django.DjangoModelFactory):
    """Create an application instance with random attributes."""

    class Meta:
        """Meta class for ApplicationFactory."""

        model = Application
        django_get_or_create = ["client_id"]

    user = factory.SubFactory(UserFactory)
    redirect_uris = "https://the.client.com"
    client_type = "public"
    authorization_grant_type = "authorization-code"
    client_secret = "tellnoone"
    name = "The client"
    skip_authorization = False
    algorithm = "RS256"
    client_id = factory.Faker("random_number")


class AccessTokenFactory(factory.django.DjangoModelFactory):
    """Create an access token instance with random attributes."""

    class Meta:
        """Meta class for AccessTokenFactory."""

        model = AccessToken
        django_get_or_create = ["token"]

    user = factory.SubFactory(UserFactory)
    token = make_random_string(30)
    application = factory.SubFactory(
        ApplicationFactory,
        user=factory.SelfAttribute("..user"),
    )
    scope = "read write"
    expires = datetime.now(pytz.utc) + timedelta(hours=1)

    def __init__(self, *args, **kwargs):
        """Initialize with a custom scope if provided."""
        scope = kwargs.pop("scope", "read write")
        super().__init__(*args, **kwargs)
        self.scope = scope


class WorkspaceFactory(factory.django.DjangoModelFactory):
    """Create a workspace instance with random attributes."""

    class Meta:
        """Meta class for WorkspaceFactory."""

        model = Workspace

    name = factory.Faker("sentence", nb_words=4)
    description = factory.Faker("sentence")
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SelfAttribute("created_by")


def delete_workspaces():
    """Delete all workspace instances."""
    Workspace.objects.all().delete()


class WorkspaceMemberFactory(factory.django.DjangoModelFactory):
    """Create a workspace member instance with random attributes."""

    class Meta:
        """Meta class for WorkspaceMemberFactory."""

        model = WorkspaceMember

    user = factory.SubFactory(UserFactory)
    workspace = factory.SubFactory(
        WorkspaceFactory,
        created_by=factory.SelfAttribute("user"),
    )
    role = "member"
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SelfAttribute("created_by")


def delete_workspace_members():
    """Delete all workspace member instances."""
    WorkspaceMember.objects.all().delete()


class ISATypeCollectionFactory(factory.django.DjangoModelFactory):
    """Create an ISA type collection instance."""

    class Meta:
        """Meta class for ISATypeCollectionFactory."""

        model = NodeTypeCollection
        django_get_or_create = ("name", "description")

    name = "isa"
    description = "ISA is a metadata framework to manage an increasingly diverse set of life science, environmental and biomedical experiments that employ one or a combination of technologies. Built around the Investigation (the project context), Study (a unit of research) and Assay (analytical measurements) concepts, ISA helps you to provide rich descriptions of experimental metadata (i.e. sample characteristics, technology and measurement types, sample-to-data relationships) so that the resulting data and discoveries are reproducible and reusable."


class InvestigationTypeFactory(factory.django.DjangoModelFactory):
    """Create an investigation type instance."""

    class Meta:
        """Meta class for InvestigationTypeFactory."""

        model = NodeType
        django_get_or_create = ("name", "collection")

    name = "investigation"
    description = "An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions."
    icon = "mdi-feather"
    collection = factory.SubFactory(ISATypeCollectionFactory)

    @factory.post_generation
    def parent(self, create, extracted, **kwargs):
        """Set parent types after creation if provided."""
        if not create:
            return

        if extracted:
            if isinstance(extracted, (list, tuple)):
                self.parent.set(extracted)
            else:
                self.parent.add(extracted)
            self.save()


class StudyTypeFactory(factory.django.DjangoModelFactory):
    """Create a study type instance."""

    class Meta:
        """Meta class for StudyTypeFactory."""

        model = NodeType
        django_get_or_create = ("name", "collection")

    name = "study"
    description = "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied."
    icon = "mdi-notebook-outline"
    collection = factory.SubFactory(ISATypeCollectionFactory)

    @factory.post_generation
    def parent(self, create, extracted, **kwargs):
        """Set parent types after creation if provided."""
        if not create:
            return

        if extracted:
            if isinstance(extracted, (list, tuple)):
                self.parent.set(extracted)
            else:
                self.parent.add(extracted)
            self.save()


class AssayTypeFactory(factory.django.DjangoModelFactory):
    """Create an assay type instance."""

    class Meta:
        """Meta class for AssayTypeFactory."""

        model = NodeType
        django_get_or_create = ("name", "collection")

    name = "assay"
    description = "An Assay represents a test performed either on material taken from a subject or on a whole initial subject, producing qualitative or quantitative measurements."
    icon = "mdi-test-tube"
    collection = factory.SubFactory(ISATypeCollectionFactory)

    @factory.post_generation
    def parent(self, create, extracted, **kwargs):
        """Set parent types after creation if provided."""
        if not create:
            return

        if extracted:
            if isinstance(extracted, (list, tuple)):
                self.parent.set(extracted)
            else:
                self.parent.add(extracted)
            self.save()


class InvestigationNodeFactory(factory.django.DjangoModelFactory):
    """Create an investigation node instance."""

    class Meta:
        """Meta class for InvestigationNodeFactory."""

        model = Node

    title = factory.Faker("sentence", nb_words=4)
    description = factory.Faker("sentence")
    type = factory.Iterator(NodeType.objects.filter(id="isa_investigation"))
    status = "active"
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SubFactory(UserFactory)
    workspace = factory.SubFactory(WorkspaceFactory)


class StudyNodeFactory(factory.django.DjangoModelFactory):
    """Create a study node instance."""

    class Meta:
        """Meta class for StudyNodeFactory."""

        model = Node

    title = factory.Faker("sentence", nb_words=4)
    description = factory.Faker("sentence")
    type = factory.Iterator(NodeType.objects.filter(id="isa_study"))
    status = "active"
    parent = factory.SubFactory(InvestigationNodeFactory)
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SubFactory(UserFactory)
    workspace = factory.SelfAttribute("parent.workspace")


class AssayNodeFactory(factory.django.DjangoModelFactory):
    """Create an assay node instance."""

    class Meta:
        """Meta class for AssayNodeFactory."""

        model = Node

    title = factory.Faker("sentence", nb_words=4)
    description = factory.Faker("sentence")
    type = factory.Iterator(NodeType.objects.filter(id="isa_assay"))
    status = "active"
    parent = factory.SubFactory(StudyNodeFactory)
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SubFactory(UserFactory)
    workspace = factory.SelfAttribute("parent.workspace")


class NodeMemberFactory(factory.django.DjangoModelFactory):
    """Create a node member instance."""

    class Meta:
        """Meta class for NodeMemberFactory."""

        model = NodeMember

    user = factory.SubFactory(UserFactory)
    node = factory.SubFactory(InvestigationNodeFactory)
    role = "owner"


class NotificationFactory(factory.django.DjangoModelFactory):
    """Create a notification instance."""

    class Meta:
        """Meta class for NotificationFactory."""

        model = Notification

    class Params:
        """Parameters for customizing the NotificationFactory."""

        workspace = factory.SubFactory(WorkspaceFactory)

    actor = factory.SubFactory(UserFactory)
    verb = "verb"
    recipient = factory.SubFactory(UserFactory)
    level = "info"
    description = factory.SelfAttribute("workspace.id")
    timestamp = datetime.now(pytz.utc)
    # Define action_object_content_type directly
    action_object = factory.lazy_attribute(lambda obj: StudyNodeFactory())
    action_object_content_type = factory.lazy_attribute(
        lambda obj: ContentType.objects.get_for_model(UserFactory()),
    )
    action_object_object_id = factory.lazy_attribute(lambda obj: StudyNodeFactory().id)
    target_content_type = factory.lazy_attribute(
        lambda obj: ContentType.objects.get_for_model(AssayNodeFactory()),
    )
    target = factory.SubFactory(AssayNodeFactory)

    @factory.lazy_attribute
    def unread(self):
        """Indicate whether the notification is unread."""
        return self._unread if hasattr(self, "_unread") else True

    @factory.lazy_attribute
    def deleted(self):
        """Indicate whether the notification is deleted."""
        return self._deleted if hasattr(self, "_deleted") else False


class SampleFactory(factory.django.DjangoModelFactory):
    """Create a sample instance."""

    class Meta:
        """Meta class for SampleFactory."""

        model = Sample

    title = factory.Faker("sentence", nb_words=4)
    node = factory.SubFactory(InvestigationNodeFactory)
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SubFactory(UserFactory)

    @factory.post_generation
    def alias(self, create, extracted, **kwargs):
        """Set the alias after the instance is created."""
        if not create:
            return
        self.alias = extracted or slugify(self.title)


class ConnectionFactory(factory.django.DjangoModelFactory):
    """Create a connection instance."""

    class Meta:
        """Meta class for ConnectionFactory."""

        model = Connection

    name = factory.Faker("sentence", nb_words=4)
    connector = "madbot_api.core.tests.fake_connector"
    created_by = factory.SubFactory(UserFactory)
    workspace = factory.SubFactory(WorkspaceFactory)


class DataFactory(factory.django.DjangoModelFactory):
    """Create a data instance."""

    class Meta:
        """Meta class for DataFactory."""

        model = Data

    connection = factory.SubFactory(ConnectionFactory)
    name = factory.Faker("sentence", nb_words=4)
    description = factory.Faker("sentence")
    created_by = factory.SubFactory(UserFactory)
    workspace = factory.SubFactory(WorkspaceFactory)


class DatalinkFactory(factory.django.DjangoModelFactory):
    """Create a datalink instance."""

    class Meta:
        """Meta class for DatalinkFactory."""

        model = DataLink

    data = factory.SubFactory(DataFactory)
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SubFactory(UserFactory)


class SampleBoundDataFactory(factory.django.DjangoModelFactory):
    """Create a sample bound data instance."""

    class Meta:
        """Meta class for SampleBoundDataFactory."""

        model = SampleBoundData

    sample = (factory.SubFactory(SampleFactory),)
    data = (factory.SubFactory(DataFactory),)
    datalink = (factory.SubFactory(DatalinkFactory),)
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SubFactory(UserFactory)


class MetadataSchemaFactory(factory.django.DjangoModelFactory):
    """Create a metadata schema instance."""

    class Meta:
        """Meta class for MetadataSchemaFactory."""

        model = MadbotRawSchema
        django_get_or_create = ["slug"]

    slug = "text"
    schema = {"type": "string"}
    source = "referential"


class MetadataFieldFactory(factory.django.DjangoModelFactory):
    """Create a MetadataField instance."""

    class Meta:
        """Meta class for MetadataFieldFactory."""

        model = MadbotMetadataField

    name = factory.Faker("sentence", nb_words=4)
    source = "referential"
    schema = factory.SubFactory(MetadataSchemaFactory)
    data_related = True
    sample_related = True
    node_related = True
    data_association_related = True

    @factory.post_generation
    def slug(self, create, extracted, **kwargs):
        """Set the slug after the instance is created."""
        if not create:
            return
        self.slug = extracted or slugify(self.name)


class MetadataFactory(factory.django.DjangoModelFactory):
    """Create a Metadata instance."""

    class Meta:
        """Meta class for MetadataFactory."""

        model = Metadata

    field = factory.SubFactory(MetadataFieldFactory)
    value = factory.Faker("sentence", nb_words=4)
    created_by = factory.SubFactory(UserFactory)
    last_edited_by = factory.SelfAttribute("created_by")


class VaultMock:
    """Mock interactions with Vault API."""

    def __init__(self, host):
        """Initialize the mock with the given host."""
        self.host = host
        responses.post(
            f"https://{self.host}/v1/auth/approle/login",
            json={"auth": {"client_token": "abcd"}},
            status=200,
        )

    def make_empty_kv(self, path):
        """Create mock responses for an empty key-value store."""
        responses.get(f"https://{self.host}/v1/{path}", json={"errors": []}, status=404)
        responses.post(f"https://{self.host}/v1/{path}", status=201)
        responses.delete(f"https://{self.host}/v1/{path}", status=204)


def create_isa_collection():
    """Create an ISA type collection with an investigation and study."""
    ISATypeCollectionFactory()
    investigation1 = InvestigationTypeFactory()
    study1 = StudyTypeFactory(
        parent=[
            investigation1,
        ],
    )
    AssayTypeFactory(
        parent=[
            study1,
        ],
    )


def build_user(
    username,
    id=None,
    password=None,
    firstname="Firstname",
    lastname="Lastname",
    email="email@address.com",
):
    """Build and save a user with the specified parameters."""
    user = MadbotUser(
        id=id,
        username=username,
        first_name=firstname,
        last_name=lastname,
        email=email,
    )
    user.save()
    return user


def delete_users():
    """Delete all users."""
    MadbotUser.objects.all().delete()


def build_application(user, client_id=None):
    """Build and save an application for the given user."""
    application = Application(
        user=user,
        redirect_uris="https://the.client.com",
        client_type="public",
        authorization_grant_type="authorization-code",
        client_secret=secrets.token_urlsafe(32),
        name="The client",
        skip_authorization=False,
        algorithm="RS256",
        client_id=client_id if client_id is not None else make_random_string(10),
    )
    application.save()
    return application


def delete_applications():
    """Delete all applications."""
    Application.objects.all().delete()


def build_accesstoken(user, token, application=None, scope="read write"):
    """Build and save an access token for the specified user."""
    if application is None:
        application = build_application(user)

    token = AccessToken(
        user=user,
        token=token,
        application=application,
        scope=scope,
        expires=datetime.now(pytz.utc) + timedelta(hours=1),
    )
    token.save()
    return token


def delete_accesstokens():
    """Delete all access tokens."""
    AccessToken.objects.all().delete()


# def build_datalink(id, owner, tool, investigation):
#     """
#     Create a datalink based on the `Datalink` Model.
#     """
#     content_type = ContentType.objects.get_for_model(investigation)
#     datalink = DataLink(
#         id=id,
#         name="fake datalink",
#         owner=owner,
#         tool=tool,
#         object_id=1,
#         dataobject_type="root",
#         dataobject_id="fake_type",
#         content_type=content_type,
#         isaobject=investigation,
#     )
#     datalink.save()
#     return datalink


# def delete_datalinks():
#     """
#     Delete all objects in the `Datalink` model.
#     """
#     DataLink.objects.all().delete()


def delete_data():
    """Delete all objects in the `Data` model."""
    Data.objects.all().delete()


def delete_nodes():
    """Delete all objects in the `Node` model."""
    Node.objects.all().delete()


def delete_notifications():
    """Delete all notifications."""
    Notification.objects.all().delete()


def delete_members():
    """Delete all node members."""
    NodeMember.objects.all().delete()


def delete_samples():
    """Delete all samples."""
    Sample.objects.all().delete()


def delete_sample_bound_data():
    """Delete all sample-bound data."""
    SampleBoundData.objects.all().delete()


def delete_metadata_schemas():
    """Delete all metadata schemas."""
    MadbotRawSchema.objects.all().delete()


def delete_metadata_fields():
    """Delete all metadata fields."""
    MadbotMetadataField.objects.all().delete()


def delete_metadata():
    """Delete all metadata."""
    Metadata.objects.all().delete()
