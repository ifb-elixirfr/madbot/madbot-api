from rest_framework import status

from madbot_api.core import connector
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    ConnectionFactory,
    DataFactory,
    DatalinkFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    SampleBoundDataFactory,
    SampleFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"
VALID_TOKEN_5 = "lkjsqidldsqddsqdsqdsqdsqdsqdeg"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_DATA_bound_sample = {
    "sample": "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
    "datalink": "5048d194-4969-4de1-bd44-25c51a995dfb",
}

connector.reload_connectors()


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )

        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_url_exist(self):
        """POST /data/{did}/bound_samples exists."""
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
        )
        self.assertNotIn(response.status_code, [404, 405, 500])


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="read")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)
        zeus = UserFactory(username="zeus")
        AccessTokenFactory(token=VALID_TOKEN_5, user=zeus)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)
        WorkspaceMemberFactory(user=zeus, workspace=workspace)

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=seraphim,
            workspace=workspace,
        )
        investigation2 = InvestigationNodeFactory(
            id="9c5e9af8-e56b-40a9-bf51-da3bb2fa3c85",
            created_by=seraphim,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=seraphim,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation1,
            role="owner",
        )
        NodeMemberFactory(
            id="80d5993e-d671-4df9-a6a7-5b2a7d515960",
            user=zeus,
            node=investigation1,
            role="collaborator",
        )
        NodeMemberFactory(
            id="20022cfa-701d-4ded-9f94-65abe2acd76b",
            user=demeter,
            node=investigation2,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=demeter,
            last_edited_by=demeter,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=demeter,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=demeter,
            workspace=workspace,
        )
        DataFactory(
            id="5d942d10-6d4f-4145-9911-91baf4d7d1b5",
            connection=connection,
            created_by=demeter,
            workspace=workspace,
        )
        DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=demeter,
            last_edited_by=demeter,
        )
        DatalinkFactory(
            id="99bd287c-5cdb-4100-8cc3-7f7e853678d1",
            node=investigation2,
            data=data,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """POST /data/{did}/bound_samples valid token."""
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """POST /data/{did}/bound_samples no token."""
        # The function tests that accessing a specific API endpoint without a token
        # returns a 401 Unauthorized status code.

        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """POST /data/{did}/bound_samples invalid token."""
        # The function tests for an invalid token by making a GET request to a
        # specific API endpoint and asserting that the response status code is 401
        # (Unauthorized).

        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """POST /data/{did}/bound_samples invalid scope."""
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_node_member(self):
        """POST /data/{did}/bound_samples not a member of the associated node."""
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample/node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """POST /data/{did}/bound_samples not a member of the associated workspace."""
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_no_parent_child_relationship(self):
        """POST /data/{did}/bound_samples direct parent-child relationship between datalink and sample."""
        data = dict(VALID_DATA_bound_sample)
        data["datalink"] = "99bd287c-5cdb-4100-8cc3-7f7e853678d1"
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "save_validation_error",
                "message": "The request body data are incorrect. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": "The sample and datalink node must have a direct parent-child relationship.",
                    },
                ],
            },
        )

    def test_data_not_part_of_datalink_data_hierarchy(self):
        """POST /data/{did}/bound_samples data is not part of the datalink data hierarchy."""
        response = self.post(
            "/api/data/5d942d10-6d4f-4145-9911-91baf4d7d1b5/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "save_validation_error",
                "message": "The request body data are incorrect. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "invalid",
                        "message": "The data must be part of the datalink's data hierarchy, and vice versa.",
                    },
                ],
            },
        )

    def test_member_role_does_not_allow_creation(self):
        """POST /data/{did}/bound_samples member's role does not allow creation."""
        response = self.post(
            "/api/data/5d942d10-6d4f-4145-9911-91baf4d7d1b5/bound_samples",
            token=VALID_TOKEN_5,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample/node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow creation.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        sample2 = SampleFactory(
            id="74baacd7-d237-46ff-8ebf-5c326f84794f",
            alias="s_2",
            title="sample_2",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample2,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """POST api/data/{did}/bound_samples with invalid workspace."""
        data = dict(VALID_DATA_bound_sample)
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_did(self):
        """POST api/data/{did}/bound_samples invalid uuid value."""
        data = dict(VALID_DATA_bound_sample)
        response = self.post(
            "/api/data/wrong_id/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_invalid_sample(self):
        """POST /api/data/{did}/bound_samples unknown sample."""
        data = dict(VALID_DATA_bound_sample)
        data["sample"] = "wrong_id"
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_invalid_datalink(self):
        """POST /api/data/{did}/bound_samples unknown datalink."""
        data = dict(VALID_DATA_bound_sample)
        data["datalink"] = "wrong_id"
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """POST /api/data/{did}/bound_samples with missing workspace."""
        data = dict(VALID_DATA_bound_sample)
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_param_did_missing(self):
        """POST /api/data/{did}/bound_samples did missing."""
        # The function tests for a missing parameter in the URL and asserts that the
        # response status code is 404.
        data = dict(VALID_DATA_bound_sample)
        response = self.post(
            "/api/data//bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_missing_sample(self):
        """POST /api/data/{did}/bound_samples with missing sample."""
        data = dict(VALID_DATA_bound_sample)
        data.pop("sample")
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_missing_datalink(self):
        """POST /api/data/{did}/bound_samples with missing datalink."""
        data = dict(VALID_DATA_bound_sample)
        data.pop("datalink")
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_create_bound_sample_with_unknown_field(self):
        """POST /api/data/{did}/bound_samples with unknown field."""
        data = dict(VALID_DATA_bound_sample)
        data.update({"fake_field": "error"})

        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """POST /api/data/{did}/bound_samples with unknown workspace."""
        data = dict(VALID_DATA_bound_sample)
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_data_did(self):
        """POST /api/data/{did}/bound_samples unknown did."""
        # The function tests for an invalid data ID and asserts that
        # the response status code is 404.
        response = self.post(
            "/api/data/af67c457-91d4-417b-90a9-c905f3b87511/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_DATA_bound_sample),
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_sample(self):
        """POST /api/data/{did}/bound_samples unknown sample."""
        # The function tests for an invalid sample ID and asserts that
        # the response status code is 404.
        data = dict(VALID_DATA_bound_sample)
        data["sample"] = "88b5ffc0-06bb-4cd9-8e7d-c594c4f074e2"
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "does_not_exist",
                        "message": '"88b5ffc0-06bb-4cd9-8e7d-c594c4f074e2" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_datalink(self):
        """POST /api/data/{did}/bound_samples unknown datalink."""
        # The function tests for an invalid datalink ID and asserts that
        # the response status code is 404.
        data = dict(VALID_DATA_bound_sample)
        data["datalink"] = "88b5ffc0-06bb-4cd9-8e7d-c594c4f074e2"
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "does_not_exist",
                        "message": '"88b5ffc0-06bb-4cd9-8e7d-c594c4f074e2" does not exist.',
                    },
                ],
            },
        )


class CreateBondSamples(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample1 = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data1 = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        data2 = DataFactory(
            id="e4c00037-d673-457b-9a6a-e729c31f353e",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        datalink2 = DatalinkFactory(
            id="da56138c-2555-4412-af94-35eb65712fb6",
            node=study,
            data=data2,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            sample=sample1,
            data=data2,
            datalink=datalink2,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_create_boundsample_for_data(self):
        """POST /data/{did}/bound_samples create bound_samples for a data."""
        data = dict(VALID_DATA_bound_sample)
        response = self.post(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 201
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_response = response.json()

        # bound_sample
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["sample"],
            {
                "id": "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
                "title": "sample_1",
                "node": "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            },
        )
        self.assertEqual(
            json_response["datalink"],
            "5048d194-4969-4de1-bd44-25c51a995dfb",
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])

    def test_create_boundsample_already_exists(self):
        """POST /data/{did}/bound_samples create bound_samples already exists."""
        data = {
            "sample": "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            "datalink": "da56138c-2555-4412-af94-35eb65712fb6",
        }
        response = self.post(
            "/api/data/e4c00037-d673-457b-9a6a-e729c31f353e/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_response = response.json()

        # bound_sample
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["sample"],
            {
                "id": "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
                "title": "sample_1",
                "node": "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            },
        )
        self.assertEqual(
            json_response["datalink"],
            "da56138c-2555-4412-af94-35eb65712fb6",
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
