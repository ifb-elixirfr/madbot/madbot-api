from rest_framework import status

from madbot_api.core import connector
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    ConnectionFactory,
    DataFactory,
    DatalinkFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    SampleBoundDataFactory,
    SampleFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

connector.reload_connectors()


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_url_exist(self):
        """GET /data/{did}/bound_samples exists."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
        )
        self.assertNotIn(response.status_code, [404, 405, 500])


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="write")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=seraphim,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=seraphim,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation1,
            role="owner",
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=demeter,
            last_edited_by=demeter,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=demeter,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=demeter,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=demeter,
            last_edited_by=demeter,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """GET /data/{did}/bound_samples valid token."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /data/{did}/bound_samples no token."""
        # The function tests that accessing a specific API endpoint without a token
        # returns a 401 Unauthorized status code.

        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /data/{did}/bound_samples invalid token."""
        # The function tests for an invalid token by making a GET request to a
        # specific API endpoint and asserting that the response status code is 401
        # (Unauthorized).

        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /data/{did}/bound_samples invalid scope."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_node_member(self):
        """GET /data/{did}/bound_samples not a member of the associated node."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "not_allowed",
                        "message": "You do not have access to this data.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """GET /data/{did}/bound_samples not a member of the associated workspace."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """GET api/data/{did}/bound_samples with invalid workspace."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_did(self):
        """GET api/data/{did}/bound_samples invalid uuid value."""
        response = self.get(
            "/api/data/wrong_id/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """GET /api/data/{did}/bound_samples with missing workspace."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/bound_samples",
            token=VALID_TOKEN_1,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_param_did_missing(self):
        """GET /api/data/{did}/bound_samples did missing."""
        # The function tests for a missing parameter in the URL and asserts that the
        # response status code is 404.

        response = self.get(
            "/api/data//bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """GET /api/data/{did}/bound_samples with unknown workspace."""
        response = self.get(
            "/api/data/af67c457-91d4-417b-90a9-c905f3b87511/bound_samples",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_data(self):
        """GET /api/data/{did}/bound_samples unknown nid."""
        # The function tests for an invalid data ID format and asserts that
        # the response status code is 404.

        response = self.get(
            "/api/data/af67c457-91d4-417b-90a9-c905f3b87511/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class GetBondSamples(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample1 = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        sample2 = SampleFactory(
            id="97de057f-84ef-4451-a95b-821f5d8a788b",
            alias="s_2",
            title="sample_2",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            sample=sample1,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            sample=sample2,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_get_boundsamples_of_data(self):
        """GET /data/{did}/bound_samples get bound_samples of a data."""
        response = self.get(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.check_pagination(response.json(), 2, None, None, 1, 1)
        json_response = response.json().get("results")

        # bound_sample_1
        bound_sample_1_json = json_response[0]
        self.assert_uuid(bound_sample_1_json["id"])
        self.assertEqual(
            bound_sample_1_json["sample"],
            {
                "id": "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
                "title": "sample_1",
                "node": "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            },
        )
        self.assertEqual(
            bound_sample_1_json["datalink"],
            "5048d194-4969-4de1-bd44-25c51a995dfb",
        )
        self.assertEqual(
            bound_sample_1_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            bound_sample_1_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(bound_sample_1_json["created_time"])
        self.assert_date(bound_sample_1_json["last_edited_time"])

        # bound_sample_2
        bound_sample_2_json = json_response[1]
        self.assert_uuid(bound_sample_2_json["id"])
        self.assertEqual(
            bound_sample_2_json["sample"],
            {
                "id": "97de057f-84ef-4451-a95b-821f5d8a788b",
                "title": "sample_2",
                "node": "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            },
        )
        self.assertEqual(
            bound_sample_2_json["datalink"],
            "5048d194-4969-4de1-bd44-25c51a995dfb",
        )
        self.assertEqual(
            bound_sample_2_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            bound_sample_2_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(bound_sample_2_json["created_time"])
        self.assert_date(bound_sample_2_json["last_edited_time"])
