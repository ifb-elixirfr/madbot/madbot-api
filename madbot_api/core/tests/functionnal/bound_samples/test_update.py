from rest_framework import status

from madbot_api.core import connector
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    ConnectionFactory,
    DataFactory,
    DatalinkFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    SampleBoundDataFactory,
    SampleFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    create_isa_collection,
    delete_applications,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"

connector.reload_connectors()


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        study = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation1,
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation1,
            created_by=persephone,
            last_edited_by=persephone,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="cda3c132-eb82-4174-8221-84cf1289f3e1",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        datalink = DatalinkFactory(
            id="5048d194-4969-4de1-bd44-25c51a995dfb",
            node=study,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        SampleBoundDataFactory(
            id="a594d485-d024-488c-9b0c-3cf4a848a7f9",
            sample=sample,
            data=data,
            datalink=datalink,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_exist(self):
        """PUT /data/{did}/bound_samples/{bsid} exists."""
        response = self.put(
            "/api/data/cda3c132-eb82-4174-8221-84cf1289f3e1/bound_samples/a594d485-d024-488c-9b0c-3cf4a848a7f9",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(
            response.json(),
            {
                "code": "unsupported_method",
                "message": "Method `PUT` not allowed.",
                "details": [],
            },
        )
