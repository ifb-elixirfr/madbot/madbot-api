# from unittest.mock import patch

# from django.core.management import call_command
# from django.test import modify_settings
# from rest_framework import status

# from madbot_api.core.tests.functionnal import (
#     MadbotTestCase,
#     build_accesstoken,
#     build_investigation,
#     build_tool,
#     build_user,
#     delete_accesstokens,
#     delete_applications,
#     delete_investigations,
#     delete_tools,
#     delete_users,
# )

# VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
# UNVALID_TOKEN = "PRdjwHUXPbnXNdjw3II8ofE9EOTIhv"
# VALID_TOKEN_INVALID_SCOPE = "SGZxItFXjFGN0giGdQT2oNjQXirBmC"
# FAKE_VAULT_ROLEID = "12345678-1234-1234-1234-1234567890ab"
# FAKE_VAULT_SECRETID = "87654321-4321-4321-4321-ba0987654321"


# class Exists(MadbotTestCase):
#     def test_exist(self):
#         """POST /api/datalinks/ exists"""

#         # Send a POST request to the specified endpoint
#         response = self.post("/api/datalinks/")

#         # asset that the response status code is not equal to 404, 405, or 500.
#         self.assertNotIn(
#             response.status_code,
#             [
#                 status.HTTP_404_NOT_FOUND,
#                 status.HTTP_405_METHOD_NOT_ALLOWED,
#                 status.HTTP_500_INTERNAL_SERVER_ERROR,
#             ],
#         )


# class Access(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         alice = build_user(username="alice")
#         build_accesstoken(token=VALID_TOKEN, user=alice, scope="write")
#         build_accesstoken(token=VALID_TOKEN_INVALID_SCOPE, user=alice, scope="read")

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()

#     def test_valid_token(self):
#         """POST /api/datalinks/ valid token"""

#         # Send a POST request to the specified endpoint
#         response = self.post("/api/datalinks/", token=VALID_TOKEN)

#         # Assert that the response status code is not 401 or 403
#         self.assertNotIn(
#             response.status_code,
#             [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
#         )

#     def test_no_token(self):
#         """POST /api/datalinks/ no token"""

#         # Send a POST request to the specified endpoint
#         response = self.post("/api/datalinks/")

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "Authentication credentials were not provided."},
#         )

#     def test_invalid_token(self):
#         """POST /api/datalinks/ invalid token"""

#         # Send a POST request to the specified endpoint
#         response = self.post("/api/datalinks/", token=UNVALID_TOKEN)

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "Authentication credentials were not provided."},
#         )

#     def test_invalid_scope(self):
#         """POST /api/datalinks/ invalid scope"""

#         # Send a POST request to the specified endpoint
#         response = self.post("/api/datalinks/", token=VALID_TOKEN_INVALID_SCOPE)

#         # Assert that the response status code is 403
#         self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "You do not have permission to perform this action."},
#         )


# class InvalidParams(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane")
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         build_tool(id=1, owner=jane, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()

#     def test_create_datalink_with_invalid_parent_type(self):
#         """POST /api/datalinks/ wrong parent_type"""
#         # Defines a test case for creating a datalink with an invalid parent_type.

#         # Prepare data for the request
#         data = {
#             "parent_type": "wrong_value",
#             "parent_id": 1,
#             "tool": 1,
#             "data_object_id": "root",
#             "data_object_type": "fake_type",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert that the response JSON matches the expected error structure
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "validation_error",
#                 "message": "Invalid query parameters. Check the `details` property for more details.",
#                 "details": None,
#             },
#         )

#     def test_create_datalink_with_invalid_tool(self):
#         """POST /api/datalinks/ invalid tool"""

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "parent_id": 1,
#             "tool": 3,
#             "data_object_id": "invalid_value",
#             "data_object_type": "fake_type",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 404
#         self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

#     @modify_settings(
#         INSTALLED_APPS={
#             "append": "madbot_api.core.tests.fake_connector",
#         }
#     )
#     @patch.multiple(
#         "madbot_api.core.views.utils.vault.settings",
#         MADBOT_VAULT_URL="https://fakevault.fr",
#         MADBOT_VAULT_ROLEID=FAKE_VAULT_ROLEID,
#         MADBOT_VAULT_SECRETID=FAKE_VAULT_SECRETID,
#         MADBOT_VAULT_MOUNT_POINT="madbot",
#     )
#     @patch("madbot_api.core.views.utils.vault.hvac.Client")
#     def test_create_datalink_with_invalid_data_object_type(self, mock_hvac_client):
#         """POST /api/datalinks/ invalid data_object_type"""

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "parent_id": 1,
#             "tool": 1,
#             "data_object_id": "root",
#             "data_object_type": "invalid_value",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "connector_error",
#                 "message": "An error occurred in the connector. Check the `details` property for more details.",
#                 "details": [
#                     {
#                         "resource": "FakeConnector",
#                         "message": "Unknown error: KeyError:'root'",
#                         "code": "unknown_error",
#                     }
#                 ],
#             },
#         )


# class MissingParams(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane")
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         build_tool(id=1, owner=jane, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()

#     def test_create_datalink_with_empty_data(self):
#         """POST /api/datalinks/ empty data"""

#         # Prepare data for the request
#         data = {}

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#     def test_create_datalink_with_data_is_none(self):
#         """POST /api/datalinks/ data is None"""

#         # Prepare data for the request
#         data = None

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#     def test_create_investigation_with_no_parent_type(self):
#         """POST /api/datalinks/ no parent_type"""

#         # Prepare data for the request
#         data = {
#             "parent_id": 1,
#             "tool": 1,
#             "data_object_id": "root",
#             "data_object_type": "fake_type",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "missing_body_param",
#                 "message": "Missing body parameter: parent_type.",
#                 "details": None,
#             },
#         )

#     def test_create_investigation_with_no_parent_id(self):
#         """POST /api/datalinks/ no parent_id"""

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "tool": 1,
#             "data_object_id": "root",
#             "data_object_type": "fake_type",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "missing_body_param",
#                 "message": "Missing body parameter: parent_id.",
#                 "details": None,
#             },
#         )

#     def test_create_investigation_with_no_tool(self):
#         """POST /api/datalinks/ no tool"""

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "parent_id": 1,
#             "data_object_id": "root",
#             "data_object_type": "fake_type",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "missing_body_param",
#                 "message": "Missing body parameter: tool.",
#                 "details": None,
#             },
#         )

#     def test_create_investigation_with_no_data_object_id(self):
#         """POST /api/datalinks/ no data_object_id"""

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "parent_id": 1,
#             "tool": 1,
#             "data_object_type": "fake_type",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "missing_body_param",
#                 "message": "Missing body parameter: data_object_id.",
#                 "details": None,
#             },
#         )

#     def test_create_investigation_with_no_data_object_type(self):
#         """POST /api/datalinks/ no data_object_type"""

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "parent_id": 1,
#             "tool": 1,
#             "data_object_id": "root",
#             "owner": 1,
#         }

#         # Send a POST request to the specified endpoint with the provided data
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "missing_body_param",
#                 "message": "Missing body parameter: data_object_type.",
#                 "details": None,
#             },
#         )


# class Create(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane", id=1)
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         build_tool(id=1, owner=jane, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()

#     @patch.multiple(
#         "madbot_api.core.views.utils.vault.settings",
#         MADBOT_VAULT_URL="https://fakevault.fr",
#         MADBOT_VAULT_ROLEID=FAKE_VAULT_ROLEID,
#         MADBOT_VAULT_SECRETID=FAKE_VAULT_SECRETID,
#         MADBOT_VAULT_MOUNT_POINT="madbot",
#     )
#     @patch("madbot_api.core.views.utils.vault.hvac.Client")
#     @modify_settings(
#         INSTALLED_APPS={
#             "append": "madbot_api.core.tests.fake_connector",
#         }
#     )
#     def test_create_datalink_success(self, mock_hvac_client):
#         """POST /api/datalinks/ with valid data"""
#         # The function tests the successful creation of a datalink.

#         # Prepare data for the request
#         data = {
#             "parent_type": "investigation",
#             "parent_id": 1,
#             "tool": 1,
#             "data_object_id": "container_1",
#             "data_object_type": "container",
#             "owner": 1,
#         }

#         # Send a POST request to create a datalink
#         response = self.post(
#             "/api/datalinks/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#         # Assert the response JSON matches the expected structure
#         self.assertEqual(
#             response.json(),
#             {
#                 "id": 1,
#                 "name": "Container 1",
#                 "tool": 1,
#                 "dataobject_id": "container_1",
#                 "dataobject_type": "container",
#                 "remote_url": None,
#                 "size": None,
#                 "icon": "fas fa-folder",
#                 "has_children": False,
#                 "owner": 1,
#             },
#         )
