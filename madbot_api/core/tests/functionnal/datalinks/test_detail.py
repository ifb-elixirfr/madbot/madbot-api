# from django.core.management import call_command
# from rest_framework import status

# from madbot_api.core.tests.functionnal import (
#     MadbotTestCase,
#     build_accesstoken,
#     build_datalink,
#     build_investigation,
#     build_tool,
#     build_user,
#     delete_accesstokens,
#     delete_applications,
#     delete_datalinks,
#     delete_investigations,
#     delete_tools,
#     delete_users,
# )

# VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
# UNVALID_TOKEN = "PRdjwHUXPbnXNdjw3II8ofE9EOTIhv"
# VALID_TOKEN_INVALID_SCOPE = "SGZxItFXjFGN0giGdQT2oNjQXirBmC"


# class Exists(MadbotTestCase):
#     def test_exist(self):
#         """GET /api/datalinks/{did}/ exists"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/1/")

#         # asset that the response status code is not equal to 404, 405, or 500.
#         self.assertNotIn(
#             response.status_code,
#             [
#                 status.HTTP_404_NOT_FOUND,
#                 status.HTTP_405_METHOD_NOT_ALLOWED,
#                 status.HTTP_500_INTERNAL_SERVER_ERROR,
#             ],
#         )


# class Access(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         alice = build_user(username="alice")
#         build_accesstoken(token=VALID_TOKEN, user=alice, scope="read")
#         build_accesstoken(token=VALID_TOKEN_INVALID_SCOPE, user=alice, scope="write")

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_datalinks()
#         delete_tools()

#     def test_valid_token(self):
#         """GET /api/datalinks/{did}/ valid token"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/1/", token=VALID_TOKEN)

#         self.assertNotIn(
#             response.status_code,
#             [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
#         )

#     def test_no_token(self):
#         """GET /api/datalinks/{did}/ no token"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/1/")

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(), {"detail": "Authentication credentials were not provided."}
#         )

#     def test_invalid_token(self):
#         """GET /api/datalinks/{did}/ invalid token"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/1/", token=UNVALID_TOKEN)

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(), {"detail": "Authentication credentials were not provided."}
#         )

#     def test_invalid_scope(self):
#         """GET /api/datalinks/{did}/ invalid scope"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/1/", token=VALID_TOKEN_INVALID_SCOPE)

#         # Assert that the response status code is 403
#         self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "You do not have permission to perform this action."},
#         )


# class Detail(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(id=1, username="alice")
#         build_accesstoken(user=jane, token=VALID_TOKEN)
#         investigation = build_investigation(id=1, owner=jane)
#         tool = build_tool(id=1, owner=jane, investigation=investigation)
#         build_datalink(id=1, owner=jane, tool=tool, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_datalinks()
#         delete_tools()

#     def test_detail(self):
#         """GET /api/datalinks/{did}/ with valid data"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/1/", token=VALID_TOKEN)

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#         self.assertEqual(
#             response.json(),
#             {
#                 "id": 1,
#                 "name": "fake datalink",
#                 "tool": {
#                     "id": 1,
#                     "name": "fake_connector",
#                     "connector": "FakeConnector",
#                     "owner": 1,
#                     "investigation": 1,
#                 },
#                 "dataobject_id": "fake_type",
#                 "dataobject_type": "root",
#                 "remote_url": None,
#                 "size": None,
#                 "icon": None,
#                 "has_children": False,
#                 "owner": 1,
#             },
#         )
