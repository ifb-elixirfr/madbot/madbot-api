# from django.core.management import call_command
# from rest_framework import status

# from madbot_api.core.tests.functionnal import (
#     MadbotTestCase,
#     build_accesstoken,
#     build_assay,
#     build_datalink,
#     build_investigation,
#     build_study,
#     build_tool,
#     build_user,
#     delete_accesstokens,
#     delete_applications,
#     delete_assays,
#     delete_datalinks,
#     delete_investigations,
#     delete_studies,
#     delete_tools,
#     delete_users,
# )

# VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
# UNVALID_TOKEN = "PRdjwHUXPbnXNdjw3II8ofE9EOTIhv"
# VALID_TOKEN_INVALID_SCOPE = "SGZxItFXjFGN0giGdQT2oNjQXirBmC"


# class Exists(MadbotTestCase):
#     def test_exist(self):
#         """GET /api/datalinks/ exists"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/")

#         # asset that the response status code is not equal to 404, 405, or 500.
#         self.assertNotIn(
#             response.status_code,
#             [
#                 status.HTTP_404_NOT_FOUND,
#                 status.HTTP_405_METHOD_NOT_ALLOWED,
#                 status.HTTP_500_INTERNAL_SERVER_ERROR,
#             ],
#         )


# class Access(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         alice = build_user(username="alice")
#         build_accesstoken(token=VALID_TOKEN, user=alice, scope="read")
#         build_accesstoken(token=VALID_TOKEN_INVALID_SCOPE, user=alice, scope="write")

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()
#         delete_datalinks()

#     def test_valid_token(self):
#         """GET /api/datalinks/ valid token"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/", token=VALID_TOKEN)

#         # Assert that the response status code is not 401 or 403
#         self.assertNotIn(
#             response.status_code,
#             [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
#         )

#     def test_no_token(self):
#         """GET /api/datalinks/ no token"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/")

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(), {"detail": "Authentication credentials were not provided."}
#         )

#     def test_invalid_token(self):
#         """GET /api/datalinks/ invalid token"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/", token=UNVALID_TOKEN)

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(), {"detail": "Authentication credentials were not provided."}
#         )

#     def test_invalid_scope(self):
#         """GET /api/datalinks/ invalid scope"""

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/", token=VALID_TOKEN_INVALID_SCOPE)

#         # Assert that the response status code is 403
#         self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "You do not have permission to perform this action."},
#         )


# class MissingParams(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane")
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         tool = build_tool(id=1, owner=jane, investigation=investigation)
#         build_datalink(id=1, owner=jane, tool=tool, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()

#     def test_detail_missing_query_params(self):
#         """GET /api/datalinks/ missing query params"""
#         # The function tests the functionality to retrieve a list of datalinks.

#         # Send a GET request to the specified endpoint
#         response = self.get("/api/datalinks/", token=VALID_TOKEN)

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

#         # Assert the response JSON contains expected error details
#         self.assertEqual(
#             response.json(),
#             {
#                 "code": "validation_error",
#                 "message": "Invalid query parameters. Check the `details` property for more details.",
#                 "details": [
#                     {
#                         "resource": "Datalink",
#                         "message": "Parent type not provided",
#                         "field": "parent_type",
#                         "code": "required",
#                     },
#                     {
#                         "resource": "Datalink",
#                         "message": "Parent id not provided",
#                         "field": "parent_id",
#                         "code": "required",
#                     },
#                 ],
#             },
#         )


# class List(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         alice = build_user(id=1, username="alice")
#         build_accesstoken(token=VALID_TOKEN, user=alice, scope="read write")
#         investigation = build_investigation(id=1, owner=alice)
#         study = build_study(id=1, investigation=investigation)
#         build_assay(id=1, study=study)
#         tool = build_tool(id=1, owner=alice, investigation=investigation)
#         build_datalink(id=1, owner=alice, tool=tool, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_datalinks()
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_studies()
#         delete_assays()
#         delete_tools()

#     def test_list_datalinks_with_investigation(self):
#         """GET /api/datalinks/ list all the datalinks with parent_type investigation"""
#         # The test method for listing datalinks with a specific investigation parent

#         # Send a GET request to the specified endpoint
#         response = self.get(
#             "/api/datalinks/?parent_type=investigation&parent_id=1", token=VALID_TOKEN
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#         # Assert that the response JSON matches the expected structure for the retrieved datalinks
#         self.assertEqual(
#             response.json(),
#             [
#                 {
#                     "id": 1,
#                     "name": "fake datalink",
#                     "tool": {
#                         "id": 1,
#                         "name": "fake_connector",
#                         "connector": "FakeConnector",
#                         "owner": 1,
#                         "investigation": 1,
#                     },
#                     "dataobject_id": "fake_type",
#                     "dataobject_type": "root",
#                     "remote_url": None,
#                     "size": None,
#                     "icon": None,
#                     "has_children": False,
#                     "owner": 1,
#                 },
#             ],
#         )

#     def test_list_datalinks_with_study(self):
#         """GET /api/datalinks/ list datalinks with a parent_type study"""
#         # The test method for listing datalinks with a specific study parent

#         # Send a GET request to the specified endpoint
#         response = self.get(
#             "/api/datalinks/?parent_type=study&parent_id=1", token=VALID_TOKEN
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_list_datalinks_with_assay(self):
#         """GET /api/datalinks/ list datalinks with a parent_type assay"""
#         # The test method for listing datalinks with a specific assay parent

#         # Send a GET request to the specified endpoint
#         response = self.get(
#             "/api/datalinks/?parent_type=assay&parent_id=1", token=VALID_TOKEN
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
