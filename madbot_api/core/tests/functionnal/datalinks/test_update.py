# from django.core.management import call_command
# from rest_framework import status

# from madbot_api.core.tests.functionnal import (
#     MadbotTestCase,
#     build_accesstoken,
#     build_datalink,
#     build_investigation,
#     build_tool,
#     build_user,
#     delete_accesstokens,
#     delete_applications,
#     delete_datalinks,
#     delete_investigations,
#     delete_tools,
#     delete_users,
# )

# VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
# UNVALID_TOKEN = "PRdjwHUXPbnXNdjw3II8ofE9EOTIhv"
# VALID_TOKEN_INVALID_SCOPE = "SGZxItFXjFGN0giGdQT2oNjQXirBmC"


# class Exists(MadbotTestCase):
#     def test_exist(self):
#         """PUT /api/datalinks/{did}/ exists"""

#         # Send a PUT request to the specified endpoint
#         response = self.put("/api/datalinks/1/")

#         # asset that the response status code is not equal to 404, 405, or 500.
#         self.assertNotIn(
#             response.status_code,
#             [
#                 status.HTTP_404_NOT_FOUND,
#                 status.HTTP_405_METHOD_NOT_ALLOWED,
#                 status.HTTP_500_INTERNAL_SERVER_ERROR,
#             ],
#         )


# class Access(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         alice = build_user(username="alice")
#         build_accesstoken(token=VALID_TOKEN, user=alice, scope="write")
#         build_accesstoken(token=VALID_TOKEN_INVALID_SCOPE, user=alice, scope="read")

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()
#         delete_datalinks()

#     def test_valid_token(self):
#         """PUT /api/datalinks/{did}/ valid token"""

#         # Send a PUT request to the specified endpoint
#         response = self.put("/api/datalinks/1/", token=VALID_TOKEN)

#         # Assert that the response status code is not 401 or 403
#         self.assertNotIn(
#             response.status_code,
#             [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
#         )

#     def test_no_token(self):
#         """PUT /api/datalinks/{did}/ no token"""

#         # Send a PUT request to the specified endpoint
#         response = self.put("/api/datalinks/1/")

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "Authentication credentials were not provided."},
#         )

#     def test_invalid_token(self):
#         """PUT /api/datalinks/{did}/ invalid token"""

#         # Send a PUT request to the specified endpoint
#         response = self.put("/api/datalinks/1/", token=UNVALID_TOKEN)

#         # Assert that the response status code is 401
#         self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "Authentication credentials were not provided."},
#         )

#     def test_invalid_scope(self):
#         """PUT /api/datalinks/{did}/ invalid scope"""

#         # Send a PUT request to the specified endpoint
#         response = self.put("/api/datalinks/1/", token=VALID_TOKEN_INVALID_SCOPE)

#         # Assert that the response status code is 403
#         self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

#         # Assert that the response JSON includes the expected authentication error detail.
#         self.assertEqual(
#             response.json(),
#             {"detail": "You do not have permission to perform this action."},
#         )


# class InvalidParams(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane")
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         tool = build_tool(id=1, owner=jane, investigation=investigation)
#         build_datalink(id=1, owner=jane, tool=tool, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()
#         delete_datalinks()

#     def test_update_with_wrong_did(self):
#         """PUT /api/datalinks/{did}/ wrong did"""

#         # Send a PUT request to the specified endpoint
#         response = self.put(
#             "/api/datalinks/wrong_did",
#             token=VALID_TOKEN,
#         )
#         # Assert that the response status code is 404
#         self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


# class MissingParams(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane")
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         tool = build_tool(id=1, owner=jane, investigation=investigation)
#         build_datalink(id=1, owner=jane, tool=tool, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()
#         delete_datalinks()

#     def test_update_body_param_is_none(self):
#         """PUT /api/datalinks/{did}/ body params is None"""

#         # Prepare data for the request
#         data = None

#         # Send a PUT request to the specified endpoint
#         response = self.put(
#             "/api/datalinks/1/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 400
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


# class Update(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         jane = build_user(username="jane", id=1)
#         build_accesstoken(token=VALID_TOKEN, user=jane)
#         investigation = build_investigation(id=1, owner=jane)
#         tool = build_tool(id=1, owner=jane, investigation=investigation)
#         build_datalink(id=1, owner=jane, tool=tool, investigation=investigation)

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_applications()
#         delete_users()
#         delete_tools()
#         delete_datalinks()

#     def test_update_datalink_success(self):
#         """PUT /api/datalinks/{did}/ update the name of the datalink"""
#         # The function tests the successful deletion of a datalink using a PUT
#         # request.

#         # Prepare data for the request
#         data = {
#             "id": 1,
#             "name": "unreal datalink",
#             "tool": 1,
#             "dataobject_id": "fake_type",
#             "dataobject_type": "root",
#             "owner": 1,
#         }

#         # Send a PUT request to the specified endpoint
#         response = self.put(
#             "/api/datalinks/1/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#         # Assert that the response JSON matches the expected structure
#         self.assertEqual(
#             response.json(),
#             {
#                 "id": 1,
#                 "name": "unreal datalink",
#                 "tool": 1,
#                 "dataobject_id": "fake_type",
#                 "dataobject_type": "root",
#                 "remote_url": None,
#                 "size": None,
#                 "icon": None,
#                 "has_children": False,
#                 "owner": 1,
#             },
#         )

#     def test_update_datalink_success_no_name(self):
#         """PUT /api/datalinks/{did}/ update dataobject_id with no body param name"""

#         # Prepare data for the request
#         data = {
#             "id": 1,
#             "tool": 1,
#             "dataobject_id": "other_fake_type",
#             "dataobject_type": "root",
#             "owner": 1,
#         }

#         # Send a PUT request to the specified endpoint
#         response = self.put(
#             "/api/datalinks/1/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#         # Assert that the response JSON matches the expected structure
#         self.assertEqual(
#             response.json(),
#             {
#                 "id": 1,
#                 "name": "fake datalink",
#                 "tool": 1,
#                 "dataobject_id": "other_fake_type",
#                 "dataobject_type": "root",
#                 "remote_url": None,
#                 "size": None,
#                 "icon": None,
#                 "has_children": False,
#                 "owner": 1,
#             },
#         )

#     def test_update_body_param_is_empty(self):
#         """PUT /api/datalinks/{did}/ update nothing"""

#         # Prepare data for the request
#         data = {}

#         # Send a PUT request to the specified endpoint
#         response = self.put(
#             "/api/datalinks/1/",
#             token=VALID_TOKEN,
#             data=data,
#             content_type="application/json",
#         )

#         # Assert that the response status code is 200
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#         # Assert that the response JSON matches the expected structure
#         self.assertEqual(
#             response.json(),
#             {
#                 "id": 1,
#                 "name": "fake datalink",
#                 "tool": 1,
#                 "dataobject_id": "fake_type",
#                 "dataobject_type": "root",
#                 "remote_url": None,
#                 "size": None,
#                 "icon": None,
#                 "has_children": False,
#                 "owner": 1,
#             },
#         )
