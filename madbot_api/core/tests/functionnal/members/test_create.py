from django.test import override_settings
from rest_framework import status

from madbot_api.core import models
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443A"
VALID_TOKEN_2 = "fOY2dYtRHp2PujfMkL2NIK5ocs442T"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_MEMBER = {"user": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", "role": "manager"}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_members()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_exist(self):
        """POST /api/nodes/{nid}/members."""
        response = self.post("/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                status.HTTP_405_METHOD_NOT_ALLOWED,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    data = {"username": "alice", "role": "owner"}

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice, scope="write")
        AccessTokenFactory(token=VALID_TOKEN_2, user=alice, scope="read")
        aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=aerith)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_members()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """POST /nodes/{nid}/members valid token."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """POST /nodes/{nid}/members no token."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """POST /nodes/{nid}/members invalid token."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """POST /nodes/{nid}/members invalid scope."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """POST /nodes/{nid}/members not a member of the given node."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")

        alice = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        john = UserFactory(username="john")
        AccessTokenFactory(token=VALID_TOKEN_2, user=john)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=john, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """POST api/nodes/{nid}/members/ with invalid workspace."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """POST api/nodes/{nid}/members/ invalid uuid value."""
        response = self.post(
            "/api/nodes/wrong_id/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_with_unknown_field(self):
        """POST api/nodes/{nid}/members/ with unknown field."""
        data = dict(VALID_MEMBER)
        data.update({"fake_field": "error"})

        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )

    def test_body_param_user_invalid_format(self):
        """POST api/nodes/{nid}/members/ invalid body user uuid value."""
        data = dict(VALID_MEMBER)
        data.update({"user": "wrong_uuid"})
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "invalid",
                        "message": '"wrong_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_body_param_unknown_user(self):
        """POST api/nodes/{nid}/members/ invalid body param user unknown value."""
        data = dict(VALID_MEMBER)
        data.update({"user": "9000c0a0-7000-400f-b0dc-d0000aa00bdf"})
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )

    def test_body_param_unknown_role(self):
        """POST api/nodes/{nid}/members/ invalid body param role value."""
        data = dict(VALID_MEMBER)
        data.update({"role": "wrong_role"})
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "invalid_choice",
                        "message": '"wrong_role" is not a valid choice.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="claire",
        )
        alice = UserFactory(id="302f9aa3-3c85-4b56-9574-ae97e739863f", username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        john = UserFactory(username="john")
        AccessTokenFactory(token=VALID_TOKEN_2, user=john)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=john, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """POST api/nodes/{nid}/members/ with missing workspace."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_body_param_user_missing(self):
        """POST api/nodes/{nid}/members/ missing body parameter ."""
        data = dict(VALID_MEMBER)
        data.pop("user")

        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_body_param_role_missing(self):
        """POST api/nodes/{nid}/members/ missing body role parameter."""
        data = dict(VALID_MEMBER)
        data.pop("role")
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_body_param_is_empty(self):
        """POST api/nodes/{nid}/members/ body is empty."""
        data = {}
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "required",
                        "message": "This field is required.",
                    },
                    {
                        "parameter": "role",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        AccessTokenFactory(token=VALID_TOKEN_1, user=claire)

        alice = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="alice")

        john = UserFactory(username="john")
        AccessTokenFactory(token=VALID_TOKEN_2, user=john)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=john, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_workspace(self):
        """POST api/nodes/{nid}/members with unknown workspace."""
        response = self.post(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/members",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_nid_node(self):
        """POST api/nodes/{nid}/members unknown node."""
        data = dict(VALID_MEMBER)
        response = self.post(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted roles."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")

        tom = UserFactory(
            id="d846139a-93df-4388-b0b0-3e7dab048bac",
            first_name="tom",
            last_name="tammy",
        )
        AccessTokenFactory(token=VALID_TOKEN_4, user=tom)
        john = UserFactory(
            id="c3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="John",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=john)
        anne = UserFactory(
            id="a78410f0-31a8-4992-b251-a5f2c1c88ffb",
            first_name="Anne",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=anne)
        alan = UserFactory(
            id="521a6f82-12f7-460d-87ad-c8433da0216d",
            first_name="Alan",
            last_name="Smithee",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=alan)
        dave = UserFactory(
            id="0159efbf-aec5-49e3-8067-1926f0c666a7",
            first_name="dave",
            last_name="valid",
        )

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=john,
        )
        WorkspaceMemberFactory(user=john, workspace=workspace)
        WorkspaceMemberFactory(user=anne, workspace=workspace)
        WorkspaceMemberFactory(user=alan, workspace=workspace)
        WorkspaceMemberFactory(user=dave, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        StudyNodeFactory(
            id="70b9c4cc-9b47-4b03-8621-599cf8b5ab1f",
            created_by=claire,
            parent=investigation,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="dbdc67f4-fc79-4ffa-b2c6-24bb60657891",
            user=john,
            node=investigation,
            role="owner",
        )
        NodeMemberFactory(
            id="2cf92620-b625-41c2-90bd-2bfde45a17d9",
            user=anne,
            node=investigation,
            role="manager",
        )
        NodeMemberFactory(
            id="ea9884ca-206e-483b-b089-b06efa099f53",
            user=alan,
            node=investigation,
            role="contributor",
        )
        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=john,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """POST api/nodes/{nid}/members/ access a node from another workspace."""
        response = self.post(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"user": "31303273-e201-4e63-be6e-ebbc8880c267", "role": "owner"},
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"cc7f17a7-01e0-4ef9-bae0-30d8610a3b18" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """POST api/nodes/{nid}/members/ user not a workspace member."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"user": "31303273-e201-4e63-be6e-ebbc8880c267", "role": "owner"},
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_create_member_owner_already_exist(self):
        """POST api/nodes/{nid}/members/ create member that already exists."""
        data = {"user": "c3e0e97d-9036-4b7b-a568-54929012c660", "role": "owner"}

        # Call the build_member API with an existing member
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert the response status code 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "save_validation_error",
                "message": "The request body data are incorrect. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": "User is already a member of this node.",
                    },
                ],
            },
        )

    def test_create_member_role_above_user(self):
        """POST api/nodes/{nid}/members create member with role above user."""
        # The function tests that a user with a manager role cannot add a member
        # with a higher role to an investigation.

        # Call the build_member API with manager role
        data = {"user": "0159efbf-aec5-49e3-8067-1926f0c666a7", "role": "owner"}
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert the response status code 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only add new members with a role below or equal to yours.",
                    },
                ],
            },
        )

    def test_create_member_not_member_parent_node(self):
        """POST api/nodes/{nid}/members create member that is not a member of the parent node."""
        # The function tests that a user with a manager role cannot add a member
        # with a higher role to an investigation.

        # Call the build_member API with manager role
        data = {"user": "0159efbf-aec5-49e3-8067-1926f0c666a7", "role": "owner"}
        response = self.post(
            "/api/nodes/70b9c4cc-9b47-4b03-8621-599cf8b5ab1f/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert the response status code 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "User must be member of the parent node to be added to a node.",
                    },
                ],
            },
        )

    def test_create_member_not_owner_or_manager(self):
        """POST /api/nodes/{nid}/members post without owner or manager role."""
        data = {"user": "0159efbf-aec5-49e3-8067-1926f0c666a7", "role": "owner"}
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You must be owner or manager of the node to add a new member.",
                    },
                ],
            },
        )


class Post(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")

        john = UserFactory(
            id="c3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="John",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=john)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=john, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        StudyNodeFactory(
            id="70b9c4cc-9b47-4b03-8621-599cf8b5ab1f",
            created_by=claire,
            parent=investigation,
        )
        NodeMemberFactory(
            id="dbdc67f4-fc79-4ffa-b2c6-24bb60657891",
            user=john,
            node=investigation,
            role="owner",
        )
        # automatically added to the study
        # NodeMemberFactory(
        #     id="936b37f1-c8a8-43d0-84ea-731f4769d6d3",
        #     user=john,
        #     node=study,
        #     role="owner",
        # )

        alice = UserFactory(
            id="31303273-e201-4e63-be6e-ebbc8880c267",
            first_name="alice",
            last_name="valid",
        )
        bob = UserFactory(
            id="e50411fa-bdf4-4568-9d99-be80b14e357e",
            first_name="bob",
            last_name="valid",
        )
        sacha = UserFactory(
            id="477a2e43-ff75-4a3a-b2f2-d3a177d52523",
            first_name="sacha",
            last_name="valid",
        )
        cleo = UserFactory(
            id="e464faa9-9f78-4607-8fa5-57675cca2179",
            first_name="cleo",
            last_name="valid",
        )
        eva = UserFactory(
            id="b3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="eva",
            last_name="valid",
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=sacha, workspace=workspace)
        WorkspaceMemberFactory(user=cleo, workspace=workspace)
        WorkspaceMemberFactory(user=eva, workspace=workspace)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_member_as_owner(self):
        """POST api/nodes/{nid}/members create member with owner role."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"user": "31303273-e201-4e63-be6e-ebbc8880c267", "role": "owner"},
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "31303273-e201-4e63-be6e-ebbc8880c267",
                "username": "avalid",
                "first_name": "alice",
                "last_name": "valid",
                "email": "alice.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "owner",
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_member_as_manager(self):
        """POST api/nodes/{nid}/members create member with manager role."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"user": "e50411fa-bdf4-4568-9d99-be80b14e357e", "role": "manager"},
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "e50411fa-bdf4-4568-9d99-be80b14e357e",
                "username": "bvalid",
                "first_name": "bob",
                "last_name": "valid",
                "email": "bob.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "manager",
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_member_as_contributor(self):
        """POST api/nodes/{nid}/members create member with contributor role."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={
                "user": "477a2e43-ff75-4a3a-b2f2-d3a177d52523",
                "role": "contributor",
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "477a2e43-ff75-4a3a-b2f2-d3a177d52523",
                "username": "svalid",
                "first_name": "sacha",
                "last_name": "valid",
                "email": "sacha.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "contributor",
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_member_as_collaborator(self):
        """POST api/nodes/{nid}/members create member with collaborator role."""
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={
                "user": "e464faa9-9f78-4607-8fa5-57675cca2179",
                "role": "collaborator",
            },
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "e464faa9-9f78-4607-8fa5-57675cca2179",
                "username": "cvalid",
                "first_name": "cleo",
                "last_name": "valid",
                "email": "cleo.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "collaborator",
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_inheritance_of_member(self):
        """POST api/nodes/{nid}/members create member with owner role and see if it is inherited in children nodes."""
        # The function tests that a user with a manager role cannot add a member
        # with a higher role to an investigation.

        data = {"user": "b3e0e97d-9036-4b7b-a568-54929012c660", "role": "manager"}
        response = self.post(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert the response status code 201
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Assert the response content
        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "b3e0e97d-9036-4b7b-a568-54929012c660",
                "username": "evalid",
                "first_name": "eva",
                "last_name": "valid",
                "email": "eva.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "c3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "manager",
        )

        # Assert the user is also manager of the study
        child_node_member = models.NodeMember.objects.get(
            user__id="b3e0e97d-9036-4b7b-a568-54929012c660",
            node__id="70b9c4cc-9b47-4b03-8621-599cf8b5ab1f",
        )
        self.assertEqual(child_node_member.role, "manager")
