from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import NodeMember
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "PRdjwHUXPbnXNdjw3II8ofE9EOTIhv"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "UHUDIHQSUDHKQSUHDKUSQHDKUHVUkf"
VALID_TOKEN_5 = "LpvhInlNT3jJwVns6BDNca6tjTELJY"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_members()
        delete_workspaces()
        delete_workspace_members()

    def test_exist(self):
        """DELETE /api/nodes/{nid}/members/{mid} exists."""
        # The function tests if a certain response status code is not in a given
        # list.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertNotIn(response.status_code, [404, 405, 500])


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=aerith)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )
        NodeMemberFactory(
            user=bob,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_valid_token(self):
        """DELETE /nodes/{nid}/members/{mid} valid token."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """DELETE /api/nodes/{nid}/members/{mid} no token."""
        # The function tests for unauthorized access by sending a DELETE request to
        # a specific API endpoint and asserting that the response status code is
        # 401.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """DELETE /api/nodes/{nid}/members/{mid} invalid token."""
        # The function tests for an invalid token by sending a DELETE request to a
        # specific API endpoint and asserting that the response status code is 401.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """DELETE /nodes/{nid}/members/{mid} invalid scope."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """DELETE /nodes/{nid}/members/{mid} not a member of the given node."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=aerith)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """DELETE /api/nodes/{nid}/members/{mid} with missing workspace."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_param_both_missing(self):
        """DELETE /api/nodes/{nid}/members/{mid} parameters both missing."""
        # The function tests the case where both parameters are missing in a DELETE
        # request to a specific API endpoint.

        response = self.delete(
            "/api/nodes//members",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_param_nid_missing(self):
        """DELETE /api/nodes/{nid}/members/{mid} nid missing."""
        # The function tests for a missing parameter in the URL and asserts that the
        # response status code is 404.

        response = self.delete(
            "/api/nodes//members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_param_mid_missing(self):
        """DELETE /api/nodes/{nid}/members/{mid} mid missing."""
        # The function tests for a missing user identifier in the API endpoint for
        # deleting a member from a node.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """DELETE /api/nodes/{nid}/members/{mid} with invalid workspace."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """DELETE /api/nodes/{nid}/members/{mid} invalid nid."""
        # The function tests for an invalid node ID format and asserts that
        # the response status code is 400.

        response = self.delete(
            "/api/nodes/14/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"14" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_mid(self):
        """DELETE /api/nodes/{nid}/members/{mid} invalid mid."""
        # The function tests for an invalid member ID format and asserts that
        # the response status code is 404.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/14",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "invalid",
                        "message": '"14" is not a valid UUID.',
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_workspace(self):
        """DELETE /api/nodes/{nid}/members/{mid} with unknown workspace."""
        response = self.delete(
            "/api/nodes/af67c457-91d4-417b-90a9-c905f3b87511/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_param_unknown_nid(self):
        """DELETE /api/nodes/{nid}/members/{mid} unknown nid."""
        # The function tests for an invalid node ID format and asserts that
        # the response status code is 404.

        response = self.delete(
            "/api/nodes/af67c457-91d4-417b-90a9-c905f3b87511/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )

    def test_param_unknown_mid(self):
        """DELETE /api/nodes/{nid}/members/{mid} unknown mid."""
        # The function tests for an invalid member ID format and asserts that
        # the response status code is 404.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        tom = UserFactory(
            id="d846139a-93df-4388-b0b0-3e7dab048bac",
            first_name="tom",
            last_name="tammy",
        )
        AccessTokenFactory(token=VALID_TOKEN_5, user=tom)

        cls.claire = UserFactory(username="claire")
        rick = UserFactory(username="rick")
        AccessTokenFactory(token=VALID_TOKEN_1, user=cls.claire)
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=cls.claire,
        )
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=cls.claire,
        )
        WorkspaceMemberFactory(user=rick, workspace=workspace)

        create_isa_collection()
        investigation_1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=cls.claire,
            workspace=workspace,
        )
        investigation_2 = InvestigationNodeFactory(
            id="f228b2aa-202e-4b4f-9a95-8cd291b8a491",
            created_by=cls.claire,
            workspace=workspace,
        )
        InvestigationNodeFactory(
            id="2ad12d14-9bd0-4047-aac6-f8d1dfc36212",
            created_by=cls.claire,
            workspace=workspace,
        )
        cls.study_1 = StudyNodeFactory(
            id="9ee6f965-0483-4e6a-a302-e72954f4ea65",
            parent=investigation_1,
            created_by=cls.claire,
            workspace=workspace,
        )

        NodeMemberFactory(
            id="2f31f991-9bc6-415e-8765-3829d573fcc1",
            user=rick,
            node=investigation_1,
            role="owner",
        )
        NodeMemberFactory(
            id="37b6003f-9383-4771-949d-fa8b58da9577",
            user=rick,
            node=investigation_2,
            role="owner",
        )

        cls.alan = UserFactory(username="alan")
        AccessTokenFactory(token=VALID_TOKEN_4, user=cls.alan)
        WorkspaceMemberFactory(user=cls.alan, workspace=workspace)
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=cls.alan,
            node=investigation_1,
            role="owner",
        )

        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_2, user=alice)
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation_1,
            role="manager",
        )

        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_3, user=bob)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        NodeMemberFactory(
            id="263852c5-28dc-4714-9bd3-185c082638b0",
            user=bob,
            node=investigation_1,
            role="contributor",
        )
        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=cls.claire,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """DELETE /api/nodes/{nid}/members/{mid} access a node from another workspace."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/2f31f991-9bc6-415e-8765-3829d573fcc1",
            token=VALID_TOKEN_1,
            workspace="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """DELETE /api/nodes/{nid}/members/{mid} user not a workspace member."""
        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/2f31f991-9bc6-415e-8765-3829d573fcc1",
            token=VALID_TOKEN_5,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_delete_member_role_above_user(self):
        """DELETE /api/nodes/{nid}/members/{mid} delete member with role above user."""
        # The function tests the deletion of a member role above the user's role in
        # a node.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/2f31f991-9bc6-415e-8765-3829d573fcc1",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only remove a member with a role below or equal to yours.",
                    },
                ],
            },
        )

    def test_delete_member_only_owner(self):
        """DELETE /api/nodes/{nid}/members/{mid} delete only owner."""
        # The function tests the deletion of a member from a node,
        # specifically when the member is the owner and there are no other owners.

        member = NodeMember.objects.get(
            user=self.claire.id,
            node="2ad12d14-9bd0-4047-aac6-f8d1dfc36212",
        )
        response = self.delete(
            "/api/nodes/2ad12d14-9bd0-4047-aac6-f8d1dfc36212/members/" + str(member.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "This member cannot be removed because they are the last owner of the current node.",
                    },
                ],
            },
        )

    def test_delete_member_owner_parent_node(self):
        """DELETE /api/nodes/{nid}/members/{mid} delete owner of parent node."""
        # The function tests the deletion of a member from a node,
        # specifically when the member is the owner and there are no other owners.
        study_member = NodeMember.objects.get(user=self.alan, node=self.study_1)
        response = self.delete(
            "/api/nodes/9ee6f965-0483-4e6a-a302-e72954f4ea65/members/"
            + str(study_member.id),
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "This member cannot be removed because they are owner of the parent node.",
                    },
                ],
            },
        )

    def test_delete_member_not_owner_or_manager(self):
        """DELETE /api/nodes/{nid}/members/{mid} delete without owner or manager role."""
        # The function tests the deletion of a member from a node,
        # specifically when the member is the owner and there are no other owners.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You must be owner or manager of the node to remove another member.",
                    },
                ],
            },
        )


class Delete(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        rick = UserFactory(username="rick")
        AccessTokenFactory(token=VALID_TOKEN_1, user=rick)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=rick, workspace=workspace)

        create_isa_collection()
        investigation_1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )

        NodeMemberFactory(
            id="2f31f991-9bc6-415e-8765-3829d573fcc1",
            user=rick,
            node=investigation_1,
            role="owner",
        )

        jane = UserFactory(username="jane")
        WorkspaceMemberFactory(user=jane, workspace=workspace)
        NodeMemberFactory(
            id="eb08961a-1947-4a47-b269-fbcbe0e934aa",
            user=jane,
            node=investigation_1,
            role="collaborator",
        )

        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_3, user=bob)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        NodeMemberFactory(
            id="263852c5-28dc-4714-9bd3-185c082638b0",
            user=bob,
            node=investigation_1,
            role="contributor",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_delete_member(self):
        """DELETE /api/nodes/{nid}/members/{mid} delete member."""
        # The function tests the deletion of a member from a node and
        # asserts that the response status code is 204 and the response content is
        # as expected.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/eb08961a-1947-4a47-b269-fbcbe0e934aa",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 204
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Assert that the member is deleted
        self.assertFalse(NodeMember.objects.filter(user__username="jane").exists())

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_leave_node(self):
        """DELETE /api/nodes/{nid}/members/{mid} member leave the node."""
        # The function tests leaving a node by deleting themselves from the node.

        response = self.delete(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/263852c5-28dc-4714-9bd3-185c082638b0",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 204
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
