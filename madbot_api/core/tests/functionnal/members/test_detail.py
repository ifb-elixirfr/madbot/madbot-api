from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_members()
        delete_workspaces()
        delete_workspace_members()

    def test_url_exist(self):
        """GET /nodes/{nid}/members/{mid} exists."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
        )
        self.assertNotIn(response.status_code, [404, 405, 500])


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=aerith)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )
        NodeMemberFactory(
            user=bob,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_members()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """GET /nodes/{nid}/members/{mid} valid token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /nodes/{nid}/members no token."""
        # The function tests that accessing a specific API endpoint without a token
        # returns a 401 Unauthorized status code.

        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /nodes/{nid}/members invalid token."""
        # The function tests for an invalid token by making a GET request to a
        # specific API endpoint and asserting that the response status code is 401
        # (Unauthorized).

        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /nodes/{nid}/members/{mid} invalid scope."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """GET /nodes/{nid}/members/{mid} not a member of the given node."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )


class UnkownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_workspace(self):
        """GET /nodes/{nid}/members/{mid} with unknown workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )


class MissingParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """GET /nodes/{nid}/members/{mid} with invalid workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """GET /nodes/{nid}/members/{mid} with invalid workspace."""
        response = self.get(
            "/api/nodes/14/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """GET /nodes/{nid}/members/{mid} invalid nid."""
        # The function tests for an invalid node ID format and asserts that
        # the response status code is 400.

        response = self.get(
            "/api/nodes/14/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"14" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_mid(self):
        """GET /nodes/{nid}/members/{mid} invalid nid."""
        # The function tests for an invalid node ID format and asserts that
        # the response status code is 400.

        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/14",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "invalid",
                        "message": '"14" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_unknown_nid(self):
        """GET /nodes/{nid}/members/{mid} unknown nid."""
        response = self.get(
            "/api/nodes/af67c457-91d4-417b-90a9-c905f3b87511/members/9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )

    def test_param_unknown_mid(self):
        """GET /nodes/{nid}/members/{mid} unknown mid."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        tom = UserFactory(
            id="d846139a-93df-4388-b0b0-3e7dab048bac",
            first_name="tom",
            last_name="tammy",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=tom)
        john = UserFactory(
            id="c3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="John",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=john)
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=john,
        )
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=john, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="4d534374-babf-4fdb-a19a-dcbedac18278",
            user=john,
            node=investigation,
            role="owner",
        )
        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=john,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """GET /nodes/{nid}/members/{mid} access a node from another workspace."""
        response = self.get(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18/members/4d534374-babf-4fdb-a19a-dcbedac18278",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"cc7f17a7-01e0-4ef9-bae0-30d8610a3b18" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """GET /nodes/{nid}/members/{mid} user not a workspace member."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/4d534374-babf-4fdb-a19a-dcbedac18278",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class GetMembers(MadbotTestCase):
    """Test the endpoint to get members of a node."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        john = UserFactory(
            id="c3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="John",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=john)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=john, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="4d534374-babf-4fdb-a19a-dcbedac18278",
            user=john,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_get_members_of_node_mid(self):
        """GET /nodes/{nid}/members/{mid} get members of a node with mid."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/4d534374-babf-4fdb-a19a-dcbedac18278",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], "4d534374-babf-4fdb-a19a-dcbedac18278")
        self.assertEqual(
            json_response["user"],
            {
                "id": "c3e0e97d-9036-4b7b-a568-54929012c660",
                "username": "jdoe",
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
            },
        )
        self.assertEqual(json_response["role"], "owner")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
