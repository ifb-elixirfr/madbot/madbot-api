from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import NodeMember
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "PRdjwHUXPbnXNdjw3II8ofE9EOTIhv"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"
VALID_TOKEN_5 = "klsdufiodsoifuoisdufoiusdoiufo"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_MEMBER = {"role": "manager"}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_members()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_exist(self):
        """PUT /api/nodes/{nid}/members/{mid} exist."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                status.HTTP_405_METHOD_NOT_ALLOWED,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        jane = UserFactory(username="jane")
        AccessTokenFactory(token=VALID_TOKEN_2, user=jane, scope="read")
        aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=aerith)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=jane, workspace=workspace)
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )
        NodeMemberFactory(
            id="65c98324-4720-4769-9234-0ffb845d3496",
            user=jane,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_valid_token(self):
        """POST /nodes/{nid}/members valid token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """PUT /api/nodes/{nid}/members/{mid} no token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """PUT /api/nodes/{nid}/members/{mid} invalid token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """PUT /nodes/{nid}/members/{mid} invalid scope."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """PUT /nodes/{nid}/members/{mid} not a member of the given node."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        jane = UserFactory(username="jane")
        AccessTokenFactory(token=VALID_TOKEN_2, user=jane)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)
        WorkspaceMemberFactory(user=jane, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )
        NodeMemberFactory(
            id="65c98324-4720-4769-9234-0ffb845d3496",
            user=jane,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """PUT api/nodes/{nid}/members/{mid} with invalid workspace."""
        response = self.put(
            "/api/nodes/wrong_id/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_node_error_format(self):
        """PUT api/nodes/{nid}/members/{mid} invalid uuid value."""
        response = self.put(
            "/api/nodes/wrong_id/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_member_error_format(self):
        """PUT api/nodes/{nid}/members/{mid} invalid uuid value."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/wrong_id2",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "invalid",
                        "message": '"wrong_id2" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_body_param_role_error(self):
        """PUT api/nodes/{nid}/members/{mid} invalid body param role value."""
        data = dict(VALID_MEMBER)
        data.update({"role": "wrong_role"})
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "invalid_choice",
                        "message": '"wrong_role" is not a valid choice.',
                    },
                ],
            },
        )

    def test_param_with_unknown_field(self):
        """PUT api/nodes/{nid}/members/ with unknown field."""
        data = dict(VALID_MEMBER)
        data.update({"fake_field": "error"})

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        aerith = UserFactory(
            id="b3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="Aerith",
            last_name="Gainsborough",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=aerith)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=aerith,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_body_param_is_empty(self):
        """PUT api/nodes/{nid}/members/{mid} body is empty."""
        data = {}
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "b3e0e97d-9036-4b7b-a568-54929012c660",
                "username": "againsborough",
                "first_name": "Aerith",
                "last_name": "Gainsborough",
                "email": "aerith.gainsborough@example.com",
            },
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "b3e0e97d-9036-4b7b-a568-54929012c660"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "owner",
        )

    def test_missing_workspace(self):
        """PUT api/nodes/{nid}/members/{mid} with missing workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")

        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_members()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_workspace(self):
        """PUT api/nodes/{nid}/members/{mid} with unknown workspace."""
        response = self.put(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_nid_node(self):
        """PUT api/nodes/{nid}/members/{mid} unknown node."""
        data = dict(VALID_MEMBER)
        response = self.put(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/members/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_mid_member(self):
        """PUT api/nodes/{iid}/members/{mid} unknown member."""
        data = dict(VALID_MEMBER)
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/9000c0a0-7000-400f-b0dc-d0000aa00bdf",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted operations."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.claire = UserFactory(username="claire")

        tom = UserFactory(
            id="c2a04316-6610-40af-a08c-995116baad2a",
            first_name="tom",
            last_name="tammy",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=tom)
        cls.alice = UserFactory(
            id="31303273-e201-4e63-be6e-ebbc8880c267",
            first_name="alice",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=cls.claire)
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=cls.alice,
        )
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=cls.claire,
        )
        WorkspaceMemberFactory(user=cls.alice, workspace=workspace)

        create_isa_collection()
        investigation_1 = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=cls.claire,
            workspace=workspace,
        )
        investigation_2 = InvestigationNodeFactory(
            id="6a4f4c80-215c-4116-81f7-c7bb59d33fae",
            created_by=cls.claire,
            workspace=workspace,
        )
        InvestigationNodeFactory(
            id="2ad12d14-9bd0-4047-aac6-f8d1dfc36212",
            created_by=cls.claire,
            workspace=workspace,
        )
        cls.study = StudyNodeFactory(
            id="88ac08f2-e1f4-4aa3-ba0f-7102dd139655",
            parent=investigation_1,
            created_by=cls.claire,
            workspace=workspace,
        )

        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=cls.alice,
            node=investigation_1,
            role="owner",
        )
        NodeMemberFactory(
            id="2bf533ab-deb6-4e6d-b52f-5295854e6182",
            user=cls.alice,
            node=investigation_2,
            role="owner",
        )

        john = UserFactory(
            id="e50411fa-bdf4-4568-9d99-be80b14e357e",
            first_name="john",
            last_name="valid",
        )
        WorkspaceMemberFactory(user=john, workspace=workspace)
        NodeMemberFactory(
            id="ce6673fa-2ca5-447a-9e88-b475ca6ac5c9",
            user=john,
            node=investigation_1,
            role="owner",
        )

        lucie = UserFactory(
            id="477a2e43-ff75-4a3a-b2f2-d3a177d52523",
            first_name="lucie",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=lucie)
        WorkspaceMemberFactory(user=lucie, workspace=workspace)
        NodeMemberFactory(
            id="65c98324-4720-4769-9234-0ffb845d3496",
            user=lucie,
            node=investigation_1,
            role="collaborator",
        )

        bob = UserFactory(
            id="d846139a-93df-4388-b0b0-3e7dab048bac",
            first_name="bob",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_4, user=bob)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        NodeMemberFactory(
            id="0f4260bd-f8cd-4bc9-8162-1fcbab9edf14",
            user=bob,
            node=investigation_1,
            role="manager",
        )
        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=cls.alice,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """PUT api/nodes/{iid}/members/{mid} access a node from another workspace."""
        response = self.put(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18/members/65c98324-4720-4769-9234-0ffb845d3496",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"cc7f17a7-01e0-4ef9-bae0-30d8610a3b18" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """PUT api/nodes/{iid}/members/{mid} user not a workspace member."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/65c98324-4720-4769-9234-0ffb845d3496",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_update_member_owner_parent_node(self):
        """PUT api/nodes/{iid}/members/{mid} update member role but he is owner of the parent node."""
        data = dict(VALID_MEMBER)
        study_member = NodeMember.objects.get(user=self.alice, node=self.study)
        response = self.put(
            "/api/nodes/88ac08f2-e1f4-4aa3-ba0f-7102dd139655/members/"
            + str(study_member.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "The role of the node member cannot be changed because they are owner of the parent node.",
                    },
                ],
            },
        )

    def test_update_member_last_owner_node(self):
        """PUT api/nodes/{iid}/members/{mid} update member role but he is the last owner of the node."""
        data = dict(VALID_MEMBER)
        member = NodeMember.objects.get(
            user=self.claire,
            node="2ad12d14-9bd0-4047-aac6-f8d1dfc36212",
        )
        response = self.put(
            "/api/nodes/2ad12d14-9bd0-4047-aac6-f8d1dfc36212/members/" + str(member.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "The role of the node member cannot be changed because they are the last owner of the current node.",
                    },
                ],
            },
        )

    def test_update_member_from_role_above(self):
        """PUT api/nodes/{iid}/members/{mid} update a member whose role is above yours."""
        data = dict(VALID_MEMBER)
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/ce6673fa-2ca5-447a-9e88-b475ca6ac5c9",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only change a member with a role below or equal to yours.",
                    },
                ],
            },
        )

    def test_update_member_to_role_above(self):
        """PUT api/nodes/{iid}/members/{mid} update member to a role above yours."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/65c98324-4720-4769-9234-0ffb845d3496",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"role": "owner"},
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only promote a member to a role below or equal to yours.",
                    },
                ],
            },
        )

    def test_update_member_promote_own_role(self):
        """PUT api/nodes/{iid}/members/{mid} update member role to promote himself."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/0f4260bd-f8cd-4bc9-8162-1fcbab9edf14",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"role": "owner"},
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You cannot promote yourself.",
                    },
                ],
            },
        )

    def test_update_member_not_owner_or_manager(self):
        """PUT /api/nodes/{nid}/members/{mid} update without owner or manager role."""
        data = {"role": "owner"}
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/0f4260bd-f8cd-4bc9-8162-1fcbab9edf14",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You must be owner or manager of the node to edit other members.",
                    },
                ],
            },
        )


class Put(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        claire = UserFactory(username="claire")
        alice = UserFactory(
            id="31303273-e201-4e63-be6e-ebbc8880c267",
            first_name="alice",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=claire,
        )
        WorkspaceMemberFactory(user=alice, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=claire,
            workspace=workspace,
        )
        InvestigationNodeFactory(
            id="6a4f4c80-215c-4116-81f7-c7bb59d33fae",
            created_by=claire,
            workspace=workspace,
        )
        StudyNodeFactory(
            id="88ac08f2-e1f4-4aa3-ba0f-7102dd139655",
            parent=investigation,
            created_by=claire,
            workspace=workspace,
        )

        NodeMemberFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            user=alice,
            node=investigation,
            role="owner",
        )

        john = UserFactory(
            id="e50411fa-bdf4-4568-9d99-be80b14e357e",
            first_name="john",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=john)
        WorkspaceMemberFactory(user=john, workspace=workspace)
        NodeMemberFactory(
            id="ce6673fa-2ca5-447a-9e88-b475ca6ac5c9",
            user=john,
            node=investigation,
            role="owner",
        )

        lucie = UserFactory(
            id="477a2e43-ff75-4a3a-b2f2-d3a177d52523",
            first_name="lucie",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=lucie)
        WorkspaceMemberFactory(user=lucie, workspace=workspace)
        NodeMemberFactory(
            id="65c98324-4720-4769-9234-0ffb845d3496",
            user=lucie,
            node=investigation,
            role="collaborator",
        )

        bob = UserFactory(
            id="d846139a-93df-4388-b0b0-3e7dab048bac",
            first_name="bob",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_4, user=bob)
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        NodeMemberFactory(
            id="0f4260bd-f8cd-4bc9-8162-1fcbab9edf14",
            user=bob,
            node=investigation,
            role="manager",
        )

        eva = UserFactory(
            id="b3e0e97d-9036-4b7b-a568-54929012c660",
            first_name="eva",
            last_name="valid",
        )
        AccessTokenFactory(token=VALID_TOKEN_5, user=eva)
        WorkspaceMemberFactory(user=eva, workspace=workspace)
        NodeMemberFactory(
            id="4d59c421-d140-4daa-9ab2-bc1728449d3d",
            user=eva,
            node=investigation,
            role="collaborator",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_update_member(self):
        """PUT api/nodes/{iid}/members/{mid} update member role."""
        data = dict(VALID_MEMBER)
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/65c98324-4720-4769-9234-0ffb845d3496",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Assert the response content
        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "477a2e43-ff75-4a3a-b2f2-d3a177d52523",
                "username": "lvalid",
                "first_name": "lucie",
                "last_name": "valid",
                "email": "lucie.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "31303273-e201-4e63-be6e-ebbc8880c267"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "manager",
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_inheritance_of_member(self):
        """PUT api/nodes/{nid}/members/{mid} update member with owner role and see if it is inherited in children nodes."""
        data = {"role": "manager"}
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/4d59c421-d140-4daa-9ab2-bc1728449d3d",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert the response status code 201
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response content
        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["user"],
            {
                "id": "b3e0e97d-9036-4b7b-a568-54929012c660",
                "username": "evalid",
                "first_name": "eva",
                "last_name": "valid",
                "email": "eva.valid@example.com",
            },
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "31303273-e201-4e63-be6e-ebbc8880c267"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["role"],
            "manager",
        )

        # Assert the user is also manager of the study
        child_node_member = NodeMember.objects.get(
            user__id="b3e0e97d-9036-4b7b-a568-54929012c660",
            node__id="88ac08f2-e1f4-4aa3-ba0f-7102dd139655",
        )
        self.assertEqual(child_node_member.role, "manager")
