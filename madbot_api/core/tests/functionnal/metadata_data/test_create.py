from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    ConnectionFactory,
    DataFactory,
    DatalinkFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    MetadataFieldFactory,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_data,
    delete_metadata,
    delete_metadata_fields,
    delete_nodes,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443A"
VALID_TOKEN_2 = "fOY2dYtRHp2PujfMkL2NIK5ocs442T"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_METADATA = {
    "field": "9d845e32-0a36-4530-a95f-0f7566a03fdb",
    "value": "test_title1",
}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=persephone,
            workspace=workspace,
        )

        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_exist(self):
        """POST /data/{did}/metadata exists."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="read")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=seraphim,
            workspace=workspace,
        )

        connection = ConnectionFactory(
            workspace=workspace,
            created_by=seraphim,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
        )

        MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """POST /nodes/{nid}/metadata valid token."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """POST /nodes/{nid}/metadata no token."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """POST /nodes/{nid}/metadata invalid token."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """POST /nodes/{nid}/metadata invalid scope."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """POST /nodes/{nid}/metadata not a member of the given workspace."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """POST /data/{did}/metadata with invalid workspace."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_datalink(self):
        """POST /data/{did}/metadata with invalid datalink."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=wrong_id",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_did(self):
        """POST /data/{did}/metadata invalid did value."""
        response = self.post(
            "/api/data/wrong_id/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_with_invalid_field(self):
        """POST /data/{did}/metadata with invalid field."""
        data = dict(VALID_METADATA)
        data.update({"fake_field": "error"})

        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="some_field",
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """POST /data/{did}/metadata with missing workspace."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_missing_datalink(self):
        """POST /data/{did}/metadata with missing datalink."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_body_param_field_missing(self):
        """POST /data/{did}/metadata missing body parameter."""
        data = dict(VALID_METADATA)
        data.pop("field")

        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "field",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    # TODO: correct test
    # def test_body_param_value_missing(self):
    #     """POST /data/{did}/metadata missing body parameter."""

    #     data = dict(VALID_METADATA)
    #     data.pop("value")

    #     response = self.post(
    #         "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
    #         token=VALID_TOKEN_1,
    #         workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
    #         data=data,
    #     )
    #     # Assert that the response status code is 400
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    #     self.assertEqual(
    #         response.json(),
    #         {
    #             "code": "save_validation_error",
    #             "message": "The request body data are incorrect. Check the `details` property for more details.",
    #             "details": [
    #                 {
    #                     "parameter": "value",
    #                     "code": "invalid",
    #                     "message": "Only one of the 'value' or 'selected_value' can be None, not both.",
    #                 }
    #             ],
    #         },
    #     )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """POST /data/{did}/metadata with unknown workspace."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_param_unknown_datalink(self):
        """POST /data/{did}/metadata with unknown datalink."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=ba68296e-0f68-4c35-9494-786a8105cd24",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_data(self):
        """POST /data/{did}/metadata unknown data."""
        data = dict(VALID_METADATA)
        response = self.post(
            "/api/data/9000c0a0-7000-400f-b0dc-d0000aa00bdf/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )


class Post(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )

        demeter = UserFactory(
            id="2cf76a02-c0cd-41e4-9910-91eaaa0c13f4",
            username="demeter",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="collaborator",
        )
        MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_create_metadata(self):
        """POST /data/{did}/metadata create metadata."""
        response = self.post(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_METADATA),
        )

        # Assert that the response status code is 201
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["field"]["id"],
            "9d845e32-0a36-4530-a95f-0f7566a03fdb",
        )
        self.assertEqual(json_response["field"]["name"], "field1")
        self.assertIsInstance(json_response["field"]["description"], str)
        self.assertIsInstance(json_response["field"]["schema"], str)
        self.assertEqual(json_response["values"][0]["value"], "test_title1")
        self.assert_uuid(json_response["values"][0]["id"])
        self.assertEqual(
            json_response["values"][0]["created_by"],
            {"object": "MadbotUser", "id": "2cf76a02-c0cd-41e4-9910-91eaaa0c13f4"},
        )
        self.assert_date(json_response["values"][0]["created_time"])
        self.assert_date(json_response["values"][0]["last_edited_time"])
