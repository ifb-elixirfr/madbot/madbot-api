from rest_framework import status

from madbot_api.core import connector
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    ConnectionFactory,
    DataFactory,
    DatalinkFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    MetadataFactory,
    MetadataFieldFactory,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_data,
    delete_metadata,
    delete_metadata_fields,
    delete_nodes,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

connector.reload_connectors()


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=persephone,
            workspace=workspace,
        )

        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        metadata_field = MetadataFieldFactory(
            name="field1",
        )

        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            data=data,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_url_exist(self):
        """GET /data/{did}/metadata exists."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="write")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=seraphim,
            workspace=workspace,
        )

        connection = ConnectionFactory(
            workspace=workspace,
            created_by=seraphim,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
        )

        metadata_field = MetadataFieldFactory(
            name="field1",
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )

        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            data=data,
            field=metadata_field,
            value="test_title1",
            created_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """GET /data/{did}/metadata valid token."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /data/{did}/metadata no token."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /data/{did}/metadata invalid token."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /data/{did}/metadata invalid scope."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """GET /data/{did}/metadata not a member of the given workspace."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            data=data,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """GET api/data/{did}/metadata with invalid workspace."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_datalink(self):
        """GET api/data/{did}/metadata with invalid datalink."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=wrong_id",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_did(self):
        """GET api/data/{did}/metadata invalid did value."""
        response = self.get(
            "/api/data/wrong_id/metadata?datalink=af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        metadata_field = MetadataFieldFactory(
            name="some_field",
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            data=data,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """GET /api/data/{did}/metadata with missing workspace."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_missing_datalink(self):
        """GET /api/data/{did}/metadata with missing datalink."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_param_nid_missing(self):
        """GET /api/data/{did}/metadata nid missing."""
        response = self.get(
            "/api/data//metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_param_datalink_missing(self):
        """GET /api/data/{did}/metadata datalink missing."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )

        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            name="field",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            data=data,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """GET /api/data/{did}/metadata with unknown workspace."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_datalink(self):
        """GET /api/data/{did}/metadata with unknown datalink."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "datalink",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_data(self):
        """GET /api/data/{did}/metadata unknown did."""
        response = self.get(
            "/api/data/af67c457-91d4-417b-90a9-c905f3b87511/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "data",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class GetDataMetadata(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )

        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cee08a2e-63cb-4fb9-ba05-1d0c48b0e56f",
            created_by=persephone,
            workspace=workspace,
        )
        connection = ConnectionFactory(
            workspace=workspace,
            created_by=persephone,
        )
        data = DataFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            connection=connection,
            created_by=persephone,
            workspace=workspace,
        )
        DatalinkFactory(
            id="facf5cec-abee-4f78-b4da-562df8149b1a",
            node=investigation,
            data=data,
            created_by=persephone,
            last_edited_by=persephone,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="collaborator",
        )
        metadata_field = MetadataFieldFactory(
            id="051cc314-aa2f-46cc-a61c-68781a42477a",
            name="field",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            data=data,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )
        MetadataFactory(
            id="606a3021-732f-446c-8e92-779d5b18976b",
            data=data,
            field=metadata_field,
            value="test_title2",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_data()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_get_metadata_of_data_include_inherited(self):
        """GET /data/{did}/metadata get metadata of a data inherited included."""
        response = self.get(
            "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a&include_inherited=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json().get("results")

        # metadata_1
        metadata_1_json = json_response[0]
        self.assertEqual(
            metadata_1_json["field"]["id"],
            "051cc314-aa2f-46cc-a61c-68781a42477a",
        )
        self.assertEqual(metadata_1_json["field"]["name"], "field")
        self.assertIsInstance(metadata_1_json["field"]["description"], str)
        self.assertIsInstance(metadata_1_json["field"]["schema"], str)
        self.assertEqual(metadata_1_json["values"][0]["value"], "test_title1")
        self.assertEqual(
            metadata_1_json["values"][0]["id"],
            "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertEqual(
            metadata_1_json["values"][0]["source"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(
            metadata_1_json["values"][0]["source"]["object"],
            "Data",
        )
        self.assertTrue(metadata_1_json["values"][0]["selected"])
        self.assertEqual(
            metadata_1_json["values"][0]["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(metadata_1_json["values"][0]["created_time"])
        self.assert_date(metadata_1_json["values"][0]["last_edited_time"])

    # TODO: correct test
    # def test_get_metadata_of_data(self):
    #     """GET /data/{did}/metadata get metadata of a data"""

    #     response = self.get(
    #         "/api/data/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata?datalink=facf5cec-abee-4f78-b4da-562df8149b1a",
    #         token=VALID_TOKEN_1,
    #         workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
    #     )
    #     # Assert that the response status code is 200
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

    #     self.check_pagination(response.json(), 2, None, None, 1, 1)
    #     json_response = response.json().get("results")

    #     # metadata_1
    #     metadata_1_json = json_response[0]
    #     self.assertEqual(metadata_1_json["id"], "e2a8e605-2ab9-490b-a5c6-af1a80bf3260")
    #     self.assertEqual(
    #         metadata_1_json["field"]["id"], "051cc314-aa2f-46cc-a61c-68781a42477a"
    #     )
    #     self.assertEqual(metadata_1_json["field"]["name"], "field")
    #     self.assertIsInstance(metadata_1_json["field"]["description"], str)
    #     self.assertIsInstance(metadata_1_json["field"]["schema"], str)
    #     self.assertEqual(metadata_1_json["value"], "test_title1")
    #     self.assertEqual(
    #         metadata_1_json["created_by"],
    #         {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
    #     )
    #     self.assert_date(metadata_1_json["created_time"])
    #     self.assert_date(metadata_1_json["last_edited_time"])

    #     # metadata_2
    #     metadata_2_json = json_response[1]
    #     self.assertEqual(metadata_2_json["id"], "606a3021-732f-446c-8e92-779d5b18976b")
    #     self.assertEqual(
    #         metadata_2_json["field"]["id"], "051cc314-aa2f-46cc-a61c-68781a42477a"
    #     )
    #     self.assertEqual(metadata_2_json["field"]["name"], "field")
    #     self.assertIsInstance(metadata_2_json["field"]["description"], str)
    #     self.assertIsInstance(metadata_2_json["field"]["schema"], str)
    #     self.assertEqual(metadata_2_json["value"], "test_title2")
    #     self.assertEqual(
    #         metadata_2_json["created_by"],
    #         {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
    #     )
    #     self.assert_date(metadata_2_json["created_time"])
    #     self.assert_date(metadata_2_json["last_edited_time"])
