from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    MetadataFactory,
    MetadataFieldFactory,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_metadata,
    delete_metadata_fields,
    delete_nodes,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443A"
VALID_TOKEN_2 = "fOY2dYtRHp2PujfMkL2NIK5ocs442T"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_METADATA = {
    "value": "updated_value",
}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=persephone,
            workspace=workspace,
        )

        metadata_field = MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )

        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            node=investigation,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_exist(self):
        """PUT /api/nodes/{nid}/metadata/{mdid} exist."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter, scope="write")
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="read")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=seraphim,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            node=investigation,
            field=metadata_field,
            value="test_title1",
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """PUT /nodes/{nid}/metadata/{mdid} valid token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """PUT /nodes/{nid}/metadata/{mdid} no token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """PUT /nodes/{nid}/metadata/{mdid} invalid token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """PUT /nodes/{nid}/metadata/{mdid} invalid scope."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_node_member(self):
        """PUT /nodes/{nid}/metadata/{mdid} not a member of the given node."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """PUT /nodes/{nid}/metadata/{mdid} not a member of the given workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            node=investigation,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """PUT api/nodes/{nid}/metadata/{mdid} with invalid workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """PUT api/nodes/{nid}/metadata/{mdid} invalid uuid value."""
        response = self.put(
            "/api/nodes/wrong_id/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_mdid(self):
        """PUT api/nodes/{nid}/metadata/{mdid} invalid uuid value."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/wrong_id",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "metadata",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_with_unknown_field(self):
        """PUT api/nodes/{nid}/metadata/{mdid} with unknown field."""
        data = dict(VALID_METADATA)
        data.update({"fake_field": "error"})

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "message": "This field is not supported.",
                        "code": "not_supported",
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        hades = UserFactory(id="302f9aa3-3c85-4b56-9574-ae97e739863f", username="hades")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hades, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hades,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            node=investigation,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """PUT api/nodes/{nid}/metadata/{mdid} with missing workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        demeter = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="demeter",
        )

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            node=investigation,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """PUT api/nodes/{nid}/metadata/{mdid} with unknown workspace."""
        response = self.put(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_METADATA,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_node(self):
        """PUT api/nodes/{nid}/metadata/{mdid} unknown node."""
        data = dict(VALID_METADATA)
        response = self.put(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_metadata(self):
        """PUT /api/nodes/{nid}/metadata/{mdid} unknown mdid."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "metadata",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class Update(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="fe5f5c23-63d1-4440-b2c8-be51ccd57384",
            username="persephone",
        )
        demeter = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="demeter",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            id="9d845e32-0a36-4530-a95f-0f7566a03fdb",
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            node=investigation,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_update_metadata(self):
        """PUT api/nodes/{nid}/metadata/{mdid} update metadata."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_METADATA),
        )

        # Assert the response status code 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["field"]["id"],
            "9d845e32-0a36-4530-a95f-0f7566a03fdb",
        )
        self.assertEqual(json_response["field"]["name"], "field1")
        self.assertIsInstance(json_response["field"]["description"], str)
        self.assertIsInstance(json_response["field"]["schema"], str)
        self.assertEqual(
            json_response["values"][0]["id"],
            "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertEqual(json_response["values"][0]["value"], "updated_value")
        self.assertEqual(
            json_response["values"][0]["created_by"],
            {"object": "MadbotUser", "id": "fe5f5c23-63d1-4440-b2c8-be51ccd57384"},
        )
        self.assert_date(json_response["values"][0]["created_time"])
        self.assert_date(json_response["values"][0]["last_edited_time"])

    def test_update_metadata_no_body(self):
        """PUT api/nodes/{nid}/metadata/{mdid} update metadata with body parameter ."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert the response status code 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["field"]["id"],
            "9d845e32-0a36-4530-a95f-0f7566a03fdb",
        )
        self.assertEqual(json_response["field"]["name"], "field1")
        self.assertIsInstance(json_response["field"]["description"], str)
        self.assertIsInstance(json_response["field"]["schema"], str)
        self.assertEqual(
            json_response["values"][0]["id"],
            "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertEqual(json_response["values"][0]["value"], "test_title1")
        self.assertEqual(
            json_response["values"][0]["created_by"],
            {"object": "MadbotUser", "id": "fe5f5c23-63d1-4440-b2c8-be51ccd57384"},
        )
        self.assert_date(json_response["values"][0]["created_time"])
        self.assert_date(json_response["values"][0]["last_edited_time"])
