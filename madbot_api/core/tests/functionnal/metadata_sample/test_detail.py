from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    MetadataFactory,
    MetadataFieldFactory,
    NodeMemberFactory,
    SampleFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_metadata,
    delete_metadata_fields,
    delete_nodes,
    delete_samples,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="f6cefa09-e5cb-42f7-8684-2f9507110020",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )
        metadata_field = MetadataFieldFactory(
            name="field1",
        )

        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            field=metadata_field,
            value="test_title1",
            sample=sample,
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_samples()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_url_exist(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} exists."""
        response = self.get(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="write")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=seraphim,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="f6cefa09-e5cb-42f7-8684-2f9507110020",
            node=investigation,
            created_by=seraphim,
            last_edited_by=seraphim,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            name="field",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            sample=sample,
            field=metadata_field,
            value="test_title1",
            created_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_samples()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} valid token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} no token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} invalid token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} invalid scope."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_node_member(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} not a member of the given node."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} not a member of the given workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="f6cefa09-e5cb-42f7-8684-2f9507110020",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            sample=sample,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_samples()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} with invalid workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} invalid uuid value."""
        response = self.get(
            "/api/nodes/wrong_id/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_sid(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} invalid sid value."""
        response = self.get(
            "/api/nodes/wrong_id/samples/wrong_id/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_mdid(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} invalid mdid value."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/wrong_id",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "metadata",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="f6cefa09-e5cb-42f7-8684-2f9507110020",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            name="field",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            sample=sample,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_samples()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """GET //api/nodes/{nid}/samples/{sid}/metadata/{mdid} with missing workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_param_all_missing(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} parameters all missing."""
        response = self.get(
            "/api/nodes//samples//metadata/",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_param_nid_missing(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} nid missing."""
        response = self.get(
            "/api/nodes//samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_param_sid_missing(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} sid missing."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples//metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="f6cefa09-e5cb-42f7-8684-2f9507110020",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        metadata_field = MetadataFieldFactory(
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            sample=sample,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """GET //api/nodes/{nid}/samples/{sid}/metadata/{mdid} with unknown workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_node(self):
        """GET //api/nodes/{nid}/samples/{sid}/metadata/{mdid} unknown nid."""
        response = self.get(
            "/api/nodes/af67c457-91d4-417b-90a9-c905f3b87511/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_metadata(self):
        """GET //api/nodes/{nid}/samples/{sid}/metadata/{mdid} unknown mdid."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "metadata",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_sample(self):
        """GET //api/nodes/{nid}/samples/{sid}/metadata/{mdid} unknown sid."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/af67c457-91d4-417b-90a9-c905f3b87511/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class GetOneSampleMetadata(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )

        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        sample = SampleFactory(
            id="f6cefa09-e5cb-42f7-8684-2f9507110020",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="collaborator",
        )
        metadata_field = MetadataFieldFactory(
            id="051cc314-aa2f-46cc-a61c-68781a42477a",
            name="field1",
        )
        MetadataFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            sample=sample,
            field=metadata_field,
            value="test_title1",
            created_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_metadata()
        delete_samples()
        delete_nodes()
        delete_metadata_fields()
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_get_metadata(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} get metadata of a sample."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["field"]["id"],
            "051cc314-aa2f-46cc-a61c-68781a42477a",
        )
        self.assertEqual(json_response["field"]["name"], "field1")
        self.assertIsInstance(json_response["field"]["description"], str)
        self.assertIsInstance(json_response["field"]["schema"], str)
        self.assertEqual(
            json_response["values"]["id"],
            "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertEqual(json_response["values"]["value"], "test_title1")
        self.assertEqual(
            json_response["values"]["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(json_response["values"]["created_time"])
        self.assert_date(json_response["values"]["last_edited_time"])

    def test_get_metadata_include_inherited(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} get metadata of a sample including the inherited."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260?include_inherited=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["field"]["id"],
            "051cc314-aa2f-46cc-a61c-68781a42477a",
        )
        self.assertEqual(json_response["field"]["name"], "field1")
        self.assertIsInstance(json_response["field"]["description"], str)
        self.assertIsInstance(json_response["field"]["schema"], str)
        self.assertEqual(
            json_response["values"][0]["id"],
            "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertEqual(json_response["values"][0]["value"], "test_title1")
        self.assertEqual(
            json_response["values"][0]["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(json_response["values"][0]["created_time"])
        self.assert_date(json_response["values"][0]["last_edited_time"])

    def test_get_metadata_including_history(self):
        """GET /api/nodes/{nid}/samples/{sid}/metadata/{mdid} get metadata of a sample with history."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/f6cefa09-e5cb-42f7-8684-2f9507110020/metadata/e2a8e605-2ab9-490b-a5c6-af1a80bf3260?history=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["field"]["id"],
            "051cc314-aa2f-46cc-a61c-68781a42477a",
        )
        self.assertEqual(json_response["field"]["name"], "field1")
        self.assertIsInstance(json_response["field"]["description"], str)
        self.assertIsInstance(json_response["field"]["schema"], str)
        self.assertEqual(
            json_response["values"]["id"],
            "e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertEqual(json_response["values"]["value"], "test_title1")
        self.assertEqual(
            json_response["values"]["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(json_response["values"]["created_time"])
        self.assert_date(json_response["values"]["last_edited_time"])
