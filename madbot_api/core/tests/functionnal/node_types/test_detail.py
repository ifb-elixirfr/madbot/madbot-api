from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_users,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
INVALID_TOKEN = "SQtMRj5mimSoGTJYFSQC4di6xQaGop"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """GET /node_types/<ntid> exists."""
        response = self.get("/api/node_types/isa_study")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                status.HTTP_405_METHOD_NOT_ALLOWED,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_valid_token(self):
        """GET /node_types/:ntid valid token."""
        response = self.get(
            "/api/node_types/isa_study",
            token=VALID_TOKEN,
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_missing_header(self):
        """GET /node_types/:ntid missing header."""
        response = self.get("/api/node_types/isa_study")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_no_token(self):
        """GET /node_types/:ntid missing token."""
        response = self.get("/api/node_types/isa_study", token="")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /node_types/:ntid invalid token."""
        response = self.get(
            "/api/node_types/isa_study",
            token=INVALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)
        create_isa_collection()

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_get_nodetype_with_unknown_ntid(self):
        """GET /nodes with unknown_ntid."""
        response = self.get(
            "/api/node_types/wrong_type",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node_type",
                        "code": "does_not_exist",
                        "message": '"wrong_type" does not exist.',
                    },
                ],
            },
        )

    def test_get_node_type_with_unsupported_fields(self):
        """GET /nodes_type/:ntid with unsupported fields."""
        response = self.get(
            "/api/node_types/isa_study?fields=id,type,title,description,status,created_time,created_by,last_edited_time,last_edited_by,parent",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "created_by",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                    {
                        "parameter": "created_time",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                    {
                        "parameter": "last_edited_by",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                    {
                        "parameter": "last_edited_time",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                    {
                        "parameter": "status",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                    {
                        "parameter": "title",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                    {
                        "parameter": "type",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )


class GetNodeTypes(MadbotTestCase):
    """Test the endpoint to get a list of node types."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)
        create_isa_collection()

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_get_detail_nodetype(self):
        """GET /node_types/:ntid get a specific node type."""
        response = self.get(
            "/api/node_types/isa_study",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(
            response.json(),
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
                "parent": ["isa_investigation"],
            },
        )

    def test_get_detail_nodetype_with_children(self):
        """GET /node_types/:ntid get list node types with parent type."""
        response = self.get(
            "/api/node_types/isa_study?fields=children",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(
            response.json(),
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
                "parent": ["isa_investigation"],
                "children": [
                    {
                        "id": "isa_assay",
                        "name": "assay",
                        "description": "An Assay represents a test performed either on material taken from a subject or on a whole initial subject, producing qualitative or quantitative measurements.",
                        "icon": "mdi-test-tube",
                        "parent": ["isa_study"],
                    },
                ],
            },
        )
