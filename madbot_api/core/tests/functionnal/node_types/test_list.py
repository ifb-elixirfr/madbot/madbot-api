from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_users,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
INVALID_TOKEN = "SQtMRj5mimSoGTJYFSQC4di6xQaGop"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """GET /node_types exists."""
        response = self.get("/api/node_types")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                status.HTTP_405_METHOD_NOT_ALLOWED,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_valid_token(self):
        """GET /node_types valid token."""
        response = self.get("/api/node_types", token=VALID_TOKEN)
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_missing_header(self):
        """GET /node_types missing header."""
        response = self.get("/api/node_types")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_missing_token(self):
        """GET /node_types missing token."""
        response = self.get("/api/node_types", token="")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /node_types invalid token."""
        response = self.get("/api/node_types", token=INVALID_TOKEN)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)
        create_isa_collection()

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_get_nodetype_with_invalid_parent(self):
        """GET /node_types with invalid parent."""
        response = self.get("/api/node_types?parent=dad", token=VALID_TOKEN)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"dad" does not exist.',
                    },
                ],
            },
        )


class GetNodeTypes(MadbotTestCase):
    """General test for the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)
        create_isa_collection()

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_get_list_nodetype(self):
        """GET /node_types get list node types."""
        response = self.get("/api/node_types", token=VALID_TOKEN)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_pagination(response.json(), 3, None, None, 1, 1)
        json_response = response.json().get("results")
        self.maxDiff = None
        self.assertEqual(
            json_response,
            [
                {
                    "id": "isa_investigation",
                    "name": "investigation",
                    "description": "An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions.",
                    "icon": "mdi-feather",
                    "parent": [],
                },
                {
                    "id": "isa_study",
                    "name": "study",
                    "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                    "icon": "mdi-notebook-outline",
                    "parent": ["isa_investigation"],
                },
                {
                    "id": "isa_assay",
                    "name": "assay",
                    "description": "An Assay represents a test performed either on material taken from a subject or on a whole initial subject, producing qualitative or quantitative measurements.",
                    "icon": "mdi-test-tube",
                    "parent": ["isa_study"],
                },
            ],
        )

    def test_get_list_nodetype_with_parent(self):
        """GET /node_types get list node types with parent type."""
        response = self.get(
            "/api/node_types?parent=isa_study",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_pagination(response.json(), 1, None, None, 1, 1)
        json_response = response.json().get("results")
        self.assertEqual(
            json_response,
            [
                {
                    "id": "isa_assay",
                    "name": "assay",
                    "description": "An Assay represents a test performed either on material taken from a subject or on a whole initial subject, producing qualitative or quantitative measurements.",
                    "icon": "mdi-test-tube",
                    "parent": ["isa_study"],
                },
            ],
        )
