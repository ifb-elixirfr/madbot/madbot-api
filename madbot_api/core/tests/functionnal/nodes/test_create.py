from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import Node, NodeMember
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "dsCIDSUNCD89789S6Dcdusihciduss"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


VALID_DATA_study = {
    "title": "study_1",
    "type": "isa_study",
    "description": "description_study_1",
    "status": "active",
}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """POST /nodes exists."""
        response = self.post("/api/nodes")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        AccessTokenFactory(token=VALID_TOKEN_4, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        john = UserFactory(username="john")
        AccessTokenFactory(token=VALID_TOKEN_3, user=john, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=john, workspace=workspace)

        NodeMemberFactory(user=john, node=investigation, role="collaborator")

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """POST /nodes valid token."""
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """POST /nodes no token."""
        response = self.post(
            "/api/nodes",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """POST /nodes invalid token."""
        response = self.post(
            "/api/nodes",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """POST /nodes invalid scope."""
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_parent_node(self):
        """POST /nodes not a member of the parent node."""
        data = dict(VALID_DATA_study)
        data["parent"] = "9e6d8390-8e7b-412b-906c-58cf10e7d89b"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"9e6d8390-8e7b-412b-906c-58cf10e7d89b" does not exist.',
                    },
                ],
            },
        )

    def test_not_enough_rights_member_on_parent_node_(self):
        """POST /nodes member's role collaborator on the parent node does not give enough rights to create a new node."""
        data = dict(VALID_DATA_study)
        data["parent"] = "9e6d8390-8e7b-412b-906c-58cf10e7d89b"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow creation.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_create_node_study_with_unknown_field(self):
        """POST /node with unknown field."""
        data = dict(VALID_DATA_study)
        data.update({"fake_field": "error"})

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )

    def test_unknown_workspace(self):
        """POST /node with unknown workspace."""
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_DATA_study,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            title="new_investigation",
            description="new_description_investigation",
            created_by=alice,
            last_edited_time=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """POST /node with missing workspace."""
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            data=VALID_DATA_study,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_create_node_study_with_no_name(self):
        """POST /node no title."""
        data = dict(VALID_DATA_study)
        data.pop("title")

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_response = response.json()
        self.assertEqual(
            json_response["title"],
            None,
        )

    def test_create_node_study_with_no_description(self):
        """POST /node no description."""
        data = dict(VALID_DATA_study)
        data.pop("description")

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_response = response.json()
        self.assertEqual(
            json_response["description"],
            None,
        )

    def test_create_node_study_with_no_parent(self):
        """POST /node no parent."""
        data = dict(VALID_DATA_study)

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_response = response.json()
        self.assertEqual(
            json_response["path"],
            [json_response["id"]],
        )

    def test_create_study_with_no_type(self):
        """POST /nodes no type."""
        data = dict(VALID_DATA_study)
        data.pop("type")

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "type",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_create_node_study_with_no_status(self):
        """POST /node no status."""
        data = dict(VALID_DATA_study)
        data.pop("status")

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.json()["status"],
            "pending",
        )


class InvalidParams(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            title="new_investigation",
            description="new_description_investigation",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """POST /nodes invalid workspace."""
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="invalid_workspace",
            data=VALID_DATA_study,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_create_study_node_with_wrong_type(self):
        """POST /nodes wrong type."""
        data_wrong_type = dict(VALID_DATA_study)
        data_wrong_type["type"] = "isa_assay"
        data_wrong_type["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_wrong_type,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "save_validation_error",
                "message": "The request body data are incorrect. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "type",
                        "code": "invalid",
                        "message": "The node type must be a child of the parent type.",
                    },
                ],
            },
        )

    def test_create_study_node_with_unknown_type(self):
        """POST /nodes unknown type."""
        data_wrong_type = dict(VALID_DATA_study)
        data_wrong_type["type"] = "isa_faketype"

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_wrong_type,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "type",
                        "code": "does_not_exist",
                        "message": '"isa_faketype" does not exist.',
                    },
                ],
            },
        )

    def test_create_study_node_with_null_type(self):
        """POST /nodes null type."""
        data_wrong_type = dict(VALID_DATA_study)
        data_wrong_type["type"] = "null"

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_wrong_type,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "type",
                        "code": "does_not_exist",
                        "message": '"null" does not exist.',
                    },
                ],
            },
        )

    def test_create_node_study_with_wrong_parent(self):
        """POST /nodes wrong parent."""
        data_wrong_parent = dict(VALID_DATA_study)
        data_wrong_parent["parent"] = "30000000-4000-4000-a000-700000000000"

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_wrong_parent,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"30000000-4000-4000-a000-700000000000" does not exist.',
                    },
                ],
            },
        )

    def test_create_study_node_with_wrong_status(self):
        """POST /nodes wrong status."""
        data_wrong_status = dict(VALID_DATA_study)
        data_wrong_status["status"] = "fake_status"
        data_wrong_status["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_wrong_status,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "status",
                        "code": "invalid_choice",
                        "message": '"fake_status" is not a valid choice.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=aerith, scope="write")
        tifa = UserFactory(username="tifa")
        AccessTokenFactory(token=VALID_TOKEN_4, user=tifa, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        WorkspaceFactory(id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4", created_by=alice)
        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            title="new_investigation",
            description="new_description_investigation",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )
        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=aerith, workspace=workspace)

        NodeMemberFactory(user=bob, node=investigation, role="owner")
        NodeMemberFactory(user=aerith, node=investigation, role="manager")

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """POST /nodes access a node from another workspace."""
        data = dict(VALID_DATA_study)
        data["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """POST /nodes user not a member of workspace."""
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_DATA_study,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class CreateNodes(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=cls.alice, scope="write")
        cls.bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=cls.bob, scope="write")
        cls.aerith = UserFactory(username="aerith")
        AccessTokenFactory(token=VALID_TOKEN_3, user=cls.aerith, scope="write")
        cls.tifa = UserFactory(username="tifa")
        AccessTokenFactory(token=VALID_TOKEN_4, user=cls.tifa, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=cls.alice,
        )
        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            title="new_investigation",
            description="new_description_investigation",
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )
        WorkspaceMemberFactory(user=cls.bob, workspace=workspace)
        WorkspaceMemberFactory(user=cls.aerith, workspace=workspace)
        WorkspaceMemberFactory(user=cls.tifa, workspace=workspace)

        NodeMemberFactory(user=cls.bob, node=investigation, role="owner")
        NodeMemberFactory(user=cls.aerith, node=investigation, role="manager")
        NodeMemberFactory(user=cls.tifa, node=investigation, role="contributor")

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_node_study_as_owner(self):
        """POST /nodes as study."""
        data = dict(VALID_DATA_study)
        data["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assert_uuid(response.json()["id"])

        self.assertEqual(
            response.json()["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )

        self.assertEqual(
            response.json()["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )

        self.assertEqual(
            response.json()["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            response.json()["title"],
            "study_1",
        )
        self.assertEqual(
            response.json()["description"],
            "description_study_1",
        )
        self.assertEqual(
            response.json()["status"],
            "active",
        )
        self.assertEqual(
            response.json()["path"],
            ["7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2", response.json()["id"]],
        )

        self.assertEqual(
            response.json()["children"],
            {
                "count": 0,
                "related": "http://testserver/api/nodes?parent="
                + response.json()["id"],
            },
        )
        self.assert_date(response.json()["created_time"])

        study = Node.objects.get(id=response.json()["id"])
        self.assertEqual(study.title, "study_1")
        self.assertEqual(study.description, "description_study_1")

        node_member_alice = NodeMember.objects.get(
            user=self.alice,
            node=study,
            created_by=self.alice,
        )
        self.assertEqual(node_member_alice.role, "owner")
        node_member_bob = NodeMember.objects.get(
            user=self.bob,
            node=study,
            created_by=self.alice,
        )
        self.assertEqual(node_member_bob.role, "owner")

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_node_study_as_manager(self):
        """POST /nodes as study."""
        data = dict(VALID_DATA_study)
        data["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assert_uuid(response.json()["id"])

        self.assertEqual(
            response.json()["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )

        self.assertEqual(
            response.json()["created_by"],
            {"object": "MadbotUser", "id": str(self.aerith.id)},
        )

        self.assertEqual(
            response.json()["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.aerith.id)},
        )
        self.assertEqual(
            response.json()["title"],
            "study_1",
        )
        self.assertEqual(
            response.json()["description"],
            "description_study_1",
        )
        self.assertEqual(
            response.json()["status"],
            "active",
        )
        self.assertEqual(
            response.json()["path"],
            ["7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2", response.json()["id"]],
        )

        self.assertEqual(
            response.json()["children"],
            {
                "count": 0,
                "related": "http://testserver/api/nodes?parent="
                + response.json()["id"],
            },
        )
        self.assert_date(response.json()["created_time"])

        study = Node.objects.get(id=response.json()["id"])
        self.assertEqual(study.title, "study_1")
        self.assertEqual(study.description, "description_study_1")

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_node_study_as_contributor(self):
        """POST /nodes as study."""
        data = dict(VALID_DATA_study)
        data["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assert_uuid(response.json()["id"])

        self.assertEqual(
            response.json()["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )

        self.assertEqual(
            response.json()["created_by"],
            {"object": "MadbotUser", "id": str(self.tifa.id)},
        )

        self.assertEqual(
            response.json()["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.tifa.id)},
        )
        self.assertEqual(
            response.json()["title"],
            "study_1",
        )
        self.assertEqual(
            response.json()["description"],
            "description_study_1",
        )
        self.assertEqual(
            response.json()["status"],
            "active",
        )
        self.assertEqual(
            response.json()["path"],
            ["7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2", response.json()["id"]],
        )

        self.assertEqual(
            response.json()["children"],
            {
                "count": 0,
                "related": "http://testserver/api/nodes?parent="
                + response.json()["id"],
            },
        )
        self.assert_date(response.json()["created_time"])

        study = Node.objects.get(id=response.json()["id"])
        self.assertEqual(study.title, "study_1")
        self.assertEqual(study.description, "description_study_1")

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_node_study_with_null_parent(self):
        """POST /nodes null parent."""
        data_null_parent = dict(VALID_DATA_study)

        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_null_parent,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(
            json_response["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )
        self.assertEqual(
            response.json()["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )

        self.assertEqual(
            response.json()["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["title"],
            "study_1",
        )
        self.assertEqual(
            json_response["description"],
            "description_study_1",
        )
        self.assertEqual(
            json_response["status"],
            "active",
        )
        self.assertEqual(
            json_response["path"],
            [json_response["id"]],
        )
        self.assertEqual(
            json_response["children"],
            {
                "count": 0,
                "related": "http://testserver/api/nodes?parent="
                + response.json()["id"],
            },
        )
        self.assert_date(json_response["created_time"])

        study = Node.objects.get(id=json_response["id"])
        self.assertEqual(study.title, "study_1")
        self.assertEqual(study.description, "description_study_1")

        node_member = NodeMember.objects.get(
            user=self.alice,
            node=study,
            created_by=self.alice,
        )
        self.assertEqual(node_member.role, "owner")

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_node_add_test_members(self):
        """POST /nodes as study and assert all members of parent node were added in the new node."""
        data = dict(VALID_DATA_study)
        data["parent"] = "7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2"
        response = self.post(
            "/api/nodes",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assert_uuid(response.json()["id"])

        # aerith
        aerith = NodeMember.objects.filter(
            node=response.json()["id"],
            user=self.aerith.id,
        )
        self.assertTrue(aerith.exists())
        self.assertEqual(aerith.count(), 1)
        self.assertEqual(aerith.first().role, "owner")

        # alice
        alice = NodeMember.objects.filter(
            node=response.json()["id"],
            user=self.alice.id,
        )
        self.assertTrue(alice.exists())
        self.assertEqual(alice.count(), 1)
        self.assertEqual(alice.first().role, "owner")

        # bob
        bob = NodeMember.objects.filter(node=response.json()["id"], user=self.bob.id)
        self.assertTrue(bob.exists())
        self.assertEqual(bob.count(), 1)
        self.assertEqual(bob.first().role, "owner")

        # tifa
        tifa = NodeMember.objects.filter(node=response.json()["id"], user=self.tifa.id)
        self.assertTrue(tifa.exists())
        self.assertEqual(tifa.count(), 1)
        self.assertEqual(tifa.first().role, "contributor")
