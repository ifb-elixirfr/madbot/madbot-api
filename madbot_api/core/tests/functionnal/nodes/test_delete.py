from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import Node
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "ILOJVSvdspopvksdffdszrtjtyjyjj"
VALID_TOKEN_5 = "vdfvvzefivpofkdfdgfdIK5dfbwbfd"
VALID_TOKEN_6 = "5xDp6b2GNr79MS8tOOe3kPHXZ0GT1F"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


VALID_DATA_study = {
    "title": "study_1",
    "type": "isa_study",
    "description": "description_study_1",
}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """DELETE /nodes/:nid exists."""
        response = self.delete("/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        AccessTokenFactory(token=VALID_TOKEN_6, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        john = UserFactory(username="john")
        AccessTokenFactory(token=VALID_TOKEN_3, user=john, scope="write")
        alan = UserFactory(username="alan")
        AccessTokenFactory(token=VALID_TOKEN_4, user=john, scope="write")
        sylvie = UserFactory(username="sylvie")
        AccessTokenFactory(token=VALID_TOKEN_5, user=john, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        cls.investigation_1 = InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=john, workspace=workspace)
        WorkspaceMemberFactory(user=alan, workspace=workspace)
        WorkspaceMemberFactory(user=sylvie, workspace=workspace)

        NodeMemberFactory(
            user=john,
            node=cls.investigation_1,
            role="collaborator",
        )
        NodeMemberFactory(
            user=alan,
            node=cls.investigation_1,
            role="contributor",
        )
        NodeMemberFactory(
            user=sylvie,
            node=cls.investigation_1,
            role="manager",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """DELETE /nodes/:nid valid token."""
        response = self.delete(
            "/api/nodes/" + str(self.investigation_1.pk),
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """DELETE /nodes/:nid no token."""
        response = self.delete(
            "/api/nodes/" + str(self.investigation_1.pk),
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """DELETE /nodes/:nid invalid token."""
        response = self.delete(
            "/api/nodes/" + str(self.investigation_1.pk),
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """DELETE /nodes/:nid invalid scope."""
        response = self.delete(
            "/api/nodes/" + str(self.investigation_1.pk),
            token=VALID_TOKEN_6,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """DELETE /nodes/:nid not a member of the given node."""
        response = self.delete(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_collaborator_not_enough_rights_member_on_node(self):
        """DELETE /nodes/:nid member's role collaborator on the given node does not give enough rights to delete the node."""
        response = self.delete(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow deletion.",
                    },
                ],
            },
        )

    def test_contributor_not_enough_rights_member_on_node(self):
        """DELETE /nodes/:nid member's role contributor on the given node does not give enough rights to delete the node."""
        response = self.delete(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow deletion.",
                    },
                ],
            },
        )

    def test_manager_not_enough_rights_member_on_node(self):
        """DELETE /nodes/:nid member's role manager on the given node does not give enough rights to delete the node."""
        response = self.delete(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_5,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow deletion.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_workspaces()
        delete_workspace_members()

    def test_delete_node_with_unknown_nid(self):
        """DELETE /nodes/:nid with unknown_nid."""
        response = self.delete(
            url="/api/nodes/32e42ddd-a861-40b4-b61d-85fdbdc8f00f",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"32e42ddd-a861-40b4-b61d-85fdbdc8f00f" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_workspace(self):
        """DELETE /nodes/:nid with unknown workspace."""
        response = self.delete(
            url="/api/nodes/32e42ddd-a861-40b4-b61d-85fdbdc8f00f",
            token=VALID_TOKEN,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )


class MissingParam(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """DELETE /nodes/:nid with missing workspace."""
        response = self.delete(
            url="/api/nodes/32e42ddd-a861-40b4-b61d-85fdbdc8f00f",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_delete_node_with_invalid_nid(self):
        """DELETE /nodes/:nid with invalid nid."""
        response = self.delete(
            "/api/nodes/1",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"1" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_workspace(self):
        """DELETE /nodes/:nid with invalid workspace."""
        response = self.delete(
            "/api/nodes/1",
            token=VALID_TOKEN,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restrictions on nodes in another workspace."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=alice,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """DELETE /node/:nid/ access a node from another workspace."""
        response = self.delete(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"cc7f17a7-01e0-4ef9-bae0-30d8610a3b18" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """DELETE /node/:nid/ user not a workspace member."""
        response = self.delete(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class DeleteNodes(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_delete_node_investigation(self):
        """DELETE /node/:nid/ delete node."""
        response = self.delete(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Node.objects.filter(id=1).exists())
