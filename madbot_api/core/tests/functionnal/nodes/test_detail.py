from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    AssayNodeFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
INVALID_TOKEN = "SQtMRj5mimSoGTJYFSQC4di6xQaGop"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """GET /nodes/:nid exists."""
        response = self.get("/api/nodes")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )
        WorkspaceMemberFactory(workspace=workspace, user=bob)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """GET /nodes/:nid valid token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_missing_token(self):
        """GET /nodes/:nid missing token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            token="",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /nodes/:nid invalid token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """GET /nodes/:nid not a member of the given node."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_workspace(self):
        """GET /nodes with unknown workspace."""
        response = self.get(
            "/api/nodes/32e42ddd-a861-40b4-b61d-85fdbdc8f00f",
            token=VALID_TOKEN,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_get_node_with_unknown_nid(self):
        """GET /nodes with unknown_nid."""
        response = self.get(
            "/api/nodes/32e42ddd-a861-40b4-b61d-85fdbdc8f00f",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"32e42ddd-a861-40b4-b61d-85fdbdc8f00f" does not exist.',
                    },
                ],
            },
        )


class MissingParam(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """GET /nodes with missing workspace."""
        response = self.get(
            "/api/nodes/32e42ddd-a861-40b4-b61d-85fdbdc8f00f",
            token=VALID_TOKEN,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """GET /nodes/:nid with invalid workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            token=VALID_TOKEN,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_get_node_with_invalid_nid(self):
        """GET /nodes/:nid with invalid nid."""
        response = self.get(
            "/api/nodes/1",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"1" is not a valid UUID.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=alice,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_not_workspace_member(self):
        """GET /nodes/:nid user not a workspace member."""
        response = self.get(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_node_another_workspace(self):
        """GET /nodes/:nid access a node from another workspace."""
        response = self.get(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"cc7f17a7-01e0-4ef9-bae0-30d8610a3b18" does not exist.',
                    },
                ],
            },
        )


class GetNodes(MadbotTestCase):
    """Test the endpoint to get nodes."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=cls.alice, scope="read")
        create_isa_collection()
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=cls.alice,
        )

        cls.investigation_1 = InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )
        cls.study_1 = StudyNodeFactory(
            id="1e4ab06c-7e89-4a02-bb18-c43c61bd1c90",
            title="study_1",
            description="description_study_1",
            parent=cls.investigation_1,
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )

        cls.study_2 = StudyNodeFactory(
            id="903ea74c-eae8-45d6-9d38-29b693dcb2db",
            title="study_1",
            description="description_study_1",
            parent=cls.investigation_1,
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )
        cls.assay_1 = AssayNodeFactory(
            id="4d85d93a-ed6b-4d7b-a10b-bbe3fffd14f3",
            title="assay_1",
            description="description_assay_1",
            parent=cls.study_1,
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_get_detail_node(self):
        """GET /nodes/:nid get detail node."""
        response = self.get(
            "/api/nodes/" + str(self.investigation_1.id),
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assert_uuid(response.json()["id"])
        self.assertEqual(response.json()["id"], self.investigation_1.id)
        self.assertEqual(
            response.json()["type"],
            {
                "id": "isa_investigation",
                "name": "investigation",
                "description": "An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions.",
                "icon": "mdi-feather",
            },
        )
        self.assertEqual(
            response.json()["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            response.json()["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            response.json()["title"],
            "investigation_1",
        )
        self.assertEqual(
            response.json()["description"],
            "description_investigation_1",
        )
        self.assertEqual(
            response.json()["status"],
            "active",
        )
        self.assertEqual(
            response.json()["path"],
            [self.investigation_1.id],
        )
        self.assert_date(response.json()["created_time"])
        self.assert_date(response.json()["last_edited_time"])

        self.assertEqual(response.json()["children"]["count"], 2)
        self.assertEqual(
            response.json()["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.investigation_1.id),
        )
