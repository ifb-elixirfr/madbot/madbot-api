from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    AssayNodeFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
INVALID_TOKEN = "SQtMRj5mimSoGTJYFSQC4di6xQaGop"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """GET /nodes exists."""
        response = self.get("/api/nodes")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )

        WorkspaceMemberFactory(workspace=workspace, user=bob)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_nodes()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """GET /nodes valid token."""
        response = self.get(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /nodes missing token."""
        response = self.get(
            "/api/nodes",
            token="",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /nodes invalid token."""
        response = self.get(
            "/api/nodes",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_not_a_member_parent_node(self):
        """GET /nodes?parent not a member of the parent node."""
        response = self.get(
            "/api/nodes?parent=9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"9e6d8390-8e7b-412b-906c-58cf10e7d89b" does not exist.',
                    },
                ],
            },
        )


class UnkownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_nodes()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_get_node_with_unknown_parent(self):
        """GET /nodes with unknown parent."""
        response = self.get(
            "/api/nodes?parent=5b791244-ad73-4711-8307-05da15584b7c",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"5b791244-ad73-4711-8307-05da15584b7c" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_workspace(self):
        """GET /nodes with unknown workspace."""
        response = self.get(
            "/api/nodes?parent=5b791244-ad73-4711-8307-05da15584b7c",
            token=VALID_TOKEN,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )


class MissingParam(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_nodes()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """GET /nodes with missing workspace."""
        response = self.get(
            "/api/nodes?parent=5b791244-ad73-4711-8307-05da15584b7c",
            token=VALID_TOKEN,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        WorkspaceFactory(id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_nodes()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """GET /nodes with invalid workspace."""
        response = self.get(
            "/api/nodes?parent=9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_get_node_with_invalid_parent(self):
        """GET /nodes with invalid parent."""
        response = self.get(
            "/api/nodes?parent=dad",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "invalid",
                        "message": '"dad" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_get_node_with_invalid_type(self):
        """GET /nodes with invalid type."""
        response = self.get(
            "/api/nodes?type=chaise",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "type",
                        "code": "does_not_exist",
                        "message": '"chaise" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

        investigation = InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=alice,
            workspace=workspace_1,
        )
        StudyNodeFactory(
            id="903ea74c-eae8-45d6-9d38-29b693dcb2db",
            title="study_1",
            description="description_study_1",
            parent=investigation,
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_not_workspace_member(self):
        """GET /nodes user not a workspace member."""
        response = self.get(
            "/api/nodes",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_node_from_workspace(self):
        """GET /nodes access a node from another workspace."""
        response = self.get(
            "/api/nodes?parent=cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "query_validation_error",
                "message": "Invalid query parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "parent",
                        "code": "does_not_exist",
                        "message": '"cc7f17a7-01e0-4ef9-bae0-30d8610a3b18" does not exist.',
                    },
                ],
            },
        )


class GetNodes(MadbotTestCase):
    """Test the endpoint to get nodes."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=cls.alice, scope="read")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=cls.alice,
        )
        create_isa_collection()
        cls.investigation_1 = InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )
        cls.study_1 = StudyNodeFactory(
            id="903ea74c-eae8-45d6-9d38-29b693dcb2db",
            title="study_1",
            description="description_study_1",
            parent=cls.investigation_1,
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )
        cls.assay_1 = AssayNodeFactory(
            id="4d85d93a-ed6b-4d7b-a10b-bbe3fffd14f3",
            title="assay_1",
            description="description_assay_1",
            parent=cls.study_1,
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_get_list_node(self):
        """GET /nodes get list nodes."""
        response = self.get(
            "/api/nodes",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_pagination(response.json(), 3, None, None, 1, 1)
        json_response = response.json().get("results")
        self.assertEqual(len(json_response), 3)

        json_response = sorted(json_response, key=lambda k: k["title"])

        investigation_json = json_response[1]
        study_json = json_response[2]
        assay_json = json_response[0]

        # Investigation 1
        self.assert_uuid(investigation_json["id"])
        self.assertEqual(investigation_json["id"], self.investigation_1.id)
        self.assertEqual(
            investigation_json["type"],
            {
                "id": "isa_investigation",
                "name": "investigation",
                "description": "An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions.",
                "icon": "mdi-feather",
            },
        )
        self.assertEqual(
            investigation_json["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            investigation_json["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            investigation_json["title"],
            "investigation_1",
        )
        self.assertEqual(
            investigation_json["description"],
            "description_investigation_1",
        )
        self.assertEqual(
            investigation_json["status"],
            "active",
        )
        self.assertEqual(
            investigation_json["path"],
            [self.investigation_1.id],
        )
        self.assert_date(investigation_json["created_time"])
        self.assert_date(investigation_json["last_edited_time"])

        self.assertEqual(investigation_json["children"]["count"], 1)
        self.assertEqual(
            investigation_json["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.investigation_1.id),
        )

        # Study 1
        self.assert_uuid(study_json["id"])
        self.assertEqual(study_json["id"], self.study_1.id)
        self.assertEqual(
            study_json["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )
        self.assertEqual(
            study_json["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            study_json["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            study_json["title"],
            "study_1",
        )
        self.assertEqual(
            study_json["description"],
            "description_study_1",
        )
        self.assertEqual(
            study_json["status"],
            "active",
        )
        self.assertEqual(
            study_json["path"],
            [self.investigation_1.id, self.study_1.id],
        )
        self.assert_date(study_json["created_time"])
        self.assert_date(study_json["last_edited_time"])

        self.assertEqual(study_json["children"]["count"], 1)
        self.assertEqual(
            study_json["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.study_1.id),
        )

        # Assay 1
        self.assert_uuid(assay_json["id"])
        self.assertEqual(
            assay_json["type"],
            {
                "id": "isa_assay",
                "name": "assay",
                "description": "An Assay represents a test performed either on material taken from a subject or on a whole initial subject, producing qualitative or quantitative measurements.",
                "icon": "mdi-test-tube",
            },
        )
        self.assertEqual(
            assay_json["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            assay_json["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            assay_json["title"],
            "assay_1",
        )
        self.assertEqual(
            assay_json["description"],
            "description_assay_1",
        )
        self.assertEqual(
            assay_json["status"],
            "active",
        )
        self.assertEqual(
            study_json["path"],
            [self.investigation_1.id, self.study_1.id],
        )
        self.assert_date(assay_json["created_time"])
        self.assert_date(assay_json["last_edited_time"])

        self.assertEqual(assay_json["children"]["count"], 0)
        self.assertEqual(
            assay_json["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.assay_1.id),
        )

    def test_get_list_node_with_no_parent(self):
        """GET /nodes get list nodes with no parent."""
        response = self.get(
            "/api/nodes?parent=null",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_pagination(response.json(), 1, None, None, 1, 1)
        json_response = response.json().get("results")
        self.assertEqual(len(json_response), 1)
        json_response = json_response[0]

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], self.investigation_1.id)

        self.assertEqual(
            json_response["type"],
            {
                "id": "isa_investigation",
                "name": "investigation",
                "description": "An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions.",
                "icon": "mdi-feather",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["title"],
            "investigation_1",
        )
        self.assertEqual(
            json_response["description"],
            "description_investigation_1",
        )
        self.assertEqual(
            json_response["status"],
            "active",
        )
        self.assertEqual(
            json_response["path"],
            [self.investigation_1.id],
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])

        self.assertEqual(json_response["children"]["count"], 1)
        self.assertEqual(
            json_response["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.investigation_1.id),
        )

    def test_get_list_node_with_parent(self):
        """GET /nodes get list nodes with parent."""
        response = self.get(
            "/api/nodes?parent=" + str(self.investigation_1.id),
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_pagination(response.json(), 1, None, None, 1, 1)
        json_response = response.json().get("results")
        self.assertEqual(len(json_response), 1)
        json_response = json_response[0]

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], self.study_1.id)

        self.assertEqual(
            json_response["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["title"],
            "study_1",
        )
        self.assertEqual(
            json_response["description"],
            "description_study_1",
        )
        self.assertEqual(
            json_response["status"],
            "active",
        )
        self.assertEqual(
            json_response["path"],
            [self.investigation_1.id, self.study_1.id],
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])

        self.assertEqual(json_response["children"]["count"], 1)
        self.assertEqual(
            json_response["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.study_1.id),
        )

    def test_get_list_node_with_study_type(self):
        """GET /nodes get list nodes with study type."""
        response = self.get(
            "/api/nodes?type=isa_study",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_pagination(response.json(), 1, None, None, 1, 1)
        json_response = response.json().get("results")
        self.assertEqual(len(json_response), 1)
        json_response = json_response[0]

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], self.study_1.id)

        self.assertEqual(
            json_response["type"],
            {
                "id": "isa_study",
                "name": "study",
                "description": "Study is a central concept containing information on the subject under study, its characteristics and any treatments applied.",
                "icon": "mdi-notebook-outline",
            },
        )

        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["title"],
            "study_1",
        )
        self.assertEqual(
            json_response["description"],
            "description_study_1",
        )
        self.assertEqual(
            json_response["status"],
            "active",
        )
        self.assertEqual(
            json_response["path"],
            [self.investigation_1.id, self.study_1.id],
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])

        self.assertEqual(json_response["children"]["count"], 1)
        self.assertEqual(
            json_response["children"]["related"],
            "http://testserver/api/nodes?parent=" + str(self.study_1.id),
        )
