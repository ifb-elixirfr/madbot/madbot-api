from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import Node
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_nodes,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "ILOJVSvdspopvksdffdszrtjtyjyjj"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


VALID_DATA_investigation = {
    "title": "investigation_1",
    "description": "description_investigation_1",
    "status": "pending",
}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """PUT /nodes/:nid exists."""
        response = self.put("/api/nodes/5b791244-ad73-4711-8307-05da15584b7c")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        AccessTokenFactory(token=VALID_TOKEN_4, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        john = UserFactory(username="john")
        AccessTokenFactory(token=VALID_TOKEN_3, user=john, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )

        WorkspaceMemberFactory(user=bob, workspace=workspace)
        WorkspaceMemberFactory(user=john, workspace=workspace)

        NodeMemberFactory(
            id="dbdc67f4-fc79-4ffa-b2c6-24bb60657891",
            user=john,
            node=investigation,
            role="collaborator",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """PUT /nodes/:nid valid token."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """PUT /nodes/:nid no token."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """PUT /nodes/:nid invalid token."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """PUT /nodes/:nid invalid scope."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_member_node(self):
        """PUT /nodes/:nid not a member of the given node."""
        data = dict(VALID_DATA_investigation)
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_not_enough_rights_member_on_node(self):
        """PUT /nodes/:nid member's role collaborator on the given node does not give enough rights to edit the node."""
        data = dict(VALID_DATA_investigation)
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow update.",
                    },
                ],
            },
        )


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="new_investigation",
            description="new_description_investigation",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_workspace(self):
        """PUT /node/:nid with unknown workspace."""
        response = self.put(
            "/api/nodes/c48f1bed-12ce-4e29-966a-c7723b3f8914",
            token=VALID_TOKEN,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_DATA_investigation,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_update_node_investigation_with_unknown_nid(self):
        """PUT /node/:nid not found."""
        data = dict(VALID_DATA_investigation)

        response = self.put(
            "/api/nodes/c48f1bed-12ce-4e29-966a-c7723b3f8914",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"c48f1bed-12ce-4e29-966a-c7723b3f8914" does not exist.',
                    },
                ],
            },
        )

    def test_update_node_investigation_with_unknown_field(self):
        """PUT /node/:nid with unknown field."""
        data = dict(VALID_DATA_investigation)
        data.update({"fake_field": "error"})

        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="new_investigation",
            description="new_description_investigation",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_missing_workspace(self):
        """PUT /node/:nid with missing workspace."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            data=VALID_DATA_investigation,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_update_node_investigation_with_empty_body(self):
        """PUT /node/:nid no body."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_node_investigation_with_no_name(self):
        """PUT /node/:nid no title."""
        data = dict(VALID_DATA_investigation)
        data.pop("title")

        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_response = response.json()
        self.assertEqual(
            json_response["title"],
            "new_investigation",
        )

    def test_update_node_investigation_with_no_description(self):
        """PUT /node/:nid no description."""
        data = dict(VALID_DATA_investigation)
        data.pop("description")

        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_response = response.json()
        self.assertEqual(
            json_response["description"],
            "new_description_investigation",
        )

    def test_update_node_investigation_with_no_status(self):
        """PUT /node no status."""
        data = dict(VALID_DATA_investigation)
        data.pop("status")

        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_response = response.json()
        self.assertEqual(
            json_response["status"],
            "active",
        )


class InvalidParams(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            title="new_investigation",
            description="new_description_investigation",
            created_by=alice,
            last_edited_by=alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_param_invalid_workspace(self):
        """PUT /nodes with invalid workspace."""
        response = self.put(
            "/api/nodes/7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            token=VALID_TOKEN,
            workspace="invalid_workspace",
            data=VALID_DATA_investigation,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_update_investigation_node_with_wrong_status(self):
        """PUT /nodes wrong status."""
        data_wrong_status = dict(VALID_DATA_investigation)
        data_wrong_status["status"] = "fake_status"

        response = self.put(
            "/api/nodes/7aee9cb2-db11-49a8-880d-3e2f6a5b7ee2",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data_wrong_status,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "status",
                        "code": "invalid_choice",
                        "message": '"fake_status" is not a valid choice.',
                    },
                ],
            },
        )

    def test_update_node_with_invalid_nid(self):
        """PUT /nodes/:nid with invalid nid."""
        response = self.put(
            "/api/nodes/1",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_DATA_investigation,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"1" is not a valid UUID.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice, scope="write")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        workspace_1 = WorkspaceFactory(
            id="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            created_by=alice,
        )
        create_isa_collection()
        InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="investigation_1",
            description="description_investigation_1",
            created_by=alice,
            workspace=workspace,
        )

        InvestigationNodeFactory(
            id="cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            title="investigation_2",
            description="description_investigation_2",
            created_by=alice,
            workspace=workspace_1,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_node_another_workspace(self):
        """PUT /node/:nid access a node from another workspace."""
        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            data=VALID_DATA_investigation,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"9e6d8390-8e7b-412b-906c-58cf10e7d89b" does not exist.',
                    },
                ],
            },
        )

    def test_not_workspace_member(self):
        """PUT /node/:nid user not a workspace member."""
        response = self.put(
            "/api/nodes/cc7f17a7-01e0-4ef9-bae0-30d8610a3b18",
            token=VALID_TOKEN_2,
            workspace="8e24f4a5-a8ae-477d-b2f9-1ecd1155cdd4",
            data=VALID_DATA_investigation,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class UpdateNodes(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=cls.alice, scope="write")
        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=cls.alice,
        )
        create_isa_collection()
        cls.investigation = InvestigationNodeFactory(
            id="9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            title="new_investigation",
            description="new_description_investigation",
            created_by=cls.alice,
            last_edited_by=cls.alice,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_update_investigation(self):
        """PUT /node/:nid."""
        data = dict(VALID_DATA_investigation)

        initial_history_count = self.investigation.history.all().count()

        response = self.put(
            "/api/nodes/9e6d8390-8e7b-412b-906c-58cf10e7d89b",
            token=VALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()

        self.assertEqual(json_response["id"], "9e6d8390-8e7b-412b-906c-58cf10e7d89b")
        self.assertEqual(
            json_response["type"],
            {
                "id": "isa_investigation",
                "name": "investigation",
                "description": "An Investigation is used to record metadata related to the investigation context, such as the title, description, associated individuals, and scholarly submissions.",
                "icon": "mdi-feather",
            },
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )

        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": str(self.alice.id)},
        )
        self.assertEqual(
            json_response["title"],
            "investigation_1",
        )
        self.assertEqual(
            json_response["description"],
            "description_investigation_1",
        )
        self.assertEqual(
            json_response["status"],
            "pending",
        )
        self.assertEqual(
            json_response["path"],
            [json_response["id"]],
        )
        self.assertEqual(
            json_response["children"],
            {
                "count": 0,
                "related": "http://testserver/api/nodes?parent="
                + response.json()["id"],
            },
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        study = Node.objects.get(id="9e6d8390-8e7b-412b-906c-58cf10e7d89b")
        self.assertEqual(study.title, "investigation_1")
        self.assertEqual(study.description, "description_investigation_1")

        self.assertEqual(
            self.investigation.history.all().count(),
            initial_history_count + 1,
        )
