from notifications.models import Notification
from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NotificationFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_notifications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "dsfmCDICJOISJpfbMdcdsofpdssdof"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jim = UserFactory(
            first_name="Jim",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jim, scope="read")
        pam = UserFactory(
            first_name="Pam",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=pam, scope="read")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jim,
        )
        WorkspaceMemberFactory(workspace=workspace, user=pam)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jim,
            workspace=workspace,
        )

        # Create notifications
        NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=False,
            deleted=True,
            workspace=workspace,
        )
        cls.notification = NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_delete_specific_exist(self):
        """DELETE /notifications/<notifid> exists."""
        response = self.delete(
            "/api/notifications/" + str(self.notification.id),
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is not 404, 405 or 500
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )

    def test_delete_all_exist(self):
        """DELETE /notifications exists."""
        response = self.delete(
            "/api/notifications?archived=false&unread=true",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is not 404, 405 or 500
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jim = UserFactory(
            first_name="Jim",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jim, scope="write")
        pam = UserFactory(
            first_name="Pam",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=pam, scope="read")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jim,
        )
        WorkspaceMemberFactory(workspace=workspace, user=pam)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jim,
            workspace=workspace,
        )

        # Create notifications
        cls.notification = NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_delete_all_valid_token(self):
        """DELETE /notifications valid token."""
        response = self.delete(
            "/api/notifications?archived=false&unread=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is not 401 or 403
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_delete_all_missing_token(self):
        """DELETE /notifications missing token."""
        response = self.delete(
            "/api/notifications?archived=false&unread=true",
            token="",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_delete_all_invalid_token(self):
        """DELETE /notifications invalid token."""
        response = self.delete(
            "/api/notifications?archived=false&unread=true",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_delete_all_invalid_scope(self):
        """DELETE /notifications invalid scope."""
        response = self.delete(
            "/api/notifications",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_delete_specific_valid_token(self):
        """DELETE /notifications/<notifid> valid token."""
        response = self.delete(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is not 401 or 403
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_delete_specific_missing_token(self):
        """DELETE /notifications/<notifid> missing token."""
        response = self.delete(
            "/api/notifications/" + str(self.notification.id),
            token="",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_delete_specific_invalid_token(self):
        """DELETE /notifications/<notifid> invalid token."""
        response = self.delete(
            "/api/notifications/" + str(self.notification.id),
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_delete_specific_invalid_scope(self):
        """DELETE /notifications/<notifid> invalid scope."""
        response = self.delete(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jim = UserFactory(
            first_name="Jim",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jim, scope="write")
        pam = UserFactory(
            first_name="Pam",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=pam, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jim,
        )
        WorkspaceMemberFactory(workspace=workspace, user=pam)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=pam,
            workspace=workspace,
        )

        # Create notifications
        NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_delete_all_invalid_notifid(self):
        """DELETE /notifications/<notifid> invalid notif id."""
        response = self.delete(
            "/api/notifications/99",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "message": '"99" does not exist.',
                        "code": "does_not_exist",
                        "parameter": "notification",
                    },
                ],
            },
        )


class DeleteAllNotifications(MadbotTestCase):
    """Test the endpoint to delete all notifications."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jim = UserFactory(
            first_name="Jim",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jim, scope="write")
        pam = UserFactory(
            first_name="Pam",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=pam, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jim,
        )
        WorkspaceMemberFactory(workspace=workspace, user=pam)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jim,
            workspace=workspace,
        )

        # Create notifications
        NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=False,
            deleted=True,
            workspace=workspace,
        )
        NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_delete_all_notifications(self):
        """DELETE /notifications remove all the notifications."""
        response = self.delete(
            "/api/notifications?archived=false&unread=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 204
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Assert that all the notifications are deleted
        self.assertEqual(Notification.objects.count(), 1)


class DeleteSpecificNotifications(MadbotTestCase):
    """Test the endpoint to delete specific notifications."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jim = UserFactory(
            first_name="Jim",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jim, scope="write")
        pam = UserFactory(
            first_name="Pam",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=pam, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jim,
        )
        WorkspaceMemberFactory(workspace=workspace, user=pam)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jim,
            workspace=workspace,
        )

        # Create notifications
        NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=False,
            deleted=True,
            workspace=workspace,
        )
        cls.notification = NotificationFactory(
            actor=pam,
            recipient=jim,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_delete_specific_notifications(self):
        """DELETE /notifications/<notifid> remove a specific notification."""
        response = self.delete(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 204
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Assert that only one notification exists
        self.assertEqual(Notification.objects.count(), 1)
