from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NotificationFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_notifications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jack = UserFactory(
            first_name="Jack",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jack, scope="write read")

        karen = UserFactory(
            first_name="Karen",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=karen, scope="read")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jack,
        )
        WorkspaceMemberFactory(workspace=workspace, user=karen)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jack,
            workspace=workspace,
        )

        # Create a notification
        cls.notification = NotificationFactory(
            actor=karen,
            recipient=jack,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_exist(self):
        """GET /notifications/<nid> exists."""
        response = self.get(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(
            response.json(),
            {
                "code": "unsupported_method",
                "message": "Method `GET` not allowed.",
                "details": [],
            },
        )
