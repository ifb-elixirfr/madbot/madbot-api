from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NotificationFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_notifications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "dsfmCDICJOISJpfbMdcdsofpdssdof"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """GET /notifications exists."""
        response = self.delete("/api/notifications")

        # Assert that the response status code is not 404, 405 or 500
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice, scope="read")
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=alice,
        )
        WorkspaceMemberFactory(workspace=workspace, user=bob)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_nodes()
        delete_accesstokens()
        delete_users()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_valid_token(self):
        """GET /notifications valid token."""
        response = self.get(
            "/api/notifications",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is not 401 or 403
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_missing_token(self):
        """GET /notifications missing token."""
        response = self.get(
            "/api/notifications",
            token="",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /notifications invalid token."""
        response = self.get(
            "/api/notifications",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_delete_all_invalid_scope(self):
        """GET /notifications invalid scope."""
        response = self.get(
            "/api/notifications",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class GetNotifications(MadbotTestCase):
    """Test the endpoint to get notifications."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jane = UserFactory(
            first_name="Jane",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jane, scope="read")
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=anne, scope="read")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jane,
        )
        WorkspaceMemberFactory(workspace=workspace, user=anne)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jane,
            workspace=workspace,
        )

        # Create notifications
        cls.notification_1 = NotificationFactory(
            actor=anne,
            recipient=jane,
            unread=False,
            deleted=True,
            workspace=workspace,
        )
        cls.notification_2 = NotificationFactory(
            actor=anne,
            recipient=jane,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_get_notifications_all(self):
        """GET /notifications list all the notifications."""
        response = self.get(
            "/api/notifications",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check pagination details
        self.check_pagination(response.json(), 2, None, None, 1, 1)

        # Extract the first notification from the response
        json_response = response.json().get("results")[0]

        # Assert various attributes of the notification
        self.assertEqual(
            json_response["actor_object"],
            {
                "full_name": "Anne Onymous",
                "email": "anne.onymous@example.com",
                "obj": str(self.notification_1.actor.__class__.__name__),
                "id": str(self.notification_1.actor.id),
            },
        )
        self.assertEqual(json_response["verb"], "verb")
        self.assertEqual(
            json_response["action_object"],
            {
                "obj": str(self.notification_1.action_object_content_type.name),
                "id": str(self.notification_1.action_object_object_id),
                "extra_info": str(self.notification_1.action_object.type.name),
            },
        )
        self.assertEqual(
            json_response["target_object"],
            {
                "obj": "node",
                "id": str(self.notification_1.target.id),
                "name": str(self.notification_1.target.title),
                "extra_info": "assay",
            },
        )
        self.assertEqual(json_response["level"], "info")
        self.assert_date(json_response["timestamp"])
        self.assertEqual(
            json_response["description"],
            "e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertFalse(json_response["unread"])
        self.assertTrue(json_response["archived"])

    def test_get_notifications_unread(self):
        """GET /notifications filter the notification by the unread status."""
        response = self.get(
            "/api/notifications?unread=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check pagination details
        self.check_pagination(response.json(), 1, None, None, 1, 1)

        # Extract the first notification from the response
        json_response = response.json().get("results")[0]

        # Assert various attributes of the notification
        self.assertEqual(
            json_response["actor_object"],
            {
                "full_name": "Anne Onymous",
                "email": "anne.onymous@example.com",
                "obj": str(self.notification_2.actor.__class__.__name__),
                "id": str(self.notification_2.actor.id),
            },
        )
        self.assertEqual(json_response["verb"], "verb")
        self.assertEqual(
            json_response["action_object"],
            {
                "obj": str(self.notification_2.action_object_content_type.name),
                "id": str(self.notification_2.action_object_object_id),
                "extra_info": str(self.notification_2.action_object.type.name),
            },
        )
        self.assertEqual(
            json_response["target_object"],
            {
                "obj": "node",
                "id": str(self.notification_2.target.id),
                "name": str(self.notification_2.target.title),
                "extra_info": str(self.notification_2.target.type.name),
            },
        )
        self.assertEqual(json_response["level"], "info")
        self.assert_date(json_response["timestamp"])
        self.assertEqual(
            json_response["description"],
            "e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertTrue(json_response["unread"])
        self.assertFalse(json_response["archived"])

    def test_get_notifications_archived(self):
        """GET /notifications filter the notification by the archived status."""
        response = self.get(
            "/api/notifications?archived=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check pagination details
        self.check_pagination(response.json(), 1, None, None, 1, 1)

        # Extract the first notification from the response
        json_response = response.json().get("results")[0]

        # Assert various attributes of the notification
        self.assertEqual(
            json_response["actor_object"],
            {
                "full_name": "Anne Onymous",
                "email": "anne.onymous@example.com",
                "obj": str(self.notification_1.actor.__class__.__name__),
                "id": str(self.notification_1.actor.id),
            },
        )
        self.assertEqual(json_response["verb"], "verb")
        self.assertEqual(
            json_response["action_object"],
            {
                "obj": str(self.notification_1.action_object_content_type.name),
                "id": str(self.notification_1.action_object_object_id),
                "extra_info": str(self.notification_1.action_object.type.name),
            },
        )
        self.assertEqual(
            json_response["target_object"],
            {
                "obj": "node",
                "id": str(self.notification_1.target.id),
                "name": str(self.notification_1.target.title),
                "extra_info": str(self.notification_1.target.type.name),
            },
        )
        self.assertEqual(json_response["level"], "info")
        self.assert_date(json_response["timestamp"])
        self.assertEqual(
            json_response["description"],
            "e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertFalse(json_response["unread"])
        self.assertTrue(json_response["archived"])
