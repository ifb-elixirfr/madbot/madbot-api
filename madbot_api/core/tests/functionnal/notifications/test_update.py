from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NotificationFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_accesstokens,
    delete_applications,
    delete_members,
    delete_nodes,
    delete_notifications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "dsfmCDICJOISJpfbMdcdsofpdssdof"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_DATA = {"unread": False, "archived": True}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_exist(self):
        """PUT /notifications/<notifid> exists."""
        response = self.put(
            "/api/notifications/1",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is not 404, 405 or 500
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jack = UserFactory(
            first_name="Jack",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jack, scope="write")
        karen = UserFactory(
            first_name="Karen",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=karen, scope="read")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jack,
        )
        WorkspaceMemberFactory(workspace=workspace, user=karen)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jack,
            workspace=workspace,
        )

        # Create a notification
        cls.notification = NotificationFactory(
            actor=karen,
            recipient=jack,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_missing_token(self):
        """PUT /notifications/<notifid> missing token."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token="",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """PUT /notifications/<notifid> invalid token."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """PUT /notifications/<notifid> invalid scope."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jack = UserFactory(
            first_name="Jack",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jack, scope="write")
        karen = UserFactory(
            first_name="Karen",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=karen, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jack,
        )
        WorkspaceMemberFactory(workspace=workspace, user=karen)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jack,
            workspace=workspace,
        )

        # Create notifications
        cls.notification = NotificationFactory(
            actor=karen,
            recipient=jack,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_update_invalid_notifid(self):
        """PUT /notifications invalid notifid."""
        response = self.put(
            "/api/notifications/99",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "notification",
                        "code": "does_not_exist",
                        "message": '"99" does not exist.',
                    },
                ],
            },
        )

    def test_update_invalid_param_unread(self):
        """PUT /notifications/<notifid> invalid param unread."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"unread": "invalid"},
            content_type="application/json",
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "code": "invalid",
                        "message": 'Invalid value "invalid".',
                        "parameter": "unread",
                    },
                ],
            },
        )

    def test_update_invalid_param_archived(self):
        """PUT /notifications/<notifid> invalid param archived."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={"archived": "invalid"},
            content_type="application/json",
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Assert the response JSON content
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "code": "invalid",
                        "message": 'Invalid value "invalid".',
                        "parameter": "archived",
                    },
                ],
            },
        )


class UpdateNotifications(MadbotTestCase):
    """Test the endpoint with valid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        jane = UserFactory(
            first_name="Jane",
            last_name="Doe",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=jane, scope="write")
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=anne, scope="write")

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=jane,
        )
        WorkspaceMemberFactory(workspace=workspace, user=anne)

        create_isa_collection()
        InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=jane,
            workspace=workspace,
        )

        # Create notifications
        cls.notif = NotificationFactory(
            actor=anne,
            recipient=jane,
            unread=False,
            deleted=True,
            workspace=workspace,
        )
        cls.notification = NotificationFactory(
            actor=anne,
            recipient=jane,
            unread=True,
            deleted=False,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_notifications()
        delete_members()
        delete_accesstokens()
        delete_users()
        delete_nodes()
        delete_workspace_members()
        delete_workspaces()
        delete_applications()

    def test_update_notifications(self):
        """PUT /notifications/<notifid> update notification."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_DATA,
            content_type="application/json",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Extract the notification from the response
        json_response = response.json()

        # Assert various attributes of the notification
        self.assertFalse(json_response["unread"])
        self.assertTrue(json_response["archived"])

    def test_update_notifications_with_empty_body(self):
        """PUT /notifications/<notifid> update notification empty body."""
        response = self.put(
            "/api/notifications/" + str(self.notification.id),
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data={},
            content_type="application/json",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)
