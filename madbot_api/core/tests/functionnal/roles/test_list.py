from django.urls import resolve
from rest_framework import status

from madbot_api.core.tests.functionnal import (
    MadbotTestCase,
    build_accesstoken,
    build_user,
    delete_accesstokens,
    delete_applications,
    delete_users,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
UNVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = build_user(id=1, username="alice")
        build_accesstoken(token=VALID_TOKEN, user=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()

    def test_url_match_view(self):
        """GET /api/roles match view."""
        # The function tests if a given URL exists and matches a specific view.

        # Use resolve to get information about the matched view
        resolver_match = resolve("/api/roles")

        self.assertEqual(
            resolver_match.view_name,
            "roles-list",
        )

    def test_url_exist(self):
        """GET /api/roles exists."""
        # The function tests if the endpoint "/api/roles" exists
        # by making a GET request and checking the response status code.

        response = self.get("/api/roles")
        self.assertNotIn(response.status_code, [404, 405, 500])


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    def test_no_token(self):
        """GET /api/roles not token."""
        # The function tests that accessing a specific API endpoint without a token
        # returns a 401 Unauthorized status code.

        response = self.get("/api/roles")
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_invalid_token(self):
        """GET /api/roles invalid token."""
        # The function tests for an invalid token by making a GET request to a
        # specific API endpoint and asserting that the response status code is 401
        # (Unauthorized).

        response = self.get("/api/roles", token=UNVALID_TOKEN)
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    pass


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    pass


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    pass


class ListRoleDescription(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = build_user(id=1, username="alice")
        build_accesstoken(token=VALID_TOKEN, user=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()

    def test_get_roles_information(self):
        """GET /api/roles list of roles and description."""
        # The function tests the GET request to the API endpoint for retrieving
        # roles information and asserts that the response status code is 200 OK and
        # the response data contains the expected fields.

        # Make a GET request to the API endpoint
        response = self.get("/api/roles", token=VALID_TOKEN)
        # Assert that the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(
            response.data["nodes"],
            [
                {
                    "role": "owner",
                    "description": "full access to the node, can delete it and can manage members",
                },
                {
                    "role": "manager",
                    "description": "full access to the node, and can manage members",
                },
                {
                    "role": "contributor",
                    "description": "full access to the node, but cannot delete it",
                },
                {
                    "role": "collaborator",
                    "description": "can view the node, but not modify any content",
                },
            ],
        )

        self.assertEqual(
            response.data["workspaces"],
            [
                {
                    "role": "owner",
                    "description": "full access to the workspace and all of its nodes, can delete it, manage members and create new nodes",
                },
                {
                    "role": "member",
                    "description": "full access to the workspace and to the nodes they are also a member of, can create new nodes",
                },
            ],
        )
