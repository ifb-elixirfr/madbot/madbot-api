from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    delete_accesstokens,
    delete_users,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()

    def test_exist(self):
        """PUT /roles exists."""
        response = self.put("/api/roles", token=VALID_TOKEN)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(
            response.json(),
            {
                "code": "unsupported_method",
                "message": "Method `PUT` not allowed.",
                "details": [],
            },
        )
