from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    SampleFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "MiZ7F2Tn2PV7sDLWHK8PkLWCOILLLC"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_url_exist(self):
        """GET /nodes/{nid}/samples exists."""
        response = self.get("/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples")
        self.assertNotIn(response.status_code, [404, 405, 500])


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="write")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=seraphim,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """GET /nodes/{nid}/samples valid token."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /nodes/{nid}/samples no token."""
        # The function tests that accessing a specific API endpoint without a token
        # returns a 401 Unauthorized status code.

        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /nodes/{nid}/samples invalid token."""
        # The function tests for an invalid token by making a GET request to a
        # specific API endpoint and asserting that the response status code is 401
        # (Unauthorized).

        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 401
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /nodes/{nid}/samples invalid scope."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_node_member(self):
        """GET /nodes/{nid}/samples not a member of the given node."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """GET /nodes/{nid}/samples not a member of the given workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=hera,
            last_edited_by=hera,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """GET api/nodes/{nid}/samples with invalid workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """GET api/nodes/{nid}/samples invalid uuid value."""
        response = self.get(
            "/api/nodes/wrong_id/samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=hera,
            last_edited_by=hera,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """GET /api/nodes/{nid}/samples with missing workspace."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_1,
        )
        # Assert that the response status code is 400
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_param_nid_missing(self):
        """GET /api/nodes/{nid}/samples nid missing."""
        # The function tests for a missing parameter in the URL and asserts that the
        # response status code is 404.

        response = self.get(
            "/api/nodes//samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=hera,
            last_edited_by=hera,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """GET /api/nodes/{nid}/samples with unknown workspace."""
        response = self.get(
            "/api/nodes/af67c457-91d4-417b-90a9-c905f3b87511/samples",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_node(self):
        """GET /api/nodes/{nid}/samples unknown nid."""
        # The function tests for an invalid node ID format and asserts that
        # the response status code is 404.

        response = self.get(
            "/api/nodes/af67c457-91d4-417b-90a9-c905f3b87511/samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class GetSamples(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        demeter = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="demeter",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="collaborator",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )
        SampleFactory(
            id="0150d151-45d8-4bd8-a772-01fd02c12679",
            alias="s_2",
            title="sample_2",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )
        study = StudyNodeFactory(
            id="7d652766-d1ca-413c-96f8-996177a5aa1d",
            parent=investigation,
            created_by=persephone,
            workspace=workspace,
        )
        SampleFactory(
            id="028dae3a-0c08-44ad-af96-7b47cd80ed5f",
            alias="s_3",
            title="sample_3",
            node=study,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_get_samples_of_node(self):
        """GET /nodes/{nid}/samples get samples of a node."""
        response = self.get(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.check_pagination(response.json(), 2, None, None, 1, 1)
        json_response = response.json().get("results")

        # sample_1
        sample_1_json = json_response[0]
        self.assert_uuid(sample_1_json["id"])
        self.assertEqual(sample_1_json["title"], "sample_1")
        self.assertEqual(
            sample_1_json["node"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(sample_1_json["source"], "local")
        self.assertEqual(
            sample_1_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            sample_1_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(sample_1_json["created_time"])
        self.assert_date(sample_1_json["last_edited_time"])

        # sample_2
        sample_2_json = json_response[1]
        self.assert_uuid(sample_2_json["id"])
        self.assertEqual(sample_2_json["title"], "sample_2")
        self.assertEqual(
            sample_2_json["node"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(sample_2_json["source"], "local")
        self.assertEqual(
            sample_2_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            sample_2_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(sample_2_json["created_time"])
        self.assert_date(sample_2_json["last_edited_time"])

    def test_get_sample_of_inherited_node(self):
        """GET /nodes/{nid}/samples?inherited_samples=true get sample of a node."""
        response = self.get(
            "/api/nodes/7d652766-d1ca-413c-96f8-996177a5aa1d/samples?inherited_samples=true",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        # Assert that the response status code is 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.check_pagination(response.json(), 3, None, None, 1, 1)
        json_response = response.json().get("results")

        # sample_3
        sample_3_json = json_response[0]
        self.assert_uuid(sample_3_json["id"])
        self.assertEqual(sample_3_json["alias"], "s_3")
        self.assertEqual(sample_3_json["title"], "sample_3")
        self.assertEqual(
            sample_3_json["node"]["id"],
            "7d652766-d1ca-413c-96f8-996177a5aa1d",
        )
        self.assertEqual(sample_3_json["source"], "local")
        self.assertEqual(
            sample_3_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            sample_3_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(sample_3_json["created_time"])
        self.assert_date(sample_3_json["last_edited_time"])

        # sample_1
        sample_1_json = json_response[1]
        self.assert_uuid(sample_1_json["id"])
        self.assertEqual(sample_1_json["alias"], "s_1")
        self.assertEqual(sample_1_json["title"], "sample_1")
        self.assertEqual(
            sample_1_json["node"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(sample_1_json["source"], "inherited")
        self.assertEqual(
            sample_1_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            sample_1_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(sample_1_json["created_time"])
        self.assert_date(sample_1_json["last_edited_time"])

        # sample_2
        sample_2_json = json_response[2]
        self.assert_uuid(sample_2_json["id"])
        self.assertEqual(sample_2_json["alias"], "s_2")
        self.assertEqual(sample_2_json["title"], "sample_2")
        self.assertEqual(
            sample_2_json["node"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(sample_2_json["source"], "inherited")
        self.assertEqual(
            sample_2_json["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            sample_2_json["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(sample_2_json["created_time"])
        self.assert_date(sample_2_json["last_edited_time"])
