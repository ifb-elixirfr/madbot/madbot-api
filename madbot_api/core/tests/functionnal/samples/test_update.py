from django.test import override_settings
from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    InvestigationNodeFactory,
    MadbotTestCase,
    NodeMemberFactory,
    SampleFactory,
    StudyNodeFactory,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    create_isa_collection,
    delete_applications,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443A"
VALID_TOKEN_2 = "fOY2dYtRHp2PujfMkL2NIK5ocs442T"
VALID_TOKEN_3 = "fOY2dYtRHp2PujfMkL2NIK5ocs441C"
VALID_TOKEN_4 = "lkjsqidljsqlijdilsjdliqjsdiljs"

INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_SAMPLE = {"title": "sample_1", "alias": "s_1"}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="alice",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_exist(self):
        """UPDATE /api/nodes/{nid}/samples/{sid} exists."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                status.HTTP_405_METHOD_NOT_ALLOWED,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        seraphim = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="seraphim",
        )
        demeter = UserFactory(username="demeter")
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter, scope="write")
        AccessTokenFactory(token=VALID_TOKEN_2, user=demeter, scope="read")
        hera = UserFactory(username="hera")
        AccessTokenFactory(token=VALID_TOKEN_3, user=hera)
        hades = UserFactory(username="hades")
        AccessTokenFactory(token=VALID_TOKEN_4, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=seraphim,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=seraphim,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_valid_token(self):
        """PUT /nodes/{nid}/samples/{sid} valid token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """PUT /nodes/{nid}/samples/{sid} no token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """PUT /nodes/{nid}/samples/{sid} invalid token."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=INVALID_TOKEN,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """PUT /nodes/{nid}/samples/{sid} invalid scope."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_2,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_not_a_node_member(self):
        """PUT /nodes/{nid}/samples/{sid} not a member of the given node."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_3,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                ],
            },
        )

    def test_not_a_workspace_member(self):
        """PUT /nodes/{nid}/samples/{sid} not a member of the given workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_4,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        hera = UserFactory(id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3", username="hera")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hera)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hera, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hera,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=hera,
            last_edited_by=hera,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_param_invalid_workspace(self):
        """PUT api/nodes/{nid}/samples/{sid} with invalid workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="invalid_workspace",
            data=VALID_SAMPLE,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "invalid",
                        "message": '"invalid_workspace" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_nid(self):
        """PUT api/nodes/{nid}/samples/{sid} invalid uuid value."""
        response = self.put(
            "/api/nodes/wrong_id/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=VALID_SAMPLE,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_invalid_sid(self):
        """PUT api/nodes/{nid}/samples/{sid} invalid uuid value."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/wrong_id",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "invalid",
                        "message": '"wrong_id" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_param_with_unknown_field(self):
        """PUT api/nodes/{nid}/samples/{sid} with unknown field."""
        data = dict(VALID_SAMPLE)
        data.update({"fake_field": "error"})

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )

    def test_param_invalid_slug(self):
        """PUT api/nodes/{nid}/samples/{sid} with invalid sample slug."""
        data = dict(VALID_SAMPLE)
        data.update({"alias": "$*-+"})

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "alias",
                        "code": "invalid",
                        "message": 'Invalid value "$*-+".',
                    },
                ],
            },
        )


class MissingParams(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="persephone",
        )
        hades = UserFactory(id="302f9aa3-3c85-4b56-9574-ae97e739863f", username="hades")
        AccessTokenFactory(token=VALID_TOKEN_1, user=hades)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=hades, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=hades,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_0",
            title="sample_0",
            node=investigation,
            created_by=hades,
            last_edited_by=hades,
        )
        SampleFactory(
            id="77581103-ddda-4e30-aa88-570d713edf12",
            alias="s_2",
            title="sample_2",
            node=investigation,
            created_by=hades,
            last_edited_by=hades,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_missing_workspace(self):
        """PUT api/nodes/{nid}/samples/{sid} with missing workspace."""
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            data=VALID_SAMPLE,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_body_param_title_missing(self):
        """PUT api/nodes/{nid}/samples/{sid} missing body parameter."""
        data = dict(VALID_SAMPLE)
        data.pop("title")

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert the response status code 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["alias"], "s_1")
        self.assertEqual(json_response["title"], "sample_0")
        self.assertEqual(
            json_response["node"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "302f9aa3-3c85-4b56-9574-ae97e739863f"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "302f9aa3-3c85-4b56-9574-ae97e739863f"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])

    def test_body_param_alias_missing(self):
        """PUT api/nodes/{nid}/samples/{sid} missing body parameter ."""
        data = dict(VALID_SAMPLE)
        data.pop("alias")

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/77581103-ddda-4e30-aa88-570d713edf12",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert the response status code 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["alias"], "s_2")
        self.assertEqual(json_response["title"], "sample_1")
        self.assertEqual(
            json_response["node"]["id"],
            "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "302f9aa3-3c85-4b56-9574-ae97e739863f"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "302f9aa3-3c85-4b56-9574-ae97e739863f"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])


class UnknownParam(MadbotTestCase):
    """Test the endpoint with unknown parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        AccessTokenFactory(token=VALID_TOKEN_1, user=persephone)

        demeter = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="demeter",
        )

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_unknown_workspace(self):
        """PUT api/nodes/{nid}/samples/{sid} with unknown workspace."""
        response = self.put(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="ba68296e-0f68-4c35-9494-786a8105cd24",
            data=VALID_SAMPLE,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "header_validation_error",
                "message": "Invalid header. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "x-workspace",
                        "code": "does_not_exist",
                        "message": '"ba68296e-0f68-4c35-9494-786a8105cd24" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_node(self):
        """PUT api/nodes/{nid}/samples/{sid} unknown node."""
        data = dict(VALID_SAMPLE)
        response = self.put(
            "/api/nodes/9000c0a0-7000-400f-b0dc-d0000aa00bdf/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )
        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "does_not_exist",
                        "message": '"9000c0a0-7000-400f-b0dc-d0000aa00bdf" does not exist.',
                    },
                ],
            },
        )

    def test_unknown_sample(self):
        """PUT /api/nodes/{nid}/samples/{sid} unknown sid."""
        # The function tests for an invalid sample ID format and asserts that
        # the response status code is 404.

        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/af67c457-91d4-417b-90a9-c905f3b87511",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
        )

        # Assert that the response status code is 404
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "sample",
                        "code": "does_not_exist",
                        "message": '"af67c457-91d4-417b-90a9-c905f3b87511" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")

        demeter = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="demeter",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="9ed12a30-46e6-4410-832b-ad4f0a1c080c",
            user=demeter,
            node=investigation,
            role="collaborator",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_1",
            title="sample_1",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    def test_create_sample_not_owner_or_manager_or_contributor(self):
        """PUT /api/nodes/{nid}/samples/{sid} post without owner or manager role."""
        data = dict(VALID_SAMPLE)
        response = self.put(
            "/api/nodes/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/samples/e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=data,
        )

        # Assert that the response status code is 403
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        # Assert the response content
        json_response = response.json()
        self.assertEqual(
            json_response,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow update.",
                    },
                ],
            },
        )


class Update(MadbotTestCase):
    """General tests of the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        persephone = UserFactory(username="persephone")
        demeter = UserFactory(
            id="3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3",
            username="demeter",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=demeter)

        workspace = WorkspaceFactory(
            id="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            created_by=persephone,
        )
        WorkspaceMemberFactory(user=demeter, workspace=workspace)

        create_isa_collection()
        investigation = InvestigationNodeFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=persephone,
            workspace=workspace,
        )
        NodeMemberFactory(
            id="ec074d6b-62ab-4fe5-92aa-0d3bee2882c0",
            user=demeter,
            node=investigation,
            role="owner",
        )
        SampleFactory(
            id="e2a8e605-2ab9-490b-a5c6-af1a80bf3260",
            alias="s_0",
            title="sample_0",
            node=investigation,
            created_by=demeter,
            last_edited_by=demeter,
        )
        study = StudyNodeFactory(
            id="7d652766-d1ca-413c-96f8-996177a5aa1d",
            parent=investigation,
            created_by=persephone,
            workspace=workspace,
        )
        SampleFactory(
            id="0150d151-45d8-4bd8-a772-01fd02c12679",
            alias="s_2",
            title="sample_2",
            node=study,
            created_by=demeter,
            last_edited_by=demeter,
        )
        workspace_2 = WorkspaceFactory(
            id="8296a9b6-8265-444d-b55d-47e136a67143",
            created_by=persephone,
        )
        investigation_2 = InvestigationNodeFactory(
            id="ac079cf5-0a78-4b8a-aa1a-3548a0e0ad64",
            created_by=persephone,
            workspace=workspace_2,
        )
        SampleFactory(
            id="589b4841-e660-4a07-9058-93d0fe60a80a",
            alias="s_1",
            title="sample_1",
            node=investigation_2,
            created_by=persephone,
            last_edited_by=persephone,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_applications()
        delete_users()
        delete_workspaces()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_update_sample_as_owner(self):
        """PUT api/nodes/{nid}/samples/{sid} update samples with owner role."""
        response = self.put(
            "/api/nodes/7d652766-d1ca-413c-96f8-996177a5aa1d/samples/0150d151-45d8-4bd8-a772-01fd02c12679",
            token=VALID_TOKEN_1,
            workspace="e4a01f2b-ad34-4e80-ae1b-23218a65cbed",
            data=dict(VALID_SAMPLE),
        )

        # Assert the response status code 200
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()
        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["alias"], "s_1")
        self.assertEqual(json_response["title"], "sample_1")
        self.assertEqual(
            json_response["node"]["id"],
            "7d652766-d1ca-413c-96f8-996177a5aa1d",
        )
        self.assertEqual(json_response["source"], "local")
        self.assertEqual(
            json_response["created_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {"object": "MadbotUser", "id": "3f8c7603-8da0-4e3d-a0f7-54d657f7d8b3"},
        )
        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
