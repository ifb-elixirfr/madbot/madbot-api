from rest_framework import status

from madbot_api import VERSION
from madbot_api.core.tests.functionnal import MadbotTestCase


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    def test_url_exist(self):
        """GET /api/version exists."""
        response = self.get("/api/version")
        self.assertNotIn(response.status_code, [404, 405, 500])


class GetVersion(MadbotTestCase):
    """Test that the endpoint returns the version."""

    def test_get_version(self):
        """GET /api/version get version."""
        # Make a GET request to the API endpoint
        response = self.get("/api/version")
        # Assert that the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test the version returned
        self.assertEqual(VERSION, response.data["version"])
