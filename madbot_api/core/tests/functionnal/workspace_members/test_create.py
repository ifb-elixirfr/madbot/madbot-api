from django.test import override_settings
from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    delete_accesstokens,
    delete_applications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "clutkn59c0001y1tlgrqvq5exclutk"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_MEMBER = {
    "user": "05c7b722-bc39-4e31-be1c-a01957cae622",
    "role": "member",
}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_url_exist(self):
        """POST /workspaces/{wid}/members exists."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """POST /workspaces/{wid}/members access allowed with valid token."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """POST /workspaces/{wid}/members access denied without token."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """POST /workspaces/{wid}/members access denied with invalid token."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=INVALID_TOKEN,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """POST /workspaces/{wid}/members access denied if invalid scope."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_2,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

        # Alan, not member
        alan = UserFactory(
            first_name="Alan",
            last_name="Smithee",
            id="05c7b722-bc39-4e31-be1c-a01957cae622",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=alan)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_unknown_param(self):
        """POST /workspaces/{wid}/members unknown param."""
        data = dict(VALID_MEMBER)
        data.update({"fake_field": "error"})

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )

    def test_invalid_wid(self):
        """POST /workspaces/{wid}/members invalid workspace id."""
        response = self.post(
            "/api/workspaces/invalid_uuid/members",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_wid(self):
        """POST /workspaces/{wid}/members unknown workspace id."""
        response = self.post(
            "/api/workspaces/d2f6e905-6a01-48a6-9180-65f01acc6dc1/members",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "does_not_exist",
                        "message": '"d2f6e905-6a01-48a6-9180-65f01acc6dc1" does not exist.',
                    },
                ],
            },
        )

    def test_invalid_user(self):
        """POST /workspaces/{wid}/members invalid user id."""
        data = dict(VALID_MEMBER)
        data.update({"user": "invalid_uuid"})

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_user(self):
        """POST /workspaces/{wid}/members unknown user id."""
        data = dict(VALID_MEMBER)
        data.update({"user": "f85b05c6-9278-4068-ad29-d451122032de"})

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "does_not_exist",
                        "message": '"f85b05c6-9278-4068-ad29-d451122032de" does not exist.',
                    },
                ],
            },
        )

    def test_invalid_role(self):
        """POST /workspaces/{wid}/members invalid role."""
        data = dict(VALID_MEMBER)
        data.update({"role": "invalid"})

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "invalid_choice",
                        "message": '"invalid" is not a valid choice.',
                    },
                ],
            },
        )


class MissingParam(MadbotTestCase):
    """Test the endpoint with missing parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

        # Alan, not member
        alan = UserFactory(
            first_name="Alan",
            last_name="Smithee",
            id="05c7b722-bc39-4e31-be1c-a01957cae622",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=alan)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspace_members()
        delete_workspaces()

    def test_missing_user(self):
        """POST /workspaces/{wid}/members missing user."""
        data = dict(VALID_MEMBER)
        data.pop("user")

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "user",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )

    def test_missing_role(self):
        """POST /workspaces/{wid}/members missing role."""
        data = dict(VALID_MEMBER)
        data.pop("role")

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "required",
                        "message": "This field is required.",
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice", id="9188c6a5-7381-452f-b3dc-d4865aa89bdf")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )

        # Alan, not member
        alan = UserFactory(
            first_name="Alan",
            last_name="Smithee",
            id="1d14482a-09c9-4243-9db6-88e5e6d084fb",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=alan)

        # Anne, member of the workspace, but not owner
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="05c7b722-bc39-4e31-be1c-a01957cae622",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=anne)
        WorkspaceMemberFactory(
            id="c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            user=anne,
            role="member",
            workspace=workspace,
            created_by=alice,
        )

        # John
        john = UserFactory(
            first_name="John",
            last_name="Doe",
            id="448ceadf-b8fe-4c52-87fd-2f032efd392b",
        )
        WorkspaceMemberFactory(
            id="f8fca896-0840-4433-bcb4-04b65059c33f",
            user=john,
            role="owner",
            workspace=workspace,
            created_by=alice,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_create_workspace_member_not_member(self):
        """POST /workspaces/{wid}/members not a member."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_2,
            data=VALID_MEMBER,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_create_workspace_member_as_member(self):
        """POST /workspaces/{wid}/members create workspace member as member."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_3,
            data=VALID_MEMBER,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "not_allowed",
                        "message": "You must be owner of the workspace to add a new member.",
                    },
                ],
            },
        )

    def test_create_workspace_member_already_member(self):
        """POST /workspaces/{wid}/members create workspace member already member."""
        data = dict(VALID_MEMBER)
        data.update({"user": "05c7b722-bc39-4e31-be1c-a01957cae622"})

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "save_validation_error",
                "message": "The request body data are incorrect. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "invalid",
                        "message": "User is already a member of this workspace.",
                    },
                ],
            },
        )


class Create(MadbotTestCase):
    """Test the endpoint with creating a workspace member."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice", id="9188c6a5-7381-452f-b3dc-d4865aa89bdf")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="05c7b722-bc39-4e31-be1c-a01957cae622",
        )
        UserFactory(
            first_name="John",
            last_name="Doe",
            id="448ceadf-b8fe-4c52-87fd-2f032efd392b",
        )
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_workspace_member(self):
        """POST /workspaces/{wid}/members create workspace member."""
        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()

        self.assert_uuid(json_response["id"])

        self.assertEqual(
            json_response["user"],
            {
                "id": "05c7b722-bc39-4e31-be1c-a01957cae622",
                "username": "aonymous",
                "first_name": "Anne",
                "last_name": "Onymous",
                "email": "anne.onymous@example.com",
            },
        )
        self.assertEqual(json_response["role"], "member")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_create_workspace_owner(self):
        """POST /workspaces/{wid}/members create workspace owner."""
        data = {"role": "owner", "user": "448ceadf-b8fe-4c52-87fd-2f032efd392b"}

        response = self.post(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
            data=data,
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        json_response = response.json()

        self.assert_uuid(json_response["id"])

        self.assertEqual(
            json_response["user"],
            {
                "id": "448ceadf-b8fe-4c52-87fd-2f032efd392b",
                "username": "jdoe",
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
            },
        )
        self.assertEqual(json_response["role"], "owner")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )
