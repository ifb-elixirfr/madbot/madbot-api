from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import WorkspaceMember
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    delete_accesstokens,
    delete_applications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "clutkn59c0001y1tlgrqvq5exclutk"
VALID_TOKEN_4 = "ILOJVSvdspopvksdffdszrtjtyjyjj"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_url_exist(self):
        """DELETE /workspaces/{wid}/members/{wmid} exists."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """DELETE /workspaces/{wid}/members/{wmid} access allowed with valid token."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """DELETE /workspaces/{wid}/members/{wmid} access denied without token."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """DELETE /workspaces/{wid}/members/{wmid} access denied with invalid token."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=INVALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """DELETE /workspaces/{wid}/members/{wmid} access denied if invalid scope."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_2,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_invalid_wid(self):
        """DELETE /workspaces/{wid}/members/{wmid} invalid workspace id."""
        response = self.delete("/api/workspaces/invalid_uuid", token=VALID_TOKEN_1)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_wid(self):
        """DELETE /workspaces/{wid}/members/{wmid} unknown workspace id."""
        response = self.delete(
            "/api/workspaces/d2f6e905-6a01-48a6-9180-65f01acc6dc1",
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "does_not_exist",
                        "message": '"d2f6e905-6a01-48a6-9180-65f01acc6dc1" does not exist.',
                    },
                ],
            },
        )

    def test_invalid_wmid(self):
        """DELETE /workspaces/{wid}/members/{wmid} invalid workspace member id."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/invalid_uuid",
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_wmid(self):
        """DELETE /workspaces/{wid}/members/{wmid} unknown workspace member id."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/d2f6e905-6a01-48a6-9180-65f01acc6dc1",
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "does_not_exist",
                        "message": '"d2f6e905-6a01-48a6-9180-65f01acc6dc1" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.alice = UserFactory(
            username="alice",
            id="9188c6a5-7381-452f-b3dc-d4865aa89bdf",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=cls.alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=cls.alice,
        )

        # Alan, not member
        alan = UserFactory(
            first_name="Alan",
            last_name="Smithee",
            id="1d14482a-09c9-4243-9db6-88e5e6d084fb",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=alan)

        # Anne, member of the workspace
        cls.anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="05c7b722-bc39-4e31-be1c-a01957cae622",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=cls.anne)
        WorkspaceMemberFactory(
            id="c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            user=cls.anne,
            role="member",
            workspace=workspace,
            created_by=cls.alice,
        )

        # May, member of the workspace
        cls.may = UserFactory(
            first_name="May",
            last_name="Anny",
            id="01a7a76f-e735-4781-b4e8-3c0e5fcbddfc",
        )
        AccessTokenFactory(token=VALID_TOKEN_4, user=cls.anne)
        WorkspaceMemberFactory(
            id="7b7331c5-604c-408d-95d6-2ce976d98bc8",
            user=cls.may,
            role="member",
            workspace=workspace,
            created_by=cls.alice,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_delete_workspace_member_not_member(self):
        """DELETE /workspaces/{wid}/members/{wmid} not member."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            token=VALID_TOKEN_2,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_delete_workspace_member_as_member(self):
        """DELETE /workspaces/{wid}/members delete workspace member as member."""
        member = WorkspaceMember.objects.get(
            user=self.may,
            workspace="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )

        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/"
            + str(member.id),
            token=VALID_TOKEN_3,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "not_allowed",
                        "message": "You must be owner of the workspace to remove another member.",
                    },
                ],
            },
        )

    def test_leave_workspace_last_owner(self):
        """DELETE /workspaces/{wid}/members leave workspace last owner."""
        member = WorkspaceMember.objects.get(
            user=self.alice,
            workspace="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )

        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/"
            + str(member.id),
            token=VALID_TOKEN_1,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "not_allowed",
                        "message": "This member cannot be removed because they are the last owner of the current workspace.",
                    },
                ],
            },
        )


class Delete(MadbotTestCase):
    """General tests for the endpoint."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice", id="9188c6a5-7381-452f-b3dc-d4865aa89bdf")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=anne)
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

        john = UserFactory(
            first_name="John",
            last_name="Doe",
            id="4cb3b4f2-0b1f-4ffc-9a5b-226b326fbe02",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=john)
        WorkspaceMemberFactory(
            id="4b45e2c7-7863-4eb4-841e-7f53e46a71e0",
            user=john,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_delete_workspace_member(self):
        """DELETE /workspaces/{wid}/members/{wmid} delete workspace member."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            WorkspaceMember.objects.filter(
                id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            ).exists(),
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_leave_workspace(self):
        """DELETE /workspaces/{wid}/members/{wmid} leave workspace."""
        response = self.delete(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/4b45e2c7-7863-4eb4-841e-7f53e46a71e0",
            token=VALID_TOKEN_3,
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            WorkspaceMember.objects.filter(
                id="4b45e2c7-7863-4eb4-841e-7f53e46a71e0",
            ).exists(),
        )
