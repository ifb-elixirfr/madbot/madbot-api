from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    delete_accesstokens,
    delete_applications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_not_exist(self):
        """GET /workspaces/{wip} does not exists."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(
            response.json(),
            {
                "code": "unsupported_method",
                "message": "Method `GET` not allowed.",
                "details": [],
            },
        )
