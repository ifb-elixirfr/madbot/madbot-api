from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    WorkspaceFactory,
    delete_accesstokens,
    delete_applications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_url_exist(self):
        """GET /workspaces/{wid}/members exists."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """GET /workspaces/{wid}/members access allowed with valid token."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /workspaces/{wid}/members access denied without token."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /workspaces/{wid}/members access denied with invalid token."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=INVALID_TOKEN,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /workspaces/{wid}/members access denied if invalid scope."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_2,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_invalid_wid(self):
        """GET /workspaces/{wid}/members invalid workspace id."""
        response = self.get(
            "/api/workspaces/invalid_uuid/members",
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_wid(self):
        """GET /workspaces/{wid}/members unknown workspace id."""
        response = self.get(
            "/api/workspaces/d2f6e905-6a01-48a6-9180-65f01acc6dc1/members",
            token=VALID_TOKEN_1,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "does_not_exist",
                        "message": '"d2f6e905-6a01-48a6-9180-65f01acc6dc1" does not exist.',
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice", id="9188c6a5-7381-452f-b3dc-d4865aa89bdf")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

        # Alan, not member
        alan = UserFactory(
            first_name="Alan",
            last_name="Smithee",
            id="1d14482a-09c9-4243-9db6-88e5e6d084fb",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=alan)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_get_workspace_members_not_member(self):
        """GET /workspaces/{wid}/members not member."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_2,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )


class List(MadbotTestCase):
    """Test the endpoint with listing members."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="9188c6a5-7381-452f-b3dc-d4865aa89bdf",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_list_workspace_members(self):
        """GET /workspaces/{wid}/members list workspace members."""
        response = self.get(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members",
            token=VALID_TOKEN_1,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.check_pagination(response.json(), 1, None, None, 1, 1)
        json_response = response.json().get("results")[0]

        self.assert_uuid(json_response["id"])

        self.assertEqual(
            json_response["user"],
            {
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
                "username": "aonymous",
                "first_name": "Anne",
                "last_name": "Onymous",
                "email": "anne.onymous@example.com",
            },
        )

        self.assertEqual(json_response["role"], "owner")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )
