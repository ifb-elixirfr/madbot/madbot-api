from django.test import override_settings
from rest_framework import status

from madbot_api.core.models import WorkspaceMember
from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    WorkspaceFactory,
    WorkspaceMemberFactory,
    delete_accesstokens,
    delete_applications,
    delete_users,
    delete_workspace_members,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
VALID_TOKEN_3 = "clutkn59c0001y1tlgrqvq5exclutk"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"

VALID_MEMBER = {"role": "owner"}


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_url_exist(self):
        """PUT /workspaces/{wid}/members/{wmid} exists."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
        )
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="read")
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_valid_token(self):
        """PUT /workspaces/{wid}/members/{wmid} access allowed with valid token."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """PUT /workspaces/{wid}/members/{wmid} access denied without token."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """PUT /workspaces/{wid}/members/{wmid} access denied with invalid token."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=INVALID_TOKEN,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """PUT /workspaces/{wid}/members/{wmid} access denied with invalid scope."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_2,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class InvalidParam(MadbotTestCase):
    """Test the endpoint with invalid parameters."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_invalid_wid(self):
        """PUT /workspaces/{wid}/members/{wmid} invalid workspace id."""
        response = self.put(
            "/api/workspaces/invalid_uuid/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_wid(self):
        """PUT /workspaces/{wid}/members/{wmid} unknown workspace id."""
        response = self.put(
            "/api/workspaces/d2f6e905-6a01-48a6-9180-65f01acc6dc1/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "does_not_exist",
                        "message": '"d2f6e905-6a01-48a6-9180-65f01acc6dc1" does not exist.',
                    },
                ],
            },
        )

    def test_invalid_wmid(self):
        """PUT /workspaces/{wid}/members/{wmid} invalid workspace member id."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/invalid_uuid",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "uri_validation_error",
                "message": "Invalid URI parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "invalid",
                        "message": '"invalid_uuid" is not a valid UUID.',
                    },
                ],
            },
        )

    def test_unknown_wmid(self):
        """PUT /workspaces/{wid}/members/{wmid} unknown workspace member id."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/d2f6e905-6a01-48a6-9180-65f01acc6dc1",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(
            response.json(),
            {
                "code": "object_not_found",
                "message": "Object not found. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "does_not_exist",
                        "message": '"d2f6e905-6a01-48a6-9180-65f01acc6dc1" does not exist.',
                    },
                ],
            },
        )

    def test_invalid_role(self):
        """PUT /workspaces/{wid}/members/{wmid} invalid role."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
            data={"role": "invalid"},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "invalid_choice",
                        "message": '"invalid" is not a valid choice.',
                    },
                ],
            },
        )

    def test_unknown_param(self):
        """PUT /workspaces/{wid}/members/{wmid} unknown param."""
        data = dict(VALID_MEMBER)
        data.update({"fake_field": "error"})

        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
            data=data,
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.json(),
            {
                "code": "body_validation_error",
                "message": "The request body does not match the schema for the expected parameters. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "fake_field",
                        "code": "not_supported",
                        "message": "This field is not supported.",
                    },
                ],
            },
        )


class Restriction(MadbotTestCase):
    """Test the endpoint with restricted access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        cls.alice = UserFactory(
            username="alice",
            id="9188c6a5-7381-452f-b3dc-d4865aa89bdf",
        )
        AccessTokenFactory(token=VALID_TOKEN_1, user=cls.alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=cls.alice,
        )

        # Alan, not member
        alan = UserFactory(
            first_name="Alan",
            last_name="Smithee",
            id="1d14482a-09c9-4243-9db6-88e5e6d084fb",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=alan)

        # Anne, member of the workspace
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="05c7b722-bc39-4e31-be1c-a01957cae622",
        )
        AccessTokenFactory(token=VALID_TOKEN_3, user=anne)
        WorkspaceMemberFactory(
            id="c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            user=anne,
            workspace=workspace,
            created_by=cls.alice,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    def test_put_workspace_member_not_member(self):
        """PUT /workspaces/{wid}/members/{wmid} not member."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            token=VALID_TOKEN_2,
            data=VALID_MEMBER,
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace",
                        "code": "not_allowed",
                        "message": "You are not a member of this workspace.",
                    },
                ],
            },
        )

    def test_update_workspace_member_as_member(self):
        """PUT /workspaces/{wid}/members create workspace member as member."""
        member = WorkspaceMember.objects.get(
            user=self.alice,
            workspace="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )

        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/"
            + str(member.id),
            token=VALID_TOKEN_3,
            data={"role": "member"},
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "workspace_member",
                        "code": "not_allowed",
                        "message": "You must be owner of the workspace to edit other members.",
                    },
                ],
            },
        )

    def test_update_workspace_member_last_owner(self):
        """PUT /workspaces/{wid}/members leave workspace last owner."""
        member = WorkspaceMember.objects.get(
            user=self.alice,
            workspace="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
        )

        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/"
            + str(member.id),
            token=VALID_TOKEN_1,
            data={"role": "member"},
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "role",
                        "code": "not_allowed",
                        "message": "The role of the workspace member cannot be changed because they are the last owner of the current workspace.",
                    },
                ],
            },
        )


class Update(MadbotTestCase):
    """Test the endpoint with valid data."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice", id="9188c6a5-7381-452f-b3dc-d4865aa89bdf")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        workspace = WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
        )
        anne = UserFactory(
            first_name="Anne",
            last_name="Onymous",
            id="f4003945-9d43-43bc-9209-c18564e635ea",
        )
        WorkspaceMemberFactory(
            id="e8b06034-ed20-4429-81cb-be44629a8b5c",
            user=anne,
            workspace=workspace,
            created_by=alice,
        )
        john = UserFactory(
            first_name="John",
            last_name="Doe",
            id="a53a5237-6603-4854-ba6f-bb40f880d4c2",
        )
        AccessTokenFactory(token=VALID_TOKEN_2, user=john)
        WorkspaceMemberFactory(
            id="c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            user=john,
            role="owner",
            workspace=workspace,
            created_by=alice,
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()
        delete_workspace_members()

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_update_workspace_member_role_owner(self):
        """PUT /workspaces/{wid}/members/{wmid} update workspace member to owner."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/e8b06034-ed20-4429-81cb-be44629a8b5c",
            token=VALID_TOKEN_1,
            data=VALID_MEMBER,
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], "e8b06034-ed20-4429-81cb-be44629a8b5c")

        self.assertEqual(
            json_response["user"],
            {
                "id": "f4003945-9d43-43bc-9209-c18564e635ea",
                "username": "aonymous",
                "first_name": "Anne",
                "last_name": "Onymous",
                "email": "anne.onymous@example.com",
            },
        )

        self.assertEqual(json_response["role"], "owner")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_update_workspace_member_role_member(self):
        """PUT /workspaces/{wid}/members/{wmid} update workspace owner to member."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            token=VALID_TOKEN_1,
            data={"role": "member"},
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], "c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b")

        self.assertEqual(
            json_response["user"],
            {
                "id": "a53a5237-6603-4854-ba6f-bb40f880d4c2",
                "username": "jdoe",
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
            },
        )

        self.assertEqual(json_response["role"], "member")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

    @override_settings(
        CHANNEL_LAYERS={"default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}},
    )
    def test_workspace_owner_demote_themselves(self):
        """PUT /workspaces/{wid}/members/{wmid} update workspace owner to member."""
        response = self.put(
            "/api/workspaces/82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b/members/c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b",
            token=VALID_TOKEN_2,
            data={"role": "member"},
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        json_response = response.json()

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], "c4b2b1d8-4d9b-4e3f-8b0e-0d5e4c1d2e9b")

        self.assertEqual(
            json_response["user"],
            {
                "id": "a53a5237-6603-4854-ba6f-bb40f880d4c2",
                "username": "jdoe",
                "first_name": "John",
                "last_name": "Doe",
                "email": "john.doe@example.com",
            },
        )

        self.assertEqual(json_response["role"], "member")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )
        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "a53a5237-6603-4854-ba6f-bb40f880d4c2",
            },
        )
