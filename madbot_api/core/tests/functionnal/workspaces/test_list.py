from rest_framework import status

from madbot_api.core.tests.functionnal import (
    AccessTokenFactory,
    MadbotTestCase,
    UserFactory,
    WorkspaceFactory,
    delete_accesstokens,
    delete_applications,
    delete_users,
    delete_workspaces,
)

VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"
VALID_TOKEN_2 = "fGtLm9Gb4Fg4DLNpfbMXM7m4c7nJma"
INVALID_TOKEN = "TestInvalidfOY3dYtRHp4PujfMkL8NIK5ocs443N"


class Exists(MadbotTestCase):
    """Test that the endpoint exists."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()

    def test_url_exist(self):
        """GET /workspaces exists."""
        response = self.get("/api/workspaces")
        self.assertNotIn(
            response.status_code,
            [
                status.HTTP_404_NOT_FOUND,
                status.HTTP_405_METHOD_NOT_ALLOWED,
                status.HTTP_500_INTERNAL_SERVER_ERROR,
            ],
        )


class Access(MadbotTestCase):
    """Test that the endpoint requires access."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        bob = UserFactory(username="bob")
        AccessTokenFactory(token=VALID_TOKEN_2, user=bob, scope="write")
        WorkspaceFactory(id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b", created_by=alice)

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()

    def test_valid_token(self):
        """GET /workspaces access allowed with valid token."""
        response = self.get("/api/workspaces", token=VALID_TOKEN_1)
        self.assertNotIn(
            response.status_code,
            [status.HTTP_401_UNAUTHORIZED, status.HTTP_403_FORBIDDEN],
        )

    def test_no_token(self):
        """GET /workspaces access denied without token."""
        response = self.get("/api/workspaces")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Authentication credentials were not provided.",
                    },
                ],
            },
        )

    def test_invalid_token(self):
        """GET /workspaces access denied with invalid token."""
        response = self.get("/api/workspaces", token=INVALID_TOKEN)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(
            response.json(),
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "The access token is invalid.",
                    },
                ],
            },
        )

    def test_invalid_scope(self):
        """GET /workspaces access denied if invalid scope."""
        response = self.get("/api/workspaces", token=VALID_TOKEN_2)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(
            response.json(),
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )


class List(MadbotTestCase):
    """Test that the endpoint returns a list of workspaces."""

    @classmethod
    def setUpClass(cls):
        """Set up the class."""
        alice = UserFactory(username="alice", id="9188c6a5-7381-452f-b3dc-d4865aa89bdf")
        AccessTokenFactory(token=VALID_TOKEN_1, user=alice)
        WorkspaceFactory(
            id="82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b",
            created_by=alice,
            name="Atlasea",
            description="Lorem of the sea",
        )
        bob = UserFactory(username="bob", id="10265196-44e1-4ac8-ad71-f993716bc185")
        WorkspaceFactory(
            id="9b0ca175-37c3-42ed-8910-3e0714476515",
            created_by=bob,
            name="Atlasea 2",
            description="Lorem of the sea 2",
        )

    @classmethod
    def tearDownClass(cls):
        """Tear down the class."""
        delete_accesstokens()
        delete_users()
        delete_applications()
        delete_workspaces()

    def test_list_workspaces(self):
        """GET /workspaces list workspaces."""
        response = self.get("/api/workspaces", token=VALID_TOKEN_1)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.check_pagination(response.json(), 1, None, None, 1, 1)
        json_response = response.json().get("results")[0]

        self.assert_uuid(json_response["id"])
        self.assertEqual(json_response["id"], "82b9e33d-4ae9-4bd3-9be5-5a81f8fa4d9b")

        self.assertEqual(json_response["name"], "Atlasea")
        self.assertEqual(json_response["description"], "Lorem of the sea")

        self.assert_date(json_response["created_time"])
        self.assert_date(json_response["last_edited_time"])
        self.assertEqual(
            json_response["created_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )

        self.assertEqual(
            json_response["last_edited_by"],
            {
                "object": "MadbotUser",
                "id": "9188c6a5-7381-452f-b3dc-d4865aa89bdf",
            },
        )
