from django.test import RequestFactory, TestCase

from madbot_api.core.tests.functionnal import build_user, delete_users
from madbot_api.core.views.utils.oauth_validators import CustomOAuth2Validator


class CustomOAuth2ValidatorTests(TestCase):
    """Test the CustomOAuth2Validator class."""

    @classmethod
    def setUpClass(cls):
        """Create a user and a RequestFactory instance."""
        cls.factory = RequestFactory()
        cls.john = build_user(
            username="john",
            email="john@example.com",
            firstname="John",
            lastname="Doe",
        )

    @classmethod
    def tearDownClass(cls):
        """Delete the created user."""
        delete_users()

    def test_get_additional_claims(self):
        """Test the behavior of get_additional_claims."""
        # Create a test request using the RequestFactory
        request = self.factory.get("/test/path/to/token/endpoint/")
        request.user = self.john

        # Create an instance of CustomOAuth2Validator
        custom_validator = CustomOAuth2Validator()

        # Call get_additional_claims with the test request and retrieve the claims
        claims = custom_validator.get_additional_claims(request)

        # Assert that all the additionnal claims matches the expected values
        self.assertEqual(claims["given_name"], "John")
        self.assertEqual(claims["family_name"], "Doe")
        self.assertEqual(claims["name"], "John Doe")
        self.assertEqual(claims["preferred_username"], "john")
        self.assertEqual(claims["email"], "john@example.com")

    def test_oidc_claim_scope_default(self):
        """Test to check the default value of oidc_claim_scope."""
        custom_validator = CustomOAuth2Validator()

        # Assert that the oidc_claim_scope attribute takes the default value
        self.assertEqual(custom_validator.oidc_claim_scope, None)

    def test_other_oidc_claim_scope_set(self):
        """Test to check the setting of oidc_claim_scope."""
        custom_validator = CustomOAuth2Validator()

        # Set the oidc_claim_scope attribute to a specific value
        custom_validator.oidc_claim_scope = "email profile"

        # Assert that the oidc_claim_scope attribute is set to the expected value
        self.assertEqual(custom_validator.oidc_claim_scope, "email profile")
