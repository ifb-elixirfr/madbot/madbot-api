from unittest.mock import Mock, patch

from django.test import TestCase
from hvac.exceptions import Forbidden

from madbot_api.core.views.utils.vault import Vault

FAKE_VAULT_ROLEID = "12345678-1234-1234-1234-1234567890ab"
FAKE_VAULT_SECRETID = "87654321-4321-4321-4321-ba0987654321"


class TestVault(TestCase):
    """Test case for the Vault class, including its authentication and secret management methods."""

    @classmethod
    def setUpClass(cls):
        """Set up class-level mocks for settings."""
        # The `self.patcher` variable is using the `patch` function from the
        # `unittest.mock` module to mock the values of the settings in the
        # `madbot_api.core.views.utils.vault` module.
        cls.patcher = patch(
            "madbot_api.core.views.utils.vault.settings",
            MADBOT_VAULT_URL="https://fakevault.fr",
            MADBOT_VAULT_ROLEID=FAKE_VAULT_ROLEID,
            MADBOT_VAULT_SECRETID=FAKE_VAULT_SECRETID,
            MADBOT_VAULT_MOUNT_POINT="madbot",
        )

        # Start the patcher and store the mocked settings in 'self.mock_settings'.
        cls.mock_settings = cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        """Stop the patcher to clean up the mock."""
        cls.patcher.stop()

    # Apply a patch to replace "madbot_api.core.views.utils.vault.hvac.Client" with the mock object.
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def setUp(self, mock_hvac_client):
        """Set up the Vault instance for testing."""
        self.vault_instance = Vault()

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_init(self, mock_hvac_client):
        """Tests if the authentication was initiated with the specified role_id and secret_id."""
        # Assert that the authentication was initiated with the specified role_id and secret_id.
        self.vault_instance.vault_client.auth.approle.login.assert_called_with(
            role_id=FAKE_VAULT_ROLEID,
            secret_id=FAKE_VAULT_SECRETID,
        )

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_refresh_authentication(self, mock_hvac_client):
        """Test the refresh_authentication method by asserting that authentication was refreshed."""
        # Call the refresh_authentication method.
        self.vault_instance.refresh_authentication()

        # Assert that the authentication was refreshed with the specified role_id and secret_id.
        self.vault_instance.vault_client.auth.approle.login.assert_called_with(
            role_id=FAKE_VAULT_ROLEID,
            secret_id=FAKE_VAULT_SECRETID,
        )

    # The outer patch is mocking the hvac.Client class itself
    # Test method for reading a secret.
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_read_secret(self, mock_hvac_client):
        """Test the read_secret method by mocking the return value."""
        # Mock the return value for reading a secret.
        self.vault_instance.vault_client.secrets.kv.v1.read_secret.return_value = {
            "data": {"key": "value"},
        }

        # Create mock objects with id attributes.
        user_mock = Mock(id=1)
        object_mock = Mock(id=2)

        # Call the read_secret method.
        result = self.vault_instance.read_secret(user_mock, object_mock)

        # Assert that the result matches the expected value.
        self.assertEqual(result, {"key": "value"})

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_read_secret_exception(self, mock_hvac_client):
        """Test the exception handling when reading a secret."""
        # Create mock objects with id attributes.
        user_mock = Mock(id=1)
        object_mock = Mock(id=2)

        self.vault_instance.vault_client.secrets.kv.v1.read_secret.side_effect = (
            Forbidden("Forbidden")
        )

        with self.assertRaises(Forbidden):
            self.vault_instance.read_secret(user_mock, object_mock)

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_write_secret(self, mock_hvac_client):
        """Test the write_secret method with mock objects and data."""
        # Create mock objects with id attributes.
        user_mock = Mock(id=1)
        tool_mock = Mock(id=2)

        # Define data for the secret.
        data_mock = {"key": "value"}

        # Call the write_secret method.
        self.vault_instance.write_secret(user_mock, tool_mock, data_mock)

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_write_secret_exception(self, mock_hvac_client):
        """Test write_secret by asserting that a Forbidden exception is raised."""
        # Create mock objects with id attributes.
        user_mock = Mock(id=1)
        tool_mock = Mock(id=2)

        self.vault_instance.vault_client.secrets.kv.v1.create_or_update_secret.side_effect = Forbidden(
            "Forbidden",
        )

        with self.assertRaises(Forbidden):
            self.vault_instance.write_secret(user_mock, tool_mock, "mock_data")

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_delete_secret(self, mock_hvac_client):
        """Test delete_secret by asserting that it is called with the correct arguments."""
        # Create mock objects with id attributes.
        user_mock = Mock(id=1)
        tool_mock = Mock(id=2)

        # Call the delete_secret method.
        self.vault_instance.delete_secret(user_mock, tool_mock)

    # The outer patch is mocking the hvac.Client class itself
    @patch("madbot_api.core.views.utils.vault.hvac.Client")
    def test_delete_secret_exception(self, mock_hvac_client):
        """Test delete_secret when a Forbidden exception is raised."""
        # Create mock objects with id attributes.
        user_mock = Mock(id=1)
        tool_mock = Mock(id=2)

        # Mock the Forbidden exception
        self.vault_instance.vault_client.secrets.kv.v1.delete_secret.side_effect = (
            Forbidden("Forbidden")
        )

        # Call the delete_secret method within an exception context.
        with self.assertRaises(Forbidden):
            self.vault_instance.delete_secret(user_mock, tool_mock)
