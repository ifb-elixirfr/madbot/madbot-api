from django.test import TestCase
from rest_framework import status
from rest_framework.exceptions import (
    MethodNotAllowed,
    NotAuthenticated,
    PermissionDenied,
)
from rest_framework.response import Response

from madbot_api.core.errors import ValidationException
from madbot_api.core.views.utils.exception_handler import custom_exception_handler

CONTEXT = {"some": "context"}


class CustomExceptionHandlerTest(TestCase):
    """Test the custom exception handler."""

    def test_custom_error_exception(self):
        """Test the `custom_exception_handler` to generate proper custom exception."""
        # Create a CustomException with specific parameters
        exc = ValidationException(
            "validation_error",
            details_code="invalid",
            parameter="example",
            parameter_value="example_value",
        )

        # Call the custom_exception_handler function with the CustomException and context
        result = custom_exception_handler(exc, CONTEXT)

        # Assert that the result is an instance of the Response class
        self.assertIsInstance(result, Response)

        # Assert that the status code of the result is equal to the status code of the exception
        self.assertEqual(result.status_code, exc.status)

        # Assert that the data in the result matches the expected structure based on the exception
        self.assertEqual(
            result.data,
            {"code": exc.code, "message": exc.message, "details": exc.details},
        )

    def test_rest_framework_method_not_allowed(self):
        """Test the `custom_exception_handler` for the MethodNotAllowed exception."""
        # Create a MethodNotAllowedException with specific parameters
        exc = MethodNotAllowed("PUT")

        class Request:
            method = "PUT"

        context = dict(CONTEXT)
        context["request"] = Request()
        result = custom_exception_handler(exc, context)

        # Assert that the status code of the result is equal to the status code of the exception
        self.assertEqual(result.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Assert that the data in the result matches the expected structure based on the exception
        self.assertEqual(
            result.data,
            {
                "code": "unsupported_method",
                "message": "Method `PUT` not allowed.",
                "details": [],
            },
        )

    def test_rest_framework_not_authenticated(self):
        """Test the `custom_exception_handler` for the NotAuthenticated exception."""
        # Create a NotAuthenticated exception with specific parameters
        exc = NotAuthenticated("Not authenticated")

        result = custom_exception_handler(exc, CONTEXT)

        # Assert that the status code of the result is equal to the status code of the exception
        self.assertEqual(result.status_code, status.HTTP_401_UNAUTHORIZED)

        # Assert that the data in the result matches the expected structure based on the exception
        self.assertEqual(
            result.data,
            {
                "code": "unauthorized",
                "message": "Access to the requested resource requires valid authentication. Check the `details` property for more details.",
                "details": [
                    {
                        "message": "Not authenticated",
                    },
                ],
            },
        )

    def test_rest_framework_permission_denied(self):
        """Test the `custom_exception_handler` for the PermissionDenied exception."""
        # Create a PermissionDenied exception with specific parameters
        exc = PermissionDenied("You do not have permission to perform this action.")

        result = custom_exception_handler(exc, CONTEXT)

        # Assert that the status code of the result is equal to the status code of the exception
        self.assertEqual(result.status_code, status.HTTP_403_FORBIDDEN)

        # Assert that the data in the result matches the expected structure based on the exception
        self.assertEqual(
            result.data,
            {
                "code": "forbidden",
                "message": "You do not have permission to perform this operation. Check the `details` property for more details.",
                "details": [
                    {
                        "parameter": "scope",
                        "code": "permission_denied",
                        "message": "You do not have permission to perform this action.",
                    },
                ],
            },
        )

    def test_unexpected_exception(self):
        """Test the `custom_exception_handler` for an unexpected exception."""
        # Create an unexpected exception
        exc = Exception("An unexpected error occurred.")

        result = custom_exception_handler(exc, CONTEXT)

        # Assert that the status code of the result is equal to the status code of the exception
        self.assertEqual(result.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

        # Assert that the data in the result matches the expected structure based on the exception
        self.assertEqual(result.data["code"], "internal_server_error")
        self.assertEqual(
            result.data["message"],
            "An unexpected error occurred. Check the `details` property for more details.",
        )
        self.assertIn("details", result.data)
        self.assertEqual(len(result.data["details"]), 1)
        # assert the details message matches the expected structure based on the exception with a regex for the UUID
        self.assertIn("message", result.data["details"][0])
        self.assertRegex(
            result.data["details"][0]["message"],
            r"The unique identifier for the error is [a-f0-9]{8}-([a-f0-9]{4}-){3}[a-f0-9]{12}. Contact your system administrator with this identifier for more information.",
        )
