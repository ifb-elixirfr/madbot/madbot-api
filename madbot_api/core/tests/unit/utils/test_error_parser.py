from django.test import TestCase
from rest_framework.exceptions import ErrorDetail

from madbot_api.core.views.utils.error import parse_serializer_error


class InvalidParam(TestCase):
    """Test the endpoint with invalid parameters."""

    def test_detail_tuple(self):
        """Error raised when detail of type tuple."""
        with self.assertRaisesMessage(
            ValueError,
            "details must be a dict, list, ErrorDetail or str",
        ):
            parse_serializer_error(("error message 1", "error message 2"), "connector")

    def test_detail_int(self):
        """Error raised when detail of type integer."""
        with self.assertRaisesMessage(
            ValueError,
            "details must be a dict, list, ErrorDetail or str",
        ):
            parse_serializer_error(1, "connector")

    def test_detail_bool(self):
        """Error raised when detail of type boolean."""
        with self.assertRaisesMessage(
            ValueError,
            "details must be a dict, list, ErrorDetail or str",
        ):
            parse_serializer_error(True, "connector")


class MissingParam(TestCase):
    """Tests with missing parameter."""

    def test_error_detail_missing_code(self):
        """Parse with ErrorDetail with missing code."""
        res = parse_serializer_error(ErrorDetail(string="error message"), "connector")
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "message": "error message",
                },
            ],
        )


class ParseError(TestCase):
    """Tests with parsing errors."""

    def test_simple_message(self):
        """Parse with simple message."""
        res = parse_serializer_error("error message", "connector")
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "message": "error message",
                },
            ],
        )

    def test_list_simple_messages(self):
        """Parse with list of simple messages."""
        res = parse_serializer_error(
            ["error message 1", "error message 2"],
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector",
                    "message": "error message 2",
                },
            ],
        )

    def test_dict_simple_messages(self):
        """Parse with dict of simple messages."""
        res = parse_serializer_error(
            {"field1": "error message 1", "field2": "error message 2"},
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector/field1",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector/field2",
                    "message": "error message 2",
                },
            ],
        )

    def test_dict_list_simple_messages(self):
        """Parse with dict of list of simple messages."""
        res = parse_serializer_error(
            {"field1": ["error message 1", "error message 2"]},
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector/field1",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector/field1",
                    "message": "error message 2",
                },
            ],
        )

    def test_list_dict_simple_messages(self):
        """Parse with list of dict of simple messages."""
        res = parse_serializer_error(
            ["error message 1", {"field1": "error message 2"}],
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector/field1",
                    "message": "error message 2",
                },
            ],
        )

    def test_error_detail(self):
        """Parse with ErrorDetail."""
        res = parse_serializer_error(
            ErrorDetail(string="error message", code="invalid"),
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "code": "invalid",
                    "message": "error message",
                },
            ],
        )

    def test_list_error_detail(self):
        """Parse with list of ErrorDetail."""
        res = parse_serializer_error(
            [
                ErrorDetail(string="error message 1", code="invalid"),
                ErrorDetail(string="error message 2", code="invalid"),
            ],
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "code": "invalid",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector",
                    "code": "invalid",
                    "message": "error message 2",
                },
            ],
        )

    def test_dict_error_detail(self):
        """Parse with dict of ErrorDetail."""
        res = parse_serializer_error(
            {
                "field1": ErrorDetail(string="error message 1", code="invalid"),
                "field2": ErrorDetail(string="error message 2", code="invalid"),
            },
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector/field1",
                    "code": "invalid",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector/field2",
                    "code": "invalid",
                    "message": "error message 2",
                },
            ],
        )

    def test_dict_list_error_detail(self):
        """Parse with dict of list of ErrorDetail."""
        res = parse_serializer_error(
            {
                "field1": [
                    ErrorDetail(string="error message 1", code="invalid"),
                    ErrorDetail(string="error message 2", code="invalid"),
                ],
            },
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector/field1",
                    "code": "invalid",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector/field1",
                    "code": "invalid",
                    "message": "error message 2",
                },
            ],
        )

    def test_list_dict_error_detail(self):
        """Parse with list of dict of ErrorDetail."""
        res = parse_serializer_error(
            [
                ErrorDetail(string="error message 1", code="invalid"),
                {"field1": ErrorDetail(string="error message 2", code="invalid")},
            ],
            "connector",
        )
        self.assertEqual(
            res,
            [
                {
                    "parameter": "connector",
                    "code": "invalid",
                    "message": "error message 1",
                },
                {
                    "parameter": "connector/field1",
                    "code": "invalid",
                    "message": "error message 2",
                },
            ],
        )

    def test_investigation_serializer_error(self):
        """Parse a serializer error message from an investigation serializer."""
        investigation_serializer_error = {
            "owner": [
                ErrorDetail(
                    string='"0" does not exist.',
                    code="does_not_exist",
                ),
            ],
            "datalinks": [
                {
                    "tool": [
                        ErrorDetail(
                            string='"0" does not exist.',
                            code="does_not_exist",
                        ),
                    ],
                    "owner": [
                        ErrorDetail(
                            string='"0" does not exist.',
                            code="does_not_exist",
                        ),
                    ],
                },
            ],
            "studies": [
                {
                    "owner": [
                        ErrorDetail(
                            string='"0" does not exist.',
                            code="does_not_exist",
                        ),
                    ],
                    "datalinks": [
                        {
                            "tool": [
                                ErrorDetail(
                                    string='"0" does not exist.',
                                    code="does_not_exist",
                                ),
                            ],
                            "owner": [
                                ErrorDetail(
                                    string='"0" does not exist.',
                                    code="does_not_exist",
                                ),
                            ],
                        },
                    ],
                    "assays": [
                        {
                            "owner": [
                                ErrorDetail(
                                    string='"0" does not exist.',
                                    code="does_not_exist",
                                ),
                            ],
                            "datalinks": [
                                {
                                    "tool": [
                                        ErrorDetail(
                                            string='"0" does not exist.',
                                            code="does_not_exist",
                                        ),
                                    ],
                                    "owner": [
                                        ErrorDetail(
                                            string='"0" does not exist.',
                                            code="does_not_exist",
                                        ),
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        }
        res = parse_serializer_error(investigation_serializer_error, "investigation")
        self.assertEqual(
            res,
            [
                {
                    "parameter": "investigation/owner",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/datalinks/tool",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/datalinks/owner",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/studies/owner",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/studies/datalinks/tool",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/studies/datalinks/owner",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/studies/assays/owner",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/studies/assays/datalinks/tool",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
                {
                    "parameter": "investigation/studies/assays/datalinks/owner",
                    "code": "does_not_exist",
                    "message": '"0" does not exist.',
                },
            ],
        )
