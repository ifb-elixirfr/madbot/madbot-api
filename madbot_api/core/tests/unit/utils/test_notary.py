# TODO: correct tests

# from copy import copy

# from anytree import LevelOrderGroupIter
# from django.test import TestCase

# from madbot_api.core.models import Node
# from madbot_api.core.tests.functionnal import (
#     AccessTokenFactory,
#     AssayNodeFactory,
#     DataFactory,
#     DatalinkFactory,
#     InvestigationNodeFactory,
#     MetadataFactory,
#     MetadataFieldFactory,
#     SampleBoundDataFactory,
#     SampleFactory,
#     StudyNodeFactory,
#     UserFactory,
#     WorkspaceFactory,
#     create_isa_collection,
#     delete_applications,
#     delete_data,
#     delete_metadata,
#     delete_metadata_fields,
#     delete_metadata_schemas,
#     delete_nodes,
#     delete_sample_bound_data,
#     delete_samples,
#     delete_users,
#     delete_workspaces,
# )
# from madbot_api.core.views.utils.notary import (
#     _combine_metadata,
#     _convert_to_anytree,
#     data_notary,
#     node_notary,
#     sample_notary,
# )

# VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"


# class InvalidParam(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         create_isa_collection()
#         cls.node = InvestigationNodeFactory()
#         cls.sample = SampleFactory()
#         cls.data = DataFactory()
#         cls.datalink1 = DatalinkFactory(
#             node=cls.node,
#             data=cls.data,
#         )
#         cls.datalink2 = DatalinkFactory()

#     @classmethod
#     def tearDownClass(cls):
#         delete_nodes()
#         delete_samples()
#         delete_data()

#     def test_node_notary_not_a_node(self):
#         """Error raised when the node parameter is not a Node object."""
#         with self.assertRaisesMessage(
#             ValueError, "The node parameter provided is not a Node object."
#         ):
#             node_notary(self.sample)
#         with self.assertRaisesMessage(
#             ValueError, "The node parameter provided is not a Node object."
#         ):
#             node_notary(self.data)
#         with self.assertRaisesMessage(
#             ValueError, "The node parameter provided is not a Node object."
#         ):
#             node_notary("node")

#     def test_sample_notary_not_a_sample(self):
#         """Error raised when the sample parameter is not a Sample object."""
#         with self.assertRaisesMessage(
#             ValueError, "The sample parameter provided is not a Sample object."
#         ):
#             sample_notary(self.node)
#         with self.assertRaisesMessage(
#             ValueError, "The sample parameter provided is not a Sample object."
#         ):
#             sample_notary(self.data)
#         with self.assertRaisesMessage(
#             ValueError, "The sample parameter provided is not a Sample object."
#         ):
#             sample_notary("sample")

#     def test_data_notary_not_a_data(self):
#         """Error raised when the data parameter is not a Data object."""
#         with self.assertRaisesMessage(
#             ValueError, "The data parameter provided is not a Data object."
#         ):
#             data_notary(self.node, self.datalink1)
#         with self.assertRaisesMessage(
#             ValueError, "The data parameter provided is not a Data object."
#         ):
#             data_notary(self.sample, self.datalink1)
#         with self.assertRaisesMessage(
#             ValueError, "The data parameter provided is not a Data object."
#         ):
#             data_notary("data", self.datalink1)

#     def test_data_notary_not_a_datalink(self):
#         """Error raised when the datalink parameter is not a Datalink object."""
#         with self.assertRaisesMessage(
#             ValueError, "The datalink parameter provided is not a Datalink object."
#         ):
#             data_notary(self.data, self.node)
#         with self.assertRaisesMessage(
#             ValueError, "The datalink parameter provided is not a Datalink object."
#         ):
#             data_notary(self.data, self.sample)
#         with self.assertRaisesMessage(
#             ValueError, "The datalink parameter provided is not a Datalink object."
#         ):
#             data_notary(self.data, "datalink")

#     def test_data_notary_datalink_not_linked_to_data(self):
#         """Error raised when the datalink parameter is not linked to the data parameter."""
#         with self.assertRaisesMessage(
#             ValueError,
#             "The data parameter and datalink parameter do not match.",
#         ):
#             data_notary(self.data, self.datalink2)

#     def test_node_notary_invalid_metadata_field_filter(self):
#         """Error raised when the metadata_field_filter parameter is not a QuerySet or a list."""
#         with self.assertRaisesMessage(
#             ValueError,
#             "The metadata_field_filter parameter provided is not a QuerySet or list object.",
#         ):
#             node_notary(self.node, metadata_field_filter="field")
#         with self.assertRaisesMessage(
#             ValueError,
#             "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
#         ):
#             node_notary(self.node, metadata_field_filter=Node.objects.all())

#     def test_sample_notary_invalid_metadata_field_filter(self):
#         """Error raised when the metadata_field_filter parameter is not a QuerySet or a list."""
#         with self.assertRaisesMessage(
#             ValueError,
#             "The metadata_field_filter parameter provided is not a QuerySet or list object.",
#         ):
#             sample_notary(self.sample, metadata_field_filter="field")
#         with self.assertRaisesMessage(
#             ValueError,
#             "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
#         ):
#             sample_notary(self.sample, metadata_field_filter=Node.objects.all())

#     def test_data_notary_invalid_metadata_field_filter(self):
#         """Error raised when the metadata_field_filter parameter is not a QuerySet or a list."""
#         with self.assertRaisesMessage(
#             ValueError,
#             "The metadata_field_filter parameter provided is not a QuerySet or list object.",
#         ):
#             data_notary(self.data, self.datalink1, metadata_field_filter="field")
#         with self.assertRaisesMessage(
#             ValueError,
#             "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
#         ):
#             data_notary(
#                 self.data, self.datalink1, metadata_field_filter=Node.objects.all()
#             )


# class TestConvertToAnytree(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         create_isa_collection()
#         cls.investigation = InvestigationNodeFactory(title="investigation")
#         cls.study = StudyNodeFactory(title="study", parent=cls.investigation)
#         cls.assay = AssayNodeFactory(title="assay", parent=cls.study)
#         cls.sample1 = SampleFactory(title="sample1", node=cls.investigation)
#         cls.sample2 = SampleFactory(title="sample2", node=cls.assay)
#         cls.data1 = DataFactory(name="data1")
#         cls.datalink = DatalinkFactory(node=cls.investigation, data=cls.data1)
#         cls.data1.node_id = cls.investigation.id
#         cls.data2 = DataFactory(name="data2")
#         cls.datalink2 = DatalinkFactory(node=cls.assay, data=cls.data2)
#         cls.data2.node_id = cls.assay.id

#     @classmethod
#     def tearDownClass(cls):
#         delete_nodes()
#         delete_samples()
#         delete_data()

#     def test_single_node(self):
#         """Test the conversion of a single node to an anytree tree."""
#         tree = _convert_to_anytree(self.investigation, [self.investigation])

#         self.assertEqual(tree.name, "investigation")
#         self.assertEqual(len(tree.children), 0)

#     def test_isa_tree(self):
#         """Test the conversion of an ISA tree to an anytree tree."""
#         tree = _convert_to_anytree(
#             self.assay, [self.investigation, self.study, self.assay]
#         )

#         self.assertEqual(tree.name, "assay")
#         self.assertEqual(len(tree.children), 1)
#         self.assertEqual(tree.children[0].name, "study")
#         self.assertEqual(len(tree.children[0].children), 1)
#         self.assertEqual(tree.children[0].children[0].name, "investigation")
#         self.assertEqual(len(tree.children[0].children[0].children), 0)

#     def test_node_with_sample(self):
#         """Test the conversion of a node with a sample to an anytree tree."""
#         tree = _convert_to_anytree(self.sample1, [self.investigation, self.sample1])

#         self.assertEqual(tree.name, "sample1")
#         self.assertEqual(len(tree.children), 1)
#         self.assertEqual(tree.children[0].name, "investigation")
#         self.assertEqual(len(tree.children[0].children), 0)

#     def test_isa_tree_with_sample(self):
#         """Test the conversion of an ISA tree with a sample to an anytree tree."""
#         tree = _convert_to_anytree(
#             self.sample2, [self.investigation, self.study, self.assay, self.sample2]
#         )

#         self.assertEqual(tree.name, "sample2")
#         self.assertEqual(len(tree.children), 1)
#         self.assertEqual(tree.children[0].name, "assay")
#         self.assertEqual(len(tree.children[0].children), 1)
#         self.assertEqual(tree.children[0].children[0].name, "study")
#         self.assertEqual(len(tree.children[0].children[0].children), 1)
#         self.assertEqual(tree.children[0].children[0].children[0].name, "investigation")
#         self.assertEqual(len(tree.children[0].children[0].children[0].children), 0)

#     def test_node_with_data(self):
#         """Test the conversion of a node with a data to an anytree tree."""
#         tree = _convert_to_anytree(self.data1, [self.data1, self.investigation])

#         self.assertEqual(tree.name, "data1")
#         self.assertEqual(len(tree.children), 1)
#         self.assertEqual(tree.children[0].name, "investigation")
#         self.assertEqual(len(tree.children[0].children), 0)

#     def test_isa_tree_with_data(self):
#         """Test the conversion of an ISA tree with a data to an anytree tree."""
#         tree = _convert_to_anytree(
#             self.data2, [self.data2, self.investigation, self.study, self.assay]
#         )

#         self.assertEqual(tree.name, "data2")
#         self.assertEqual(len(tree.children), 1)
#         self.assertEqual(tree.children[0].name, "assay")
#         self.assertEqual(len(tree.children[0].children), 1)
#         self.assertEqual(tree.children[0].children[0].name, "study")
#         self.assertEqual(len(tree.children[0].children[0].children), 1)
#         self.assertEqual(tree.children[0].children[0].children[0].name, "investigation")
#         self.assertEqual(len(tree.children[0].children[0].children[0].children), 0)

#     def test_node_with_data_and_sample(self):
#         """Test the conversion of a node with a data and a sample to an anytree tree."""
#         sample1 = copy(self.sample1)
#         sample1.data_id = self.data1.id
#         tree = _convert_to_anytree(
#             self.data1, [sample1, self.data1, self.investigation]
#         )

#         self.assertEqual(tree.name, "data1")
#         self.assertEqual(len(tree.children), 2)
#         self.assertEqual(tree.children[0].name, "sample1")
#         self.assertEqual(len(tree.children[0].children), 0)
#         self.assertEqual(tree.children[1].name, "investigation")
#         self.assertEqual(len(tree.children[1].children), 0)

#     def test_isa_tree_with_data_and_sample(self):
#         """Test the conversion of an ISA tree with a data and a sample to an anytree tree."""
#         sample2 = copy(self.sample2)
#         sample2.data_id = self.data2.id
#         tree = _convert_to_anytree(
#             self.data2,
#             [sample2, self.data2, self.investigation, self.study, self.assay],
#         )

#         self.assertEqual(tree.name, "data2")
#         self.assertEqual(len(tree.children), 2)
#         self.assertEqual(tree.children[0].name, "sample2")
#         self.assertEqual(len(tree.children[0].children), 0)
#         self.assertEqual(tree.children[1].name, "assay")
#         self.assertEqual(len(tree.children[1].children), 1)
#         self.assertEqual(tree.children[1].children[0].name, "study")
#         self.assertEqual(len(tree.children[1].children[0].children), 1)
#         self.assertEqual(tree.children[1].children[0].children[0].name, "investigation")
#         self.assertEqual(len(tree.children[1].children[0].children[0].children), 0)

#     def test_isa_tree_with_data_and_bound_samples(self):
#         """Test the conversion of an ISA tree with a data and two bound samples to an anytree tree."""
#         sample1 = copy(self.sample1)
#         sample1.data_id = self.data2.id
#         sample2 = copy(self.sample2)
#         sample2.data_id = self.data2.id
#         tree = _convert_to_anytree(
#             self.data2,
#             [
#                 self.data2,
#                 sample1,
#                 sample2,
#                 self.investigation,
#                 self.study,
#                 self.assay,
#             ],
#         )

#         self.assertEqual(tree.name, "data2")
#         self.assertEqual(len(tree.children), 2)

#         for ancestors_level in LevelOrderGroupIter(tree):
#             for ancestor in ancestors_level:
#                 if ancestor.name == "sample1":
#                     self.assertEqual(len(ancestor.children), 0)
#                 elif ancestor.name == "sample2":
#                     self.assertEqual(len(ancestor.children), 1)
#                     self.assertEqual(ancestor.children[0].name, "assay")
#                     self.assertEqual(len(ancestor.children[0].children), 1)
#                     self.assertEqual(ancestor.children[0].children[0].name, "study")
#                     self.assertEqual(len(ancestor.children[0].children[0].children), 1)
#                     self.assertEqual(
#                         ancestor.children[0].children[0].children[0].name,
#                         "investigation",
#                     )
#                     self.assertEqual(
#                         len(ancestor.children[0].children[0].children[0].children), 0
#                     )


# class TestCombineMetadata(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         jim = UserFactory(username="jim")
#         AccessTokenFactory(token=VALID_TOKEN_1, user=jim)
#         WorkspaceFactory(created_by=jim)
#         create_isa_collection()
#         cls.investigation = InvestigationNodeFactory()
#         cls.study = StudyNodeFactory(parent=cls.investigation)
#         cls.assay = AssayNodeFactory(parent=cls.study)
#         cls.sample1 = SampleFactory(node=cls.investigation)
#         cls.sample2 = SampleFactory(node=cls.assay)

#         cls.data1 = DataFactory()
#         cls.datalink1 = DatalinkFactory(node=cls.investigation, data=cls.data1)
#         cls.data1.node_id = cls.investigation.id

#         cls.data2 = DataFactory()
#         cls.datalink2 = DatalinkFactory(node=cls.assay, data=cls.data2)
#         cls.data2.node_id = cls.assay.id

#         cls.metadata_field1 = MetadataFieldFactory(name="field1")
#         cls.metadata_field2 = MetadataFieldFactory(name="field2")
#         cls.metadata_field3 = MetadataFieldFactory(name="field3")

#         cls.metadata_investigation_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="investigation - field1",
#             node=cls.investigation,
#             created_by=jim,
#         )
#         cls.metadata_study_2 = MetadataFactory(
#             field=cls.metadata_field2,
#             value="study - field2",
#             node=cls.study,
#             created_by=jim,
#         )
#         cls.metadata_investigation_3 = MetadataFactory(
#             field=cls.metadata_field3,
#             value="investigation - field3",
#             node=cls.investigation,
#             created_by=jim,
#         )
#         cls.metadata_assay_3 = MetadataFactory(
#             field=cls.metadata_field3,
#             value="assay - field3",
#             node=cls.assay,
#             created_by=jim,
#         )
#         cls.metadata_sample1_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="sample1 - field1",
#             sample=cls.sample1,
#             created_by=jim,
#         )
#         cls.metadata_sample2_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="sample2 - field1",
#             sample=cls.sample2,
#         )
#         cls.metadata_data1_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="data1 - field1",
#             data=cls.data1,
#             created_by=jim,
#         )
#         cls.metadata_data2_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="data2 - field1",
#             data=cls.data2,
#             created_by=jim,
#         )

#     @classmethod
#     def tearDownClass(cls):
#         delete_nodes()
#         delete_samples()
#         delete_data()
#         delete_metadata()
#         delete_metadata_fields()
#         delete_metadata_schemas()
#         delete_applications()
#         delete_users()
#         delete_workspaces()

#     def test_single_node_metadata(self):
#         """Test the combination of metadata for a single node."""
#         tree = _convert_to_anytree(self.investigation, [self.investigation])
#         result = _combine_metadata(self.investigation, "node", tree)

#         expected = [
#             {"field": self.metadata_field1, "values": [self.metadata_investigation_1]},
#             {"field": self.metadata_field3, "values": [self.metadata_investigation_3]},
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_metadata(self):
#         """Test the combination of metadata for an ISA tree."""
#         tree = _convert_to_anytree(
#             self.assay, [self.investigation, self.study, self.assay]
#         )
#         result = _combine_metadata(self.assay, "node", tree)

#         expected = [
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3, self.metadata_investigation_3],
#             },
#             {
#                 "field": self.metadata_field2,
#                 "values": [self.metadata_study_2],
#             },
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_investigation_1],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_metadata_with_filter(self):
#         """Test the combination of metadata for an ISA tree with a metadata_field_filter."""
#         tree = _convert_to_anytree(
#             self.assay, [self.investigation, self.study, self.assay]
#         )
#         result = _combine_metadata(
#             self.assay, "node", tree, metadata_field_filter=[self.metadata_field3]
#         )

#         expected = [
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3, self.metadata_investigation_3],
#             }
#         ]

#         self.assertEqual(result, expected)

#     def test_node_with_sample_metadata(self):
#         """Test the combination of metadata for a single node with a sample."""
#         tree = _convert_to_anytree(self.sample1, [self.sample1, self.investigation])
#         result = _combine_metadata(self.sample1, "sample", tree)

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_sample1_1, self.metadata_investigation_1],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_investigation_3]},
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_with_sample_metadata(self):
#         """Test the combination of metadata for an ISA tree with a sample."""
#         tree = _convert_to_anytree(
#             self.sample2, [self.investigation, self.study, self.assay, self.sample2]
#         )
#         result = _combine_metadata(self.sample2, "sample", tree)

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_sample2_1, self.metadata_investigation_1],
#             },
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3],
#             },
#             {
#                 "field": self.metadata_field2,
#                 "values": [self.metadata_study_2],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_with_sample_metadata_with_filter(self):
#         """Test the combination of metadata for an ISA tree with a sample and a metadata_field_filter."""
#         tree = _convert_to_anytree(
#             self.sample2, [self.investigation, self.study, self.assay, self.sample2]
#         )
#         result = _combine_metadata(
#             self.sample2, "sample", tree, metadata_field_filter=[self.metadata_field1]
#         )

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_sample2_1, self.metadata_investigation_1],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_node_with_data_metadata(self):
#         """Test the combination of metadata for a single node with a data."""
#         tree = _convert_to_anytree(self.data1, [self.data1, self.investigation])
#         result = _combine_metadata(self.data1, "data", tree)

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_data1_1, self.metadata_investigation_1],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_investigation_3]},
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_with_data_metadata(self):
#         """Test the combination of metadata for an ISA tree with a data."""
#         tree = _convert_to_anytree(
#             self.data2, [self.data2, self.investigation, self.study, self.assay]
#         )
#         result = _combine_metadata(self.data2, "data", tree)

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_data2_1, self.metadata_investigation_1],
#             },
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3],
#             },
#             {
#                 "field": self.metadata_field2,
#                 "values": [self.metadata_study_2],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_with_data_metadata_with_filter(self):
#         """Test the combination of metadata for an ISA tree with a data and a metadata_field_filter."""
#         tree = _convert_to_anytree(
#             self.data2, [self.data2, self.investigation, self.study, self.assay]
#         )
#         result = _combine_metadata(
#             self.data2, "data", tree, metadata_field_filter=[self.metadata_field1]
#         )

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_data2_1, self.metadata_investigation_1],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_isa_tree_with_data_and_sample_metadata(self):
#         """Test the combination of metadata for an ISA tree with a data and two bound samples."""
#         sample1 = copy(self.sample1)
#         sample1.data_id = self.data2.id
#         sample2 = copy(self.sample2)
#         sample2.data_id = self.data2.id
#         tree = _convert_to_anytree(
#             self.data2,
#             [self.data2, sample1, sample2, self.investigation, self.study, self.assay],
#         )
#         result = _combine_metadata(self.data2, "data", tree)

#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [
#                     self.metadata_data2_1,
#                     self.metadata_sample1_1,
#                     self.metadata_sample2_1,
#                 ],
#             },
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3],
#             },
#             {
#                 "field": self.metadata_field2,
#                 "values": [self.metadata_study_2],
#             },
#         ]

#         self.assertEqual(result, expected)


# class Notary(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         jim = UserFactory(username="jim")
#         AccessTokenFactory(token=VALID_TOKEN_1, user=jim)
#         WorkspaceFactory(created_by=jim)
#         create_isa_collection()
#         cls.investigation = InvestigationNodeFactory()
#         cls.study = StudyNodeFactory(parent=cls.investigation)
#         cls.assay = AssayNodeFactory(parent=cls.study)
#         cls.sample1 = SampleFactory(node=cls.investigation)
#         cls.sample2 = SampleFactory(node=cls.assay)
#         cls.sample3 = SampleFactory(node=cls.assay)

#         cls.data1 = DataFactory()
#         cls.datalink1 = DatalinkFactory(node=cls.investigation, data=cls.data1)
#         cls.data1.node_id = cls.investigation.id

#         cls.data2 = DataFactory()
#         cls.datalink2 = DatalinkFactory(node=cls.assay, data=cls.data2)
#         cls.data2.node_id = cls.assay.id

#         cls.data3 = DataFactory()
#         cls.datalink3 = DatalinkFactory(node=cls.assay, data=cls.data3)
#         cls.data3.node_id = cls.assay.id

#         # create bound between data3 and sample3
#         cls.sample_bound_data = SampleBoundDataFactory(
#             sample=cls.sample3, data=cls.data3, datalink=cls.datalink3
#         )

#         cls.metadata_field1 = MetadataFieldFactory(name="field1")
#         cls.metadata_field2 = MetadataFieldFactory(name="field2")
#         cls.metadata_field3 = MetadataFieldFactory(name="field3")

#         cls.metadata_investigation_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="investigation - field1",
#             node=cls.investigation,
#             created_by=jim,
#         )
#         cls.metadata_study_2 = MetadataFactory(
#             field=cls.metadata_field2,
#             value="study - field2",
#             node=cls.study,
#             created_by=jim,
#         )
#         cls.metadata_investigation_3 = MetadataFactory(
#             field=cls.metadata_field3,
#             value="investigation - field3",
#             node=cls.investigation,
#             created_by=jim,
#         )
#         cls.metadata_assay_3 = MetadataFactory(
#             field=cls.metadata_field3,
#             value="assay - field3",
#             node=cls.assay,
#             created_by=jim,
#         )
#         cls.metadata_sample1_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="sample1 - field1",
#             sample=cls.sample1,
#             created_by=jim,
#         )
#         cls.metadata_sample2_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="sample2 - field1",
#             sample=cls.sample2,
#             created_by=jim,
#         )
#         cls.metadata_sample3_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="sample3 - field1",
#             sample=cls.sample3,
#         )
#         cls.metadata_data1_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="data1 - field1",
#             data=cls.data1,
#             created_by=jim,
#         )
#         cls.metadata_data2_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="data2 - field1",
#             data=cls.data2,
#             created_by=jim,
#         )
#         cls.metadata_data3_1 = MetadataFactory(
#             field=cls.metadata_field1,
#             value="data3 - field1",
#             data=cls.data3,
#             created_by=jim,
#         )

#     @classmethod
#     def tearDownClass(cls):
#         delete_nodes()
#         delete_sample_bound_data()
#         delete_samples()
#         delete_data()
#         delete_metadata()
#         delete_metadata_fields()
#         delete_metadata_schemas()
#         delete_applications()
#         delete_users()
#         delete_workspaces()

#     def test_node_notary_single_node(self):
#         """Test the node notary for a single node."""
#         result = node_notary(self.investigation)
#         expected = [
#             {"field": self.metadata_field1, "values": [self.metadata_investigation_1]},
#             {"field": self.metadata_field3, "values": [self.metadata_investigation_3]},
#         ]

#         self.assertEqual(result, expected)

#     def test_node_notary_isa_tree(self):
#         """Test the node notary for an ISA tree."""
#         result = node_notary(self.assay)
#         expected = [
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3, self.metadata_investigation_3],
#             },
#             {"field": self.metadata_field2, "values": [self.metadata_study_2]},
#             {"field": self.metadata_field1, "values": [self.metadata_investigation_1]},
#         ]

#         self.assertEqual(result, expected)

#     def test_node_notary_isa_tree_with_filter(self):
#         """Test the node notary for an ISA tree with a metadata_field_filter."""
#         result = node_notary(self.assay, metadata_field_filter=[self.metadata_field3])
#         expected = [
#             {
#                 "field": self.metadata_field3,
#                 "values": [self.metadata_assay_3, self.metadata_investigation_3],
#             }
#         ]

#         self.assertEqual(result, expected)

#     def test_sample_notary_single_node(self):
#         """Test the sample notary for a single node."""
#         result = sample_notary(self.sample1)
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_sample1_1, self.metadata_investigation_1],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_investigation_3]},
#         ]

#         self.assertEqual(result, expected)

#     def test_sample_notary_isa_tree(self):
#         """Test the sample notary for an ISA tree."""
#         result = sample_notary(self.sample2)
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_sample2_1, self.metadata_investigation_1],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_assay_3]},
#             {"field": self.metadata_field2, "values": [self.metadata_study_2]},
#         ]

#         self.assertEqual(result, expected)

#     def test_sample_notary_isa_tree_with_filter(self):
#         """Test the sample notary for an ISA tree with a metadata_field_filter."""
#         result = sample_notary(
#             self.sample2, metadata_field_filter=[self.metadata_field1]
#         )
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_sample2_1, self.metadata_investigation_1],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_data_notary_single_node(self):
#         """Test the data notary for a single node."""
#         result = data_notary(self.data1, self.datalink1)
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_data1_1, self.metadata_investigation_1],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_investigation_3]},
#         ]

#         self.assertEqual(result, expected)

#     def test_data_notary_isa_tree(self):
#         """Test the data notary for an ISA tree."""
#         result = data_notary(self.data2, self.datalink2)
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_data2_1, self.metadata_investigation_1],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_assay_3]},
#             {"field": self.metadata_field2, "values": [self.metadata_study_2]},
#         ]

#         self.assertEqual(result, expected)

#     def test_data_notary_isa_tree_with_filter(self):
#         """Test the data notary for an ISA tree with a metadata_field_filter."""
#         result = data_notary(
#             self.data2, self.datalink2, metadata_field_filter=[self.metadata_field1]
#         )
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [self.metadata_data2_1, self.metadata_investigation_1],
#             },
#         ]

#         self.assertEqual(result, expected)

#     def test_data_notary_isa_tree_with_bound_sample(self):
#         """Test the data notary for an ISA tree and a bound sample."""
#         result = data_notary(self.data3, self.datalink3)
#         expected = [
#             {
#                 "field": self.metadata_field1,
#                 "values": [
#                     self.metadata_data3_1,
#                     self.metadata_sample3_1,
#                 ],
#             },
#             {"field": self.metadata_field3, "values": [self.metadata_assay_3]},
#             {"field": self.metadata_field2, "values": [self.metadata_study_2]},
#         ]

#         self.assertEqual(result, expected)
