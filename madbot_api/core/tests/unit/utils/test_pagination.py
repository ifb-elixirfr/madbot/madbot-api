# from django.core.management import call_command
# from rest_framework import status

# from madbot_api.core.tests.functionnal import (
#     MadbotTestCase,
#     build_accesstoken,
#     build_investigation,
#     build_member,
#     build_study,
#     build_user,
#     delete_accesstokens,
#     delete_investigations,
#     delete_studies,
#     delete_users,
# )

# VALID_TOKEN_1 = "fOY3dYtRHp4PujfMkL8NIK5ocs443N"


# class Pagination(MadbotTestCase):
#     @classmethod
#     def setUpClass(cls):
#         call_command("flush", "--no-input")
#         cls.alice = build_user(username="alice")
#         build_accesstoken(token=VALID_TOKEN_1, user=cls.alice, scope="read")
#         investigation_test = build_investigation(id=1, owner=cls.alice)
#         for i in range(1, 20):
#             build_study(
#                 investigation=investigation_test, id=i, name="fake_study_" + str(i)
#             )
#         for i in range(2, 20):
#             user_test = build_user(username="user_" + str(i))
#             build_member(user=user_test, isa_object=investigation_test, role="manager")

#     @classmethod
#     def tearDownClass(cls):
#         delete_investigations()
#         delete_accesstokens()
#         delete_users()
#         delete_studies()

#     def test_studies_pagination(self):
#         response = self.get("/api/investigations/1/studies/", token=VALID_TOKEN_1)

#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertEqual(response.json()["count"], 19)
#         self.assertEqual(len(response.json()["results"]), 10)
#         self.assertEqual(response.json()["current_page"], 1)
#         self.assertEqual(response.json()["total_pages"], 2)
#         next = response.json()["next"]
#         response = self.get(next, token=VALID_TOKEN_1)
#         self.assertEqual(len(response.json()["results"]), 9)
#         self.assertEqual(response.json()["current_page"], 2)
#         self.assertEqual(response.json()["total_pages"], 2)

#     def test_members_pagination(self):
#         response = self.get("/api/investigations/1/members/", token=VALID_TOKEN_1)

#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertEqual(response.json()["count"], 19)
#         self.assertEqual(len(response.json()["results"]), 10)
#         self.assertEqual(response.json()["current_page"], 1)
#         self.assertEqual(response.json()["total_pages"], 2)
#         next = response.json()["next"]
#         response = self.get(next, token=VALID_TOKEN_1)
#         self.assertEqual(len(response.json()["results"]), 9)
#         self.assertEqual(response.json()["current_page"], 2)
#         self.assertEqual(response.json()["total_pages"], 2)
