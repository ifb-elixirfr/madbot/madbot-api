from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_nested import routers

from madbot_api.core.views import (
    bound_data,
    bound_samples,
    connections,
    connectors,
    data,
    data_association,
    datalinks,
    external_data,
    metadata,
    metadata_fields,
    node_members,
    node_types,
    nodes,
    notifications,
    roles,
    samples,
    stats,
    submission_data,
    submission_members,
    submission_metadata,
    submission_metadata_objects,
    submissions,
    submissions_settings,
    tasks,
    users,
    version,
    workspace_members,
    workspaces,
)

router = routers.DefaultRouter(trailing_slash=False)


# -----------------------------------------------------------------------------
# Applications
# -----------------------------------------------------------------------------

application_user_params_url_patterns = [
    # base-path = api/applications/<appName>/userparams/
    path(
        "<str:application_name>/userparams",
        users.ApplicationUserParamView.as_view(
            {
                "get": "get_application_user_parameters",
                "put": "update_application_user_parameters",
            },
        ),
    ),
]

# -----------------------------------------------------------------------------
# Tasks
# -----------------------------------------------------------------------------

router.register(r"tasks", tasks.TaskViewSet, basename="tasks")

# -----------------------------------------------------------------------------
# Roles
# -----------------------------------------------------------------------------

router.register(r"roles", roles.RoleViewSet, basename="roles")


# -----------------------------------------------------------------------------
# Workspaces
# -----------------------------------------------------------------------------

# base-path = api/workspaces & api/workspaces/<uuid:pk>
router.register(r"workspaces", workspaces.WorkspaceViewSet, basename="workspaces")

# `workspace_router` is creating a nested router for the `workspace` endpoint.
# It allows for creating URLs that are nested under the `connections` endpoint.
# For example, the URL pattern `api/workspaces/<uuid:pk>/members` would be
# handled by the `WorkspaceMemberViewSet` viewset.
workspace_router = routers.NestedSimpleRouter(
    router,
    r"workspaces",
    lookup="workspace",
    trailing_slash=False,
)

workspace_router.register(
    r"members",
    workspace_members.WorkspaceMemberViewSet,
)


# -----------------------------------------------------------------------------
# Node
# -----------------------------------------------------------------------------

# base-path = api/node_types & api/node_types/<uuid:pk>
router.register(r"node_types", node_types.NodeTypeViewSet, basename="nodetypes")

# base-path = api/nodes & api/nodes/<uuid:pk>
router.register(r"nodes", nodes.NodeViewSet)

# `nodes_router` is creating a nested router for the `node` endpoint. It allows
# for creating URLs that are nested under the `nodes` endpoint. For example,
# the URL pattern `api/nodes/<uuid:pk>/samples` would be handled by the
# `SampleViewset` viewset.
nodes_router = routers.NestedSimpleRouter(
    router,
    r"nodes",
    lookup="node",
    trailing_slash=False,
)
nodes_router.register(r"members", node_members.NodeMemberViewSet)


# -----------------------------------------------------------------------------
# Samples
# -----------------------------------------------------------------------------

nodes_router.register(r"samples", samples.SampleViewset)

samples_router = routers.NestedSimpleRouter(
    nodes_router,
    r"samples",
    lookup="sample",
    trailing_slash=False,
)

# -----------------------------------------------------------------------------
# Bound data
# -----------------------------------------------------------------------------

samples_router.register(
    r"bound_data",
    bound_data.SampleBoundDataViewset,
    basename="bound_data",
)


# -----------------------------------------------------------------------------
# Connections
# -----------------------------------------------------------------------------

# base-path = api/connections & api/connections/<uuid:pk>
router.register(r"connections", connections.ConnectionViewSet, basename="connections")

# `connection_router` is creating a nested router for the `connection` endpoint.
# It allows for creating URLs that are nested under the `connections` endpoint.
# For example, the URL pattern `api/connections/<uuid:pk>/external_data` would
# be handled by the `ExternalDataViewSet` viewset.
connection_router = routers.NestedSimpleRouter(
    router,
    r"connections",
    lookup="connections",
    trailing_slash=False,
)

# -----------------------------------------------------------------------------
# External data
# -----------------------------------------------------------------------------

connection_router.register(
    r"external_data",
    external_data.ExternalDataViewSet,
    basename="externaldata",
)


# -----------------------------------------------------------------------------
# Datalinks
# -----------------------------------------------------------------------------

nodes_router.register(r"datalinks", datalinks.DataLinkViewSet)


# -----------------------------------------------------------------------------
# Data
# -----------------------------------------------------------------------------

# base-path = api/data & api/data/<uuid:pk>
router.register(r"data", data.DataViewSet)

data_router = routers.NestedSimpleRouter(
    router,
    r"data",
    lookup="data",
    trailing_slash=False,
)

data_router.register(r"metadata", metadata.MetadataViewSet, basename="data_metadata")

# -----------------------------------------------------------------------------
# Bound sample
# -----------------------------------------------------------------------------

data_router.register(
    r"bound_samples",
    bound_samples.DataBoundSampleViewset,
    basename="bound_samples",
)

# -----------------------------------------------------------------------------
# Associated data
# -----------------------------------------------------------------------------

data_router.register(
    r"associations",
    data_association.DataAssociationViewSet,
    basename="data_association",
)

data_association_router = routers.NestedSimpleRouter(
    data_router,
    r"associations",
    lookup="association",
    trailing_slash=False,
)

# -----------------------------------------------------------------------------
# Metadata Fields
# -----------------------------------------------------------------------------

router.register(
    r"metadata_fields",
    metadata_fields.MetadataFieldViewSet,
    basename="metadata_fields",
)

# -----------------------------------------------------------------------------
# Metadata
# -----------------------------------------------------------------------------

nodes_router.register(r"metadata", metadata.MetadataViewSet)

samples_router.register(r"metadata", metadata.MetadataViewSet)

data_router.register(r"metadata", metadata.MetadataViewSet)

data_association_router.register(r"metadata", metadata.MetadataViewSet)

# -----------------------------------------------------------------------------
# Notifications
# -----------------------------------------------------------------------------

# base-path = api/notifications & api/notifications/<id:pk>
router.register(
    r"notifications",
    notifications.NotificationViewSet,
    basename="notifications",
)

# -----------------------------------------------------------------------------
#  Submissions
# -----------------------------------------------------------------------------

router.register(
    r"submissions",
    submissions.SubmissionViewSet,
    basename="submissions",
)
submission_router = routers.NestedSimpleRouter(
    router,
    r"submissions",
    lookup="submission",
    trailing_slash=False,
)

# -----------------------------------------------------------------------------
#  SubmissionsMembers
# -----------------------------------------------------------------------------

submission_router.register(r"members", submission_members.SubmissionMemberViewSet)

# -----------------------------------------------------------------------------
#  SubmissionsData
# -----------------------------------------------------------------------------

submission_router.register(r"data", submission_data.SubmissionDataViewSet)

# -----------------------------------------------------------------------------
#  SubmissionsSettings
# -----------------------------------------------------------------------------

submission_router.register(r"settings", submissions_settings.SubmissionSettingsViewSet)

# -----------------------------------------------------------------------------
#  SubmissionsMetadataObject
# -----------------------------------------------------------------------------

submission_router.register(
    r"metadata_objects",
    submission_metadata_objects.SubmissionMetadataObjectViewSet,
    basename="metadata_objects",
)

# -----------------------------------------------------------------------------
#  SubmissionsMetadata
# -----------------------------------------------------------------------------

# Nest the submission_metadata under submission_metadata_objects
submission_metadata_router = routers.NestedSimpleRouter(
    submission_router,
    r"metadata_objects",
    lookup="metadata_objects",
    trailing_slash=False,
)

submission_metadata_router.register(
    r"metadata",
    submission_metadata.SubmissionMetadataViewSet,
    basename="metadata",
)

# -----------------------------------------------------------------------------
# Actions endpoint for submissions
# -----------------------------------------------------------------------------

submission_router.register(
    r"actions",
    submissions.SubmissionActionViewSet,
    basename="actions",
)

# -----------------------------------------------------------------------------
# URLs
# -----------------------------------------------------------------------------

urlpatterns = [
    # base-path = api/
    # path("tokens/", tokens.TokensView.as_view()),
    # path("user/", tokens.UserAPI.as_view()),
    # path("logout/", tokens.LogoutAPI.as_view()),
    path("applications/", include(application_user_params_url_patterns)),
    path("users/", users.SearchUsers.as_view(), name="searchUsers"),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "notifications",
        notifications.NotificationViewSet.as_view({"get": "list", "delete": "destroy"}),
    ),
    path("version", version.VersionView.as_view(), name="version"),
    path("stats", stats.StatsView.as_view(), name="stats"),
    path("", include(router.urls)),
    path("", include(workspace_router.urls)),
    path("", include(nodes_router.urls)),
    path("", include(samples_router.urls)),
    path("connectors", connectors.ConnectorsListAPIView.as_view()),
    path("connectors/<slug:name>/", connectors.ConnectorsDetailAPIView.as_view()),
    path(
        "connectors/<slug:name>/groups",
        connectors.ConnectorsGroupsAPIView.as_view(),
    ),
    path(
        "connectors/<slug:name>/metadata_mapping",
        connectors.ConnectorsMetadataMappingViewSet.as_view(),
    ),
    path("", include(connection_router.urls)),
    path("", include(data_router.urls)),
    path("", include(data_association_router.urls)),
    path("", include(submission_router.urls)),
    path("", include(submission_metadata_router.urls)),
    path("accounts/", include("allauth.urls")),
]
