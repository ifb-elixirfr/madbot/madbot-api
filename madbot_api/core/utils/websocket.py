from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer


class WebSocketDataSync:
    """Utility class to handle WebSocket messages for data synchronization between clients and the server."""

    def __init__(self):
        """Initialize the WebSocketDataSync."""
        self.channel_layer = get_channel_layer()

    def sync_update(
        self,
        model_type,
        object_id: str,
        action,
        source: str,
    ) -> None:
        """
        Send a WebSocket message about a model update.

        Args:
        ----
            model_type: The type of model being updated (e.g., "submission", "data")
            object_id: The ID of the updated object
            action: The type of update (create/update/delete)
            source: The source of the update (e.g., "submission", "task")

        """
        group_name = f"{model_type}_{object_id}"

        message = {
            "type": f"{model_type}_message",
            "message": {
                "action": f"{action}",
                f"{model_type}_id": str(object_id),
            },
            "source": source,
        }

        async_to_sync(self.channel_layer.group_send)(group_name, message)


# Create a singleton instance
notifier = WebSocketDataSync()
