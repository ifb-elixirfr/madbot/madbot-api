from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers

from madbot_api.core.models import Data, DataLink, NodeMember, Sample, SampleBoundData
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetNodeMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class SampleBoundDataSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serialize data bound to a sample."""

    title = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    sample = serializers.PrimaryKeyRelatedField(queryset=Sample.objects.all())
    data = serializers.PrimaryKeyRelatedField(queryset=Data.objects.all())
    datalink = serializers.PrimaryKeyRelatedField(queryset=DataLink.objects.all())

    class Meta:
        """Meta class for SampleBoundDataSerializer."""

        model = SampleBoundData
        fields = [
            "id",
            "title",
            "sample",
            "data",
            "datalink",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "title",
            "sample",
            "data",
            "datalink",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]

    def get_title(self, obj):
        """
        Retrieve the name attribute from the data object.

        Args:
        ----
            obj: The object containing the data with a title attribute.

        Returns:
        -------
            str: The title of the data.

        """
        return obj.data.name

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.data.created_by)

    def to_representation(self, obj):
        """
        Convert the instance to a dictionary representation with a nested sample key.

        Args:
        ----
            obj: The instance to serialize.

        Returns:
        -------
            dict: A dictionary representation of the object with an additional
            "sample" key containing nested details.

        """
        representation = super().to_representation(obj)
        representation["data"] = {
            "id": str(obj.data.id),
            "title": obj.data.name,
        }
        return representation


########################################################################################
# ViewSets class
########################################################################################


class SampleBoundDataViewset(
    MadbotViewSet,
    WorkspaceHeaderMixin,
    BelongsToWorkspaceMixin,
    GetNodeMixin,
    GetObjectMixin,
):
    """ViewSet for handling sample-bound data."""

    queryset = SampleBoundData.objects.all().select_related(
        "created_by", "last_edited_by"
    )
    serializer_class = SampleBoundDataSerializer
    pagination_class = StandardPagination

    @extend_schema(
        responses={
            200: SampleBoundDataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of the associated node.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "sample",
                    "code": "does_not_exist",
                    "message": '"<sample_value>" does not exist.',
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, node_pk=None, sample_pk=None):
        """List the information related to all sample bound data objects."""
        # verify the workspace
        workspace = self.get_workspace()
        # verify the sample
        node = self.get_node(workspace)
        sample = self.get_the_object(Sample, sample_pk, "uri")
        # check if it's the right workspace for the data
        self.check_object_belongs_to_node(node, sample)
        # Get the user's node memberships
        user_node_ids = NodeMember.objects.filter(user=request.user).values_list(
            "node_id",
            flat=True,
        )

        serializer = self.get_serializer(
            self.get_queryset().filter(
                sample=sample,
                datalink__node__workspace=workspace,
                datalink__node__in=user_node_ids,
                sample__node__workspace=workspace,
                sample__node__in=user_node_ids,
            ),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))
