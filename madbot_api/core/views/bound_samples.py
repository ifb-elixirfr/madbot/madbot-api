from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import Data, DataLink, NodeMember, Sample, SampleBoundData
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetGeneralMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_query_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class DataBoundSampleSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serialize data bound to a sample."""

    title = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()
    sample = serializers.PrimaryKeyRelatedField(queryset=Sample.objects.all())
    data = serializers.PrimaryKeyRelatedField(queryset=Data.objects.all())
    datalink = serializers.PrimaryKeyRelatedField(queryset=DataLink.objects.all())

    class Meta:
        """Meta class for DataBoundSampleSerializer."""

        model = SampleBoundData
        fields = [
            "id",
            "title",
            "sample",
            "data",
            "datalink",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "title",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]

    def get_title(self, obj):
        """
        Retrieve the title attribute from the sample object.

        Args:
        ----
            obj: The object containing the sample with a title attribute.

        Returns:
        -------
            str: The title of the sample.

        """
        return obj.sample.title

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.sample.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.sample.last_edited_by)

    def to_representation(self, obj):
        """
        Convert the instance to a dictionary representation with a nested sample key.

        Args:
        ----
            obj: The instance to serialize.

        Returns:
        -------
            dict: A dictionary representation of the object with an additional
            "sample" key containing nested details.

        """
        representation = super().to_representation(obj)
        representation["sample"] = {
            "id": str(obj.sample.id),
            "title": obj.sample.title,
            "node": str(obj.sample.node.id),
        }
        return representation

    # def get_data(self, obj):
    #     """
    #     The function `get_data` returns information about a given object,
    #     including the count of bound samples and a related URL.

    #     :param obj: The `obj` parameter in the `get_data` method seems to
    #     represent an object that contains information about a sample. The method
    #     is using this object to retrieve data related to the sample, such as the
    #     count of bound samples and a URL for accessing related data
    #     :return: The `get_data` method returns a dictionary containing the count
    #     of bound samples for the given object (`obj`) and a related URL for
    #     accessing bound data related to the object. The dictionary has two keys:
    #     "count" which holds the count of bound samples, and "related" which holds
    #     the URL for accessing bound data related to the object.
    #     """
    #     base_url = self.context["request"].build_absolute_uri("/api/")
    #     related_url = f"{base_url}data/{obj.data.id}/bound_data"
    #     return {
    #         "count": obj.data.count(),
    #         "related": related_url,
    #     }


########################################################################################
# ViewSets class
########################################################################################


class DataBoundSampleViewset(
    MadbotViewSet,
    WorkspaceHeaderMixin,
    BelongsToWorkspaceMixin,
    GetGeneralMixin,
    GetObjectMixin,
):
    """ViewSet for handling data-bound samples."""

    queryset = SampleBoundData.objects.all().select_related(
        "created_by", "last_edited_by"
    )
    serializer_class = DataBoundSampleSerializer
    pagination_class = StandardPagination

    @extend_schema(
        responses={
            200: DataBoundSampleSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of the associated node.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "data",
                    "code": "does_not_exist",
                    "message": '"<data_value>" does not exist.',
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, data_pk=None):
        """List the information related to all sample bound data objects."""
        # verify the workspace
        workspace = self.get_workspace()
        # verify the data
        data = self.get_the_object(Data, data_pk, "uri")
        # check if it's the right workspace for the data
        self.check_object_belongs_to_workspace(workspace, data)
        # check access data
        self._check_data_access(data, workspace)

        # Get the user's node memberships
        user_node_ids = NodeMember.objects.filter(user=request.user).values_list(
            "node_id",
            flat=True,
        )

        serializer = self.get_serializer(
            self.get_queryset().filter(
                data=data,
                datalink__node__workspace=workspace,
                datalink__node__in=user_node_ids,
                sample__node__workspace=workspace,
                sample__node__in=user_node_ids,
            ),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            201: DataBoundSampleSerializer,
            200: DataBoundSampleSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "data",
                    "code": "does_not_exist",
                    "message": '"<data_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "forbidden",
                    "message": "You are not a member of the associated node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, data_pk=None, *args, **kwargs):
        """Create a new sample bound data object."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the data
        data = self.get_the_object(Data, data_pk, "uri")
        # check if it's the right workspace for the data
        self.check_object_belongs_to_workspace(workspace, data)

        # Create the serializer validate and validate the data
        serializer = self.get_serializer(
            data={
                **request.data,
                "data": data_pk,
            },
        )
        serializer.is_valid(raise_exception=True)

        # verify the sample
        sample = self.get_the_object(Sample, request.data.get("sample"), "body")
        # check if it's the right workspace for the sample
        self.check_object_belongs_to_workspace(workspace, sample.node)

        # verify the datalink
        datalink = self.get_the_object(DataLink, request.data.get("datalink"), "body")
        # check if it's the right workspace for the datalink
        self.check_object_belongs_to_workspace(workspace, datalink.data)

        # Check if the user has the right permissions
        node_membership_sample = NodeMember.objects.filter(
            node=sample.node,
            user=request.user,
        ).first()

        node_membership_data = NodeMember.objects.filter(
            node=datalink.node,
            user=request.user,
        ).first()

        # Check if the user's role allows creation
        if not node_membership_sample:
            raise ForbiddenException(
                details={
                    "parameter": "sample/node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )
        if not node_membership_data:
            raise ForbiddenException(
                details={
                    "parameter": "datalink/node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )
        # Check if the user's role allows creation
        if node_membership_sample.role not in ["owner", "manager", "contributor"]:
            raise ForbiddenException(
                details={
                    "parameter": "sample/node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            )

        if node_membership_data.role not in ["owner", "manager", "contributor"]:
            raise ForbiddenException(
                details={
                    "parameter": "datalink/node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            )

        # Check if the sample is already linked to the data
        existing_bound_data = SampleBoundData.objects.filter(
            sample=sample,
            data=data,
            datalink=datalink,
        ).first()

        if existing_bound_data:
            # Return the existing bound data instead of creating a new one
            serializer = self.get_serializer(existing_bound_data)
            return Response(serializer.data, status=status.HTTP_200_OK)

        serializer.save(
            created_by=request.user,
            last_edited_by=request.user,
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk, data_pk=None):
        """Delete a specific sample bound data item."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the data
        data = self.get_the_object(Data, data_pk, "uri")
        # check if it's the right workspace for the data
        self.check_object_belongs_to_workspace(workspace, data)

        # retrieve the item
        item = self.get_object()

        # Check if the user has the right permissions
        node_membership_sample = NodeMember.objects.filter(
            node=item.sample.node,
            user=request.user,
        ).first()

        node_membership_data = NodeMember.objects.filter(
            node=item.datalink.node,
            user=request.user,
        ).first()

        # Check if the user is a member of the node
        if not node_membership_sample:
            raise ForbiddenException(
                details={
                    "parameter": "sample/node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )
        if not node_membership_data:
            raise ForbiddenException(
                details={
                    "parameter": "datalink/node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        # Check if the user has the right permissions
        if node_membership_sample.role not in ["owner", "manager", "contributor"]:
            raise ForbiddenException(
                details={
                    "parameter": "sample/node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )

        # Check if the user has the right permissions
        if node_membership_data.role not in ["owner", "manager", "contributor"]:
            raise ForbiddenException(
                details={
                    "parameter": "datalink/node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )

        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
