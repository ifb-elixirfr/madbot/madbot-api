from datetime import timedelta

from celery import current_app
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from jsonschema import ValidationError as JsonValidationError
from jsonschema import validate
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core import connector as connectors
from madbot_api.core.errors import ValidationException
from madbot_api.core.models import (
    Connection,
    ConnectorRawSchema,
    Credential,
    Data,
    DataLink,
    SharedConnectionParam,
    Submission,
)
from madbot_api.core.views.connectors import ConnectorSerializer
from madbot_api.core.views.utils.connection import (
    check_connection_state,
)
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_connector_error,
    cesr_400_query_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)
from madbot_api.core.views.utils.vault import Vault


########################################################################################
# Serializer methods
########################################################################################
class CredentialManager:
    """Manager class for handling credentials related to connections."""

    def get_connection_credential(self, connection_id, user):
        """
        Retrieve the credential for a connection and user.

        Args:
        ----
            connection_id (UUID): The ID of the connection.
            user (MadbotUser): The user who owns the credential.

        Returns:
        -------
            Credential: The credential object if found, otherwise None.

        """
        try:
            return Credential.objects.select_related("connection__workspace").get(
                connection=connection_id,
                user=user,
            )
        except Credential.DoesNotExist:
            return None


class PrivateParamsSerializer(serializers.Serializer):
    """Serializer for validating private parameters."""

    private_param = serializers.DictField()

    def validate_private_param(self, private_param, connector):
        """
        Validate the private_params field.

        Args:
        ----
            private_param (dict): A dictionary of private parameters to be validated.
            connector (Connector): The connector class associated

        """
        # Check if private_params is provided in the request data
        if private_param is None or not private_param:
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="private_params",
            )

        # Check if private_params is a non-empty list of dictionaries
        if not isinstance(private_param, dict):
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="private_params",
                parameter_value="JSON object list",
            )

        # Check each private parameter using PrivateParamsSerializer
        for key, value in private_param.items():
            if not isinstance(key, str):
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="private_params",
                    parameter_value=key,
                )
            # Validate against json schema
            try:
                try:
                    schema = ConnectorRawSchema.objects.get(
                        slug=key, source=connector.get_name().lower()
                    ).schema
                except ConnectorRawSchema.DoesNotExist:
                    raise ValidationException(
                        code="body_validation_error",
                        details_code="schema_not_found",
                        parameter="private_params",
                        parameter_value=key,
                    )

                if schema:
                    private_param_dict = {key: private_param[key]}
                    validate(instance=private_param_dict, schema=schema)
            except JsonValidationError:
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid_value_on_schema",
                    parameter="private_params",
                    parameter_value=key,
                )

            if not isinstance(value, str):
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="private_params",
                    parameter_value=value,
                )


class SharedConnectionParamSerializer(serializers.ModelSerializer):
    """Serializer for handling connection-related data."""

    class Meta:
        """Meta class for SharedConnectionParamSerializer."""

        model = SharedConnectionParam
        fields = ("key", "value")


class ConnectionSerializer(
    MadbotSerializer,
    InteractorSerializerMixin,
    CredentialManager,
):
    """Serializer for handling connection-related data."""

    connector_obj = serializers.SerializerMethodField()
    shared_params = SharedConnectionParamSerializer(many=True)
    credential = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    last_edited_time = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    current_tasks = serializers.SerializerMethodField()

    # datalinks = serializers.SerializerMethodField()

    class Meta:
        """Meta class for ConnectionSerializer."""

        model = Connection
        fields = [
            "id",
            "name",
            "connector",
            "shared_params",
            "connector_obj",
            "credential",
            "created_by",
            "created_time",
            "last_edited_time",
            "workspace",
            "status",
            "current_tasks",
            # "datalinks",
        ]
        read_only_fields = [
            "id",
            "name",
            "credential",
            "created_by",
            "created_time",
            "last_edited_time",
            "status",
            "current_tasks",
            # "datalinks",
        ]

    def __init__(self, *args, **kwargs):
        """
        Initialize the ConnectionSerializer instance.

        Args:
        ----
            *args: Variable-length argument list.
            **kwargs: Arbitrary keyword arguments.

        """
        super().__init__(*args, **kwargs)
        self.credentials = {}
        if self.instance is not None:
            if isinstance(self.instance, models.QuerySet):
                instance_ids = [instance.id for instance in self.instance]
            else:
                instance_ids = [self.instance.id]

            for instance_id in instance_ids:
                credential = self.get_connection_credential(
                    instance_id,
                    self.context["request"].user,
                )
                self.credentials[instance_id] = credential

    def get_current_tasks(self, obj):
        """Return the current tasks for the current connection."""
        return obj.get_current_tasks()

    def get_connector_obj(self, obj):
        """
        Return serialized data for a connector object based on the connector class.

        Args:
        ----
            obj: The object containing information about a connector.

        Returns:
        -------
            An instance of the ConnectorSerializer class with the connector
            class obtained from `connectors.get_connector_class(obj.connector)`
            and the context set to include the request from self.context.

        """
        # Retrieve the credential from the pre-fetched credentials map
        credential = self.credentials.get(obj.id)
        connector_class = connectors.get_connector_class(obj.connector)

        serialized_data = ConnectorSerializer(
            connector_class,
            context={"request": self.context["request"]},
        ).data

        if issubclass(connector_class, connectors.DataConnector):
            # Add root information only for data connectors
            if hasattr(connector_class, "get_root_id") and hasattr(
                connector_class,
                "get_root_name",
            ):
                serialized_data["root"] = {
                    "id": connector_class.get_root_id(),
                    "name": connector_class.get_root_name(),
                }

        if credential is None:
            return serialized_data

        # combined_params = {
        #     **obj.get_public_param(),
        #     **obj.get_private_param(
        #         user=self.context["request"].user,
        #         credential=credential,
        #     ),
        # }

        return serialized_data

    def to_representation(self, instance):
        """
        Modify the representation dictionary by renaming the key "connector_obj" to "connector".

        Args:
        ----
            instance: The instance of the model object being serialized into
            a Python data structure.

        Returns:
        -------
            A modified representation of the instance, where the key "connector_obj"
            is renamed to "connector".

        """
        representation = super().to_representation(instance)
        representation["connector"] = representation.pop("connector_obj")
        # Check if "root" exists in the "connector" before including it in the representation
        if "root" in representation["connector"]:
            representation["root"] = representation["connector"].pop("root")
        return representation

    def get_credential(self, obj):
        """Retrieve the ID of a credential object associated with a user."""
        credential = self.credentials.get(obj.id)
        if credential is None:
            return {
                "id": None,
                "last_update_time": None,
                "status": None,
            }

        try:
            return {
                "id": credential.id,
                "last_update_time": credential.last_edited_time,
                "status": credential.status,
            }
        except Exception:
            return ""

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    # def get_datalinks(self, obj):
    #     return {"count": "", "related": ""}

    def get_last_edited_time(self, obj):
        """Return the last edited time of an object's credential for the current user."""
        credential = self.credentials.get(obj.id)
        if credential is None:
            return None

        try:
            return credential.last_edited_time
        except Exception:
            return ""

    def get_status(self, obj):
        """
        Return the status of a credential object associated with a user.

        Args:
        ----
            obj: The object containing information related to the credential.

        Returns:
        -------
            A string representing the status of the credential. Returns "not_connected"
            if no credential is found or an empty string in case of an exception.

        """
        credential = self.credentials.get(obj.id)
        if credential is None:
            return "not_connected"

        try:
            return credential.status
        except Exception:
            return ""

    def validate_shared_params(self, shared_param, connector):
        """
        Validate the format of shared_params field.

        Args:
        ----
            shared_param: The parameter containing shared parameters to validate.
            connector: The connector class associated

        Returns:
        -------
            The validated shared_param dictionary.

        Raises:
        ------
            ValidationException: If the shared_params format is invalid or does not
            conform to the expected schema.

        """
        # Check if shared_params is a non-empty list of dictionaries
        if not isinstance(shared_param, dict):
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="shared_params",
                parameter_value="JSON object list",
            )

        if not shared_param:
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="shared_params",
            )

        for key, value in shared_param.items():
            if not isinstance(key, str):
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="shared_params",
                    parameter_value=key,
                )
            # Validate against json schema
            try:
                try:
                    schema = ConnectorRawSchema.objects.get(
                        slug=key, source=connector.get_name().lower()
                    ).schema
                except ConnectorRawSchema.DoesNotExist:
                    raise ValidationException(
                        code="body_validation_error",
                        details_code="schema_not_found",
                        parameter="shared_params",
                        parameter_value=key,
                    )

                if schema:
                    shared_param_dict = {key: shared_param[key]}
                    validate(instance=shared_param_dict, schema=schema)
            except JsonValidationError:
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid_value_on_schema",
                    parameter="shared_params",
                    parameter_value=value,
                )

        return shared_param

    def validate_connector(self, connector):
        """
        Validate the connector field.

        Args:
        ----
            connector: The connector to validate.

        Returns:
        -------
            The validated connector.

        Raises:
        ------
            ValidationException: If the connector is missing or invalid.

        """
        if not connector:
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="connector",
            )
        try:
            connectors.get_connector_class(connector)
            if (
                connectors.get_connector_class(connector)
                not in connectors.get_connectors()
            ):
                raise ValidationException(
                    code="body_validation_error",
                    details_code="does_not_exist",
                    parameter="connector",
                )
        except Exception:
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid_format",
                parameter="connector",
                parameter_value="module.class",
            )
        return connector


########################################################################################
# Filter methods
########################################################################################

# In comming for connector and credential


########################################################################################
# ViewSets class
########################################################################################


class ConnectionViewSet(MadbotViewSet, WorkspaceHeaderMixin, BelongsToWorkspaceMixin):
    """ViewSet for handling connections."""

    queryset = (
        Connection.objects.select_related(
            "created_by",
            "workspace",
        )
        .prefetch_related("workspace", "shared_params")
        .order_by("name")
    )
    serializer_class = ConnectionSerializer

    @extend_schema(
        responses={
            200: ConnectionSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "type",
                    "code": "invalid",
                    "message": "<parameter_value> does not exist.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="connector_type",
                location=OpenApiParameter.QUERY,
                description="Filter connections by type",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        """List information related to all Connection objects."""
        workspace = self.get_workspace()

        # Get the connector type from the request
        connector_type = self.request.query_params.get("connector_type")

        # Define allowed connector types
        allowed_types = ["data_connector", "submission_connector"]

        if connector_type:
            # Validate the connector type
            if connector_type not in allowed_types:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="does_not_exist",
                    parameter="connector_type",
                    parameter_value=connector_type,
                )

            # List all connectors of the given type
            connector_list = [
                f"{connector.__module__}.{connector.__name__}"
                for connector in connectors.get_connectors()
                if connector_type in connector.get_types()
            ]

            # Filter the connections by connector type
            queryset = self.get_queryset().filter(
                workspace=workspace,
                connector__in=connector_list,
            )
        else:
            # If no type is provided, retrieve all connections for the workspace
            queryset = self.get_queryset().filter(workspace=workspace)

        # Get the time threshold from settings
        threshold_time = timezone.now() - timedelta(
            seconds=settings.VERIFY_CONNECTIONS_FREQUENCY
        )

        # Get the credentials that need verification
        connections_to_verify = queryset.filter(
            credential__last_edited_time__lt=threshold_time
        )

        # Trigger the async verification task only if there are connections to verify
        if connections_to_verify.exists():
            # Send the task
            current_app.send_task(
                "check_workspace_connections",
                args=[str(workspace.id), str(request.user.id)],
                kwargs={
                    "workspace": str(workspace.id),
                    "created_by": "",
                    "target": {
                        "object": "connection",
                        "id": "",
                    },
                },
            )

        # Serialize and return the paginated response
        serialized_data = self.get_serializer(
            self.filter_queryset(queryset),
            many=True,
            context={"request": request},
        ).data

        return self.get_paginated_response(self.paginate_queryset(serialized_data))

    @extend_schema(
        responses={
            201: ConnectionSerializer,
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "connector",
                        "message": "This field is required",
                        "code": "required",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "connector",
                        "message": "Not a valid connector name",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "connector",
                        "message": "<connector name> does not exist.",
                        "code": "does_not_exist",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "shared_params",
                        "message": "This field is required",
                        "code": "required",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "shared_params",
                        "message": "shared_params must be a JSON object list. Each object must have a key and value property. The id must be a string not empty.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "shared_params",
                        "message": "Invalid value for shared_params <value>",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "shared_params",
                        "message": "Missing shared_params key",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "This field is required",
                        "code": "required",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "private_params must be a JSON object list. Each object must have a key and value property. The id must be a string not empty.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "Invalid value for private_params <value>",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "`private_params` can’t be empty.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "unreachable",
                    },
                ],
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "authentication_failed",
                    },
                ],
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, *args, **kwargs):
        """Create a new Connection object."""
        workspace = self.get_workspace()

        data = request.data.copy()
        serializer = self.get_serializer(data=data)

        # validate the connector
        serializer.validate_connector(data.get("connector"))
        connector = connectors.get_connector_class(data.get("connector"))

        # raise an error if no shared params are found
        if not data.get("shared_params", None):
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="shared_param",
            )
        shared_param_data = request.data.get("shared_params")
        # validate the shared_params
        serializer.validate_shared_params(shared_param_data, connector)

        # try getting the private params
        if data.get("private_params", None):
            private_param_data = request.data.get("private_params")
            del data["private_params"]
        # private params are optional for connection with submission connectors
        # then raise an error if the connector is no a submission connector
        elif (
            "submission_connector"
            not in connectors.get_connector_class(data.get("connector")).get_types()
        ):
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="private_param",
            )
        # otherwise pursue with creating a connection without private params
        else:
            try:
                filter_query = Q(connector=data.get("connector"), workspace=workspace)
                for key, value in shared_param_data.items():
                    filter_query &= Q(
                        shared_params__key=key,
                        shared_params__value=value,
                    )
                connection = Connection.objects.get(filter_query)
            except Connection.DoesNotExist:
                # create the connection if it doesn't exist
                connection = Connection.objects.create(
                    connector=data.get("connector"),
                    created_by=request.user,
                    workspace=workspace,
                )
                for key, value in shared_param_data.items():
                    SharedConnectionParam.objects.create(
                        connection=connection,
                        key=key,
                        value=value,
                    )
                    connection.name = value
                    connection.save()

            serializer = self.get_serializer(
                connection,
                context={"request": request},
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        # validate the private_params
        private_param_serializer = PrivateParamsSerializer(data=private_param_data)
        private_param_serializer.validate_private_param(private_param_data, connector)

        try:
            filter_query = Q(connector=data.get("connector"), workspace=workspace)

            for key, value in shared_param_data.items():
                filter_query &= Q(shared_params__key=key, shared_params__value=value)

            connection = Connection.objects.get(filter_query)

        except Connection.DoesNotExist:
            # create the connection if it doesn't exist
            connection = Connection.objects.create(
                connector=data.get("connector"),
                created_by=request.user,
                workspace=workspace,
            )

            for key, value in shared_param_data.items():
                SharedConnectionParam.objects.create(
                    connection=connection,
                    key=key,
                    value=value,
                )

        try:
            # Try to connect
            connector_instance = check_connection_state(
                connection.connector,
                shared_param_data,
                private_param_data,
            )

            # Change the connection name
            connection.name = connector_instance.get_instance_name()
            connection.save()

            # Create or update the credential status
            credential = Credential.objects.get_or_create(
                connection=connection,
                user=request.user,
                status="connected",
            )

            serializer = self.get_serializer(
                connection,
                context={
                    "request": request,
                },
            )

            # Vault instanciation
            vault = Vault()
            # connection secret written in Vault
            vault.write_secret(request.user, credential[0], private_param_data)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            # Delete the connection if the connection attempt fails
            connection.delete()
            raise e

    @extend_schema(
        responses={
            200: ConnectionSerializer,
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "private_params must be a JSON object list. Each object must have a key and value property. The id must be a string not empty.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "Invalid value for private_params <value>",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "private_params",
                        "message": "`private_params` can’t be empty.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_403_forbidden(
                [
                    {
                        "parameter": "connection",
                        "message": "Not a member of the node.",
                        "code": "unauthorized",
                    },
                ],
            ),
            **cesr_404_object_not_found(),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "unreachable",
                    },
                ],
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "authentication_failed",
                    },
                ],
            ),
        },
    )
    @extend_schema(
        responses={
            200: ConnectionSerializer,
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def partial_update(self, request, *args, **kwargs):
        """Update the private parameters for the connection item."""
        workspace = self.get_workspace()
        instance = self.get_object()

        # Check the requested node is part of the workspace
        self.check_object_belongs_to_workspace(workspace, instance)

        private_params = request.data.get("private_params")
        query_shared_params = SharedConnectionParam.objects.get(connection=instance)
        shared_params = {query_shared_params.key: query_shared_params.value}

        if private_params is not None:
            if not private_params:
                raise ValidationException(
                    code="body_validation_error",
                    details_code="required",
                    parameter="private_params",
                )

            # Update private_params for the instance
            instance.private_params = private_params
            instance.save()

            # get the credentials and update them
            credential = Credential.objects.get_or_create(
                connection=instance,
                user=request.user,
            )

            # Attempt to establish the connection
            check_connection_state(instance.connector, shared_params, private_params)

            # Update the secret in the Vault
            vault = Vault()
            vault.write_secret(request.user, credential[0], private_params)

            # Update the status of the credential one the connection is established
            credential[0].status = "connected"
            credential[0].save()

            serializer = self.get_serializer(instance, context={"request": request})

        else:
            # Send the task
            current_app.send_task(
                "check_workspace_connections",
                args=[str(workspace.id), str(request.user.id), str(instance.id)],
                kwargs={
                    "workspace": str(workspace.id),
                    "created_by": {"object": "user", "id": str(request.user.id)},
                    "target": {
                        "object": "connection",
                        "id": str(instance.id),
                    },
                },
            )

            serializer = self.get_serializer(instance, context={"request": request})

        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_404_object_not_found(),
            **cesr_403_forbidden(
                [
                    {
                        "parameter": "connection",
                        "message": "Not a member of the node.",
                        "code": "unauthorized",
                    },
                ],
            ),
            **cesr_400_uri_validation_error(
                [
                    {
                        "parameter": "connection",
                        "message": "<id> is not a valid UUID.",
                        "code": "invalid",
                    },
                ],
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk):
        """Delete an Credential for the connection item."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check the requested node is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        if item.credential.exists():
            # Recover the credential
            credential = item.credential.get()
            # Delete the credential if it exists
            vault = Vault()
            vault.delete_secret(request.user, credential)
            credential.delete()

        # Check if any dataLink or data objects exist for this connection
        if (
            not DataLink.objects.filter(data__connection=item).exists()
            and not Data.objects.filter(connection=item).exists()
            and not Submission.objects.filter(connection=item).exists()
        ):
            item.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
