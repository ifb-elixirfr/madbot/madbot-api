from django.db.models import CharField, Func, Value
from django.db.models.functions import Cast
from django.templatetags.static import static
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication
from oauth2_provider.contrib.rest_framework.permissions import (
    TokenMatchesOASRequirements,
)
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core import connector as connectors
from madbot_api.core.errors import (
    ConnectorException,
    ObjectNotFoundException,
    ValidationException,
)
from madbot_api.core.models import (
    ConnectorMetadataField,
    ConnectorRawSchema,
    MadbotMetadataField,
    MetadataMapping,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_connector_error,
    cesr_400_query_validation_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import MadbotSerializer

########################################################################################
# Serializer methods
########################################################################################


class MetadataObjectTypeDTOSerializer(serializers.Serializer):
    """Serializer for MetadataObjectType DTO."""

    name = serializers.CharField()
    description = serializers.CharField()
    minimal_occurrence = serializers.IntegerField()
    maximal_occurrence = serializers.IntegerField()


class ConnectorSerializer(serializers.Serializer):
    """Serializer for Connector."""

    id = serializers.CharField()
    name = serializers.CharField()
    description = serializers.CharField()
    icon = serializers.URLField()
    fields = serializers.DictField()
    settings = serializers.ListField()
    types = serializers.ListField()
    metadata_object_types = MetadataObjectTypeDTOSerializer()
    supported_files = serializers.ListField()

    def to_representation(self, instance):
        """
        Return a dictionary representation of the connector instance's attributes.

        Args:
        ----
            instance: The connector instance to serialize.

        Returns:
        -------
            A dictionary containing the connector's attributes, including id, name,
            description, icon URL, connection fields, and additional settings if
            applicable.

        """
        request = self.context.get("request")
        base_url = request.build_absolute_uri("/api/")
        shared_fields, private_fields = instance.get_connection_fields()
        source = instance.get_name().lower()

        def get_schema_url(slug):
            schema = ConnectorRawSchema.objects.get(slug=slug, source=source)
            return f"{base_url}schemas/connectors/{schema.source}/{schema.slug}"

        shared = [url for field in shared_fields if (url := get_schema_url(field.slug))]
        private = [
            url for field in private_fields if (url := get_schema_url(field.slug))
        ]

        data = {
            "id": instance.get_id(),
            "name": instance.get_name(),
            "description": instance.get_description(),
            "documentation": (
                instance.get_connection_documentation()
                if hasattr(instance, "get_connection_documentation")
                and instance.get_connection_documentation()
                else None
            ),
            "icon": request.build_absolute_uri(static(instance.get_logo())),
            "types": instance.get_types(),
            "fields": {"shared": shared, "private": private},
        }

        if "submission_connector" in instance.get_types():
            settings_slugs = instance.get_settings_fields()
            data["settings"] = [
                get_schema_url(field.slug)
                for field in settings_slugs
                if (url := get_schema_url(field.slug))
            ]
            data["supported_files"] = instance.get_supported_file_extensions()
            data["metadata_object_types"] = MetadataObjectTypeDTOSerializer(
                [
                    mot_dto.dict()
                    for mot_dto in instance.get_all_metadata_object_types().values()
                ],
                many=True,
            ).data

        return data


class MadbotMetadataFieldSerializer(MadbotSerializer):
    """Serializer for MetadataField."""

    related_types = serializers.SerializerMethodField()
    schema = serializers.SerializerMethodField()

    class Meta:
        """Meta class for MetadataFieldSerializer."""

        model = MadbotMetadataField
        fields = ["id", "slug", "name", "description", "schema", "related_types"]

    def get_schema(self, obj):
        """
        Return the schema associated with the metadata field.

        Args:
        ----
            obj: The metadata field instance.

        Returns:
        -------
            The schema of the metadata field.

        """
        schema_instance = obj.schema
        return schema_instance.schema

    def get_related_types(self, obj):
        """
        Return a list of related types for the metadata field.

        Args:
        ----
            obj: The metadata field instance.

        Returns:
        -------
            A list of related types associated with the metadata field.

        """
        types = ["data", "sample", "node"]
        return [t for t in types if getattr(obj, f"{t}_related", False)]


class ConnectorMetadataFieldSerializer(MadbotSerializer):
    """Serializer for MetadataField."""

    related_types = serializers.SerializerMethodField()
    schema = serializers.SerializerMethodField()

    class Meta:
        """Meta class for MetadataFieldSerializer."""

        model = ConnectorMetadataField
        fields = [
            "id",
            "slug",
            "name",
            "description",
            "schema",
            "related_types",
            "level",
        ]

    def get_schema(self, obj):
        """
        Return the schema associated with the metadata field.

        Args:
        ----
            obj: The metadata field instance.

        Returns:
        -------
            The schema of the metadata field.

        """
        schema_instance = obj.schema
        return schema_instance.schema

    def get_related_types(self, obj):
        """
        Return a list of related types for the metadata field.

        Args:
        ----
            obj: The metadata field instance.

        Returns:
        -------
            A list of related types associated with the metadata field.

        """
        types = ["data", "sample", "node"]
        return [t for t in types if getattr(obj, f"{t}_related", False)]


class MetadataMappingSerializer(MadbotSerializer):
    """Serializer for MetadataMapping."""

    connector_metadata_field = ConnectorMetadataFieldSerializer()
    madbot_metadata_field = MadbotMetadataFieldSerializer()

    class Meta:
        """Meta class for MetadataMappingSerializer."""

        model = MetadataMapping
        fields = [
            "connector_metadata_field",
            "madbot_metadata_field",
            "connector",
        ]


########################################################################################
# APIView class
########################################################################################


class ConnectorsListAPIView(APIView):
    """ViewSet for handling connectors."""

    pagination_class = StandardPagination
    permission_classes = (IsAuthenticated,)
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenMatchesOASRequirements]
    required_alternate_scopes = {
        "GET": [["read"]],
    }

    @extend_schema(
        responses={
            200: ConnectorSerializer,
            **cesr_400_query_validation_error(
                [
                    {
                        "parameter": "type",
                        "message": str(type) + " does not exist.",
                        "code": "does_not_exist",
                    },
                ],
            ),
        },
        parameters=[
            OpenApiParameter(
                name="type",
                location=OpenApiParameter.QUERY,
                description="Filter connectors by type",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def get(self, request, *args, **kwargs):
        """Retrieve a list of connectors."""
        connector_list = []
        if self.request.query_params.get("type"):
            connector_type = str(self.request.query_params.get("type"))
            type_allowed = ["data_connector", "submission_connector"]
            if connector_type not in type_allowed:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="does_not_exist",
                    parameter="type",
                    parameter_value=connector_type,
                )
            for connector in connectors.get_connectors():
                if connector_type in connector.get_types():
                    connector_list.append(connector)
        else:
            for connector in connectors.get_connectors():
                connector_list.append(connector)

        paginator = self.pagination_class()
        page = paginator.paginate_queryset(connector_list, request)
        serializer = ConnectorSerializer(page, many=True, context={"request": request})
        return paginator.get_paginated_response(serializer.data)


class ConnectorsDetailAPIView(APIView):
    """ViewSet for handling connectors."""

    permission_classes = (IsAuthenticated,)
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenMatchesOASRequirements]
    required_alternate_scopes = {
        "GET": [["read"]],
    }

    @extend_schema(
        responses={
            200: ConnectorSerializer,
            **cesr_404_object_not_found(),
        },
    )
    def get(self, request, name):
        """Retrieve a specific connector by name."""
        connector_list = connectors.get_connectors()
        request_connector = None
        for connector in connector_list:
            if connector.get_name().lower() == name.lower():
                request_connector = connector
                break
        if not request_connector:
            raise ObjectNotFoundException(
                parameter="name",
                parameter_value=name,
            )
        serializer = ConnectorSerializer(
            request_connector,
            context={"request": request},
        )
        return Response(serializer.data)


class JSONExtract(Func):
    """A function to extract JSON values from a JSON string."""

    function = "json_extract"
    template = "%(function)s(%(expressions)s)"


class ConnectorsGroupsAPIView(APIView):
    """ViewSet for handling connector groups."""

    permission_classes = (IsAuthenticated,)
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenMatchesOASRequirements]
    required_alternate_scopes = {
        "GET": [["read"]],
    }

    @extend_schema(
        responses={
            200: ConnectorSerializer,
            **cesr_400_query_validation_error(
                [
                    {
                        "parameter": "parent",
                        "message": "Invalid type",
                        "code": "invalid_type",
                    },
                ],
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connector",
                        "message": "Invalid connector type",
                        "code": "invalid_connector_type",
                    },
                ],
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="parent",
                location=OpenApiParameter.QUERY,
                description="Filter groups by parent",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def get(self, request, name):
        """Retrieve the list of metadata fields groups for a specific connector."""
        connector_list = connectors.get_connectors()
        request_connector = None
        for connector in connector_list:
            if connector.get_name().lower() == name.lower():
                request_connector = connector
                break
        if not request_connector:
            raise ObjectNotFoundException(
                parameter="name",
                parameter_value=name,
            )

        # check that the connector is a submission connector
        if "submission_connector" not in request_connector.get_types():
            raise ConnectorException(
                details_code="invalid_connector_type",
                parameter="connector",
                parameter_value=request_connector.get_types(),
            )

        parent = request.query_params.get("parent", None)
        if parent and not isinstance(parent, str):
            raise ValidationException(
                code="query_validation_error",
                details_code="invalid_type",
                parameter="parent",
                parameter_value=parent,
            )
        groups = request_connector.get_metadata_fields_groups(parent)
        return Response(groups)


class ConnectorsMetadataMappingViewSet(APIView):
    """ViewSet for handling connectors metadata mapping."""

    @extend_schema(
        responses={
            200: MetadataMappingSerializer,
            **cesr_404_object_not_found(),
            **cesr_400_query_validation_error(
                detail={
                    "parameter_name": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported",
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "related_types",
                    "code": "invalid",
                    "message": "Invalid value <parameter_name>.",
                },
            ),
            **cesr_400_connector_error(
                detail={
                    "parameter": "connector",
                    "code": "invalid_connector_type",
                    "message": "Invalid connector type",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="group",
                location=OpenApiParameter.QUERY,
                description="Filter by group name",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def get(self, request, name, *args, **kwargs):
        """List the metadata field mapping to a specific connector and an optional group."""
        connector_list = connectors.get_connectors()
        request_connector = next(
            (
                connector
                for connector in connector_list
                if connector.get_name().lower() == name.lower()
            ),
            None,
        )
        if not request_connector:
            raise ObjectNotFoundException(
                parameter="name",
                parameter_value=name,
            )

        if "submission_connector" not in request_connector.get_types():
            raise ConnectorException(
                details_code="invalid_connector_type",
                parameter="connector",
                parameter_value=request_connector.get_types(),
            )

        group = request.query_params.get("group", None)

        # Query using Django ORM
        metadata_mappings = MetadataMapping.objects.filter(
            connector=request_connector.__name__,
        ).select_related("connector_metadata_field", "madbot_metadata_field")

        if group and isinstance(group, str):
            metadata_mappings = metadata_mappings.annotate(
                extracted_group=Cast(
                    JSONExtract("external_field", Value("$.groups")),
                    CharField(),
                ),
            ).filter(extracted_group__icontains=group)

        serializer = MetadataMappingSerializer(metadata_mappings, many=True)
        return Response(serializer.data)
