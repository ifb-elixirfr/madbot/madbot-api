from celery import current_app
from django.db.models import F
from django.shortcuts import get_object_or_404
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core import connector as connectors
from madbot_api.core.connector import ConnectorError, DataConnector
from madbot_api.core.errors import ConnectorException, ForbiddenException
from madbot_api.core.models import (
    Credential,
    Data,
    DataAccessRight,
    DataLink,
    ExternalAccess,
    NodeMember,
    WorkspaceMember,
)
from madbot_api.core.views.connections import ConnectionSerializer
from madbot_api.core.views.utils.connection import check_connection_state
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.object_handeling import object_to_dictionary
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_connector_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import MadbotSerializer
from madbot_api.core.views.utils.vault import Vault


class DataAccessRightSerializer(MadbotSerializer):
    """Serializer for data access right."""

    class Meta:
        """Meta class for DataAccessRightSerializer."""

        model = DataAccessRight
        fields = ["access_right", "last_updated_time", "last_verification_time"]


class DataSerializer(MadbotSerializer):
    """Serializer for Data."""

    size = serializers.SerializerMethodField()
    external_accesses = serializers.SerializerMethodField()
    data_access = serializers.SerializerMethodField()
    datalinks = serializers.SerializerMethodField()

    class Meta:
        """Meta class for DataSerializer."""

        model = Data
        fields = [
            "id",
            "name",
            "connection",
            "external_id",
            "size",
            "icon",
            "type",
            "media_type",
            "file_extension",
            "external_accesses",
            "created_time",
            "last_update_time",
            "workspace",
            "data_access",
            "datalinks",
        ]
        read_only_fields = [
            "id",
            "name",
            "size",
            "icon",
            "type",
            "media_type",
            "file_extension",
            "created_time",
            "last_update_time",
            "workspace",
            "data_access",
            "datalinks",
        ]

    def get_size(self, obj):
        """
        Return the size of the object in octets.

        Args:
        ----
            obj: The object for which to determine the size.

        Returns:
        -------
            A dictionary containing the size of the object and the unit "octets".

        """
        return {"value": obj.size, "unit": "octets"}

    def get_connection_obj(self, obj):
        """
        Return serialized data for the connection object with context information.

        Args:
        ----
            obj: The object containing a connection attribute to be serialized.

        Returns:
        -------
            The serialized data of the connection object within the provided object,
            including the context of the current request.

        """
        return ConnectionSerializer(
            obj.connection,
            context={"request": self.context["request"]},
        ).data

    def get_external_accesses(self, obj):
        """
        Retrieve external access values for the given object.

        Args:
        ----
            obj: The object used to retrieve external access values based on its
            external_id attribute.

        Returns:
        -------
            A list of dictionaries containing the external access type and access
            values for the object's external_id.

        """
        # Construct a list of dictionaries of the external accesses
        return [
            {
                "type": item["external_accesses__type"],
                "access": item["external_accesses__access"],
            }
            for item in Data.objects.filter(external_id=obj.external_id).values(
                "external_accesses__type",
                "external_accesses__access",
            )
        ]

    def get_data_access(self, obj):
        """
        Retrieve data access right value for the current user.

        Args:
        ----
            obj: The object used to retrieve access right value.

        Returns:
        -------
            A list of dictionaries containing the access right and last updated type and
            last verification time.

        """
        try:
            da = obj.access_right.get(user=self.context["request"].user)
            return DataAccessRightSerializer(da).data
        except DataAccessRight.DoesNotExist:
            return {}

    def get_datalinks(self, obj):
        """
        Retrieve datalink to access the current data.

        Args:
        ----
            obj: The object used to retrieve datalink for.

        Returns:
        -------
            A list of dictionaries containing the datalinks.

        """
        return obj.datalinks.filter(
            node__node_members__user=self.context["request"].user
        ).values("id", "node", node_title=F("node__title"))


########################################################################################
# ViewSets class
########################################################################################


class DataViewSet(MadbotViewSet, WorkspaceHeaderMixin, BelongsToWorkspaceMixin):
    """ViewSet for handling the data."""

    queryset = Data.objects.select_related("workspace", "connection").prefetch_related(
        "connection",
    )

    serializer_class = DataSerializer

    @extend_schema(
        responses={
            200: DataSerializer,
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        """List the information related to all the Data objects."""
        workspace = self.get_workspace()

        # Prepare kwargs to send with the task
        task_kwargs = {
            "workspace": str(workspace.id),
            "created_by": {},
            "target": {
                "object": "data",
                "id": "",
            },
        }

        current_app.send_task(
            "check_data_access",
            args=[
                str(workspace.id),
                str(request.user.id),
            ],
            kwargs=task_kwargs,
        )

        serializer = self.get_serializer(
            self.get_queryset()
            .filter(workspace=workspace)
            .filter(
                datalinks__in=DataLink.objects.filter(
                    node__workspace=workspace,
                    node__node_members__user=request.user,
                ).distinct(),
            )
            .distinct(),
            many=True,
        )

        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: DataSerializer,
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, pk):
        """Retrieve a Data item's information."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check the requested node is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        # Check if the user has access to this data
        datalinks_node_ids = item.datalinks.all().values_list("node_id", flat=True)
        if (
            not WorkspaceMember.objects.filter(
                workspace=workspace,
                user=self.request.user,
                role="owner",
            ).exists()
            and not NodeMember.objects.filter(
                node__id__in=datalinks_node_ids,
                user=self.request.user,
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": "You do not have access to this data.",
                },
            )

        serializer = self.get_serializer(item, context={"request": request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            201: DataSerializer,
            200: DataSerializer,
            **cesr_404_object_not_found(),
            **cesr_400_body_validation_error(
                detail={
                    "message": "This field is required.",
                    "field": "connection",
                    "code": "required",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "message": "This field is required.",
                    "field": "external_id",
                    "code": "required",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "field": "connection",
                    "message": "<field_value> does not exist.",
                    "code": "does_not_exist",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "field": "connection",
                    "message": "<field_value> is not a valid UUID.",
                    "code": "invalid",
                },
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "unreachable",
                    },
                ],
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "authentication_failed",
                    },
                ],
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, *args, **kwargs):
        """Create a new Data object."""
        workspace = self.get_workspace()

        # Body params check
        serializer = self.get_serializer(
            data=request.data,
            context={"request": request},
        )
        serializer.is_valid()

        # verify the connector
        connector_class = connectors.get_connector_class(
            serializer.validated_data["connection"].connector,
        )
        if not issubclass(connector_class, DataConnector):
            raise ConnectorException(
                details_code="invalid_connector_type",
                parameter="connector",
                parameter_value=connector_class,
            )

        credential = get_object_or_404(
            Credential,
            connection=serializer.validated_data["connection"],
            user=request.user,
        )

        # Convert the shared parameters related to the credential's connection to a dictionary representation.
        shared_params_dict = object_to_dictionary(
            credential.connection.shared_params.all(),
        )

        vault = Vault()
        private_param_data = vault.read_secret(request.user, credential)

        # Attempt to establish the connection
        connector_instance = check_connection_state(
            serializer.validated_data["connection"].connector,
            shared_params_dict,
            private_param_data,
        )

        # Test if external data id exists
        try:
            external_data = connector_instance.get_data_object(
                serializer.validated_data["external_id"],
            )
        except ConnectorError as e:
            raise ConnectorException(error=e)
        except Exception:
            raise ConnectorException(
                details_code="dataobject_not_found",
                parameter="external_data",
                parameter_value=serializer.validated_data["external_id"],
            )

        try:
            existing_data = Data.objects.get(
                external_id=external_data.id,
                connection=request.data["connection"],
            )
            DataAccessRight.objects.get_or_create(
                data=existing_data, user=request.user, access_right="accessible"
            )

            status_code = status.HTTP_200_OK
            response_data = self.get_serializer(
                existing_data, context={"request": request}
            ).data
        except Data.DoesNotExist:
            external_access_instances = [
                ExternalAccess.objects.create(
                    type=key,
                    access=value,
                )
                for key, value in external_data.accesses.items()
            ]
            new_data = Data.objects.create(
                name=external_data.name,
                description=external_data.description,
                type=external_data.type,
                media_type=external_data.media_type,
                size=external_data.size,
                connection=credential.connection,
                file_extension=external_data.file_extension,
                external_id=external_data.id,
                icon=external_data.icon,
                has_children=external_data.has_children,
                workspace=workspace,
            )

            DataAccessRight.objects.create(
                data=new_data, user=request.user, access_right="accessible"
            )

            new_data.external_accesses.set(external_access_instances)
            status_code = status.HTTP_201_CREATED
            response_data = self.get_serializer(
                new_data, context={"request": request}
            ).data

        return Response(response_data, status=status_code)
