from django.db.models import Count, F
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import (
    ForbiddenException,
    ObjectNotFoundException,
    ValidationException,
)
from madbot_api.core.models import (
    Data,
    DataAssociation,
    NodeMember,
    WorkspaceMember,
)
from madbot_api.core.views.data import DataSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)


class DataAssociationSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Data associations."""

    data_obj = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for DataAssociationSerializer."""

        model = DataAssociation
        fields = [
            "id",
            "data_objects",
            "data_obj",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "data_obj",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_data_obj(self, obj):
        """
        Return serialized data for the current data.

        Args:
        ----
            obj: The object containing information about the current data.

        Returns:
        -------
            An instance of the DataSerializer class.


        """
        return DataSerializer(
            obj.data_objects.all(),
            context={"request": self.context["request"]},
            many=True,
        ).data

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.


        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.


        """
        return self.get_interactor_information(obj.last_edited_by)

    def to_representation(self, instance):
        """
        Modify the representation dictionary".

        Args:
        ----
            instance: The instance of the model object being serialized into
            a Python data structure.

        Returns:
        -------
            A modified representation of the instance, where the key "datalink"
            is removed and the key "associated_data_obj" and "associated_datalink_obj"
            are renamed to "associated_data" and "associated_datalink".


        """
        representation = super().to_representation(instance)
        representation["data_objects"] = representation.pop("data_obj")
        return representation


########################################################################################
# ViewSets class
########################################################################################


class DataAssociationViewSet(
    MadbotViewSet, GetObjectMixin, WorkspaceHeaderMixin, BelongsToWorkspaceMixin
):
    """ViewSet for handling the data associations."""

    queryset = DataAssociation.objects.all()

    serializer_class = DataAssociationSerializer
    pagination_class = StandardPagination

    @extend_schema(
        responses={
            200: DataAssociationSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": "You do not have access to this data.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, data_pk=None):
        """List the information related to all the Data Association objects."""
        workspace = self.get_workspace()
        current_data = self.get_the_object(Data, data_pk, "uri")

        # check the access to the current data
        datalinks_node_ids = current_data.datalinks.all().values_list(
            "node_id", flat=True
        )
        if (
            not WorkspaceMember.objects.filter(
                workspace=workspace,
                user=self.request.user,
                role="owner",
            ).exists()
            and not NodeMember.objects.filter(
                node__id__in=datalinks_node_ids,
                user=self.request.user,
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": "You do not have access to this data.",
                },
            )

        # Check the requested data is part of the workspace
        self.check_object_belongs_to_workspace(workspace, current_data)

        # retrieve the association accessible to the user
        accessible_associations = []
        # if the user is a workspace owner, return all associations
        if WorkspaceMember.objects.filter(
            workspace=workspace,
            user=self.request.user,
            role="owner",
        ).exists():
            accessible_associations = current_data.associations.all()
        else:
            for association in current_data.associations.all():
                all_data_accessible = True

                # Get all data in the association except the current one
                other_data_objects = association.data_objects.exclude(
                    id=current_data.id
                )

                # For each data in the association, check if the user has access
                for other_data in other_data_objects:
                    # Check if the user has access to at least one datalink of the other_data
                    if not other_data.datalinks.filter(
                        node__node_members__user=request.user
                    ).exists():
                        all_data_accessible = False
                        break

                if all_data_accessible:
                    accessible_associations.append(association)

        serializer = self.get_serializer(
            accessible_associations,
            many=True,
            context={
                "request": request,
                "current_data": current_data,
            },
        )

        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: DataAssociationSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "association",
                    "code": "not_allowed",
                    "message": "You do not have access to this association.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, pk, data_pk=None):
        """Retrieve a Data Association item's information."""
        workspace = self.get_workspace()
        current_data = self.get_the_object(Data, data_pk, "uri")

        # Check the requested data is part of the workspace
        self.check_object_belongs_to_workspace(workspace, current_data)
        # get the association object
        association = self.get_object()

        # check that the association correspond to that data
        if not association.data_objects.filter(id=data_pk).exists():
            raise ObjectNotFoundException(
                parameter="association",
                parameter_value=pk,
            )

        # Check if the user has access to this association
        # if the user is a workspace owner, pass this verification
        if not WorkspaceMember.objects.filter(
            workspace=workspace,
            user=self.request.user,
            role="owner",
        ).exists():
            # Get all data in the association except the current one
            other_data_objects = association.data_objects.exclude(id=current_data.id)

            # For each data in the association, check if the user has access
            for other_data in other_data_objects:
                # Check if the user has access to at least one datalink of the other_data
                if not other_data.datalinks.filter(
                    node__node_members__user=request.user
                ).exists():
                    raise ForbiddenException(
                        details={
                            "parameter": "association",
                            "code": "not_allowed",
                            "message": "You do not have access to this association.",
                        },
                    )

        serializer = self.get_serializer(
            association,
            context={
                "request": request,
                "current_data": current_data,
            },
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            201: DataAssociationSerializer,
            200: DataAssociationSerializer,
            **cesr_404_object_not_found(),
            **cesr_400_body_validation_error(
                detail={
                    "field": "datalink",
                    "code": "invalid",
                    "message": "datalink is not linked with the associated Data",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "field": "associated_datalink",
                    "code": "invalid",
                    "message": "Associated datalink is not linked with the associated Data",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "datalink",
                    "code": "not_allowed",
                    "message": "You do not have access to this datalink.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "associated_datalink",
                    "code": "not_allowed",
                    "message": "You do not have access to this datalink.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, data_pk=None):
        """Create a new Data Association object."""
        workspace = self.get_workspace()

        # Body params check
        serializer = self.get_serializer(
            data=request.data,
            context={"request": request},
        )
        serializer.is_valid()

        # check that there is only one data to associate
        if len(request.data.get("data_objects")) != 1:
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid",
                parameter="data_objects",
                parameter_value="You can only associate exactly two data objects, please provide only one data to associate.",
            )

        # get the data
        current_data = self.get_the_object(Data, data_pk, "uri")
        associated_data = [
            self.get_the_object(Data, did, "body")
            for did in request.data.get("data_objects")
        ]

        # check that all data belong to the workspace and that the user has access to all of them
        current_data_membership = current_data.datalinks.filter(
            node__node_members__user=request.user
        ).annotate(role=F("node__node_members__role"))

        if (
            WorkspaceMember.objects.filter(
                workspace=workspace,
                user=self.request.user,
                role="owner",
            ).exists()
            and not current_data_membership.exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": f"You do not have access to this data: {current_data.id}.",
                },
            )
        if not current_data_membership.filter(
            role__in=["owner", "manager", "contributor"]
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            )
        self.check_object_belongs_to_workspace(workspace, current_data)

        # For each data in the association, check if the user has access and edition rights
        for associated_datum in associated_data:
            # Check if the user has access to at least one datalink of the associated_data
            memberships = associated_datum.datalinks.filter(
                node__node_members__user=request.user
            ).annotate(role=F("node__node_members__role"))

            if not memberships.exists():
                raise ForbiddenException(
                    details={
                        "parameter": "data_objects",
                        "code": "not_allowed",
                        "message": f"You do not have access to this data: {associated_datum.id}.",
                    },
                )
            if not memberships.filter(
                role__in=["owner", "manager", "contributor"]
            ).exists():
                raise ForbiddenException(
                    details={
                        "parameter": "data_objects",
                        "code": "not_allowed",
                        "message": "The member's role does not allow creation.",
                    },
                )
            self.check_object_belongs_to_workspace(workspace, associated_datum)

        # check if an association already exists
        # Get all DataAssociation objects that contain all data in the associated data list
        associations_candidates = (
            DataAssociation.objects.filter(
                data_objects__in=[
                    current_data,
                    *associated_data,
                ]  # Filter associations that contain any data in the list
            )
            .annotate(
                data_count=Count(
                    "data_objects"
                )  # Count the number of data objects in each association
            )
            .filter(
                data_count=len(
                    [current_data, *associated_data]
                )  # Filter associations where the number of data objects matches the associated data list
            )
        )
        for association in associations_candidates:
            if set(association.data_objects.all()) == set(
                [current_data, *associated_data]
            ):
                serializer = self.get_serializer(
                    association,
                    context={
                        "request": request,
                        "current_data": current_data,
                    },
                )
                return Response(serializer.data, status=status.HTTP_200_OK)

        association = DataAssociation.objects.create(
            created_by=request.user,
            last_edited_by=request.user,
        )
        association.data_objects.add(
            *[
                current_data,
                *associated_data,
            ]
        )
        serializer = self.get_serializer(
            association,
            context={
                "request": request,
                "current_data": current_data,
            },
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            204: None,
            **cesr_403_forbidden(
                detail={
                    "parameter": "association",
                    "code": "not_allowed",
                    "message": "You do not have access to this association.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "association",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk, data_pk=None):
        """Delete a specific Data Association item."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the data
        data = self.get_the_object(Data, data_pk, "uri")
        # check if it's the right workspace for the data
        self.check_object_belongs_to_workspace(workspace, data)

        # retrieve the item
        association = self.get_object()

        # check that the association correspond to that data
        if not association.data_objects.filter(id=data_pk).exists():
            raise ObjectNotFoundException(
                parameter="association",
                parameter_value=pk,
            )

        # Check if the user has access to this association

        # For each data in the association, check if the user has access and deletion rights
        for datum in association.data_objects.all():
            # Check if the user has access to at least one datalink of the datum
            memberships = datum.datalinks.filter(
                node__node_members__user=request.user
            ).annotate(role=F("node__node_members__role"))

            if not memberships.exists():
                raise ForbiddenException(
                    details={
                        "parameter": "association",
                        "code": "not_allowed",
                        "message": "You do not have access to this association.",
                    },
                )
            if not memberships.filter(
                role__in=["owner", "manager", "contributor"]
            ).exists():
                raise ForbiddenException(
                    details={
                        "parameter": "association",
                        "code": "not_allowed",
                        "message": "The member's role does not allow deletion.",
                    },
                )

        association.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
