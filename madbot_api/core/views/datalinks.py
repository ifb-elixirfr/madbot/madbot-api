from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import Data, DataLink, Node, NodeMember
from madbot_api.core.views.data import DataSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetNodeMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.notification import trigger_notification
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class DatalinkSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Datalink."""

    data_obj = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    data = serializers.PrimaryKeyRelatedField(queryset=Data.objects.all())
    node = serializers.PrimaryKeyRelatedField(queryset=Node.objects.all())

    class Meta:
        """Meta class for DatalinkSerializer."""

        model = DataLink
        fields = ["id", "node", "data", "data_obj", "created_by", "created_time"]
        read_only_fields = [
            "id",
            "date_id",
            "node_id",
            "data_obj",
            "created_by",
            "created_time",
        ]

    def get_data_obj(self, obj):
        """
        Return serialized data object with request context.

        :param obj: The `obj` parameter in the `get_data_obj` method is likely an
        object that contains data that needs to be serialized. The method appears
        to be using a `DataSerializer` to serialize the data within the `obj`
        object. The serialized data is then returned with additional context
        information that includes
        :return: A DataSerializer object is being returned, with the data
        attribute of the input obj and the context set to include the request from
        the current context.
        """
        return DataSerializer(
            obj.data,
            context={"request": self.context["request"]},
        ).data

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def to_representation(self, instance):
        """
        Transform the instance into a dictionary representation.

        :param instance: The instance to be transformed into a dictionary.
        :return: A dictionary representation of the instance with the `data_obj`
        field renamed to `data`.
        """
        representation = super().to_representation(instance)
        representation["data"] = representation.pop("data_obj")
        return representation


########################################################################################
# ViewSets class
########################################################################################


class DataLinkViewSet(
    MadbotViewSet,
    GetNodeMixin,
    BelongsToWorkspaceMixin,
    WorkspaceHeaderMixin,
):
    """ViewSet for handling datalinks."""

    queryset = DataLink.objects.select_related(
        "data__connection",
        "node",
        "created_by",
    ).prefetch_related(
        "data__connection__shared_params",
        "data__connection__credential",
    )
    serializer_class = DatalinkSerializer

    @extend_schema(
        responses={
            200: DatalinkSerializer,
            **cesr_403_forbidden(
                [
                    {
                        "parameter": "node",
                        "message": "Not a member of the node.",
                        "code": "unauthorized",
                    },
                ],
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, node_pk=None, *args, **kwargs):
        """List the information related to all the Node objects."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        serializer = self.get_serializer(
            self.get_queryset().filter(node=node),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            201: DatalinkSerializer,
            200: DatalinkSerializer,
            **cesr_403_forbidden(
                [
                    {
                        "parameter": "node",
                        "message": "Not a member of the node.",
                        "code": "unauthorized",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "data",
                        "message": "<data id> does not exist.",
                        "code": "does_not_exist",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "data",
                        "message": "<data id> is not a valid UUID.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "data",
                        "message": "This field is required",
                        "code": "required",
                    },
                ],
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, node_pk=None, *args, **kwargs):
        """Create a new Datalink object."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # Check if the user has the right permissions
        if NodeMember.objects.filter(
            user=request.user,
            node=node_pk,
            role="collaborator",
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member’s role does not allow creation.",
                },
            )

        try:
            existing_link = DataLink.objects.get(
                data=request.data["data"],
                node=node_pk,
            )
            serializer = self.get_serializer(existing_link)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except DataLink.DoesNotExist:
            # If it doesn't exist, create a new DataLink entry
            serializer = self.get_serializer(
                data={**request.data, "node": node_pk},
                context={"request": request},
            )
            serializer.is_valid()
            datalink = serializer.save()

            # Send notification
            members = node.node_members.all().exclude(user=request.user)
            for member in members:
                trigger_notification(
                    sender=request.user,
                    recipient=member.user,
                    verb="created",
                    action_object=datalink,
                    target=node,
                    actor=request.user,
                    workspace=workspace.id,
                )

            return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                [
                    {
                        "parameter": "node",
                        "message": "<node id> is not a valid UUID.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_uri_validation_error(
                [
                    {
                        "parameter": "datalink",
                        "message": "<datalink id> is not a valid UUID.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_403_forbidden(
                [
                    {
                        "parameter": "node",
                        "message": "Not a member of the node.",
                        "code": "unauthorized",
                    },
                ],
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk, node_pk=None):
        """Delete an Datalink item."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # Check if the user has the right permissions
        if NodeMember.objects.filter(
            user=request.user,
            node=node_pk,
            role="collaborator",
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member’s role does not allow deletion.",
                },
            )

        item = self.get_object()
        self.check_object_belongs_to_node(node, item)
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
