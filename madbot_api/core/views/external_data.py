import logging

from django.shortcuts import get_object_or_404
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers

from madbot_api.core import connector as connectors
from madbot_api.core.connector import ConnectorError
from madbot_api.core.errors import ConnectorException, ValidationException
from madbot_api.core.models import Connection, Credential
from madbot_api.core.views.utils.connection import check_connection_state
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.object_handeling import object_to_dictionary
from madbot_api.core.views.utils.schema_response import (
    cesr_400_connector_error,
    cesr_400_query_validation_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.vault import Vault

logger = logging.getLogger(__name__)

########################################################################################
# Serializer methods
########################################################################################


class ExternalDataSerializer(serializers.Serializer):
    """Serialize external data attributes for representation."""

    id = serializers.CharField()
    name = serializers.CharField()
    size = serializers.DictField(child=serializers.CharField())
    file_extension = serializers.CharField()
    type = serializers.CharField()
    icon = serializers.CharField()
    has_children = serializers.BooleanField()

    def get_size(self, obj):
        """
        Return the size of an object in octets if it exists.

        Check if the `size` attribute exists in the object. If it does,
        return a dictionary containing the size value and the unit "octets".

        Args:
        ----
            obj: The object that has a `size` attribute.

        Returns:
        -------
            dict or None: A dictionary with the size value and the unit
            "octets" if the size exists; otherwise, return None.

        """
        if obj.size:
            return {"value": obj.size, "unit": "octets"}
        return None

    def get_has_children(self, obj):
        """
        Check if the given object has any children.

        Determine whether the object has any children by calling the
        `exists()` method on its `children` attribute.

        Args:
        ----
            obj: The object that may have children.

        Returns:
        -------
            bool: True if the object has children, otherwise False.

        """
        return obj.children.exists()

    def to_representation(self, instance):
        """
        Convert the instance to a dictionary representation.

        Args:
        ----
            instance: The instance to serialize.

        Returns:
        -------
            dict: A dictionary representation of the instance.

        """
        return {
            "id": instance.id,
            "name": instance.name,
            "size": {"value": instance.size, "unit": "octets"},
            "type": instance.type,
            "media_type": instance.media_type,
            "file_extension": instance.file_extension,
            "icon": instance.icon,
            "has_children": instance.has_children,
        }


########################################################################################
# ViewSets class
########################################################################################


class ExternalDataViewSet(
    MadbotViewSet,
    WorkspaceHeaderMixin,
    BelongsToWorkspaceMixin,
    GetObjectMixin,
):
    """ViewSet for handling external data."""

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="parent",
                type={
                    "type": "string",
                },
                description="Parent id to filter externalData.",
                style="form",
            ),
        ],
        responses={
            200: ExternalDataSerializer,
            **cesr_400_query_validation_error(
                [
                    {
                        "parameter": "parent",
                        "message": "Unknown parent data object.",
                        "code": "unknown",
                    },
                ],
            ),
            **cesr_404_object_not_found(),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "unreachable",
                    },
                ],
            ),
            **cesr_400_connector_error(
                [
                    {
                        "parameter": "connection",
                        "message": "Custom message from the connector",
                        "code": "authentication_failed",
                    },
                ],
            ),
        },
    )
    def list(self, request, connections_pk=None, *args, **kwargs):
        """Retrieve a list of external data objects through the given connection."""
        logger.debug("list : get_workspace")
        workspace = self.get_workspace()

        logger.debug("list : retrieve connection")
        connection = self.get_the_object(Connection, connections_pk, context="uri")

        logger.debug("list : check connection belongs to workspace")
        self.check_object_belongs_to_workspace(workspace, connection)

        logger.debug("list : check connection's connector is of type data_connector")
        if (
            "data_connector"
            not in connectors.get_connector_class(connection.connector).get_types()
        ):
            raise ValidationException(
                code="uri_validation_error",
                details_code="invalid",
                parameter="connector",
                parameter_value="connector is not a data connector",
            )

        logger.debug("list : retrieve credential")
        try:
            credential = get_object_or_404(
                Credential,
                connection=connections_pk,
                user=request.user,
            )
            # Convert the shared parameters related to the credential's connection to a dictionary representation.
            shared_params_dict = object_to_dictionary(
                credential.connection.shared_params.all(),
            )

            logger.debug("list : retrieve secret")
            vault = Vault()
            private_param_data = vault.read_secret(request.user, credential)

            logger.debug("list : instanciate connector")
            # Attempt to establish the connection
            connector_instance = check_connection_state(
                credential.connection.connector,
                shared_params_dict,
                private_param_data,
            )

            # Get parent ID
            parent = self.request.query_params.get("parent", None)

            logger.debug("list : retrieve data objects")
            # Fetch the list of external data objects using the connector
            external_data_objects = connector_instance.get_data_objects(
                parent_id=parent,
            )
        except Exception as e:
            if isinstance(e, ConnectorError):
                raise ConnectorException(error=e)
            if getattr(e, "__context__") == "Failed to authenticate user.":
                raise ConnectorException(
                    details_code="authentication_failed",
                    parameter="credentials",
                )
            raise ConnectorException(
                details_code="hostname_unreachable",
                parameter="hostname",
                parameter_value=shared_params_dict.get("url"),
            )

        # Serialize the data and return the response
        serializer = ExternalDataSerializer(external_data_objects, many=True)
        return self.get_paginated_response(self.paginate_queryset(serializer.data))
