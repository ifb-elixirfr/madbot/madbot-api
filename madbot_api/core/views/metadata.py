from django.urls import reverse
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import (
    ForbiddenException,
    ObjectNotFoundException,
    ValidationException,
)
from madbot_api.core.models import (
    Data,
    DataAssociation,
    DataLink,
    MadbotMetadataField,
    MadbotUser,
    Metadata,
    Node,
    NodeMember,
    Sample,
    WorkspaceMember,
)
from madbot_api.core.views.metadata_fields import MetadataMappingSerializer
from madbot_api.core.views.utils import camelcase_to_snakecase
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetNodeMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.notary import (
    data_association_notary,
    data_notary,
    node_notary,
    sample_notary,
)
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class MetadataFieldSerializer(MadbotSerializer):
    """Serializer for MetadataField model."""

    schema = serializers.SerializerMethodField()
    related_types = serializers.SerializerMethodField()
    metadata_mapping = MetadataMappingSerializer(many=True, read_only=True)

    class Meta:
        """Meta class for MetadataFieldSerializer."""

        model = MadbotMetadataField
        fields = [
            "id",
            "slug",
            "name",
            "schema",
            "description",
            "metadata_mapping",
            "related_types",
        ]
        read_only_fields = [
            "id",
            "slug",
            "name",
            "schema",
            "description",
            "metadata_mapping",
            "related_types",
        ]

    def get_schema(self, obj):
        """
        Build and return the absolute URI for the specified schema.

        Args:
        ----
            obj: The object for which the schema URI is being generated.

        Returns:
        -------
            str: The absolute URI for the schema associated with the object's metadata.

        """
        return self.context["request"].build_absolute_uri(
            reverse("schemas", args=[obj.schema.source, obj.schema.slug])
            + "?ref=false",
        )

    def get_related_types(self, obj):
        """
        Retrieve a list of related types for the specified object.

        Args:
        ----
            obj: The object for which related types are being retrieved.

        Returns:
        -------
            list: A list of related types, filtered based on the object's attributes.

        """
        types = ["data", "sample", "node", "data_association"]
        return [t for t in types if getattr(obj, f"{t}_related")]


class MetadataValuesSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Metadata model."""

    source = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()
    selected = serializers.SerializerMethodField()

    class Meta:
        """Meta class for MetadataValuesSerializer."""

        model = Metadata
        fields = [
            "id",
            "source",
            "value",
            "selected",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "source",
            "value",
            "selected",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]

    def get_selected(self, obj):
        """
        Check if the metadata instance is selected by any other metadata instance.

        Args:
        ----
            obj: An instance of the Metadata model.

        Returns:
        -------
            bool: `True` if the metadata instance is selected by any other metadata instance; otherwise, `False`.

        """
        associated_object = self.context.get("associated_object")

        if isinstance(associated_object, Node):
            filter_kwargs = {"node": associated_object}
        elif isinstance(associated_object, Sample):
            filter_kwargs = {"sample": associated_object}
        elif isinstance(associated_object, Data):
            filter_kwargs = {"data": associated_object}
        elif isinstance(associated_object, DataAssociation):
            filter_kwargs = {"data_association": associated_object}

        return (
            obj.node == associated_object
            or obj.sample == associated_object
            or obj.data == associated_object
            or obj.data_association == associated_object
            or self.context.get("count") == 1
            or obj.metadata_selected_value.filter(**filter_kwargs).exists()
        )

    def get_source(self, obj):
        """
        Retrieve details about the associated object for the given metadata instance.

        Args:
        ----
            obj: An instance of the Metadata model.

        Returns:
        -------
            dict: A dictionary containing the associated object's class name, ID, and name.

        """
        associated_object = obj.get_associated_object()
        source = {
            "object": associated_object.__class__.__name__,
            "id": associated_object.id,
        }
        if hasattr(associated_object, "title") or hasattr(associated_object, "name"):
            source["name"] = (
                associated_object.title
                if hasattr(associated_object, "title")
                else associated_object.name
            )
        return source

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)


class MetadataSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Metadata model."""

    field = MetadataFieldSerializer()
    history = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for MetadataSerializer."""

        model = Metadata
        fields = [
            "id",
            "field",
            "value",
            "node",
            "sample",
            "data",
            "data_association",
            "history",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "history",
            "field",
            "node",
            "sample",
            "data",
            "data_association",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def __init__(self, *args, **kwargs):
        """
        Initialize the serializer, conditionally removing the 'history' field.

        Args:
        ----
            args: Positional arguments to be passed to the superclass's initializer.
            kwargs: Keyword arguments to be passed to the superclass's initializer.
                    It should contain 'context' with 'request' and 'action'.

        """
        request = kwargs.get("context", {}).get("request")
        action = kwargs.get("context", {}).get("action")
        # Check if request and query_params are available
        if request and hasattr(request, "query_params"):
            # return the history object only if the query parameter `history` is set to `true` and action is `retrieve`
            if (
                request.query_params.get("history", None) != "true"
                or action != "retrieve"
            ):
                self.fields.pop("history")

        super().__init__(*args, **kwargs)

    def to_representation(self, obj):
        """
        Convert the object instance to a representation, omitting certain fields.

        Args:
        ----
            obj: The object instance to be represented.

        Returns:
        -------
            dict: A dictionary representation of the object instance, excluding certain fields.

        """
        representation = super().to_representation(obj)

        representation.pop("node")
        representation.pop("sample")
        representation.pop("data")
        representation.pop("data_association")

        return representation

    def get_history(self, obj):
        """
        Retrieve the history of the metadata object.

        Args:
        ----
            obj: The object that has a `history` attribute.

        Returns:
        -------
            list: A list of dictionaries containing the history of the metadata object,
                where each dictionary includes the history ID, date, value, and editor information.

        """
        return [
            {
                "id": history.history_id,
                "date": history.history_date,
                "value": history.value,
                "edited_by": self.get_interactor_information(
                    MadbotUser.objects.get(id=history.history_user_id),
                ),
            }
            for history in obj.history.all()
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)


class MetadataSerializerCreate(MadbotSerializer):
    """Serializer for Metadata model during create."""

    class Meta:
        """Meta class for MetadataSerializerCreate."""

        model = Metadata
        fields = ["field", "value", "selected_value"]


class MetadataSerializerUpdate(MadbotSerializer):
    """Serializer for Metadata model during update."""

    class Meta:
        """Meta class for MetadataSerializerUpdate."""

        model = Metadata
        fields = ["value", "selected_value"]


########################################################################################
# ViewSets class
########################################################################################


class MetadataViewSet(
    MadbotViewSet,
    WorkspaceHeaderMixin,
    GetNodeMixin,
    GetObjectMixin,
    BelongsToWorkspaceMixin,
):
    """ViewSet for handling metadata."""

    queryset = (
        Metadata.objects.all()
        .select_related("created_by", "last_edited_by")
        .order_by("field__slug")
    )

    def get_serializer_class(self):
        """
        Return the serializer class based on the current action.

        Returns
        -------
            The appropriate serializer class based on the action:
            - MetadataSerializerUpdate for "update"
            - MetadataSerializerCreate for "create"
            - MetadataSerializer for other actions

        """
        if self.action == "update":
            return MetadataSerializerUpdate
        if self.action == "create":
            return MetadataSerializerCreate
        return MetadataSerializer

    def get_associated_object(self, workspace, kwargs):
        """
        Retrieve the associated object based on the provided parameters.

        Args:
        ----
            workspace: The workspace context.
            kwargs: The keyword arguments containing identifiers for nodes, samples, or data.

        Returns:
        -------
            The associated object (Node, Sample, or Data) based on the parameters provided.

        """
        # if the associated object is a node
        if kwargs.get("node_pk") and not kwargs.get("sample_pk"):
            node = self.get_node(workspace)
            # check that the node belongs to the workspace
            self.check_object_belongs_to_workspace(workspace, node)
            return node

        # if the associated object is a sample
        if kwargs.get("node_pk") and kwargs.get("sample_pk"):
            sample = self.get_the_object(Sample, kwargs.get("sample_pk"), "uri")
            # check that the node belongs to the workspace
            self.check_object_belongs_to_workspace(workspace, sample.node)
            # check that the sample belongs to the node
            self.check_object_belongs_to_node(self.get_node(workspace), sample)
            return sample

        # if the associated object is a data
        if kwargs.get("data_pk") and not kwargs.get("association_pk"):
            data = self.get_the_object(Data, kwargs.get("data_pk"), "uri")
            # check that the data belongs to the workspace
            self.check_object_belongs_to_workspace(workspace, data)
            return data

        # if the associated object is a data association
        if kwargs.get("data_pk") and kwargs.get("association_pk"):
            association = self.get_the_object(
                DataAssociation, kwargs.get("association_pk"), "uri"
            )
            data = self.get_the_object(Data, kwargs.get("data_pk"), "uri")
            # check that the data belongs to the workspace
            self.check_object_belongs_to_workspace(workspace, data)
            # check that the association belongs to the data
            if data not in association.data_objects.all():
                raise ObjectNotFoundException(
                    parameter="association",
                    parameter_value=getattr(association, "id", None),
                )
            return association
        return None

    def check_datalink_access(self, datalink, workspace):
        """
        Check if the user has access to the specified data link.

        Args:
        ----
            datalink: The DataLink object to check access against.
            workspace: The workspace context.

        Raises:
        ------
            ForbiddenException: If the user does not have access to the data link.

        """
        if (
            not WorkspaceMember.objects.filter(
                workspace=workspace,
                user=self.request.user,
                role="owner",
            ).exists()
            and not NodeMember.objects.filter(
                node__id=datalink.node.id,
                user=self.request.user,
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": "You do not have access to this data.",
                },
            )

    def handle_data_object(self, request, workspace):
        """
        Handle the retrieval of the data object based on the provided datalink.

        Args:
        ----
            request: The request object containing query parameters.
            workspace: The workspace context.

        Returns:
        -------
            The DataLink object if found and accessible.

        Raises:
        ------
            ValidationException: If the datalink is missing or invalid.

        """
        datalink_id = request.query_params.get("datalink")
        if datalink_id is None:
            raise ValidationException(
                code="query_validation_error",
                details_code="required",
                parameter="datalink",
            )
        try:
            datalink = DataLink.objects.get(id=datalink_id)
            self.check_datalink_access(datalink, workspace)
            return datalink
        except DataLink.DoesNotExist:
            raise ValidationException(
                code="uri_validation_error",
                details_code="does_not_exist",
                parameter_value=datalink_id,
                parameter="datalink",
            )
        except Exception:
            raise ValidationException(
                code="uri_validation_error",
                details_code="invalid_uuid",
                parameter_value=datalink_id,
                parameter="datalink",
            )

    @extend_schema(
        responses={
            201: MetadataSerializer,
            200: MetadataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter_name": "associated_object",
                    "code": "required",
                    "message": "This field is required",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "field",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="datalink",
                location=OpenApiParameter.QUERY,
                description="Datalink ID specific to the provided data",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def create(self, request, *args, **kwargs):
        """Create a new Metadata object."""
        workspace = self.get_workspace()

        associated_object = self.get_associated_object(workspace, kwargs)
        if not associated_object:
            raise ValidationException(
                code="uri_validation_error",
                details_code="required",
                parameter="associated_object",
            )

        # check that the datalink is provided
        if isinstance(associated_object, Data):
            datalink = self.handle_data_object(request, workspace)

        # check that the `value` or `selected_value` fields are provided
        if not any(
            [
                "value" in request.data,
                "selected_value" in request.data,
            ],
        ):
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="value or selected_value",
            )

        # if both are provided, raise a validation error
        if "value" in request.data and "selected_value" in request.data:
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid",
                parameter="value or selected_value",
                parameter_value="both are provided",
            )

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()

        # if we are creating a metadata by selecting an existing value
        if "selected_value" in request.data:
            selected_value = Metadata.objects.get(id=request.data["selected_value"])

            # check that this selected value is associated with the metadata field provided in the request
            if str(selected_value.field.id) != request.data["field"]:
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="field",
                    parameter_value=request.data["field"],
                )

            # check that the current object can inherit metadata from the object the metadata is from
            if (
                not (
                    # if the selected value is associated with a node, check that the current object is a descendant of the node
                    (
                        selected_value.node is not None
                        and (
                            (
                                # if the associated object is a Sample, check that its parent node is a descendant of or the actual selected value node
                                isinstance(associated_object, Sample)
                                and associated_object.node
                                in selected_value.node.descendants(include_self=True)
                            )
                            or (
                                isinstance(associated_object, Data)
                                and datalink.node
                                in selected_value.node.descendants(include_self=True)
                            )
                            or (
                                # if the associated object is a Node object, check that it is a descendant of or the actual selected value node
                                isinstance(associated_object, Node)
                                and associated_object
                                in selected_value.node.descendants(include_self=True)
                            )
                        )
                    )
                    # if the selected value is associated with a sample, check that the current object is a data that is bound to the sample
                    or (
                        selected_value.sample is not None
                        and isinstance(associated_object, Data)
                        and associated_object.bound_samples.filter(
                            sample_id=selected_value.sample.id,
                        ).exists()
                    )
                )
                # as arborescent data is not available yet, one cannot inherit metadata from a data object
                or selected_value.data is not None
                # data association don't inherit metadata
                or isinstance(associated_object, DataAssociation)
            ):
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="selected_value",
                    parameter_value=f"cannot inherit metadata {request.data['selected_value']}",
                )

        metadata_field = MadbotMetadataField.objects.get(id=request.data["field"])

        # if a metadata object with the same field and associated object already exists, return it instead
        if Metadata.objects.filter(
            field=metadata_field,
            **{
                f"{camelcase_to_snakecase(associated_object.__class__.__name__)}": associated_object
            },
        ).exists():
            metadata = Metadata.objects.get(
                field=metadata_field,
                **{
                    f"{camelcase_to_snakecase(associated_object.__class__.__name__)}": associated_object,
                },
            )
            notary_functions = {
                Node: node_notary,
                Sample: sample_notary,
                Data: data_notary,
                DataAssociation: data_association_notary,
            }.get(type(associated_object), lambda x: None)

            if isinstance(associated_object, Data):
                value = notary_functions(associated_object, datalink, [metadata.field])
            else:
                value = notary_functions(associated_object, [metadata.field])
            representation = {
                "id": metadata.id,
                "field": MetadataFieldSerializer(
                    metadata.field,
                    context={"request": request},
                ).data,
                "values": MetadataValuesSerializer(
                    value[0]["values"],
                    many=True,
                    context={
                        "request": request,
                        "associated_object": associated_object,
                        "count": len(value[0]["values"]),
                    },
                ).data,
            }

            return Response(
                representation,
                status=status.HTTP_200_OK,
            )

        metadata = serializer.save(
            created_by=request.user,
            last_edited_by=request.user,
            node=associated_object if isinstance(associated_object, Node) else None,
            sample=associated_object if isinstance(associated_object, Sample) else None,
            data=associated_object if isinstance(associated_object, Data) else None,
            data_association=associated_object
            if isinstance(associated_object, DataAssociation)
            else None,
        )
        notary_functions = {
            Node: node_notary,
            Sample: sample_notary,
            Data: data_notary,
            DataAssociation: data_association_notary,
        }.get(type(associated_object), lambda x: None)

        # if the associated object is a data object, check that the datalink is provided
        if isinstance(associated_object, Data):
            datalink = self.handle_data_object(request, workspace)

            value = notary_functions(associated_object, datalink, [metadata.field])
        else:
            value = notary_functions(associated_object, [metadata.field])

        representation = {
            "id": metadata.id,
            "field": MetadataFieldSerializer(
                metadata.field,
                context={"request": request},
            ).data,
            "values": MetadataValuesSerializer(
                value[0]["values"],
                many=True,
                context={
                    "request": request,
                    "associated_object": associated_object,
                    "count": len(value[0]["values"]),
                },
            ).data,
        }

        return Response(representation, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: MetadataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter_name": "associated_object",
                    "code": "required",
                    "message": "This field is required",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="include_inherited",
                location=OpenApiParameter.QUERY,
                description="Should the inherited metadata be included in the response",
                required=False,
                type=OpenApiTypes.BOOL,
                default=True,
            ),
            OpenApiParameter(
                name="datalink",
                location=OpenApiParameter.QUERY,
                description="Datalink ID specific to the provided data",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        """List the information related to all the Metadata objects."""
        workspace = self.get_workspace()
        associated_object = self.get_associated_object(workspace, kwargs)
        if not associated_object:
            raise ValidationException(
                code="uri_validation_error",
                details_code="required",
                parameter="associated_object",
            )

        if isinstance(associated_object, Data):
            datalink = self.handle_data_object(request, workspace)

        if request.query_params.get("include_inherited", None) == "true":
            # Determine the value based on the type of the associated object
            notary_functions = {
                Node: node_notary,
                Sample: sample_notary,
                Data: data_notary,
                DataAssociation: data_association_notary,
            }.get(type(associated_object), lambda x: None)

            # if the associated object is a data object, check that the datalink is provided
            if isinstance(associated_object, Data):
                value = notary_functions(associated_object, datalink)
            else:
                value = notary_functions(associated_object)
            if value:
                data = [
                    {
                        "field": MetadataFieldSerializer(
                            item["field"],
                            context={
                                "request": request,
                                "associated_object": associated_object,
                            },
                        ).data,
                        "values": MetadataValuesSerializer(
                            item["values"],
                            many=True,
                            context={
                                "request": request,
                                "associated_object": associated_object,
                                "count": len(item["values"]),
                            },
                        ).data,
                    }
                    for item in value
                ]

                return self.get_paginated_response(self.paginate_queryset(data))
            return self.get_paginated_response(self.paginate_queryset([]))
        # filter the queryset based on the associated object
        queryset = self.get_queryset().filter(
            node=associated_object if isinstance(associated_object, Node) else None,
            sample=(
                associated_object if isinstance(associated_object, Sample) else None
            ),
            data=associated_object if isinstance(associated_object, Data) else None,
            data_association=associated_object
            if isinstance(associated_object, DataAssociation)
            else None,
        )

        serializer = self.get_serializer(
            queryset,
            many=True,
            context={
                "request": request,
                "associated_object": associated_object,
            },
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: MetadataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter_name": "associated_object",
                    "code": "required",
                    "message": "This field is required",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="history",
                location=OpenApiParameter.QUERY,
                description="Should the history of the metadata be included in the response",
                required=False,
                type=OpenApiTypes.BOOL,
                default=False,
            ),
            OpenApiParameter(
                name="include_inherited",
                location=OpenApiParameter.QUERY,
                description="Should the inherited metadata be included in the response",
                required=False,
                type=OpenApiTypes.BOOL,
                default=True,
            ),
            OpenApiParameter(
                name="datalink",
                location=OpenApiParameter.QUERY,
                description="Datalink ID specific to the provided data",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def retrieve(self, request, *args, **kwargs):
        """Retrieve a Metadata objects."""
        workspace = self.get_workspace()
        associated_object = self.get_associated_object(workspace, kwargs)
        if not associated_object:
            raise ValidationException(
                code="uri_validation_error",
                details_code="required",
                parameter="associated_object",
            )

        # check the associated object belongs to the workspace
        metadata = self.get_object()

        if isinstance(associated_object, Data):
            datalink = self.handle_data_object(request, workspace)

        if request.query_params.get("include_inherited", None) == "true":
            # Determine the value based on the type of the associated object
            notary_functions = {
                Node: node_notary,
                Sample: sample_notary,
                Data: data_notary,
            }.get(type(associated_object), lambda x: None)

            # if the associated object is a data object, check that the datalink is provided
            if isinstance(associated_object, Data):
                value = notary_functions(associated_object, datalink, [metadata.field])
            else:
                value = notary_functions(associated_object, [metadata.field])

            representation = {
                "id": metadata.id,
                "field": MetadataFieldSerializer(
                    metadata.field,
                    context={"request": request},
                ).data,
                "values": MetadataSerializer(
                    value[0]["values"],
                    many=True,
                    context={
                        "request": request,
                        "associated_object": associated_object,
                        "count": len(value[0]["values"]),
                    },
                ).data,
            }

            return Response(representation, status=status.HTTP_200_OK)
        metadata = self.get_object()
        representation = {
            "id": metadata.id,
            "field": MetadataFieldSerializer(
                metadata.field,
                context={"request": request},
            ).data,
            "values": self.get_serializer(
                metadata,
                context={
                    "request": request,
                    "associated_object": associated_object,
                },
            ).data,
        }

        return Response(representation, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            201: MetadataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter_name": "associated_object",
                    "code": "required",
                    "message": "This field is required",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "field",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="datalink",
                location=OpenApiParameter.QUERY,
                description="Datalink ID specific to the provided data",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def update(self, request, *args, **kwargs):
        """Update a Metadata object."""
        workspace = self.get_workspace()
        # retrieve the pk of the associated object, can be either `node_pk`, `sample_pk`, or `data_pk`
        associated_object = self.get_associated_object(workspace, kwargs)
        if not associated_object:
            raise ValidationException(
                code="uri_validation_error",
                details_code="required",
                parameter="associated_object",
            )

        # check the associated object belongs to the workspace

        metadata = self.get_object()

        # check that both `value` and `selected_value` are not provided
        if "value" in request.data and "selected_value" in request.data:
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid",
                parameter="value or selected_value",
                parameter_value="both are provided",
            )

        # if we are modifying the `selected_value`
        if "selected_value" in request.data:
            selected_value = Metadata.objects.get(id=request.data["selected_value"])

            # check that this selected value is associated with the metadata field provided in the request
            if str(selected_value.field.id) != request.data["field"]:
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="field",
                    parameter_value=request.data["field"],
                )

            # check that the current object can inherit metadata from the object the metadata is from
            if (
                not (
                    # if the selected value is associated with a node, check that the current object is a descendant of the node
                    (
                        selected_value.node is not None
                        and (
                            (
                                # if the associated object is a Sample or Data object, check that its parent node is a descendant of or the actual selected value node
                                (
                                    isinstance(associated_object, Sample)
                                    or isinstance(associated_object, Data)
                                )
                                and associated_object.node
                                in selected_value.node.descendants(include_self=True)
                            )
                            # if the associated object is a Node object, check that it is a descendant of or the actual selected value node
                            or (
                                isinstance(associated_object, Node)
                                and associated_object
                                in selected_value.node.descendants(include_self=True)
                            )
                        )
                    )
                    # if the selected value is associated with a sample, check that the current object is a data that is bound to the sample
                    or (
                        selected_value.sample is not None
                        and isinstance(associated_object, Data)
                        and associated_object.bound_samples.filter(
                            sample_id=selected_value.sample.id,
                        ).exists()
                    )
                )
                # as arborescent data is not available yet, one cannot inherit metadata from a data object
                or selected_value.data is not None
            ):
                raise ValidationException(
                    code="body_validation_error",
                    details_code="invalid",
                    parameter="selected_value",
                    parameter_value=f"cannot inherit metadata {request.data['selected_value']}",
                )

        serializer = self.get_serializer(metadata, data=request.data)
        serializer.is_valid()
        serializer.save(last_edited_by=request.user)

        notary_functions = {
            Node: node_notary,
            Sample: sample_notary,
            Data: data_notary,
        }.get(type(associated_object), lambda x: None)

        # if the associated object is a data object, check that the datalink is provided
        if isinstance(associated_object, Data):
            datalink = self.handle_data_object(request, workspace)
            value = notary_functions(associated_object, datalink, [metadata.field])
        else:
            value = notary_functions(associated_object, [metadata.field])

        representation = {
            "id": metadata.id,
            "field": MetadataFieldSerializer(
                metadata.field,
                context={"request": request},
            ).data,
            "values": MetadataSerializer(
                value[0]["values"],
                many=True,
                context={
                    "request": request,
                    "associated_object": associated_object,
                    "count": len(value[0]["values"]),
                },
            ).data,
        }

        return Response(representation, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            201: MetadataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter_name": "associated_object",
                    "code": "required",
                    "message": "This field is required",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<associated_object>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="datalink",
                location=OpenApiParameter.QUERY,
                description="Datalink ID specific to the provided data",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def destroy(self, request, *args, **kwargs):
        """Delete a Metadata object."""
        workspace = self.get_workspace()
        associated_object = self.get_associated_object(workspace, kwargs)
        if not associated_object:
            raise ValidationException(
                code="uri_validation_error",
                details_code="required",
                parameter="associated_object",
            )

        # check the associated object belongs to the workspace
        if isinstance(associated_object, Sample):
            self.check_object_belongs_to_workspace(workspace, associated_object.node)
        else:
            self.check_object_belongs_to_workspace(workspace, associated_object)

        if isinstance(associated_object, Data):
            self.handle_data_object(request, workspace)

        metadata = self.get_object()
        metadata.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
