from itertools import chain

import django_filters
from django.urls import reverse
from drf_spectacular.utils import extend_schema
from rest_framework import serializers

from madbot_api.core.models import (
    ConnectorMetadataField,
    MadbotMetadataField,
    MetadataMapping,
)
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import (
    DataTypesFilter,
    HiddenFilter,
    MetadataSearchFilter,
    RelatedTypesFilter,
    SourceFilter,
)
from madbot_api.core.views.utils.schema_response import cesr_400_query_validation_error
from madbot_api.core.views.utils.serializer import MadbotSerializer

########################################################################################
# Serializer methods
########################################################################################


class MetadataMappingSerializer(MadbotSerializer):
    """Serializer for MetadataMapping model."""

    class Meta:
        """Meta class for MetadataMappingSerializer."""

        model = MetadataMapping
        fields = [
            "connector_metadata_field",
            "madbot_metadata_field",
            "connector",
        ]
        read_only_fields = [
            "connector_metadata_field",
            "madbot_metadata_field",
            "connector",
        ]


class MetadataFieldSerializer(MadbotSerializer):
    """Serializer for MetadataField model."""

    related_types = serializers.SerializerMethodField()
    schema = serializers.SerializerMethodField()
    metadata_mapping = MetadataMappingSerializer(many=True, read_only=True)
    rank = serializers.IntegerField(
        default=1,
        initial=1,
    )  # rank is a property used by the "search" filter to rank results

    class Meta:
        """Meta class for MetadataFieldSerializer."""

        model = MadbotMetadataField
        fields = [
            "id",
            "slug",
            "name",
            "description",
            "related_types",
            "data_types",
            "schema",
            "metadata_mapping",
            "rank",
            "source",
        ]
        read_only_fields = [
            "id",
            "slug",
            "name",
            "description",
            "related_types",
            "data_types",
            "schema",
            "metadata_mapping",
            "rank",
            "source",
        ]

    def get_related_types(self, obj):
        """
        Retrieve related types for the given object.

        Args:
        ----
            obj: The object for which to determine related types.

        Returns:
        -------
            A list of related types (e.g., "data", "sample", "node") based on
            the attributes of the provided object.

        """
        types = ["data", "sample", "node", "data_association"]
        return [t for t in types if getattr(obj, f"{t}_related")]

    def get_schema(self, obj):
        """
        Build and return the absolute URI for the specified schema.

        Args:
        ----
            obj: The object for which the schema URI is being generated.

        Returns:
        -------
            str: The absolute URI for the schema associated with the object's metadata.

        """
        if isinstance(obj, MadbotMetadataField):
            return self.context["request"].build_absolute_uri(
                reverse("schemas", args=[obj.schema.source, obj.schema.slug])
                + "?ref=false",
            )
        return self.context["request"].build_absolute_uri(
            reverse(
                "schemas_connectors",
                args=[obj.schema.source, obj.schema.slug],
            )
            + "?ref=false",
        )


########################################################################################
# Filter methods
########################################################################################


# The MetadataFieldFilter class is a Django FilterSet that filters metadata fields
class MetadataFieldFilter(django_filters.FilterSet):
    """Filter methods for metadata fields."""

    data_type = DataTypesFilter(field_name="data_types")
    related_types = RelatedTypesFilter(field_name="related_types")
    search = MetadataSearchFilter(field_name="search")
    source = SourceFilter(field_name="source")
    hidden = HiddenFilter(field_name="hidden")


########################################################################################
# ViewSets class
########################################################################################


class MetadataFieldViewSet(MadbotViewSet):
    """ViewSet for handling metadata field."""

    serializer_class = MetadataFieldSerializer
    filterset_class = MetadataFieldFilter
    permission_classes = []

    def get_queryset(self):
        """
        Retrieve all metadata fields.

        This method returns all MadbotMetadataField and ConnectorMetadataField objects,
        combining them into a single queryset and ordering by name.
        """
        madbot_fields_qs = self.filter_queryset(MadbotMetadataField.objects.all())
        connector_fields_qs = self.filter_queryset(ConnectorMetadataField.objects.all())

        return sorted(
            chain(madbot_fields_qs, connector_fields_qs),
            key=lambda obj: (getattr(obj, "rank", float("inf")), obj.name.lower()),
        )

    @extend_schema(
        responses={
            200: MetadataFieldSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter_name": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported",
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "related_types",
                    "code": "invalid",
                    "message": "Invalid value <parameter_name>.",
                },
            ),
        },
    )
    def list(self, request, *args, **kwargs):
        """List the information related to all the MetadataField objects."""
        serializer = self.get_serializer(
            self.get_queryset(),
            many=True,
        )

        # if the "search" query parameter is not present in the request, remove the rank field
        if "search" not in request.query_params:
            for data in serializer.data:
                data.pop("rank")

        return self.get_paginated_response(self.paginate_queryset(serializer.data))
