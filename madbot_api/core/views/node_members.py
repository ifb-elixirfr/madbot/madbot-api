from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import filters, serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import NodeMember, WorkspaceMember
from madbot_api.core.serializers.user import UserSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import MemberFilter
from madbot_api.core.views.utils.member import count_owners
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetNodeMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.notification import trigger_notification
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_query_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class NodeMemberSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for NodeMember model."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for NodeMemberSerializer."""

        model = NodeMember
        fields = [
            "id",
            "user",
            "node",
            "role",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)

    def to_representation(self, instance):
        """
        Return a representation of the instance, including user information.

        Args:
        ----
            instance: The instance to represent.

        Returns:
        -------
            dict: A dictionary representation of the instance, including user data.

        """
        representation = super().to_representation(instance)
        user = UserSerializer(instance.user).data
        representation["user"] = user
        return representation


class NodeMemberSerializerUpdate(NodeMemberSerializer):
    """Serializer for NodeMember model for updating."""

    ROLE_MEMBER = [
        ("owner", "owner"),
        ("manager", "manager"),
        ("contributor", "contributor"),
        ("collaborator", "collaborator"),
    ]
    role = serializers.ChoiceField(required=False, choices=ROLE_MEMBER)

    class Meta:
        """Meta class for NodeMemberSerializerUpdate."""

        model = NodeMember
        fields = [
            "id",
            "user",
            "node",
            "role",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "user",
            "node",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]


########################################################################################
# ViewSets class
########################################################################################


class NodeMemberViewSet(
    MadbotViewSet,
    GetNodeMixin,
    BelongsToWorkspaceMixin,
    WorkspaceHeaderMixin,
):
    """ViewSet for handling node members."""

    serializer_class = NodeMemberSerializer
    queryset = NodeMember.objects.all().order_by("user__username")

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_class = MemberFilter

    pagination_class = StandardPagination

    @extend_schema(
        responses={
            201: NodeMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "user",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "role",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You must be owner or manager of the node to add a new member.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You can only add new members with a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "User must be member of the parent node to be added to a node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "User is already a member of this node.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, node_pk=None, *args, **kwargs):
        """Create a new NodeMember object."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # if the user is a workspace owner and they are trying to add themselves to a node
        # let them do and pass other validation
        if not (
            WorkspaceMember.objects.filter(
                workspace=workspace,
                user=request.user,
                role="owner",
            ).exists()
            and str(request.user.id) == request.data.get("user", None)
        ):
            try:
                current_member = NodeMember.objects.get(
                    node__pk=node_pk,
                    user=request.user.id,
                )
            # check if the user is a member of the node
            except ObjectDoesNotExist:
                raise ForbiddenException(
                    details={
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You are not a member of this node.",
                    },
                )
            # check if the user is owner or manager of the node
            if current_member.role not in ["owner", "manager"]:
                raise ForbiddenException(
                    details={
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You must be owner or manager of the node to add a new member.",
                    },
                )

            # check if the user is trying to add a member with a role above his
            if (
                request.data.get("role", None)
                and request.data.get("role", None) == "owner"
                and current_member.role == "manager"
            ):
                raise ForbiddenException(
                    details={
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only add new members with a"
                        " role below or equal to yours.",
                    },
                )

        # Check the body data
        serializer = NodeMemberSerializer(data={**request.data, "node": node.id})
        serializer.is_valid()

        # check if the member is a member of the workspace
        if not WorkspaceMember.objects.filter(
            workspace=workspace,
            user=request.data.get("user", None),
        ).exists() and request.data.get("user", None):
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "User must be member of the workspace to be added to a node.",
                },
            )

        # check if the member is a member of the parent node
        if (
            not NodeMember.objects.filter(
                node=node.parent,
                user=request.data.get("user", None),
            ).exists()
            and node.parent
            and request.data.get("user", None)
        ):
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "User must be member of the parent node to be added to a node.",
                },
            )

        # Create the NodeMember with the associated node
        member = serializer.save(created_by=request.user, last_edited_by=request.user)

        # trigger the notification and send notification
        node_members = node.node_members.all().exclude(user=request.user)
        for node_member in node_members:
            trigger_notification(
                sender=request.user,
                recipient=node_member.user,
                verb="created",
                action_object=member,
                target=node,
                actor=request.user,
                workspace=workspace.id,
            )

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: NodeMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "node",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "node",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "user",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "user",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, node_pk=None):
        """List the information related to all the NodeMembers objects."""
        workspace = self.get_workspace()
        self.get_node(workspace)

        serializer = NodeMemberSerializer(
            self.filter_queryset(self.get_queryset().filter(node__pk=node_pk)),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: NodeMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(
        self,
        request,
        pk,
        node_pk=None,
    ):
        """Retrieve a given member object from a node."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # get the member to retrieve
        item = self.get_object()
        self.check_object_belongs_to_node(node, item)

        serializer = NodeMemberSerializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            200: NodeMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "role",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You must be owner or manager of the node to edit other members.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "The role of the node member cannot be changed because they are owner of the parent node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "The role of the node member cannot be changed because they are the last owner of the current node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You can only change a member to a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You can only change a member with a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You cannot promote yourself.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def update(
        self,
        request,
        pk,
        node_pk=None,
    ):
        """Update a NodeMember item."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # get the member to update
        item = self.get_object()
        self.check_object_belongs_to_node(node, item)

        try:
            current_member = NodeMember.objects.get(
                node__pk=node_pk,
                user=request.user.id,
            )
        # check if the user is a member of the node
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        if not (
            WorkspaceMember.objects.filter(
                workspace=workspace,
                user=request.user.id,
                role="owner",
            ).exists()
            and request.user.id == item.user.id
        ):
            # check if the user has the right to modify members
            if current_member.role not in [
                "owner",
                "manager",
            ]:  # TODO: maybe make this a decorator
                raise ForbiddenException(
                    details={
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "You must be owner or manager of the node to edit other members.",
                    },
                )

            # check is the user is trying to promote themselves
            if (
                item.user.id == request.user.id
                and request.data.get("role", None) == "owner"
                and current_member.role == "manager"
            ):
                raise ForbiddenException(
                    details={
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You cannot promote yourself.",
                    },
                )

            # check if the user is trying to demote a member that has a role above his
            if item.role == "owner" and current_member.role == "manager":
                raise ForbiddenException(
                    details={
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only change a member with a"
                        " role below or equal to yours.",
                    },
                )
            # check if the user is trying to promote a member to a role above his
            if (
                request.data.get("role", None) == "owner"
                and current_member.role == "manager"
            ):
                raise ForbiddenException(
                    details={
                        "parameter": "node_member",
                        "code": "not_allowed",
                        "message": "You can only promote a member to a"
                        " role below or equal to yours.",
                    },
                )

        # check is the member is owner of the parent node
        if NodeMember.objects.filter(
            node=node.parent,
            user=item.user,
            role="owner",
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "The role of the node member cannot be changed"
                    " because they are owner of the parent node.",
                },
            )
        # check is the user is trying to update the last owner
        if item.role == "owner" and count_owners(node) == 1:
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "The role of the node member cannot be changed"
                    " because they are the last owner of the current node.",
                },
            )

        serializer = NodeMemberSerializerUpdate(item, data=request.data)
        serializer.is_valid()
        member = serializer.save(last_edited_by=request.user)

        # trigger the notification
        trigger_notification(
            sender=request.user,
            recipient=item.user,
            verb="changed",
            action_object=member,
            target=node,
            actor=request.user,
            workspace=workspace.id,
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You must be owner or manager of the node to remove another member.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You can only remove a member with a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because they are owner of the parent node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because they are the last owner of the current node.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(
        self,
        request,
        pk,
        node_pk=None,
    ):
        """Delete a NodeMember item."""
        # check if the node_pk is a valid UUID

        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # get the member to delete
        item = self.get_object()
        self.check_object_belongs_to_node(node, item)

        try:
            current_member = NodeMember.objects.get(node__pk=node_pk, user=request.user)
        # check if the user is a member of the node
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        # check if the user has the right to modify members
        if (
            current_member.role
            not in [
                "owner",
                "manager",
            ]
            and str(current_member.id) != pk
        ):  # TODO: maybe make this a decorator
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You must be owner or manager of the node to remove another member.",
                },
            )
        # check if the user is trying to remove a member that has a role above his
        if item.role == "owner" and current_member.role == "manager":
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "You can only remove a member with a"
                    " role below or equal to yours.",
                },
            )
        # check is the member is owner of the parent node

        if NodeMember.objects.filter(
            node=node.parent,
            user=item.user,
            role="owner",
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because"
                    " they are owner of the parent node.",
                },
            )
        # check is the user is trying to remove the last owner
        if item.role == "owner" and count_owners(node) == 1:
            raise ForbiddenException(
                details={
                    "parameter": "node_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because"
                    " they are the last owner of the current node.",
                },
            )
        item.delete()

        # trigger the notification
        trigger_notification(
            sender=request.user,
            recipient=item.user,
            verb="suppressed",
            action_object=item,
            target=node,
            actor=request.user,
            workspace=workspace.id,
        )
        return Response(status=status.HTTP_204_NO_CONTENT)
