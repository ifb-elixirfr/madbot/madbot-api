import django_filters
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import serializers
from rest_framework.response import Response

from madbot_api.core.models import NodeType
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import ParentFilter
from madbot_api.core.views.utils.schema_response import (
    cesr_400_query_validation_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import MadbotSerializer

########################################################################################
# Serializer methods
########################################################################################


class NodeTypeSerializer(MadbotSerializer):
    """Serializer for NodeType model."""

    parent = serializers.SerializerMethodField(required=False, read_only=True)

    class Meta:
        """Meta class for NodeTypeSerializer."""

        model = NodeType
        fields = ["id", "name", "description", "icon", "parent"]
        read_only_fields = ["id", "name", "description"]
        authorized_extra_fields = {
            "children": serializers.SerializerMethodField(
                required=False,
                read_only=True,
            ),
        }

    def get_parent(self, obj):
        """
        Return a list of IDs of the parent objects of a given object.

        Args:
        ----
            obj: The object for which to retrieve its parent.

        Returns:
        -------
            list or None: A list of the IDs of the parent objects if the
            given obj has any; otherwise, None.

        """
        if obj.parent:
            return list(obj.parent.values_list("id", flat=True))
        return None

    def get_children(self, obj):
        """
        Return serialized data of all the children objects of a given object.

        Args:
        ----
            obj: The instance of a model object for which to retrieve children.

        Returns:
        -------
            list: The serialized data of the children objects of the given obj.

        """
        children = obj.children.all()
        serializer = self.__class__(children, many=True)
        return serializer.data


########################################################################################
# Filter methods
########################################################################################


# The NodeTypeFilter class is a Django filter set that includes a filter for the "parent"
# field.
class NodeTypeFilter(django_filters.FilterSet):
    """Filter a queryset based on the parent field."""

    parent = ParentFilter(field_name="parent")


########################################################################################
# ViewSets class (DRF)
########################################################################################


class NodeTypeViewSet(MadbotViewSet):
    """ViewSet for handling node types."""

    serializer_class = NodeTypeSerializer
    queryset = NodeType.objects.select_related("collection").prefetch_related(
        "parent",
        "children",
    )
    filterset_class = NodeTypeFilter

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="fields",
                type={
                    "type": "array",
                    "items": {
                        "type": "string",
                        "enum": list(
                            NodeTypeSerializer.Meta.authorized_extra_fields.keys(),
                        ),
                    },
                },
                description="Fields to include in the response.",
                style="form",
            ),
        ],
        responses={
            200: NodeTypeSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "parent",
                    "code": "invalid",
                    "message": "<parameter_value> does not exist.",
                },
            ),
            # **cesr_400_query_validation_error(
            #     detail={
            #         "parameter": "<parameter_name>",
            #         "code": "unsupported",
            #         "message": "Some fields are not supported in the fields query parameter : <parameter_name>",
            #     }
            # ),
        },
    )
    def list(self, request):
        """List all nodeType objects."""
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: NodeTypeSerializer,
            **cesr_404_object_not_found(),
            **cesr_400_query_validation_error(
                detail={
                    "parameter_name": "<parameter_name>",
                    "code": "unsupported",
                    "message": "This field is not supported.",
                },
            ),
        },
    )
    def retrieve(self, request, pk):
        """Retrieve a nodeType item."""
        item = self.get_object()
        serializer = self.get_serializer(item)
        return Response(serializer.data)
