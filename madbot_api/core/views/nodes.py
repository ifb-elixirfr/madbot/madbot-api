import django_filters
from django.db.models import Q
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException, ValidationException
from madbot_api.core.models import (
    DataLink,
    Node,
    NodeMember,
    Sample,
    WorkspaceMember,
)
from madbot_api.core.views.node_types import NodeTypeSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import ParentFilter, TypeFilter
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.notification import trigger_notification
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_query_validation_error,
    cesr_400_save_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class NodeSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Node model."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()
    path = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()
    samples = serializers.SerializerMethodField()
    datalinks = serializers.SerializerMethodField()

    class Meta:
        """Meta class for NodeSerializer."""

        model = Node
        fields = [
            "id",
            "type",
            "title",
            "description",
            "status",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
            "workspace",
            "path",
            "parent",
            "children",
            "samples",
            "datalinks",
        ]
        read_only_fields = [
            "id",
            "samples",
            "datalinks",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
            "path",
            "children",
        ]
        # authorized_extra_fields = {
        #     "children": serializers.SerializerMethodField(required=False),
        # }

    def get_path(self, obj):
        """
        Return a list of IDs representing the path from a given object to its root parent.

        Args:
        ----
            obj: The object with an "id" attribute and a "parent" attribute.

        Returns:
        -------
            list: A list of strings representing the path from the given object to its root parent.

        """
        path = [str(obj.id)]
        while obj.parent:
            path.append(str(obj.parent.id))
            obj = obj.parent
        return path[::-1]

    def to_representation(self, instance):
        """
        Return a representation of an instance with modified type information.

        Args:
        ----
            instance: An instance of a model object that needs to be serialized.

        Returns:
        -------
            dict: The representation of the instance with the "type" field modified.

        """
        representation = super().to_representation(instance)
        data_type = NodeTypeSerializer(instance.type).data
        if "parent" in data_type:
            data_type.pop("parent")
        representation["type"] = data_type
        representation.pop("parent")
        return representation

    def get_children(self, obj):
        """
        Return the count of children for a given object and a related URL to retrieve them.

        Args:
        ----
            obj: The object for which to retrieve the children.

        Returns:
        -------
            dict: A dictionary with keys "count" and "related" containing the number of children
            and a URL to retrieve the related children objects.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}nodes?parent={obj.id}"
        return {
            "count": obj.children.count(),
            "related": related_url,
        }

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)

    def get_samples(self, obj):
        """
        Return information about the samples, including the count and a related URL.

        Args:
        ----
          obj: The object for which to retrieve the sample.

        Returns:
        -------
          A dictionary with keys "count" and "related" containing the number of samples
          and a URL to retrieve the related sample objects.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}nodes/{obj.id}/samples"
        return {
            "count": Sample.objects.filter(
                Q(node__id=obj.id) | Q(node__in=obj.ancestors())
            ).count(),
            "related": related_url,
        }

    def get_datalinks(self, obj):
        """
        Return information about the datalinks, including the count and a related URL.

        Args:
        ----
          obj: The object for which to retrieve the datalink.

        Returns:
        -------
          A dictionary with keys "count" and "related" containing the number of datalinks
          and a URL to retrieve the related datalink objects.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}nodes/{obj.id}/datalinks"
        return {
            "count": DataLink.objects.filter(node__id=obj.id).count(),
            "related": related_url,
        }


class NodeSerializerUpdate(NodeSerializer):
    """Serializer for updating Node objects."""

    class Meta:
        """Meta class for NodeSerializerUpdate."""

        model = Node
        fields = [
            "id",
            "type",
            "title",
            "description",
            "status",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
            "workspace",
            "path",
            "children",
            "parent",
        ]
        read_only_fields = [
            "id",
            "type",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
            "workspace",
            "path",
            "children",
            "parent",
        ]


########################################################################################
# Filter methods
########################################################################################


# The NodeFilter class is a Django FilterSet that filters nodes based on their parent and
# type.
class NodeFilter(django_filters.FilterSet):
    """Filter a queryset based on the parent and type fields."""

    parent = ParentFilter(field_name="parent")
    type = TypeFilter(field_name="type")


########################################################################################
# ViewSets class
########################################################################################


class NodeViewSet(MadbotViewSet, WorkspaceHeaderMixin, BelongsToWorkspaceMixin):
    """ViewSet for handling nodes."""

    queryset = Node.objects.all().order_by("title")
    filterset_class = NodeFilter

    def get_serializer_class(self):
        """
        Return a different serializer class based on the action being performed.

        Returns
        -------
            class: The serializer class to be used for the action. Returns `NodeSerializerUpdate`
            if the action is "update", otherwise returns `NodeSerializer`.

        """
        if self.action == "update":
            return NodeSerializerUpdate
        return NodeSerializer

    @extend_schema(
        responses={
            201: NodeSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "status",
                    "code": "invalid_choice",
                    "message": '"<status_value>" is not a valid choice.',
                },
            ),
            **cesr_400_save_validation_error(
                detail={
                    "parameter": "type",
                    "code": "invalid",
                    "message": "The node type must be a child of the parent type.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, *args, **kwargs):
        """Create a new Node object."""
        workspace = self.get_workspace()

        if "status" not in request.data.keys():
            request.data["status"] = "pending"
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()

        if "parent" in request.data:
            try:
                parent_node = Node.objects.get(
                    id=request.data["parent"],
                    workspace=workspace,
                )
            except Exception:
                raise ValidationException(
                    code="body_validation_error",
                    details_code="does_not_exist",
                    parameter="parent",
                    parameter_value=request.data["parent"],
                )
            # Check if the user is a member of the parent node
            if not NodeMember.objects.filter(
                node=parent_node,
                user=request.user,
            ).exists():
                raise ValidationException(
                    code="body_validation_error",
                    details_code="does_not_exist",
                    parameter="parent",
                    parameter_value=request.data["parent"],
                )
            # Check if the user's role allows creation
            if not NodeMember.objects.filter(
                node=parent_node,
                user=request.user,
                role__in=["owner", "manager", "contributor"],
            ).exists():
                raise ForbiddenException(
                    details={
                        "parameter": "node",
                        "code": "not_allowed",
                        "message": "The member's role does not allow creation.",
                    },
                )
        node = serializer.save(
            created_by=request.user,
            last_edited_by=request.user,
            workspace=workspace,
        )
        if "parent" in request.data:
            # Send notification
            members = node.node_members.all().exclude(user=request.user)
            for member in members:
                trigger_notification(
                    sender=request.user,
                    recipient=member.user,
                    verb="created",
                    action_object=node,
                    target=parent_node,
                    actor=request.user,
                    workspace=workspace.id,
                )

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: NodeSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "parent",
                    "code": "does_not_exist",
                    "message": '"<parent_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter_name": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        """List the information related to all the Node objects."""
        workspace = self.get_workspace()

        # if the user is owner of the workspace, return all nodes
        if WorkspaceMember.objects.filter(
            workspace=workspace,
            user=request.user,
            role="owner",
        ).exists():
            queryset = self.get_queryset().filter(workspace=workspace)
        # else, filer out the ones where the user is not member
        else:
            queryset = (
                self.get_queryset()
                .filter(workspace=workspace)
                .filter(node_members__user=request.user)
            )

        serializer = self.get_serializer(self.filter_queryset(queryset), many=True)
        # Check if the 'parent' parameter is present in the request query parameters
        if (
            "parent" in request.query_params
            and request.query_params.get("parent") != "null"
        ):
            try:
                parent_node = Node.objects.get(
                    id=request.query_params.get("parent"),
                    workspace=workspace,
                )
            except Exception:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="does_not_exist",
                    parameter="parent",
                    parameter_value=request.query_params.get("parent"),
                )

            # Check if the user is a member of the parent node
            if (
                not NodeMember.objects.filter(
                    node=parent_node,
                    user=request.user,
                ).exists()
                and not WorkspaceMember.objects.filter(
                    workspace=workspace,
                    user=request.user,
                    role="owner",
                ).exists()
            ):
                raise ValidationException(
                    code="query_validation_error",
                    details_code="does_not_exist",
                    parameter="parent",
                    parameter_value=request.query_params.get("parent"),
                )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: NodeSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "node",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, pk):
        """Retrieve an Node item's information."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check the requested node is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        # Check if the user is a member of the node or owner of the workspace
        if (
            not NodeMember.objects.filter(node=item, user=request.user).exists()
            and not WorkspaceMember.objects.filter(
                workspace=workspace,
                user=request.user,
                role="owner",
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            200: NodeSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "status",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "node",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def update(self, request, pk):
        """Update an Node item."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check the requested node is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        serializer = self.get_serializer(item, data=request.data)

        serializer.is_valid()

        # Check if the user is a member of the node
        if not NodeMember.objects.filter(node=item, user=request.user).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        # Check if the user's role allows update
        if not NodeMember.objects.filter(
            node=item,
            user=request.user,
            role="owner",
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            )
        serializer.save(last_edited_by=request.user)
        updated_item = self.get_object()
        updated_serializer = self.get_serializer(updated_item)
        return Response(updated_serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "node",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk):
        """Delete an Node item."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check the requested node is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        # Check if the user is a member of the node
        if not NodeMember.objects.filter(node=item, user=request.user).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        # Check if the user's role allows deletion
        if not NodeMember.objects.filter(
            node=item,
            user=request.user,
            role="owner",
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
