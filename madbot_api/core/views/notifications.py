import django_filters
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from notifications.models import Notification
from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication
from oauth2_provider.contrib.rest_framework.permissions import (
    TokenMatchesOASRequirements,
)
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ObjectNotFoundException, ValidationException
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import FilterArchived, FilterUnread
from madbot_api.core.views.utils.mixin import WorkspaceHeaderMixin
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_query_validation_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import MadbotSerializer


def send_notification_socket(username, obj, source):
    """
    Send a notification object to a specific user's channel group using Django Channels.

    Args:
    ----
        username: The username of the user to whom the notification will be sent.
        obj: The object or data to send as a notification, containing relevant information
        such as a message or notification details.
        source: The source from which the notification originates.

    """
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "notifications_" + str(username),
        {"type": "notification.message", "message": obj, "source": source},
    )


class NotificationsSerializer(MadbotSerializer):
    """Serializer for Notification model."""

    actor_object = serializers.SerializerMethodField(required=False)
    action_object = serializers.SerializerMethodField(required=False)
    target_object = serializers.SerializerMethodField(required=False)
    archived = serializers.BooleanField(required=False, source="deleted")
    verb = serializers.CharField(required=False)
    workspace = serializers.CharField(required=False)

    class Meta:
        """Meta class for NotificationsSerializer."""

        model = Notification
        fields = [
            "id",
            "actor_object",
            "verb",
            "action_object",
            "target_object",
            "workspace",
            "level",
            "timestamp",
            "description",
            "unread",
            "archived",
        ]
        read_only_fields = [
            "id",
            "actor_object",
            "verb",
            "action_object",
            "target_object",
            "workspace",
            "level",
            "timestamp",
            "description",
        ]

    def get_workspace_id(self, obj):
        """
        Return the workspace ID from the object's description.

        Args:
        ----
            obj: The object from which to retrieve the workspace ID.

        Returns:
        -------
            str: The workspace ID extracted from the object's description.

        """
        return obj.description

    def get_actor_object(self, obj):
        """
        Return a dictionary containing information about the actor object.

        Args:
        ----
            obj: The object that contains an actor attribute with relevant information.

        Returns:
        -------
            dict: A dictionary containing the actor's full name, email, class name, and ID.

        """
        return {
            "full_name": obj.actor.get_full_name(),
            "email": obj.actor.email,
            "obj": obj.actor.__class__.__name__,
            "id": str(obj.actor.id),
        }

    def get_action_object(self, obj):
        """
        Return a dictionary containing information about the action object.

        Args:
        ----
            obj: The object that contains information about an action object.

        Returns:
        -------
            dict: A dictionary with keys "obj" and "id", representing the content type
            of the action object and its ID, respectively. Includes "extra_info" if applicable.

        """
        if obj.action_object.__class__.__name__ == "Node":
            extra_info = obj.action_object.type.name
        elif obj.action_object.__class__.__name__ == "Submission":
            extra_info = obj.action_object.status
        elif obj.action_object.__class__.__name__ == "NodeMember":
            extra_info = obj.action_object.role
        else:
            extra_info = None

        return {
            "obj": obj.action_object_content_type.name,
            "extra_info": extra_info,
            "id": str(obj.action_object_object_id),
        }

    def get_target_object(self, obj):
        """
        Return a dictionary containing information about a target object.

        Args:
        ----
            obj: The object that has a target attribute with relevant information.

        Returns:
        -------
            dict: A dictionary containing the target object's content type name, type ID,
            and title. Includes "extra_info" if applicable.

        """
        if obj.target.__class__.__name__ == "Node":
            extra_info = obj.target.type.name
            name = obj.target.title
        elif obj.target.__class__.__name__ == "Submission":
            extra_info = None
            name = obj.target.title
        elif obj.target:
            extra_info = None
            name = obj.target.name
        else:
            extra_info = None
            name = None

        return {
            "obj": obj.target_content_type.name,
            "extra_info": extra_info,
            "id": str(obj.target.id),
            "name": name,
        }


class NotificationsFilter(django_filters.FilterSet):
    """FilterSet for Notifications."""

    unread = FilterUnread(field_name="unread", required=True)
    archived = FilterArchived(field_name="deleted", required=True)


class NotificationViewSet(MadbotViewSet, WorkspaceHeaderMixin):
    """ViewSet for handling notification."""

    filterset_class = NotificationsFilter
    serializer_class = NotificationsSerializer
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenMatchesOASRequirements]
    required_alternate_scopes = {
        "POST": [["write"]],
        "GET": [["read"]],
        "PUT": [["write"]],
        "DELETE": [["write"]],
        "PATCH": [["write"]],
    }

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="unread",
                type=bool,
                location=OpenApiParameter.QUERY,
                description="Filter notifications by unread status.",
            ),
            OpenApiParameter(
                name="archived",
                type=bool,
                location=OpenApiParameter.QUERY,
                description="Filter notifications by archived status.",
            ),
        ],
        responses={
            200: NotificationsSerializer,
            **cesr_400_query_validation_error(
                [
                    {
                        "parameter": "unread",
                        "message": "unread_value is not a valid boolean.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_query_validation_error(
                [
                    {
                        "parameter": "archived",
                        "message": "archived_value is not a valid boolean.",
                        "code": "invalid",
                    },
                ],
            ),
        },
    )
    def list(self, request, *args, **kwargs):
        """List all the notification."""
        workspace = self.get_workspace()
        queryset = Notification.objects.filter(
            recipient=request.user,
            description=workspace.id,
        ).all()
        queryset = self.filter_queryset(queryset)

        paginator = StandardPagination()
        serializer = self.get_serializer(
            paginator.paginate_queryset(queryset.order_by("-timestamp"), request),
            many=True,
            context={"request": request},
        )

        return paginator.get_paginated_response(
            serializer.data,
            extra_data={
                "countAllUnread": Notification.objects.filter(
                    recipient=request.user,
                    unread=True,
                ).count(),
            },
        )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="archived",
                type=bool,
                location=OpenApiParameter.QUERY,
                description="Filter notifications by archived status.",
                required=True,
            ),
            OpenApiParameter(
                name="unread",
                type=bool,
                location=OpenApiParameter.QUERY,
                description="Filter notifications by unread status.",
                required=True,
            ),
        ],
        responses={
            204: None,
            **cesr_404_object_not_found(),
        },
    )
    def destroy(self, request, pk=None):
        """Delete all notification or a specific one."""
        workspace = self.get_workspace()
        if pk is None:
            notification = Notification.objects.filter(
                recipient=request.user,
                description=workspace.id,
            ).all()

            # Validate the request data
            unread = request.query_params.get("unread")
            archived = request.query_params.get("archived")

            if unread is None:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="required",
                    parameter="unread",
                )

            if archived is None:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="required",
                    parameter="archived",
                )
            notification = self.filter_queryset(notification)
            notification.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)

        try:
            notification = Notification.objects.get(pk=pk, description=workspace.id)
        except Notification.DoesNotExist:
            raise ObjectNotFoundException(
                parameter="notification",
                parameter_value=str(pk),
            )

        # Check if the user has permission to delete this notification
        if notification.recipient != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        notification.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
        responses={
            204: None,
            **cesr_404_object_not_found(),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "unread",
                        "message": "unread_value is not a valid boolean.",
                        "code": "invalid",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "archived",
                        "message": "archived_value is not a valid boolean.",
                        "code": "invalid",
                    },
                ],
            ),
        },
    )
    def update(self, request, pk=None):
        """Update a notification."""
        workspace = self.get_workspace()
        try:
            notification = Notification.objects.get(pk=pk, description=workspace.id)
        except Notification.DoesNotExist:
            raise ObjectNotFoundException(parameter="notification", parameter_value=pk)
        # Serialize the notification with the updated data
        serializer = self.get_serializer(notification, data=request.data)
        # Validate the serializer
        serializer.is_valid()
        serializer.save()
        # Update the notification based on request data
        notification.unread = serializer.data.get("unread", notification.unread)
        notification.deleted = serializer.data.get("archived", notification.deleted)
        notification.save()
        return Response(serializer.data)
