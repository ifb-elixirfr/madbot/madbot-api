from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication
from oauth2_provider.contrib.rest_framework.permissions import (
    TokenMatchesOASRequirements,
)
from rest_framework import status, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from madbot_api.core.views.utils.member import (
    nodes_roles_and_descriptions,
    submissions_roles_and_descriptions,
    workspaces_roles_and_descriptions,
)


class RoleViewSet(viewsets.ViewSet):
    """A viewset for retrieving role information."""

    permission_classes = (IsAuthenticated,)
    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenMatchesOASRequirements]
    required_alternate_scopes = {
        "POST": [["write"]],
        "GET": [["read"]],
        "PUT": [["write"]],
        "DELETE": [["write"]],
        "PATCH": [["write"]],
    }

    def list(self, request):
        """List of roles and their descriptions."""
        combined_roles = {
            "workspaces": workspaces_roles_and_descriptions(),
            "nodes": nodes_roles_and_descriptions(),
            "submissions": submissions_roles_and_descriptions(),
        }

        return Response(combined_roles, status=status.HTTP_200_OK)
