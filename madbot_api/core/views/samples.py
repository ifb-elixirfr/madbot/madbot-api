from django.core.exceptions import ObjectDoesNotExist
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import Node, NodeMember, Sample
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetNodeMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_query_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class SampleSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Sample model."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()
    data = serializers.SerializerMethodField()
    datalink = serializers.SerializerMethodField()
    node = serializers.PrimaryKeyRelatedField(queryset=Node.objects.all())
    title = serializers.CharField(required=True, allow_blank=False)
    alias = serializers.SlugField(required=True, allow_blank=False)
    source = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SampleSerializer."""

        model = Sample
        fields = [
            "id",
            "alias",
            "title",
            "node",
            "source",
            "data",
            "datalink",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "data",
            "datalink",
            "source",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_source(self, obj):
        """
        Determine the source of the object as either 'local', 'inherited', or 'descendant'.

        The function checks if the given object is:
        - `local`: if it is not associated with the current node's ancestors or descendants.
        - `inherited`: if it is associated with one of the ancestors of the current node.
        - `descendant`: if it is associated with one of the descendants of the current node.

        Args:
        ----
            obj: The object being checked for its source.

        Returns:
        -------
            A string indicating the source of the object:
                - "inherited": if the object is from an ancestor node.
                - "descendant": if the object is from a descendant node.
                - "local": if the object is not associated with the current node's ancestors or descendants.

        """
        inherited_samples = self.context.get("inherited_samples", False)
        if inherited_samples:
            current_node = self.context.get("node", None)
            if current_node and obj.node in current_node.ancestors():
                return "inherited"

        descendant_samples = self.context.get("descendant_samples", False)
        if descendant_samples:
            current_node = self.context.get("node", None)
            if current_node and obj.node in current_node.descendants():
                return "descendant"

        return "local"

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)

    def get_data(self, obj):
        """
        Return information about a given object, including the count of bound samples and a related URL.

        Args:
        ----
            obj: An object that contains information about a sample.

        Returns:
        -------
            A dictionary containing:
                - "count": The count of bound samples for the given object (`obj`).
                - "related": The URL for accessing bound data related to the object.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}nodes/{obj.node.id}/sample/{obj.id}/bound_data"
        return {
            "count": obj.bound_data.count(),
            "related": related_url,
        }

    def get_datalink(self, obj):
        """
        Return information about bound datalinks related to a given object.

        The method retrieves information about bound datalinks associated with the
        object, including the count of distinct datalink values and a URL for
        accessing the bound datalinks.

        Args:
        ----
            obj: An object that has a relationship with a node and a sample.

        Returns:
        -------
            A dictionary containing:
                - "count": The number of distinct values in the "datalink" field
                of the `bound_data` related to the given `obj`.
                - "related": The URL for accessing the bound datalinks related to
                the given `obj`.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}nodes/{obj.node.id}/sample/{obj.id}/bound_datalinks"
        return {
            "count": obj.bound_data.values("datalink").distinct().count(),
            "related": related_url,
        }

    def to_representation(self, obj):
        """
        Modify the representation of the object to include node details.

        Args:
        ----
            obj: The object being serialized.

        Returns:
        -------
            The modified representation of the object, including node information.

        """
        representation = super().to_representation(obj)

        representation["node"] = {
            "id": obj.node.id,
            "title": obj.node.title,
            "description": obj.node.description,
        }

        return representation


class SampleSerializerUpdate(SampleSerializer):
    """Serializer for updating a sample."""

    title = serializers.CharField(required=False, allow_blank=False)
    alias = serializers.SlugField(required=False, allow_blank=False)
    node = serializers.PrimaryKeyRelatedField(
        queryset=Node.objects.all(),
        required=False,
    )

    class Meta:
        """Meta class for SampleSerializerUpdate."""

        model = Sample
        fields = [
            "id",
            "alias",
            "title",
            "node",
            "source",
            "data",
            "datalink",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "node",
            "data",
            "source",
            "datalink",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]


########################################################################################
# ViewSets class
########################################################################################


class SampleViewset(
    MadbotViewSet,
    WorkspaceHeaderMixin,
    BelongsToWorkspaceMixin,
    GetNodeMixin,
):
    """ViewSet for managing samples."""

    queryset = Sample.objects.all()
    pagination_class = StandardPagination

    def get_serializer_class(self):
        """
        Return a different serializer class based on the action being performed.

        Returns
        -------
            The serializer class to be used for the current action.

        """
        if self.action == "update":
            return SampleSerializerUpdate
        return SampleSerializer

    @extend_schema(
        responses={
            200: SampleSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
            OpenApiParameter(
                name="inherited_samples",
                location=OpenApiParameter.QUERY,
                description="Should inherited samples be included in the list",
                required=False,
                type=OpenApiTypes.BOOL,
            ),
            OpenApiParameter(
                name="descendant_samples",
                location=OpenApiParameter.QUERY,
                description="Should samples from descendant nodes be included in the list",
                required=False,
                type=OpenApiTypes.BOOL,
            ),
        ],
    )
    def list(self, request, node_pk=None):
        """List the information related to all the samples objects."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        queryset = self.get_queryset().filter(node=node)

        inherited_samples = bool(
            request.query_params.get("inherited_samples", "false").lower() == "true",
        )
        if inherited_samples:
            parent_nodes = node.ancestors()
            queryset = queryset | self.get_queryset().filter(node__in=parent_nodes)

        descendant_samples = bool(
            request.query_params.get("descendant_samples", "false").lower() == "true",
        )
        if descendant_samples:
            descendant_nodes = node.descendants()
            queryset = queryset | self.get_queryset().filter(node__in=descendant_nodes)

        serializer = self.get_serializer(
            queryset,
            many=True,
            context={
                "request": request,
                "inherited_samples": inherited_samples,
                "descendant_samples": descendant_samples,
                "node": node,
            },
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: SampleSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "node",
                    "code": "does_not_exist",
                    "message": '"<node_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "node",
                    "code": "does_not_exist",
                    "message": '"<node_value>" does not exist.',
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, pk, node_pk=None):
        """Retrieve the information of one sample associated to the node."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        item = self.get_object()
        self.check_object_belongs_to_node(node, item)

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            201: SampleSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "node",
                    "code": "does_not_exist",
                    "message": '"<node_value>" does not exist.',
                },
            ),
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "node",
                    "code": "does_not_exist",
                    "message": '"<node_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "forbidden",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, node_pk=None, *args, **kwargs):
        """Create a new sample object associated to the node."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # Check if the user's role allows creation
        if not NodeMember.objects.filter(node=node, user=request.user).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )
        # Check if the user's role allows creation
        if not NodeMember.objects.filter(
            node=node,
            user=request.user,
            role__in=["owner", "manager", "contributor"],
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            )

        serializer = self.get_serializer(data={**request.data, "node": node.id})
        serializer.is_valid()

        serializer.save(
            created_by=request.user,
            last_edited_by=request.user,
        )

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: SampleSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def update(
        self,
        request,
        pk,
        node_pk=None,
    ):
        """Update a sample object associated to the node."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # check if the user is a member of the node
        try:
            NodeMember.objects.get(node__pk=node_pk, user=request.user.id)
        # check if the user is a member of the node
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        # Check if the user's role allows the update
        if not NodeMember.objects.filter(
            node=node,
            user=request.user,
            role__in=["owner", "manager", "contributor"],
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            )

        # get the sample to update
        item = self.get_object()
        self.check_object_belongs_to_node(node, item)

        serializer = self.get_serializer(item, data=request.data)
        serializer.is_valid()
        serializer.save(last_edited_by=request.user)

        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk, node_pk=None):
        """Delete a sample item."""
        workspace = self.get_workspace()
        node = self.get_node(workspace)

        # Check if the user is a member of the node
        if not NodeMember.objects.filter(node=node, user=request.user).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        # Check if the user has the right permissions
        if not NodeMember.objects.filter(
            node=node,
            user=request.user,
            role__in=["owner", "manager", "contributor"],
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )

        item = self.get_object()
        self.check_object_belongs_to_node(node, item)
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
