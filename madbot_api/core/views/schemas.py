from django.shortcuts import get_object_or_404
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from jsonschema import RefResolver
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core import connector
from madbot_api.core.errors import ValidationException
from madbot_api.core.models import ConnectorRawSchema, MadbotRawSchema
from madbot_api.core.views.utils.schema_response import cesr_400_query_validation_error


def resolve_refs(schema, resolver):
    """
    Recursively resolve references in a JSON schema.

    The function checks if the schema contains a reference (`$ref`). If so, it retrieves
    the referenced schema from the appropriate database table and resolves any nested references.
    If the schema is a dictionary or a list, it applies the same resolution process
    to all keys or items, respectively.

    Args:
    ----
        schema: The JSON schema to process, which may contain references.
        resolver: An instance of the resolver used for resolving references.

    Returns:
    -------
        The schema with all references resolved.

    """
    if isinstance(schema, dict):
        if "$ref" in schema:
            ref_slug = schema["$ref"].split("/")[-1]

            # Try to find the schema in MadbotRawSchema
            metadata_schemas = MadbotRawSchema.objects.filter(slug=ref_slug).order_by(
                "-version"
            )
            connector_schemas = ConnectorRawSchema.objects.filter(
                slug=ref_slug
            ).order_by("-version")

            if metadata_schemas.exists():
                resolved_schema = metadata_schemas[
                    0
                ].schema  # Take the most recent version
            elif connector_schemas.exists():
                resolved_schema = connector_schemas[
                    0
                ].schema  # Take the most recent version
            else:
                raise ValidationException(
                    code="reference_error",
                    details_code="not_found",
                    parameter="$ref",
                    parameter_value=schema["$ref"],
                )

            return resolve_refs(resolved_schema, resolver)

        return {key: resolve_refs(value, resolver) for key, value in schema.items()}

    if isinstance(schema, list):
        return [resolve_refs(item, resolver) for item in schema]

    return schema


def dereference_schema(schema):
    """
    Resolve all references in a JSON schema.

    This function initializes a reference resolver for the provided schema and
    then calls `resolve_refs` to resolve all `$ref` references in the schema.

    Args:
    ----
        schema: The JSON schema containing references to be resolved.

    Returns:
    -------
        The schema with all references fully resolved.

    """
    resolver = RefResolver.from_schema(schema)
    return resolve_refs(schema, resolver)


class CoreSchemasView(APIView):
    """View to retrieve JSON schemas of the core."""

    @extend_schema(
        responses={
            200: OpenApiTypes.OBJECT,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "source",
                    "code": "does_not_exist",
                    "message": '"<source>" does not exist',
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="version",
                location=OpenApiParameter.QUERY,
                description="Schema version to retrieve",
                required=False,
                type=OpenApiTypes.STR,
            ),
        ],
    )
    def get(self, request, source=None, slug=None):
        """Retrieve JSON schemas based on source, slug, and optional version."""
        version = request.query_params.get("version", None)
        ref = request.query_params.get("ref", "true").lower() in ["true", "1"]

        if source == "referential":
            if slug:
                queryset = MadbotRawSchema.objects.filter(slug=slug, source=source)
                if version:
                    queryset = queryset.filter(version=version)
                schema_obj = get_object_or_404(queryset.order_by("-version"))
                schema_json = schema_obj.schema
            else:
                schema_json = list(
                    MadbotRawSchema.objects.filter(source=source).values(
                        "slug", "schema", "version"
                    )
                )

        else:
            raise ValidationException(
                code="query_validation_error",
                details_code="does_not_exist",
                parameter="source",
                parameter_value=source,
            )

        if not ref:
            schema_json = dereference_schema(schema_json)

        # Add JSON schema identifiers and ensure $schema is added correctly
        if isinstance(schema_json, list):
            for schema in schema_json:
                if isinstance(schema, dict):
                    schema["$schema"] = "https://json-schema.org/draft/2019-09/schema"
                    schema["$id"] = (
                        f"{request.build_absolute_uri('/api/')}schemas/{source}/{schema['slug']}"
                    )
                else:
                    schema_dict = schema.dict()
                    schema_dict["$schema"] = (
                        "https://json-schema.org/draft/2019-09/schema"
                    )
                    schema_dict["$id"] = (
                        f"{request.build_absolute_uri('/api/')}schemas/{source}/{schema.slug}"
                    )

        else:
            # If it's a single schema, add the $schema field directly
            schema_json["$schema"] = "https://json-schema.org/draft/2019-09/schema"
            schema_json["$id"] = (
                f"{request.build_absolute_uri('/api/')}schemas/{source}/{slug}"
            )

        return Response(schema_json)


class ConnectorSchemasView(APIView):
    """View to retrieve JSON schemas of the core."""

    def get(self, request, source=None, slug=None):
        """Retrieve JSON schemas from ConnectorRawSchema based on source, slug, and optional version."""
        version = request.query_params.get("version", None)
        ref = request.query_params.get("ref", "true").lower() in ["true", "1"]

        if source:
            connector_instance = next(
                (
                    c
                    for c in connector.get_connectors()
                    if c.get_name().lower() == source.lower()
                ),
                None,
            )
            if not connector_instance:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="does_not_exist",
                    parameter="source",
                    parameter_value=source,
                )

        # Retrieve schema based on source and slug, considering version if provided
        if slug:
            queryset = ConnectorRawSchema.objects.filter(slug=slug, source=source)
            if version:
                queryset = queryset.filter(version=version)
            schema_obj = get_object_or_404(queryset.order_by("-version"))
            schema_json = schema_obj.schema
        else:
            # If no slug is provided, return all schemas of the source
            schema_json = list(
                ConnectorRawSchema.objects.filter(source=source).values(
                    "slug", "schema", "version"
                )
            )

        # Resolve references if requested
        if not ref:
            schema_json = dereference_schema(schema_json)

        # Add JSON schema identifiers and ensure $schema is added correctly
        if isinstance(schema_json, list):
            for schema in schema_json:
                if isinstance(schema, dict):
                    schema["$schema"] = "https://json-schema.org/draft/2019-09/schema"
                    schema["$id"] = (
                        f"{request.build_absolute_uri('/api/')}schemas/connectors/{source}/{schema['slug']}"
                    )
                else:
                    schema_dict = schema.dict()
                    schema_dict["$schema"] = (
                        "https://json-schema.org/draft/2019-09/schema"
                    )
                    schema_dict["$id"] = (
                        f"{request.build_absolute_uri('/api/')}schemas/connectors/{source}/{schema.slug}"
                    )
        else:
            # If it's a single schema, add the $schema field directly
            schema_json["$schema"] = "https://json-schema.org/draft/2019-09/schema"
            schema_json["$id"] = (
                f"{request.build_absolute_uri('/api/')}schemas/connectors/{source}/{slug}"
            )

        return Response(schema_json)
