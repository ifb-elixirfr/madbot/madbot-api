from datetime import datetime, timezone

from django.core.cache import cache
from django.db.models import Count, Sum
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core.models import (
    Data,
    MadbotUser,
    Metadata,
    Node,
    Submission,
    Workspace,
)

########################################################################################
# Serializer methods
########################################################################################


class StatisticsUsersSerializer(serializers.Serializer):
    """Serializer for the Users in stats of the current Madbot instance."""

    global_count = serializers.IntegerField()


class StatisticsWorkspacesSerializer(serializers.Serializer):
    """Serializer for the Workspaces in stats of the current Madbot instance."""

    global_count = serializers.IntegerField()


class StatisticsMetadataSerializer(serializers.Serializer):
    """Serializer for the Metadata in stats of the current Madbot instance."""

    global_count = serializers.IntegerField()


class StatisticsSubmissionsSerializer(serializers.Serializer):
    """Serializer for the Submission in stats of the current Madbot instance."""

    global_count = serializers.IntegerField()
    destination_distribution = serializers.ListField()
    status_distribution = serializers.ListField()


class StatisticsDataSerializer(serializers.Serializer):
    """Serializer for the Data in stats of the current Madbot instance."""

    global_count = serializers.IntegerField()
    types_distribution = serializers.ListField()
    source_distribution = serializers.ListField()


class StatisticsNodesSerializer(serializers.Serializer):
    """Serializer for the Nodes in stats of the current Madbot instance."""

    global_count = serializers.IntegerField()
    types_distribution = serializers.ListField()


class StatisticsSerializer(serializers.Serializer):
    """Serializer for the stats of the current Madbot instance."""

    users = StatisticsUsersSerializer()
    workspaces = StatisticsWorkspacesSerializer()
    nodes = StatisticsNodesSerializer()
    data = StatisticsDataSerializer()
    metadata = StatisticsMetadataSerializer()
    timestamp = serializers.SerializerMethodField()

    def get_timestamp(self, obj):
        """Generate the current timestamp in ISO 8601 format."""
        return datetime.now(timezone.utc).isoformat()


########################################################################################
# ViewSets class
########################################################################################


class StatsView(APIView):
    """Retrieve the stats of the current Madbot instance."""

    def get(self, request):
        """Return the stats of the current Madbot instance."""
        cache_key = "stats"
        cached_data = cache.get(cache_key)

        if cached_data:
            return Response(cached_data)

        ## Nodes
        nodes_count = Node.objects.count()
        nodes_types_distribution = list(
            Node.objects.values("type").annotate(count=Count("id"))
        )

        ### Data
        data_count = Data.objects.count()
        data_types_distribution = list(
            Data.objects.values("media_type").annotate(count=Count("id")).order_by()
        )
        data_size = Data.objects.aggregate(sum=Sum("size"))["sum"]
        data_sources_distrubtion = list(
            Data.objects.values("connection__connector")
            .annotate(count=Count("id"))
            .order_by()
        )

        ## Submission
        submission_count = Submission.objects.count()
        submission_destination = list(
            Submission.objects.values("connection__connector")
            .annotate(count=Count("id"))
            .order_by()
        )

        submission_status = list(
            Submission.objects.values("status").annotate(count=Count("id")).order_by()
        )

        data = {
            "users": {"global_count": MadbotUser.objects.count()},
            "workspaces": {"global_count": Workspace.objects.count()},
            "data": {
                "global_count": data_count,
                "global_size": data_size,
                "types_distribution": data_types_distribution,
                "source_distribution": data_sources_distrubtion,
            },
            "nodes": {
                "global_count": nodes_count,
                "types_distribution": nodes_types_distribution,
            },
            "submissions": {
                "global_count": submission_count,
                "destination_distribution": submission_destination,
                "status_distribution": submission_status,
            },
            "metadata": {"global_count": Metadata.objects.count()},
        }
        serializer = StatisticsSerializer(data)

        cache.set(cache_key, serializer.data, timeout=3600)

        return Response(serializer.data)
