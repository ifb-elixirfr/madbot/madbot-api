from uuid import UUID

from django.core.exceptions import ObjectDoesNotExist
from drf_spectacular.utils import (
    OpenApiExample,
    OpenApiParameter,
    OpenApiTypes,
    extend_schema,
)
from rest_framework import serializers, status
from rest_framework.exceptions import ErrorDetail
from rest_framework.response import Response

from madbot_api.core import connector as connectors
from madbot_api.core.errors import (
    ForbiddenException,
    ObjectNotFoundException,
    ValidationException,
)
from madbot_api.core.models import (
    Data,
    DataLink,
    Submission,
    SubmissionData,
    SubmissionMember,
)
from madbot_api.core.tasks import async_create_metadata_objects
from madbot_api.core.utils.websocket import notifier
from madbot_api.core.views.data import DataSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
    GetObjectMixin,
    GetSubmissionMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)


class SubmissionDataSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for the submission data."""

    data = DataSerializer()
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SubmissionDataSerializer."""

        model = SubmissionData
        fields = [
            "id",
            "submission",
            "data",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "submission",
            "data",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.


        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.


        """
        return self.get_interactor_information(obj.last_edited_by)


class DataObjectSerializer(serializers.Serializer):
    """Serializer for data_objects in the create body."""

    data = serializers.CharField()
    datalink = serializers.CharField()

    def validate_data(self, value):
        """Validate the data field."""
        try:
            return str(UUID(value))
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for data.")

    def validate_datalink(self, value):
        """Validate the datalink field."""
        try:
            return str(UUID(value))
        except ValueError:
            raise serializers.ValidationError("Invalid UUID format for datalink.")


class SubmissionDataSerializerCreate(serializers.Serializer):
    """Serializer for the create endpoint of submission data."""

    data_objects = serializers.ListField(child=DataObjectSerializer())


########################################################################################
# ViewSets class
########################################################################################


class SubmissionDataViewSet(
    MadbotViewSet,
    GetObjectMixin,
    WorkspaceHeaderMixin,
    GetSubmissionMixin,
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
):
    """ViewSet for handling the data associations."""

    queryset = SubmissionData.objects.all()

    serializer_class = SubmissionDataSerializer
    pagination_class = StandardPagination

    @extend_schema(
        responses={
            200: SubmissionDataSerializer,
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, submission_pk=None):
        """List the information related to all the submission data objects."""
        workspace = self.get_workspace()
        submission = self.get_the_object(Submission, submission_pk, "uri")

        # Check the requested data is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        serializer = self.get_serializer(
            self.queryset.filter(submission=submission),
            many=True,
            context={
                "request": request,
            },
        )

        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: SubmissionDataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(
        self,
        request,
        pk,
        submission_pk=None,
    ):
        """Retrieve a given submission_data object."""
        # verify the workspace
        workspace = self.get_workspace()
        # verify the submission
        submission = self.get_submission(workspace)

        # get the submission_data to retrieve
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        request=OpenApiTypes.OBJECT,
        examples=[
            OpenApiExample(
                "Default request body",
                value={
                    "data_objects": [
                        {
                            "data": "7244ff48-7fcc-4ebf-9682-c78cd8b786e7",
                            "datalink": "26eb68be-0d58-4e92-b269-b0102e082785",
                        }
                    ]
                },
                request_only=True,
            )
        ],
        responses={
            201: SubmissionDataSerializerCreate,
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "datalink",
                    "code": "does_not_exist",
                    "message": "Could not find data: <datalink_id>.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "datalink",
                    "code": "does_not_exist",
                    "message": "Could not find data: <datalink_id>.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, submission_pk=None, *args, **kwargs):
        """Create a new submission_data object."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the submission
        submission = self.get_submission(workspace)

        # Check if the submission is in "generating metadata" status
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )

        # check if it's the right workspace for the submission
        self.check_object_belongs_to_workspace(workspace, submission)

        try:
            # check if the user is a member of the submission
            current_member = SubmissionMember.objects.get(
                submission__id=submission_pk,
                user=request.user.id,
            )
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )
        if current_member.role not in [
            "owner",
            "data broker",
            "manager",
            "contributor",
        ]:
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            )

        # Check the body data
        serializer = SubmissionDataSerializerCreate(
            data={**request.data, "submission": submission.id},
        )
        serializer.is_valid()

        data_ids, datalink_ids = zip(
            *[
                (item["data"], item["datalink"])
                for item in request.data.get("data_objects")
            ]
        )

        data_objects = Data.objects.filter(id__in=data_ids)
        datalink_objects = DataLink.objects.filter(id__in=datalink_ids)

        errors = []

        # check that all data belong to the workspace and that the user has access to all of them
        for data_id, datalink_id in zip(data_ids, datalink_ids):
            # retrieve the data and the datalink
            # if one of them is not found, continue to the next object
            data = None
            datalink = None
            try:
                data = data_objects.get(id=data_id)
            except ObjectDoesNotExist:
                errors.append(
                    ErrorDetail(
                        f"Could not find data: {data_id}.", code="does_not_exist"
                    )
                )
                continue
            try:
                datalink = datalink_objects.get(id=datalink_id)
            except ObjectDoesNotExist:
                errors.append(
                    ErrorDetail(
                        f"Could not find datalink: {datalink_id}.",
                        code="does_not_exist",
                    )
                )
                continue

            # check that the data belong to the workspace
            try:
                self.check_object_belongs_to_workspace(workspace, data)
            except ObjectNotFoundException:
                errors.append(
                    ErrorDetail(
                        f"Could not find data: {data_id}.", code="does_not_exist"
                    )
                )
                continue

            # Check if the user has access to the datalink
            memberships = datalink.node.node_members.filter(user=request.user)
            if not memberships.exists():
                errors.append(
                    ErrorDetail(
                        f"Could not find datalink: {datalink_id}.",
                        code="does_not_exist",
                    )
                )
                continue
            if not memberships.filter(
                role__in=["owner", "data broker", "manager", "contributor"]
            ).exists():
                errors.append(
                    ErrorDetail(
                        f"The member's role does not allow creation on datalink: {datalink_id}.",
                        code="invalid",
                    )
                )
                continue

            # check that the datalink correspond to the data
            if str(datalink.data.id) != data_id:
                errors.append(
                    ErrorDetail(
                        f"Datalink ({datalink_id}) does not correspond to data ({data_id}).",
                        code="invalid",
                    )
                )
                continue
            # check that the data belong to the suppoerted file extensions
            connector_class = connectors.get_connector_class(
                submission.connection.connector
            )
            supported_files = connector_class.get_supported_file_extensions()
            if supported_files != [] and data.file_extension not in supported_files:
                errors.append(
                    ErrorDetail(
                        f"The data identified by ({data_id}) does not have an extension supported by the {connector_class.get_name()} connector.",
                        code="invalid",
                    )
                )
                continue

        if errors:
            raise ValidationException(code="body_validation_error", details=errors)

        submission_data_to_create = [
            SubmissionData(
                submission=submission,
                data=data_objects.get(id=data_object.get("data")),
                datalink=datalink_objects.get(id=data_object.get("datalink")),
                created_by=request.user,
                last_edited_by=request.user,
            )
            for data_object in request.data.get("data_objects")
        ]
        # Bulk create the submission data objects
        submission_data = SubmissionData.objects.bulk_create(submission_data_to_create)

        # Update the submission status to "generating metadata"
        submission.status = Submission.STATUS_GENERATING_METADATA
        submission.save()

        # Send WebSocket message to notify clients about submission update
        notifier.sync_update(
            model_type="submission",
            object_id=submission.id,
            action="update",
            source="submission",
        )

        async_create_metadata_objects.delay(submission.id, request.user.id)

        serializer = self.get_serializer(
            submission_data, many=True, context={"request": request}
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "submission",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk, submission_pk=None):
        """Delete a SubmissionData item."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the submission
        submission = self.get_submission(workspace)

        # Check if the submission is in "generating metadata" status
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )

        # check if it's the right workspace for the submission
        self.check_object_belongs_to_workspace(workspace, submission)

        try:
            # check if the user is a member of the submission
            current_member = SubmissionMember.objects.get(
                submission__id=submission_pk,
                user=request.user.id,
            )
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )
        # Check if the user's role allows deletion
        if current_member.role not in [
            "owner",
            "data broker",
            "manager",
            "contributor",
        ]:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )

        # get the submission_data to delete
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        # Delete item
        item.delete()

        # Update the submission status to "generating metadata"
        submission.status = Submission.STATUS_GENERATING_METADATA
        submission.save()

        # Send WebSocket message to notify clients about submission update
        notifier.sync_update(
            model_type="submission",
            object_id=submission.id,
            action="update",
            source="submission",
        )

        async_create_metadata_objects.delay(submission.id, request.user.id)

        return Response(status=status.HTTP_204_NO_CONTENT)
