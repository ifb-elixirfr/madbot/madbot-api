from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import filters, serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import SubmissionMember, WorkspaceMember
from madbot_api.core.serializers.user import UserSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import MemberFilter
from madbot_api.core.views.utils.member import count_owners
from madbot_api.core.views.utils.mixin import (
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
    GetSubmissionMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.notification import trigger_notification
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class SubmissionMemberSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for SubmissionMember model."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SubmissionMemberSerializer."""

        model = SubmissionMember
        fields = [
            "id",
            "user",
            "role",
            "submission",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)

    def to_representation(self, instance):
        """
        Modify the representation of an instance to include user data.

        Args:
        ----
            instance: The instance of the model that the serializer is serializing.

        Returns:
        -------
            dict: The modified representation of the instance, including user data
            and excluding the submission field.

        """
        representation = super().to_representation(instance)
        user = UserSerializer(instance.user).data
        representation["user"] = user
        # Exclude submission field from the representation
        representation.pop("submission", None)
        return representation


class SubmissionMemberSerializerUpdate(SubmissionMemberSerializer):
    """Serializer for updating SubmissionMember data."""

    role = serializers.ChoiceField(
        required=False,
        choices=SubmissionMember.ROLE_SUBMISSION_MEMBER,
    )

    class Meta:
        """Meta class for SubmissionMemberSerializerUpdate."""

        model = SubmissionMember
        fields = [
            "id",
            "user",
            "submission",
            "role",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "user",
            "submission",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]


########################################################################################
# ViewSets class
########################################################################################


class SubmissionMemberViewSet(
    MadbotViewSet,
    GetSubmissionMixin,
    WorkspaceHeaderMixin,
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
):
    """ViewSet for handling submission members."""

    serializer_class = SubmissionMemberSerializer
    queryset = (
        SubmissionMember.objects.all()
        .order_by("user__username")
        .select_related("created_by", "last_edited_by", "submission", "user")
    )

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_class = MemberFilter

    @extend_schema(
        responses={
            200: SubmissionMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, submission_pk=None):
        """Retrieve the list of submission members for a specific submission."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the submission
        submission = self.get_submission(workspace)

        # check if it's the right workspace for the submission
        self.check_object_belongs_to_workspace(workspace, submission)

        serializer = SubmissionMemberSerializer(
            self.filter_queryset(
                self.get_queryset().filter(submission__pk=submission_pk),
            ),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            201: SubmissionMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "user",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "role",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You must be owner of the submission to add a new member.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You can only add new members with a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "user",
                    "code": "not_allowed",
                    "message": "User is already a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, submission_pk=None, *args, **kwargs):
        """Create a new submission member object."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the submission
        submission = self.get_submission(workspace)

        # check if it's the right workspace for the submission
        self.check_object_belongs_to_workspace(workspace, submission)

        current_member = None

        # if the user is a workspace owner and they are trying to add themselves to a submission
        # let them do and pass other validation
        if not (
            WorkspaceMember.objects.filter(
                workspace=workspace,
                user=request.user,
                role="owner",
            ).exists()
            and str(request.user.id) == request.data.get("user", None)
        ):
            try:
                # check if the user is a member of the submission
                current_member = SubmissionMember.objects.get(
                    submission__id=submission_pk,
                    user=request.user.id,
                )
            except ObjectDoesNotExist:
                raise ForbiddenException(
                    details={
                        "parameter": "submission",
                        "code": "not_allowed",
                        "message": "You are not a member of this submission.",
                    },
                )

            # check if the user is owner or manager the submission
            if current_member.role not in ["owner", "manager"]:
                raise ForbiddenException(
                    details={
                        "parameter": "role",
                        "code": "not_allowed",
                        "message": "The member's role does not allow creation.",
                    },
                )

        # check if a user other than the owner is trying to create a member with the data broker role
        if request.data.get("role", None) == "data broker" and not (
            current_member and current_member.role == "owner"
        ):
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "Only the submission owner can name a data broker.",
                },
            )

        # Check the body data
        serializer = SubmissionMemberSerializer(
            data={**request.data, "submission": submission.id},
        )
        serializer.is_valid()

        # check if the user is a member of the workspace
        if not WorkspaceMember.objects.filter(
            workspace=workspace,
            user=request.data.get("user", None),
        ).exists() and request.data.get("user", None):
            raise ForbiddenException(
                details={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "User must be member of the workspace to be added to a submission.",
                },
            )

        # check if the user is trying to add a member with a owner role
        if request.data.get("role", None) == "owner":
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You can only have one owner by submission.",
                },
            )

        # Create the SubmissionMember with the associated submission
        member = serializer.save(created_by=request.user, last_edited_by=request.user)

        # trigger the notification and send notification
        submission_members = submission.submission_members.all().exclude(
            user=request.user,
        )
        for submission_member in submission_members:
            pass

            trigger_notification(
                sender=request.user,
                recipient=submission_member.user,
                verb="created",
                action_object=member,
                target=submission,
                actor=request.user,
                workspace=submission.workspace.id,
            )

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: SubmissionMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(
        self,
        request,
        pk,
        submission_pk=None,
    ):
        """Retrieve a given member object from a submission."""
        workspace = self.get_workspace()
        submission = self.get_submission(workspace)

        # get the member to retrieve
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        serializer = SubmissionMemberSerializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            200: SubmissionMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "role",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "You must be owner of the submission to edit other members.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "The role of the submission member cannot be changed because they are the last owner of the current submission.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "You can only change a member to a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "You cannot promote yourself.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def update(
        self,
        request,
        pk,
        submission_pk=None,
    ):
        """Update a submission member item."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the submission
        submission = self.get_submission(workspace)

        # check if it's the right workspace for the submission
        self.check_object_belongs_to_workspace(workspace, submission)

        # get the member to update
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        try:
            current_member = SubmissionMember.objects.get(
                submission__pk=submission_pk,
                user=request.user.id,
            )
        # check if the user is a member of the submission
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

        if not (
            WorkspaceMember.objects.filter(
                workspace=workspace,
                user=request.user.id,
                role="owner",
            ).exists()
            and request.user.id == item.user.id
        ):
            # check is the user is trying to promote themselves
            if item.user.id == request.user.id:
                raise ForbiddenException(
                    details={
                        "parameter": "submission_member",
                        "code": "not_allowed",
                        "message": "You cannot promote yourself.",
                    },
                )

            # check if the user has the right to modify members
            if current_member.role not in ["owner", "manager"]:
                raise ForbiddenException(
                    details={
                        "parameter": "submission_member",
                        "code": "not_allowed",
                        "message": "The member's role does not allow update.",
                    },
                )

        # check if a user other than the owner is trying to promote a member to the data broker role
        if (
            request.data.get("role", None) == "data broker"
            and current_member.role != "owner"
        ):
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "Only the submission owner can name a data broker.",
                },
            )

        # check if the user is trying to promote a member to the owner role
        if request.data.get("role", None) == "owner":
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You can only have one owner by submission.",
                },
            )

        # check is the user is trying to update the last owner
        if item.role == "owner" and count_owners(submission) == 1:
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "The role of the submission member cannot be changed"
                    " because they are the last owner of the current submission.",
                },
            )

        serializer = SubmissionMemberSerializerUpdate(item, data=request.data)
        serializer.is_valid()
        submission_member = serializer.save(last_edited_by=request.user)

        # trigger the notification
        trigger_notification(
            sender=request.user,
            recipient=item.user,
            verb="changed",
            action_object=submission_member,
            target=submission,
            actor=request.user,
            workspace=submission.workspace.id,
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "You must be owner of the submission to remove another member.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because they are the last owner of the current submission.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "You can't remove a submission member with a role above yours.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(
        self,
        request,
        pk,
        submission_pk=None,
    ):
        """Delete a submission member item."""
        # verify the workspace
        workspace = self.get_workspace()

        # verify the submission
        submission = self.get_submission(workspace)

        # check if it's the right workspace for the submission
        self.check_object_belongs_to_workspace(workspace, submission)

        # get the member to delete
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        try:
            current_member = SubmissionMember.objects.get(
                submission__pk=submission_pk,
                user=request.user,
            )
        # check if the user is a member of the submission
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

        # check if the user has the right to modify members
        if (
            current_member.role not in ["owner", "manager"]
            and str(current_member.id) != pk
        ):
            raise ForbiddenException(
                details={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )
        # check if the user is trying to remove a member that has a role above his
        if item.role == "owner" and current_member.role == "member":
            raise ForbiddenException(
                details={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "You can't remove a submission member with a role above yours.",
                },
            )
        # check is the user is trying to remove the last owner
        if item.role == "owner" and count_owners(submission) == 1:
            raise ForbiddenException(
                details={
                    "parameter": "submission_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because"
                    " they are the last owner of the current submission.",
                },
            )
        item.delete()

        # trigger the notification
        trigger_notification(
            sender=request.user,
            recipient=item.user,
            verb="suppressed",
            action_object=item,
            target=submission,
            actor=request.user,
            workspace=submission.workspace.id,
        )
        return Response(status=status.HTTP_204_NO_CONTENT)
