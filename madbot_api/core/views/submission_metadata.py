from django.db.models import Case, F, IntegerField, Value, When
from drf_spectacular.utils import (
    OpenApiExample,
    OpenApiParameter,
    OpenApiTypes,
    extend_schema,
)
from jsonschema.exceptions import ValidationError
from referencing import Registry, Resource
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core import connector as connectors
from madbot_api.core.errors import ForbiddenException, ObjectNotFoundException
from madbot_api.core.models import (
    ConnectorRawSchema,
    Data,
    DataAssociation,
    Node,
    Sample,
    Submission,
    SubmissionMetadata,
    SubmissionMetadataObject,
    ValueField,
)
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
    GetObjectMixin,
    GetSubmissionMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.schema_validation import MadbotValidator
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class ValueFieldSerializer(serializers.ModelSerializer):
    """Serializer for the ValueField model."""

    source = serializers.SerializerMethodField()

    class Meta:
        """Meta class for ValueFieldSerializer."""

        model = ValueField
        fields = ["value", "source"]

    def get_source(self, obj):
        """
        Get source information from the source_metadata if it exists.

        Args:
            obj: ValueField instance

        Returns:
            dict: Source information including object type, id, and name if available
            None: If no source_metadata exists

        """
        if not obj.source_metadata:
            return None

        associated_object = obj.source_metadata.get_associated_object()
        if isinstance(associated_object, Node):
            return {
                "object": "Node",
                "id": associated_object.id,
                "name": associated_object.title,
            }

        if isinstance(associated_object, Sample):
            return {
                "object": "Sample",
                "id": associated_object.id,
                "name": associated_object.title,
                "node": {
                    "id": associated_object.node.id if associated_object.node else None,
                    "name": (
                        associated_object.node.title if associated_object.node else None
                    ),
                },
            }

        if isinstance(associated_object, Data):
            # the data can have multiple datalinks, we need to select one to return
            # we can use the context for that, if we inherit some metadata from a data, this data
            # is surely associated to the submission, and this association present a datalink
            # we can use this datalink to return the data
            submission_metadata = (
                obj.submission_metadata.first()
                if obj.submission_metadata.first()
                else obj.submission_metadata_candidate_values.first()
            )
            datalink = (
                submission_metadata.submission.data.filter(data=associated_object)
                .first()
                .datalink
            )
            return {
                "object": "Data",
                "id": associated_object.id,
                "name": associated_object.name,
                "datalink": {
                    "id": datalink.id,
                    "node": {
                        "id": datalink.node.id,
                        "name": datalink.node.title,
                    },
                },
            }

        if isinstance(associated_object, DataAssociation):
            data_list = []
            submission_metadata = (
                obj.submission_metadata.first()
                if obj.submission_metadata.first()
                else obj.submission_metadata_candidate_values.first()
            )
            for data in associated_object.data_objects.all():
                datalink_repr = None
                if submission_metadata.submission.data.filter(data=data).exists():
                    datalink = (
                        submission_metadata.submission.data.filter(data=data)
                        .first()
                        .datalink
                    )
                    datalink_repr = {
                        "id": datalink.id,
                        "node": {
                            "id": datalink.node.id,
                            "name": datalink.node.title,
                        },
                    }
                data_list.append(
                    {
                        "object": "Data",
                        "id": data.id,
                        "name": data.name,
                        "datalink": datalink_repr,
                    }
                )
            return {
                "object": "DataAssociation",
                "id": associated_object.id,
                "data": data_list,
            }

        return None


class SubmissionMetadataSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for the submission metadata."""

    field = serializers.SerializerMethodField()
    value = ValueFieldSerializer(read_only=True)
    candidate_values = ValueFieldSerializer(many=True, read_only=True)
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SubmissionMetadataSerializer."""

        model = SubmissionMetadata
        fields = [
            "id",
            "submission",
            "submission_metadata_object",
            "field",
            "value",
            "candidate_values",
            "is_valid",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "submission",
            "submission_metadata_object",
            "field",
            "candidate_values",
            "is_valid",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_field(self, obj):
        """
        Get the field information for the submission metadata.

        Args:
            obj: The submission metadata object.

        Returns:
            dict: The field information.

        """
        connector = obj.submission.connection.connector
        connector_class = connectors.get_connector_class(connector)

        return {
            "slug": obj.field.slug,
            "name": obj.field.name,
            "level": obj.field.level,
            "schema": f"{self.context['request'].build_absolute_uri('/api/schemas/connectors/')}{connector_class.get_name().lower()}/{obj.field.schema.slug}",
        }

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.


        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.


        """
        return self.get_interactor_information(obj.last_edited_by)


class SubmissionMetadataCreateSerializer(serializers.Serializer):
    """Serializer for creating submission metadata."""

    metadata = serializers.ListField(
        child=serializers.DictField(
            child=serializers.JSONField(),
            help_text="List of metadata objects with 'field' and 'value'.",
        )
    )

    class Meta:
        """Meta class for SubmissionMetadataCreateSerializer."""

        fields = ["metadata"]

    def validate_metadata(self, value):
        """
        Validate the metadata field.

        Args:
        ----
            value: A list of metadata dictionaries to validate.

        Returns:
        -------
            list: Validated metadata.

        """
        for item in value:
            if "field" not in item or "value" not in item:
                raise serializers.ValidationError(
                    "Each metadata item must include 'field' and 'value'."
                )

        return value


class SubmissionMetadataUpdateSerializer(serializers.Serializer):
    """Serializer for updating submission metadata."""

    value = serializers.JSONField(required=True, allow_null=True)
    source = serializers.CharField(required=False, allow_null=True)


########################################################################################
# ViewSets class
########################################################################################


class SubmissionMetadataViewSet(
    MadbotViewSet,
    GetObjectMixin,
    WorkspaceHeaderMixin,
    GetSubmissionMixin,
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
):
    """ViewSet for handling the SubmissionMetadata."""

    queryset = SubmissionMetadata.objects.select_related("submission").all()

    serializer_class = SubmissionMetadataSerializer
    pagination_class = StandardPagination

    @extend_schema(
        responses={
            200: SubmissionMetadataSerializer,
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, submission_pk=None, *args, **kwargs):
        """List the information related to all the submission metadata items."""
        # Verify the workspace
        workspace = self.get_workspace()

        # Verify the submission
        submission = self.get_submission(workspace)

        # Verify the submission metadataObject
        metadata_object = self.get_the_object(
            SubmissionMetadataObject, kwargs["metadata_objects_pk"], "uri"
        )

        # Check the submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        # Check the metadata object is part of the workspace
        self.check_object_belongs_to_workspace(workspace, metadata_object.submission)

        connector = submission.connection.connector
        connector_class = connectors.get_connector_class(connector)
        metadata_object_type = connector_class.get_all_metadata_object_types().get(
            metadata_object.type
        )
        # Filter duplicates: remove from mandatory, recommended, and optional fields already in priority
        priority_list = metadata_object_type.priority
        priority_mapping = {field: index for index, field in enumerate(priority_list)}
        priority_set = set(priority_list)  # Convert list to set
        mandatory_set = (
            set([field.slug for field in metadata_object_type.mandatory_fields_schemas])
            - priority_set
        )
        recommended_set = (
            set(
                [
                    field.slug
                    for field in metadata_object_type.recommended_fields_schemas
                ]
            )
            - priority_set
        )
        optional_set = (
            set([field.slug for field in metadata_object_type.optional_fields_schemas])
            - priority_set
        )
        serializer = self.get_serializer(
            self.queryset.filter(
                submission=submission, submission_metadata_object=metadata_object
            ).order_by(
                Case(
                    *[
                        When(field__slug=field, then=Value(index))
                        for field, index in priority_mapping.items()
                    ],
                    When(
                        field__slug__in=mandatory_set,
                        then=Value(len(priority_list)),
                    ),
                    When(
                        field__slug__in=recommended_set,
                        then=Value(len(priority_list) + 1),
                    ),
                    When(
                        field__slug__in=optional_set,
                        then=Value(len(priority_list) + 2),
                    ),
                    default=Value(len(priority_list) + 3),
                    output_field=IntegerField(),
                ),
                F("field").asc(),
            ),
            many=True,
            context={"request": request},
        )

        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: SubmissionMetadataSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, pk, submission_pk=None, *args, **kwargs):
        """Retrieve a given submission metadata item."""
        # Verify the workspace
        workspace = self.get_workspace()

        # Verify the submission
        submission = self.get_submission(workspace)

        # Verify the submission object Metadata
        metadata_object = self.get_the_object(
            SubmissionMetadataObject, kwargs["metadata_objects_pk"], "uri"
        )

        # Check the submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        # Check the metadata object is part of the workspace
        self.check_object_belongs_to_workspace(workspace, metadata_object.submission)

        # get the submission_data to retrieve
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        request=OpenApiTypes.OBJECT,
        examples=[
            OpenApiExample(
                "Default request body",
                value={
                    "metadata": [
                        {
                            "field": "slug_field",
                            "value": "field_value",
                        },
                        {
                            "field": "another_slug_field",
                            "value": "another_field_value",
                        },
                    ]
                },
                request_only=True,
            )
        ],
        responses={
            201: SubmissionMetadataSerializer,
            200: SubmissionMetadataSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow creation.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "metadata",
                    "code": "does_not_exist",
                    "message": "Could not find data: <metadata_id>.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, submission_pk=None, *args, **kwargs):
        """Create new submission metadata objects."""
        # Verify the workspace
        workspace = self.get_workspace()

        # Verify the submission
        submission = self.get_submission(workspace, self.action)

        # Check if the submission is in "generating metadata" status
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )
        # Verify the submission object Metadata
        metadata_object = self.get_the_object(
            SubmissionMetadataObject, kwargs["metadata_objects_pk"], "uri"
        )

        # Check the submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        # Check the metadata object is part of the workspace
        self.check_object_belongs_to_workspace(workspace, metadata_object.submission)

        # Validate the request payload
        serializer = SubmissionMetadataCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Extract validated metadata items
        metadata_items = serializer.validated_data["metadata"]

        submission_metadata_to_create = []
        existing_metadata = []

        for item in metadata_items:
            # Check if the metadata field already exists
            existing_item = SubmissionMetadata.objects.filter(
                submission=submission,
                submission_metadata_object=metadata_object,
                field=item["field"],
            ).first()

            if existing_item:
                # Add to the list of existing metadata
                existing_metadata.append(existing_item)
                continue

            submission_metadata_to_create.append(
                SubmissionMetadata(
                    submission=submission,
                    submission_metadata_object=metadata_object,
                    field=item["field"],
                    value=item["value"],
                    created_by=request.user,
                    last_edited_by=request.user,
                )
            )

        # Bulk create submission metadata if there are new items
        created_metadata = []
        if submission_metadata_to_create:
            created_metadata = SubmissionMetadata.objects.bulk_create(
                submission_metadata_to_create
            )

        # Combine newly created and existing metadata
        all_metadata = created_metadata + existing_metadata

        # Serialize the metadata objects for response
        response_serializer = SubmissionMetadataSerializer(
            all_metadata, many=True, context={"request": request}
        )

        # Determine response status based on whether new metadata was created
        response_status = (
            status.HTTP_201_CREATED if created_metadata else status.HTTP_200_OK
        )

        return Response(response_serializer.data, status=response_status)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "submission",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk, submission_pk=None, *args, **kwargs):
        """Delete a submission metadata item."""
        # Verify the workspace
        workspace = self.get_workspace()

        # Verify the submission
        submission = self.get_submission(workspace, self.action)

        # Check if the submission is in "generating metadata" status
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )
        # Verify the submission object Metadata
        metadata_object = self.get_the_object(
            SubmissionMetadataObject, kwargs["metadata_objects_pk"], "uri"
        )

        # Check the submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        # Check the metadata object is part of the workspace
        self.check_object_belongs_to_workspace(workspace, metadata_object.submission)

        # get the submission_data to delete
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        # Delete item
        item.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @extend_schema(
        request=OpenApiTypes.OBJECT,
        examples=[
            OpenApiExample(
                "Default request body",
                value={
                    "field": "slug_field",
                    "value": "field_value",
                },
                request_only=True,
            )
        ],
        responses={
            200: SubmissionMetadataSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "metadata",
                    "code": "does_not_exist",
                    "message": "Could not find data: <metadata_id>.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def update(self, request, submission_pk=None, *args, **kwargs):
        """Update the value of an existing submission metadata field."""
        # Verify the workspace
        workspace = self.get_workspace()

        # Verify the submission
        submission = self.get_submission(workspace, self.action)

        # Check if the submission is in "generating metadata" status
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )

        # Verify the submission metadata object
        metadata_object = self.get_the_object(
            SubmissionMetadataObject, kwargs["metadata_objects_pk"], "uri"
        )

        # Check the submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        # Check the metadata object is part of the workspace
        self.check_object_belongs_to_workspace(workspace, metadata_object.submission)

        # get the submission_data to update
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        # Validate the request payload
        serializer = SubmissionMetadataUpdateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Extract validated field and value
        new_value = serializer.validated_data["value"]
        source = serializer.validated_data.get("source", None)

        connector_class = connectors.get_connector_class(
            submission.connection.connector
        )

        # Get the schema from database
        try:
            schema = ConnectorRawSchema.objects.get(
                slug=item.field.schema.slug,
                source=connector_class.get_name().lower(),
            )
        except ConnectorRawSchema.DoesNotExist:
            raise ObjectNotFoundException(
                parameter="field",
                parameter_value=item.field.slug,
            )

        # Create registry with the schema from database
        def retrieve_from_db(uri: str):
            schema_slug = uri.split("/")[-1]
            try:
                db_schema = ConnectorRawSchema.objects.get(
                    slug=schema_slug,
                    source=connector_class.get_name().lower(),
                )
                contents = db_schema.schema
                contents["$schema"] = "https://json-schema.org/draft/2019-09/schema"
                return Resource.from_contents(contents)
            except ConnectorRawSchema.DoesNotExist:
                raise ObjectNotFoundException(
                    parameter="field",
                    parameter_value=schema_slug,
                )

        # Validate the value against the schema and set is_valid accordingly
        is_valid = True
        try:
            registry = Registry(retrieve=retrieve_from_db)
            validator = MadbotValidator(schema.schema, registry=registry)
            validator.validate(new_value)
        except ValidationError:
            is_valid = False

        # Get the field's level from the schema
        field_level = schema.schema.get("level")

        # Get current completeness values
        completeness = metadata_object.completeness

        # If validity status changed, update the completeness
        if item.is_valid != is_valid:
            if is_valid:
                # Field became valid, increment complete count
                completeness[field_level]["complete"] += 1
            else:
                # Field became invalid, decrement complete count
                completeness[field_level]["complete"] = max(
                    0, completeness[field_level]["complete"] - 1
                )

        # Update the value, validity status and last edited fields
        item.value_data = {
            "value": new_value,
            "source_metadata_id": source,
        }
        item.is_valid = is_valid
        item.last_edited_by = request.user
        item.save()

        # Update the metadata object's completeness
        metadata_object.completeness = completeness
        metadata_object.save()

        # Update the submission's status
        submission.check_submission_readiness()

        # Serialize the updated metadata entry for response
        response_serializer = SubmissionMetadataSerializer(
            item, context={"request": request}
        )

        return Response(response_serializer.data, status=status.HTTP_200_OK)
