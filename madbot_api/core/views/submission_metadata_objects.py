from drf_spectacular.utils import (
    OpenApiParameter,
    OpenApiTypes,
    extend_schema,
)
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core import connector as connectors
from madbot_api.core.errors import ObjectNotFoundException
from madbot_api.core.models import (
    SubmissionMetadataObject,
)
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
    GetObjectMixin,
    GetSubmissionMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.pagination import StandardPagination
from madbot_api.core.views.utils.schema_response import (
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class SubmissionMetadataObjectSerializer(MadbotSerializer):
    """Serializer for the submission metadata objects."""

    metadata = serializers.SerializerMethodField()
    summary_fields = serializers.SerializerMethodField()
    is_valid = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SubmissionMetadataObjectSerializer."""

        model = SubmissionMetadataObject
        fields = [
            "id",
            "submission",
            "summary_fields",
            "type",
            "metadata",
            "completeness",
            "cross_validation",
            "is_valid",
        ]
        read_only_fields = [
            "id",
            "submission",
            "summary_fields",
            "type",
            "metadata",
            "completeness",
            "cross_validation",
            "is_valid",
        ]

    def get_metadata(self, obj):
        """
        Return metadata information for an object, including the count and a related URL.

        Args:
        ----
        obj: The object for which metadata is being retrieved. Expected to have
             `submission` and `id` attributes.

        Returns:
        -------
        dict: A dictionary containing the metadata count and related URL.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}submission/{obj.submission.id}/metadata_objects/{obj.id}/metadata"
        return {
            "count": obj.submission.metadata.count(),
            "related": related_url,
        }

    def get_is_valid(self, obj):
        """
        Return a boolean for whether the metadata object is valid based on completeness and cross-validation fields.

        Args:
        ----
        obj: The object for which the validity is being determined.

        Returns:
        -------
        bool: True if valid, False otherwise.

        """
        # Check completeness
        mandatory_complete = obj.completeness.get("mandatory", {}).get("complete", 0)
        mandatory_total = obj.completeness.get("mandatory", {}).get("total", 0)

        # If mandatory fields are incomplete, it's invalid
        if mandatory_complete < mandatory_total:
            return False

        # Check cross-validation for errors
        if obj.cross_validation and len(obj.cross_validation) > 0:
            return False

        return True

    def get_summary_fields(self, obj):
        """
        Return summary information for an object.

        Args:
        ----
        obj: The object for which metadata is being retrieved. Expected to have
            `submission` and `id` attributes.

        Returns:
        -------
        dict: A dictionary containing the summary metadata.

        """
        connector = obj.submission.connection.connector
        connector_class = connectors.get_connector_class(connector)

        # Retrieve the connector's MetadataObjectType for this metadataObject
        metadata_object_type = connector_class.get_all_metadata_object_types().get(
            obj.type
        )

        # Return summary fields of the connector's MetadataObjectType
        if metadata_object_type:
            return metadata_object_type.summary_fields

        return []


########################################################################################
# ViewSets class
########################################################################################


class SubmissionMetadataObjectViewSet(
    MadbotViewSet,
    GetObjectMixin,
    WorkspaceHeaderMixin,
    GetSubmissionMixin,
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
):
    """ViewSet for handling the SubmissionMetadataObject."""

    queryset = SubmissionMetadataObject.objects.select_related("submission").all()

    serializer_class = SubmissionMetadataObjectSerializer
    pagination_class = StandardPagination

    @extend_schema(
        responses={
            200: SubmissionMetadataObjectSerializer,
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, submission_pk=None):
        """List the information related to all the submission metadata objects."""
        # verify the workspace
        workspace = self.get_workspace()
        # verify the submission
        submission = self.get_submission(workspace)

        # Check the requested data is part of the workspace
        self.check_object_belongs_to_workspace(workspace, submission)

        queryset = self.queryset.filter(submission=submission)

        type_filter = request.query_params.get("type", None)

        if type_filter:
            connector_class = connectors.get_connector_class(
                submission.connection.connector
            )
            if (
                type_filter
                not in connector_class.get_all_metadata_object_types().keys()
            ):
                raise ObjectNotFoundException(
                    parameter="type",
                    parameter_value=type_filter,
                )
            queryset = queryset.filter(type=type_filter)

        serializer = self.get_serializer(
            queryset,
            many=True,
            context={
                "request": request,
            },
        )

        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: SubmissionMetadataObjectSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(
        self,
        request,
        pk,
        submission_pk=None,
    ):
        """Retrieve a given submission metadata object."""
        # verify the workspace
        workspace = self.get_workspace()
        # verify the submission
        submission = self.get_submission(workspace)

        # get the submission_data to retrieve
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)
