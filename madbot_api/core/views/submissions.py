from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.exceptions import ErrorDetail
from rest_framework.response import Response

from madbot_api.core import connector
from madbot_api.core import connector as connectors
from madbot_api.core.errors import (
    ForbiddenException,
    ValidationException,
)
from madbot_api.core.models import (
    Connection,
    ConnectorRawSchema,
    Submission,
    SubmissionMember,
    SubmissionSettings,
    WorkspaceMember,
)
from madbot_api.core.tasks import async_create_metadata_objects, async_submit
from madbot_api.core.utils.websocket import notifier
from madbot_api.core.views.connections import ConnectionSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetGeneralMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_query_validation_error,
    cesr_400_uri_validation_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)


class ConnectionSubmissionSerializer(ConnectionSerializer):
    """Serializer for Connection objects that are associated with Submission objects."""

    connector_obj = serializers.SerializerMethodField()

    class Meta:
        """Meta class for ConnectionSubmissionSerializer."""

        model = ConnectionSerializer.Meta.model
        fields = ConnectionSerializer.Meta.fields

    def __init__(self, *args, **kwargs):
        """
        Initialize an object with the option to exclude specified fields.

        Args:
        ----
            *args: Positional arguments for the parent class initializer.
            **kwargs: Keyword arguments for the parent class initializer,
            with an option to specify `exclude_fields` to exclude certain fields
            from the serializer.

        """
        exclude_fields = kwargs.pop("exclude_fields", [])
        super().__init__(*args, **kwargs)

        # pop the excluded fields
        for field in exclude_fields:
            self.fields.pop(field, None)

    def get_connector_obj(self, obj):
        """
        Return a dictionary containing the ID and name of a connector object based on the input object.

        Args:
        ----
            obj: The parameter `obj` is an object that represents a connector in your system.
            This method retrieves a connector object based on the connector type specified in the
            `obj` parameter.

        Returns:
        -------
            A dictionary containing the "id" and "name" attributes of the connector object
            retrieved based on the connector type of the input object.

        """
        connector_obj = connector.get_connector_class(obj.connector)
        return {
            "id": connector_obj.get_id(),
            "name": connector_obj.get_name(),
        }


class SubmissionSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Submission objects."""

    connection_obj = serializers.SerializerMethodField()
    data = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()
    settings = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SubmissionSerializer."""

        model = Submission
        fields = [
            "id",
            "workspace",
            "title",
            "status",
            "data",
            "settings",
            "connection",
            "connection_obj",
            "last_failed_async_task",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "status",
            "connection_obj",
            "last_failed_async_task",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_settings(self, obj):
        """
        Return settings information for a submission, including the count and a related URL.

        Args:
        ----
            obj: The submission object.

        Returns:
        -------
            dict: A dictionary containing the settings count and related URL.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}submissions/{obj.id}/settings"
        return {
            "count": obj.settings.count(),
            "related": related_url,
        }

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)

    def get_data(self, obj):
        """
        Return information about the data related to a given object.

        This includes the count of data entries and a URL to access the related data.

        Args:
        ----
            obj: The parameter `obj` is an object representing a submission.
            The method constructs a URL for accessing data related to the submission object.

        Returns:
        -------
            A dictionary containing two key-value pairs:
            - "count": The count of data related to the object.
            - "related": The URL for accessing the related data.

        """
        base_url = self.context["request"].build_absolute_uri("/api/")
        related_url = f"{base_url}submissions/{obj.id}/data"
        return {
            "count": obj.data.count(),
            "related": related_url,
        }

    def get_connection_obj(self, obj):
        """
        Return serialized data for a connection object with context information.

        This method serializes the connection attribute of the input object using
        `ConnectionSubmissionSerializer`, including the request context.

        Args:
        ----
            obj: The parameter `obj` is likely an object that contains a connection attribute.
            This method uses a `ConnectionSerializer` to serialize the connection attribute
            of the `obj` object.

        Returns:
        -------
            The serialized data of the `connection` object within the `obj` parameter,
            with the context of the current request.

        """
        return ConnectionSubmissionSerializer(
            obj.connection,
            context={"request": self.context["request"]},
            exclude_fields=["workspace"],
        ).data

    def to_representation(self, instance):
        """
        Modify the representation dictionary by renaming the key "connection_obj" to "connection".

        This method customizes how the model instance is represented in a Python data structure
        before being converted into a JSON response.

        Args:
        ----
            instance: The `instance` parameter represents the model object being serialized.

        Returns:
        -------
            A modified representation of the instance with the key "connection_obj" renamed
            to "connection".

        """
        representation = super().to_representation(instance)
        representation["connection"] = representation.pop("connection_obj")
        return representation


class SubmissionUpdateSerializer(SubmissionSerializer):
    """Serializer for updating submission data."""

    class Meta:
        """Meta class for SubmissionUpdateSerializer."""

        model = Submission
        fields = ["title"]
        read_only_fields = [
            "id",
            "workspace",
            "status",
            "data",
            "connection",
            "connection_obj",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]


class SubmissionViewSet(
    MadbotViewSet,
    GetObjectMixin,
    WorkspaceHeaderMixin,
    BelongsToWorkspaceMixin,
):
    """Viewset for Submission objects."""

    serializer_class = SubmissionSerializer
    queryset = Submission.objects.all().select_related(
        "created_by",
        "last_edited_by",
        "connection",
        "workspace",
    )

    @extend_schema(
        responses={
            200: SubmissionSerializer,
            **cesr_400_query_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        """List the information related to all the submission objects."""
        # Retrieve the workspace from the request header
        workspace = self.get_workspace()

        # if the user is owner of the workspace, return all submissions
        if WorkspaceMember.objects.filter(
            workspace=workspace,
            user=request.user,
            role="owner",
        ).exists():
            queryset = self.get_queryset().filter(workspace=workspace)
        # else, filer out the ones where the user is not member
        else:
            queryset = (
                self.get_queryset()
                .filter(workspace=workspace)
                .filter(submission_members__user=request.user)
            )

        # Serialize the filtered submissions
        serializer = self.get_serializer(queryset, many=True)

        # Paginate and return the response
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            201: SubmissionSerializer,
            200: SubmissionSerializer,
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "connection",
                        "code": "invalid",
                        "message": '"<parameter_value>" is not a valid UUID.',
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "connection",
                        "message": "This field is required",
                        "code": "required",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "settings",
                        "message": "This field is required",
                        "code": "required",
                    },
                ],
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, *args, **kwargs):
        """Create a new Submission object."""
        workspace = self.get_workspace()

        # Check that the settings parameter is a valid JSON object
        if not request.data.get("settings"):
            request.data["settings"] = {}
        elif not isinstance(request.data["settings"], dict):
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid_format",
                parameter="settings",
                parameter_value="JSON object",
            )

        # Serialize data
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()

        # Get the connection object
        connection = self.get_the_object(
            Connection,
            request.data["connection"],
            context="body",
        )
        # check that the connection object is from the correct type of connectors
        connector_list = [
            f"{connector.__module__}.{connector.__name__}"
            for connector in connectors.get_connectors()
            if "submission_connector" in connector.get_types()
        ]
        if connection.connector not in connector_list:
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid",
                parameter="connection",
                parameter_value=f"{request.data['connection']} connection is not from a submission connector",
            )

        # Prepare to collect errors
        errors = []

        # Create the submission object
        submission = serializer.save(
            status=Submission.STATUS_GENERATING_METADATA,
            created_by=request.user,
            last_edited_by=request.user,
            workspace=workspace,
        )

        # Create SubmissionSettings for provided settings in the request
        for field, value in request.data.get("settings", {}).items():
            try:
                SubmissionSettings.objects.create(
                    submission=submission,
                    field=field,
                    value=value,
                    created_by=request.user,
                    last_edited_by=request.user,
                )
            except Exception as e:
                errors.append(
                    ErrorDetail(
                        f"Error creating submission settings for {field}: {str(e)}",
                        code="invalid",
                    )
                )

        # Create SubmissionSettings for missing required fields from the connector schema
        connector_class = connectors.get_connector_class(connection.connector)
        required_settings = connector_class.get_settings_fields()
        base_url = request.build_absolute_uri("/api/")
        connector_settings = []
        for required_setting in required_settings:
            schema = ConnectorRawSchema.objects.get(
                slug=required_setting.slug,
                source=connector_class.get_name().lower(),
            )
            connector_settings.append(
                f"{base_url}schemas/{schema.source}/{schema.slug}"
            )

        for setting in connector_settings:
            if setting.split("/")[-1] not in request.data["settings"]:
                try:
                    SubmissionSettings.objects.create(
                        submission=submission,
                        field=setting.split("/")[-1],
                        value=None,
                        created_by=request.user,
                        last_edited_by=request.user,
                    )
                except Exception as e:
                    errors.append(
                        ErrorDetail(
                            f"Error creating default submission settings for {setting}: {str(e)}",
                            code="invalid",
                        )
                    )

        if errors:
            submission.delete()
            raise ValidationException(code="body_validation_error", details=errors)

        async_create_metadata_objects.delay(submission.id, request.user.id)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: SubmissionSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "submission",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, pk):
        """Retrieve a submission item's information."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check the requested submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        # Check if the user is a member of the submission, or owner of the workspace
        if (
            not SubmissionMember.objects.filter(
                submission=item,
                user=request.user,
            ).exists()
            and not WorkspaceMember.objects.filter(
                workspace=workspace,
                user=request.user,
                role="owner",
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            200: SubmissionSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "submission",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def partial_update(self, request, pk):
        """Update a submission item."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check if the submission is generating metadata
        if item.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )

        # Check the requested submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        # Check if the user is a member of the submission
        if not SubmissionMember.objects.filter(
            submission=item,
            user=request.user,
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

        # Check if the user's role allows update
        if not SubmissionMember.objects.filter(
            submission=item,
            user=request.user,
            role__in=["owner", "data broker", "contributor"],
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            )

        serializer = SubmissionUpdateSerializer(item, data=request.data, partial=True)
        serializer.is_valid()
        # save the updated submission
        serializer.save(last_edited_by=request.user)

        # Serialize the updated submission entry for response
        response_serializer = self.get_serializer(item, context={"request": request})
        return Response(response_serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "submission",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, pk):
        """Delete a Submission item."""
        workspace = self.get_workspace()
        item = self.get_object()

        # Check if the submission is generating metadata
        if item.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )

        # Check the requested submission is part of the workspace
        self.check_object_belongs_to_workspace(workspace, item)

        # Check if the user is a member of the submission
        if not SubmissionMember.objects.filter(
            submission=item,
            user=request.user,
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

        # Check if the user's role allows deletion
        if not SubmissionMember.objects.filter(
            submission=item,
            user=request.user,
            role__in=["owner", "manager"],
        ).exists():
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )

        # Delete item
        item.delete()

        # TODO: trigger a notification

        return Response(status=status.HTTP_204_NO_CONTENT)


class SubmissionActionSerializer(serializers.Serializer):
    """Serializer for Submission actions."""

    action = serializers.ChoiceField(
        choices=[("generate_metadata", "Generate Metadata"), ("submit", "Submit")]
    )


class SubmissionActionViewSet(
    MadbotViewSet, GetGeneralMixin, WorkspaceHeaderMixin, BelongsToWorkspaceMixin
):
    """Viewset for handling actions on Submission objects."""

    def generate_metadata(self, submission, request):
        """Generate metadata for a submission item."""
        # Trigger metadata generation
        submission.status = Submission.STATUS_GENERATING_METADATA
        submission.save()

        # Send WebSocket message to notify clients about submission update
        notifier.sync_update(
            model_type="submission",
            object_id=submission.id,
            action="update",
            source="submission",
        )

        async_create_metadata_objects.delay(submission.id, request.user.id)

        # Serialize the updated submission entry for response
        response_serializer = SubmissionSerializer(
            submission, context={"request": request}
        )
        return Response(response_serializer.data, status=status.HTTP_200_OK)

    def submit(self, submission, request):
        """Submit a submission item."""
        submission.status = Submission.STATUS_SUBMISSION_IN_PROGRESS
        submission.save()

        notifier.sync_update(
            model_type="submission",
            object_id=submission.id,
            action="update",
            source="submission",
        )

        async_submit.delay(submission.id, request.user.id)

        response_serializer = SubmissionSerializer(
            submission, context={"request": request}
        )
        return Response(response_serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        request=SubmissionActionSerializer,
        responses={
            200: SubmissionSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "submission",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def create(self, request, pk):
        """Handle actions on a submission item."""
        serializer = SubmissionActionSerializer(data=request.data)

        workspace = self.get_workspace()
        submission = self.check_and_get_object(
            workspace,
            Submission,
            pk,
            "uri",
        )

        # Check if the submission is generating metadata
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )

        serializer.is_valid(raise_exception=True)
        action = serializer.validated_data["action"]

        if action == "generate_metadata":
            return self.generate_metadata(submission, request)
        if action == "submit":
            return self.submit(submission, request)

        # else, return a 400 error
        raise ValidationException(
            code="body_validation_error",
            details_code="invalid",
            parameter="action",
            parameter_value=action,
        )
