from drf_spectacular.utils import (
    OpenApiExample,
    OpenApiParameter,
    OpenApiTypes,
    extend_schema,
)
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import (
    Submission,
    SubmissionSettings,
)
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.mixin import (
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
    GetObjectMixin,
    GetSubmissionMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.schema_response import (
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)


class SubmissionSettingsSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for Submission settings objects."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for SubmissionSettingsSerializer."""

        model = SubmissionSettings
        fields = [
            "id",
            "submission",
            "field",
            "value",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "submission",
            "field",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)


class SubmissionSettingsUpdateSerializer(serializers.Serializer):
    """Serializer for updating submission settings."""

    value = serializers.CharField(required=True)


########################################################################################
# ViewSets class
########################################################################################


class SubmissionSettingsViewSet(
    MadbotViewSet,
    GetObjectMixin,
    WorkspaceHeaderMixin,
    GetSubmissionMixin,
    BelongsToObjectMixin,
    BelongsToWorkspaceMixin,
):
    """ViewSet for handling the settings associations."""

    serializer_class = SubmissionSettingsSerializer
    queryset = SubmissionSettings.objects.select_related("submission").all()

    @extend_schema(
        responses={
            200: SubmissionSettingsSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, submission_pk=None):
        """List the information related to all the submission settings objects."""
        workspace = self.get_workspace()
        # verify the submission
        submission = self.get_submission(workspace)

        serializer = self.get_serializer(
            self.queryset.filter(submission=submission),
            many=True,
            context={
                "request": request,
            },
        )

        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            200: SubmissionSettingsSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(
        self,
        request,
        pk,
        submission_pk=None,
    ):
        """Retrieve a given submission settings object."""
        # verify the workspace
        workspace = self.get_workspace()
        # verify the submission
        submission = self.get_submission(workspace)

        # get the submission settings to retrieve
        item = self.get_object()
        self.check_object_belongs_to_submission(submission, item)

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        request=OpenApiTypes.OBJECT,
        examples=[
            OpenApiExample(
                "Default request body",
                value={"value": "value of the settings"},
                request_only=True,
            )
        ],
        responses={
            200: SubmissionSettingsSerializer,
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            ),
            **cesr_404_object_not_found(),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def update(self, request, pk, submission_pk=None):
        """Update a submission settings."""
        workspace = self.get_workspace()
        # verify the submission
        submission = self.get_submission(workspace, self.action)

        # Check if the submission is in "generating metadata" status
        if submission.status == Submission.STATUS_GENERATING_METADATA:
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "The submission is currently generating metadata, and no operations are allowed during this process.",
                },
            )
        # get the submission_settings to retrieve
        item = self.get_object()

        # Check the settings object is part of the workspace
        self.check_object_belongs_to_submission(submission, item)

        # Validate the request payload
        update_serializer = SubmissionSettingsUpdateSerializer(data=request.data)
        update_serializer.is_valid(raise_exception=True)

        # Extract settings value
        item.value = update_serializer.validated_data["value"]
        item.last_edited_by = request.user

        # start validation on save() method
        item.save()

        # Serialize the updated metadata entry for response
        response_serializer = self.get_serializer(item)
        return Response(response_serializer.data, status=status.HTTP_200_OK)
