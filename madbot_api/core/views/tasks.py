import json
import uuid

from celery import current_app
from django.apps import apps
from django.db.models import Q
from django_celery_results.models import TaskResult
from drf_spectacular.utils import OpenApiParameter, OpenApiTypes, extend_schema
from rest_framework import serializers, status
from rest_framework.exceptions import ErrorDetail
from rest_framework.response import Response

from madbot_api.core.errors import (
    ForbiddenException,
    ObjectNotFoundException,
    ValidationException,
)
from madbot_api.core.models import NodeMember, SubmissionMember
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.exception_handler import custom_get_object_or_404
from madbot_api.core.views.utils.mixin import (
    BelongsToWorkspaceMixin,
    GetGeneralMixin,
    GetObjectMixin,
    WorkspaceHeaderMixin,
)
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_403_forbidden,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

# https://django-celery-results.readthedocs.io/en/latest/reference/django_celery_results.models.html#django_celery_results.models.TaskResult

OBJECT_TYPE = ["submission", "data", "node"]


def get_all_objects_workspace(id, user):
    """
    Construct a query to retrieve all relevant objects associated with a given workspace.

    This function builds a query using Django's Q objects that includes nodes, submissions,
    credentials, and data links associated with the specified workspace ID and user.

    Args:
    ----
        id (int): The ID of the workspace for which to retrieve associated objects.
        user (User): The user object representing the current user, used to filter
                     associated objects.

    Returns:
    -------
        Q: A Q object representing the combined query for tasks that match the specified
           workspace ID and user, including:
           - Nodes associated with the workspace and user.
           - Submissions associated with the workspace and user.
           - Credentials associated with the workspace and user.
           - Datalinks associated with nodes in the workspace and user.

    """
    query = Q()

    # Nodes
    for item in list(
        NodeMember.objects.filter(user=user, node__workspace__id=id).values_list(
            "node__id",
            flat=True,
        ),
    ):
        query |= Q(task_kwargs__contains="'node', 'id': '" + str(item) + "'")

    # Submission
    for item in list(
        SubmissionMember.objects.filter(
            user=user,
            submission__workspace__id=id,
        ).values_list("submission__id", flat=True),
    ):
        query |= Q(task_kwargs__contains="'submission', 'id': '" + str(item) + "'")

    # Connexion
    # for item in list(
    #     Credential.objects.filter(user=user, connection__workspace__id=id).values_list(
    #         "id", flat=True
    #     )
    # ):
    #     query |= Q(task_kwargs__contains="'credential', 'id': '" + str(item) + "'")

    # Data
    for item in list(
        NodeMember.objects.filter(user=user, node__workspace__id=id)
        .exclude(node__datalinks__isnull=True)
        .values_list("node__datalinks__id", flat=True),
    ):
        query |= Q(task_kwargs__contains="'data', 'id': '" + str(item) + "'")

    return query


class TaskSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for handling task-related data."""

    task_kwargs = serializers.JSONField(read_only=True)

    class Meta:
        """Meta class for TaskSerializer."""

        model = TaskResult
        fields = [
            "date_created",
            "date_done",
            "status",
            "task_id",
            "task_name",
            "task_kwargs",
        ]
        read_only_fields = fields

    def to_representation(self, instance):
        """
        Modify the representation of an instance by converting task_kwargs to a dictionary.

        The method retrieves the original representation of the instance and checks if
        the `task_kwargs` is a string. If it is, the string is cleaned and converted
        to a dictionary. If the `task_kwargs` is now a dictionary, its key-value pairs
        are added to the representation, replacing the original `task_kwargs`.

        Args:
        ----
            instance: The instance of the model being serialized.

        Returns:
        -------
            A dictionary representation of the instance, with task_kwargs processed
            and integrated into the representation.

        """
        representation = super().to_representation(instance)

        # Check if task_kwargs is a string and convert it to a dict if necessary
        task_kwargs_str = representation.get("task_kwargs")
        if isinstance(task_kwargs_str, str):
            # First, strip away outer quotes and replace single quotes with double quotes
            cleaned_str = task_kwargs_str.strip('"').replace("'", '"')
            task_kwargs = json.loads(cleaned_str)
            representation["task_kwargs"] = task_kwargs

        # Pop task_kwargs if it's now a dict and update the representation
        task_kwargs = representation.pop("task_kwargs", {})
        if isinstance(task_kwargs, dict):
            representation.update(task_kwargs)

        return representation

    def validate(self, data):
        """Validate the input data for required fields and correct formats."""
        missing_fields = [
            field
            for field in ["action", "object", "id"]
            if field not in self.initial_data
        ]
        if missing_fields:
            raise ValidationException(
                code="query_validation_error",
                details={
                    field_name: [
                        ErrorDetail("This field is required.", code="required"),
                    ]
                    for field_name in missing_fields
                },
            )
        if not isinstance(self.initial_data.get("action"), str):
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid_format",
                parameter="action",
                parameter_value="string",
            )

        if (
            not isinstance(self.initial_data.get("object"), str)
            or self.initial_data.get("object") not in OBJECT_TYPE
        ):
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid_choice",
                parameter="object",
                parameter_value=self.initial_data.get("object"),
            )

        try:
            uuid.UUID(str(self.initial_data.get("id")))
        except ValueError:
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid_uuid",
                parameter="id",
                parameter_value=self.initial_data.get("id"),
            )

        return self.initial_data


class TaskViewSet(
    MadbotViewSet,
    GetObjectMixin,
    WorkspaceHeaderMixin,
    BelongsToWorkspaceMixin,
    GetGeneralMixin,
):
    """ViewSet for handling task-related operations."""

    queryset = TaskResult.objects.all().order_by("date_created")
    serializer_class = TaskSerializer
    lookup_field = "task_id"

    @extend_schema(
        responses={
            200: TaskSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "object",
                    "code": "not_allowed",
                    "message": "You are not a member of this object.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def list(self, request, *args, **kwargs):
        """Retrieve the list of tasks in a specific workspace."""
        # Retrieve the workspace from the request header and check if the user is a member of this workspace
        workspace = self.get_workspace()

        pattern = "'workspace': " + "'" + str(workspace.id) + "'"
        queryset = self.get_queryset().filter(task_kwargs__contains=pattern)

        if not self._is_workspace_owner(workspace):
            query = get_all_objects_workspace(workspace.id, request.user)

            if len(query) != 0:
                queryset = queryset.filter(query)
            else:
                queryset = TaskResult.objects.none()

        # Define a mapping of query parameters to task keyword arguments
        filter_map = {
            "submission": "'submission', 'id': '{}'",
            "node": "'node', 'id': '{}'",
            "data": "'data', 'id': '{}'",
        }

        # Apply additional filters based on query parameters
        for param, filter_template in filter_map.items():
            param_value = request.query_params.get(param)
            if param_value:
                queryset = queryset.filter(
                    task_kwargs__contains=filter_template.format(param_value),
                )

        # Serialize the filtered tasks
        serializer = self.get_serializer(
            queryset,
            many=True,
        )

        # Paginate and return the response
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    # @extend_schema(
    #     responses={
    #         201: TaskSerializer,
    #         **cesr_404_body_validation_error(
    #             detail={
    #                 "parameter": "<parameter>",
    #                 "code": "required",
    #                 "message": "This field is required.",
    #             },
    #         ),
    #         **cesr_404_body_validation_error(
    #             detail={
    #                 "parameter": "<parameter_name>",
    #                 "code": "does_not_exist",
    #                 "message": '"<parameter_value>" does not exist.',
    #             },
    #         ),
    #         **cesr_404_body_validation_error(
    #             detail={
    #                 "parameter": "<parameter>",
    #                 "code": "not_supported",
    #                 "message": "This field is not supported.",
    #             },
    #         ),
    #         **cesr_404_forbidden(
    #             detail={
    #                 "parameter": "object",
    #                 "code": "not_allowed",
    #                 "message": "You are not a member of this object.",
    #             },
    #         ),
    #         **cesr_404_body_validation_error(
    #             detail={
    #                 "parameter": "<parameter>",
    #                 "code": "not_supported",
    #                 "message": "This field is not supported.",
    #             },
    #         ),
    #     },
    #     parameters=[
    #         OpenApiParameter(
    #             name="X-Workspace",
    #             location=OpenApiParameter.HEADER,
    #             description="Workspace ID",
    #             required=True,
    #             type=OpenApiTypes.UUID,
    #         ),
    #     ],
    # )
    # def create(self, request, *args, **kwargs):
    #     """Create a tasks in a specific workspace."""
    #     # Retrieve the workspace and check if the user is a member of this workspace
    #     workspace = self.get_workspace()

    #     # Validate incoming data with the serializer
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)

    #     # Validate task name
    #     task_name = request.data["object"] + "_" + request.data["action"]
    #     if task_name not in current_app.tasks.keys():
    #         raise ValidationException(
    #             code="validation_error",
    #             details_code="doest_not_exist",
    #             parameter="task_name",
    #             parameter_value=task_name,
    #         )

    #     # Verify if the target object exists and the user is a member of this object
    #     target_object = self.check_and_get_object(
    #         workspace,
    #         apps.get_model("core", str(request.data["object"]).capitalize()),
    #         request.data.get("id"),
    #         "body",
    #     )

    #     # Check if object (submission, node, data,...) is part of the workspace
    #     self.check_object_belongs_to_workspace(workspace, target_object)

    #     # Prepare kwargs to send with the task
    #     task_kwargs = {
    #         "workspace": str(workspace.id),
    #         "created_by": {"object": "user", "id": str(request.user.id)},
    #         "target": {
    #             "object": request.data["object"],
    #             "id": str(request.data["id"]),
    #         },
    #     }

    #     # Send the task to Celery
    #     res = current_app.send_task(
    #         task_name,
    #         args=[request.data["id"]],
    #         kwargs=task_kwargs,
    #     )

    #     serializer = self.get_serializer(
    #         {
    #             "date_created": res.date_done,
    #             "date_done": "",
    #             "status": res.status,
    #             "task_id": res.task_id,
    #             "task_name": request.data["object"] + "_" + request.data["action"],
    #             "task_kwargs": task_kwargs,
    #         },
    #     )
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: TaskSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "object",
                    "code": "not_allowed",
                    "message": "You are not a member of this object.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def retrieve(self, request, task_id):
        """Retrieve a task item's information."""
        # Retrieve the workspace and check if the user is a member of this workspace
        workspace = self.get_workspace()
        pattern = "'workspace': " + "'" + str(workspace.id) + "'"
        item = custom_get_object_or_404(
            model=TaskResult,
            context="uri",
            task_id=task_id,
        )

        # Check if task is part of the workspace
        if item.task_kwargs.find(pattern) == -1:
            raise ObjectNotFoundException(parameter="task", parameter_value=task_id)

        cleaned_str = item.task_kwargs.strip('"').replace("'", '"')
        task_kwargs = json.loads(cleaned_str)

        # Check if user is owner of associated target object.
        self.check_and_get_object(
            workspace,
            apps.get_model("core", str(task_kwargs["target"]["object"]).capitalize()),
            str(task_kwargs["target"]["id"]),
            "body",
        )

        serializer = self.get_serializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            200: TaskSerializer,
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "object",
                    "code": "not_allowed",
                    "message": "You are not a member of this object.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "user",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            ),
        },
        parameters=[
            OpenApiParameter(
                name="X-Workspace",
                location=OpenApiParameter.HEADER,
                description="Workspace ID",
                required=True,
                type=OpenApiTypes.UUID,
            ),
        ],
    )
    def destroy(self, request, task_id):
        """Delete an task item."""
        # Retrieve the workspace and check if the user is a member of this workspace
        workspace = self.get_workspace()
        pattern = "'workspace': " + "'" + str(workspace.id) + "'"
        item = custom_get_object_or_404(model=TaskResult, task_id=task_id)

        # Check if task is part of the workspace
        if item.task_kwargs.find(pattern) == -1:
            raise ObjectNotFoundException(parameter="task", parameter_value=task_id)

        # check if user can delete item
        # TODO: Add a check if user is owner of associated target object.
        # ie : I'm owner of a node, i'm want to stop/delete a task started by another node member
        if (
            not self._is_workspace_owner(workspace)
            and item.task_kwargs.find(
                "'user', 'id': " + "'" + str(request.user.id) + "'",
            )
            == -1
        ):
            raise ForbiddenException(
                details={
                    "parameter": "user",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )

        # Stop the task (and delete automatically by celery after a time specify in the settings)
        current_app.control.revoke(task_id, terminate=True)
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
