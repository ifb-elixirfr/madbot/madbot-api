import base64
import logging

import hvac
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.signals import user_logged_out
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from py_jwt_verifier import PyJwtException, PyJwtVerifier
from rest_framework import generics, permissions, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api.core.serializers.user import LDAPLoginSerializer, UserSerializer

User = get_user_model()
logger = logging.getLogger(__name__)


class UserAPI(generics.RetrieveAPIView):
    """Retrieving the currently authenticated user's details."""

    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserSerializer

    def get_user(self):
        """
        Retrieve the currently authenticated user.

        This method accesses the request object to return the user instance
        that is currently authenticated in the session.

        Returns
        -------
            The currently authenticated user instance.

        """
        return self.request.user


def get_credentials_from_base64(request):
    """
    Extract credentials from the Authorization header in Base64 format.

    The function checks for the presence of the `HTTP_AUTHORIZATION` key
    in the request's META data. If it exists and is formatted correctly,
    it decodes the Base64 encoded credentials to extract the username and
    password.

    Args:
    ----
        request: The HTTP request object containing the Authorization header.

    Returns:
    -------
        A dictionary containing the extracted "username" and "password" if
        credentials are found; otherwise, returns None.

    """
    if "HTTP_AUTHORIZATION" in request.META:
        auth = request.META["HTTP_AUTHORIZATION"].split()
        if len(auth) == 2:
            if auth[0].lower() == "basic":
                data = base64.b64decode(auth[1])
                username, password = data.decode("utf-8").split(":")
                return {"username": username, "password": password}
    return None


class TokensView(generics.GenericAPIView):
    """
    Provides access to the tokens in the database.

    This method processes authentication requests based on the configured
    authentication backend. It either authenticates users through LDAP
    using credentials extracted from the request's Authorization header,
    or it verifies JWT tokens for user authentication.
    """

    authentication_classes = []

    def post(self, request, *args, **kwargs):
        """Handle user authentication via LDAP or JWT."""
        user = None

        if settings.MADBOT_AUTH_BACKEND == "LDAP":
            data = get_credentials_from_base64(request)
            serializer = LDAPLoginSerializer(data=data)
            serializer.is_valid()
            user = serializer.validated_data
            if user and user.is_active:
                user.save()
            else:
                return Response(
                    {
                        "Login Error": "The provided Login and Password has been rejected",
                    },
                )

        elif settings.MADBOT_AUTH_BACKEND == "JWT":
            # retrieve JWT from authorization header
            authorization_header = request.headers.get("Authorization")
            authorization_parts = authorization_header.split(" ")
            if len(authorization_parts) != 2 or authorization_parts[0] != "Token":
                return Response(
                    {"Authentication error": "invalid authorization header"},
                )

            # check JWT
            jwt = authorization_parts[1]
            validator = PyJwtVerifier(jwt, auto_verify=False, cache_enabled=False)
            try:
                payload = validator.verify(True)
            except PyJwtException as e:
                raise e

            # retrieve or create associated user
            try:
                user = User.objects.get(email=payload["payload"]["email"])
            except User.DoesNotExist:
                user = User(username=payload["payload"]["preferred_username"])
                if "family_name" in payload["payload"]:
                    user.last_name = payload["payload"]["family_name"]
                else:
                    user.last_name = payload["payload"]["name"].split(" ")[-1]
                if "given_name" in payload["payload"]:
                    user.first_name = payload["payload"]["given_name"]
                else:
                    user.first_name = payload["payload"]["name"].split(" ")[0]
                user.email = payload["payload"]["email"]
                user.is_staff = False
                user.is_superuser = False
                user.save()

        else:
            return Response(
                {"Auth Error": "Auth type is not define in madbot configuration"},
            )

        if user:
            knox_instance, knox_token = AuthToken.objects.create(user=user)
            return Response(
                {
                    "expiry": knox_instance.expiry,
                    "token": knox_token,
                    "user": UserSerializer(
                        user,
                        context=self.get_serializer_context(),
                    ).data,
                },
            )

        return Response({"Auth Error": "Authentication error"})


class LogoutAPI(APIView):
    """Logout user and delete JWT token."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        """
        Log out the user and revoke the Vault token if present.

        This method handles user logout by revoking the Vault token stored in
        the session, flushing the session, and sending a logout signal.
        It ensures that all user-related session data is cleared.

        """
        if "vault_token" in request.session:
            try:
                vault_client = hvac.Client(
                    url=settings.MADBOT_VAULT_URL,
                    token=request.session["vault_token"],
                )
                vault_client.logout(revoke_token=True)
                logger.info("Vault token revoked successfully")

            except Exception as e:
                logger.error("Couldn't revoke Vault token: ", {e})

        request.session.flush()
        request._auth.delete()
        user_logged_out.send(
            sender=request.user.__class__,
            request=request,
            user=request.user,
        )
        return Response(None, status=status.HTTP_204_NO_CONTENT)
