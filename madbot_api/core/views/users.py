from allauth.account.signals import user_logged_in
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.core.mail import EmailMessage, send_mail
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import get_object_or_404, redirect, render
from drf_spectacular.utils import extend_schema
from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication
from oauth2_provider.contrib.rest_framework.permissions import (
    TokenMatchesOASRequirements,
)
from oauth2_provider.models import Application
from rest_framework import generics, permissions, status, viewsets
from rest_framework.response import Response

from madbot_api.core.errors import ValidationException
from madbot_api.core.models import ApplicationUserParam, MadbotUser
from madbot_api.core.serializers.user import (
    ApplicationUserParamSerializer,
    UserSerializer,
)
from madbot_api.core.views.utils.permissions import method_permission_classes
from madbot_api.core.views.utils.users import validate_json


@extend_schema(
    description="Get the list of users",
)
class SearchUsers(generics.ListAPIView):
    """Retrieve a list of users based on search parameters."""

    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = [OAuth2Authentication]
    serializer_class = UserSerializer

    def get_queryset(self):
        """
        Return the queryset of User objects based on the search parameter.

        If a search parameter is provided, the method filters the queryset based on the following criteria:
        - If the search value matches the username, email, or full name of a user, the user is included in the queryset.

        Returns
        -------
            queryset (QuerySet): The filtered queryset of User objects.

        """
        queryset = MadbotUser.objects.all()
        search = self.request.query_params.get("search")
        if search is not None:
            query = Q()
            # if value is username
            query |= Q(username__icontains=search)
            # if value is email
            query |= Q(email__icontains=search)
            # if value is fullname
            names = search.split()
            name_query = Q()
            for name in names:
                name_query &= Q(first_name__icontains=name) | Q(
                    last_name__icontains=name,
                )
            query |= name_query
            queryset = queryset.filter(query)
        return queryset


class ApplicationUserParamView(viewsets.ModelViewSet):
    """Handle application user parameter views."""

    authentication_classes = [OAuth2Authentication]
    permission_classes = [TokenMatchesOASRequirements]
    required_alternate_scopes = {
        "GET": [["read"]],
        "POST": [["write"]],
        "PUT": [["write"]],
        "DELETE": [["write"]],
    }

    @method_permission_classes([permissions.IsAuthenticated])
    def get_application_user_parameters(self, request, *args, **kwargs):
        """
        Retrieve application user parameters for a specified application.

        Args:
        ----
            request: The request object.
            *args: Additional positional arguments.
            **kwargs: Additional keyword arguments including `application_name`.

        Returns:
        -------
            Response: The serialized application user parameters if found; otherwise, an empty response.

        """
        app = get_object_or_404(Application, name=kwargs["application_name"])
        try:
            app_user_parms = ApplicationUserParam.objects.get(
                user=request.user,
                content_type=ContentType.objects.get_for_model(Application),
                object_id=app.pk,
            )
            serializer = ApplicationUserParamSerializer(app_user_parms)
            return Response(serializer.data)
        except ApplicationUserParam.DoesNotExist:
            return Response({})

    @method_permission_classes([permissions.IsAuthenticated])
    def update_application_user_parameters(self, request, *args, **kwargs):
        """
        Update application user parameters for a specified application.

        Args:
        ----
            request: The request object containing parameters to update.
            *args: Additional positional arguments.
            **kwargs: Additional keyword arguments including `application_name`.

        Returns:
        -------
            Response: A response indicating the status of the update operation.

        """
        if "params" not in request.data:
            raise ValidationException(
                code="body_validation_error",
                details_code="required",
                parameter="params",
            )

        if not validate_json(request.data["params"]):
            raise ValidationException(
                code="body_validation_error",
                details_code="invalid_format",
                parameter="params",
                parameter_value="JSON",
            )

        app = get_object_or_404(Application, name=kwargs["application_name"])

        try:
            app_user_parms = ApplicationUserParam.objects.get(
                user=request.user,
                content_type=ContentType.objects.get_for_model(Application),
                object_id=app.pk,
            )

            serializer = ApplicationUserParamSerializer(
                app_user_parms,
                data=request.data,
            )

            if serializer.is_valid():
                serializer.save()
                return Response(
                    {
                        "code": "update_done",
                        "message": "Application user parameters have been updated",
                    },
                    status=status.HTTP_200_OK,
                )
            raise ValidationException(
                code="body_validation_error",
                details=serializer.errors,
            )
        except ApplicationUserParam.DoesNotExist:
            ApplicationUserParam.objects.create(
                user=request.user,
                content_object=app,
                params=request.data["params"],
            )
            return Response(
                {
                    "code": "update_done",
                    "message": "Application user parameters have been updated",
                },
                status=status.HTTP_200_OK,
            )


class UpdateUserForm(forms.ModelForm):
    """Define the form for updating user profiles."""

    first_name = forms.CharField(
        max_length=150,
        required=True,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    last_name = forms.CharField(
        max_length=150,
        required=True,
        widget=forms.TextInput(attrs={"class": "form-control"}),
    )

    class Meta:
        """Meta class for UpdateUserForm."""

        model = MadbotUser
        fields = ["first_name", "last_name"]


@login_required
def profile(request):
    """
    Manage the user's profile update process.

    Args:
    ----
        request: The HTTP request object.

    Returns:
    -------
        HttpResponse: The rendered profile page with user form.

    """
    if request.method == "POST":
        user_form = UpdateUserForm(request.POST, instance=request.user)
        if user_form.is_valid():
            user_form.save()
            messages.success(request, "Your profile is updated successfully")
            return redirect(to="users-profile")
    else:
        user_form = UpdateUserForm(instance=request.user)

    return render(
        request,
        "profile.html",
        {"user_form": user_form},
    )


@receiver(user_logged_in)
def check_user_approval(sender, request, user, **kwargs):
    """
    Log out the user and set a session error message if the user's account is not approved by an administrator.

    This function is triggered when a user logs in. If the user is not approved,
    the function logs them out and stores a session message indicating that
    the account needs approval before they can access the application.

    Args:
    ----
        sender (type[User]): The model class that triggered the signal (User).
        request (HttpRequest): The request object that initiated the login event.
        user (User): The user instance attempting to log in.
        **kwargs (dict): Additional keyword arguments (unused in this function).

    Sets:
    -----
        request.session["approval_error"]: A message informing the user that their account is pending approval.

    """
    if not user.is_approved:
        logout(request)
        request.session["approval_error"] = (
            "Your account must be approved by an administrator before you can log in."
        )


@receiver(post_save, sender=MadbotUser)
def send_approval_email(sender, instance, created, **kwargs):
    """
    Send an email to the user after the account is created or updated.

    - If the account is created and not approved, an email is sent notifying the user
      that their account is pending approval by an administrator.
    - If the account is updated and approved, an email is sent informing the user
      that their account has been approved by an administrator.

    Args:
    ----
        sender (Model): The model class that triggered the signal (MadbotUser).
        instance (MadbotUser): The instance of the user that was saved.
        created (bool): Flag indicating if the instance was created or updated.
        **kwargs (dict): Additional keyword arguments (unused in this function).

    Sends the following emails:
        - A pending approval email when a new user is created and is not approved.
        - An approval confirmation email when an existing user is approved.

    """
    if not created and instance.is_approved:
        send_mail(
            "Account approved",
            "Your account has been approved by an administrator",
            settings.DEFAULT_FROM_EMAIL,
            [instance.email],
            fail_silently=False,
        )
    if created and not instance.is_approved:
        email = EmailMessage(
            "Account Pending Approval",
            "Your account must be approved by an administrator before you can log in.",
            settings.DEFAULT_FROM_EMAIL,
            [instance.email],
            [settings.DEFAULT_SUPPORT_EMAIL],
            reply_to=[settings.DEFAULT_SUPPORT_EMAIL],
        )
        email.content_subtype = "html"
        email.send(fail_silently=True)
