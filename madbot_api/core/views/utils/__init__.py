import re


def camelcase_to_snakecase(string: str) -> str:
    """
    Convert a CamelCase string to snake_case.

    This function takes a string in CamelCase format and converts it to snake_case format.
    It uses a regular expression to insert underscores before capital letters and then
    converts the entire string to lowercase.

    Args:
    ----
        string (str): The CamelCase string to be converted.

    Returns:
    -------
        str: The converted snake_case string.

    """
    return re.sub(r"(?<!^)(?=[A-Z])", "_", string).lower()
