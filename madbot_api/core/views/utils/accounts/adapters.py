from allauth.account.adapter import DefaultAccountAdapter
from allauth.core.exceptions import ImmediateHttpResponse
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.contrib.auth import logout
from django.core.exceptions import ValidationError
from django.shortcuts import redirect

from madbot_api.core.models import MadbotUser


class CustomAccountAdapter(DefaultAccountAdapter):
    """Custom Account Adapter that checks if the user is authenticated and approved before granting access."""

    def authenticate(self, request, **credentials):
        """
        Authenticate the user and checks if the account is approved by an administrator.

        This method overrides the default authentication process to ensure that
        the user account is approved before allowing the login. If the account
        is not approved, a ValidationError is raised with an appropriate message.

        Args:
        ----
            request (HttpRequest): The request object containing user credentials.
            **credentials: The user credentials (e.g., username, password).

        Returns:
        -------
            user (User): The authenticated user if the account is approved.

        Raises:
        ------
            ValidationError: If the user is not approved, a validation error is raised
                            with a message indicating that the account must be approved.

        """
        user = super().authenticate(request, **credentials)
        if user and not user.is_approved:
            raise ValidationError(
                "Your account must be approved by an administrator before you can log in."
            )
        return user

    def save_user(self, request, sociallogin, form=None):
        """
        Save the user associated with the social account.

        This ensures the user record is created with the necessary fields, including `is_approved`.
        """
        user = super().save_user(request, sociallogin, form)
        if not user.is_approved:
            raise ImmediateHttpResponse(redirect("awaiting_approval"))
        return user


class CustomSocialAccountAdapter(DefaultSocialAccountAdapter):
    """
    Custom Social Account Adapter to check if the user is authenticated and approved.

    This ensures that users logging in through social accounts are approved by an administrator
    before they can proceed.
    """

    def save_user(self, request, sociallogin, form=None):
        """
        Save the user associated with the social account.

        This ensures the user record is created with the necessary fields, including `is_approved`.
        """
        user = super().save_user(request, sociallogin, form)
        if not user.is_approved:
            raise ImmediateHttpResponse(redirect("awaiting_approval"))
        return user

    def pre_social_login(self, request, sociallogin):
        """Redirect unapproved users to an approval page before completing the login."""
        user = sociallogin.user

        try:
            madbotuser = MadbotUser.objects.get(
                pk=user.pk, username=user.username, email=user.email
            )
            if madbotuser and not user.is_approved:
                # Log the user out
                logout(request)
                # Redirect to the pending approval page
                raise ImmediateHttpResponse(redirect("awaiting_approval"))
        except MadbotUser.DoesNotExist:
            pass
