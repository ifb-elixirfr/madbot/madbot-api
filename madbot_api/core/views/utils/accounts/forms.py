from allauth.account.forms import SignupForm
from allauth.socialaccount.forms import SignupForm as SignupForm_socialaccount
from django import forms as d_forms
from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _


class CustomSignupForm(SignupForm):
    """
    A custom sign-up form that extends the default Allauth signup form.

    This form collects additional user information such as first name, last name,
    and a mandatory acceptance of terms and conditions. It also modifies the
    field order for a more logical flow during user registration.

    Attributes
    ----------
        first_name (CharField): A field to collect the user's first name.
        last_name (CharField): A field to collect the user's last name.
        terms_approved (BooleanField): A checkbox to confirm acceptance of the terms
            and conditions, as well as the privacy notice.

    """

    first_name = d_forms.CharField(max_length=30, label="First name")
    last_name = d_forms.CharField(max_length=30, label="Last name")
    # ruff: noqa: S308
    terms_approved = d_forms.BooleanField(
        label=mark_safe(  # nosec B308 B703
            _(
                'I accept the <a href="{terms_url}" target="_blank">Terms and Conditions</a> and <a href="{privacy_url}" target="_blank">Privacy Notice</a> '
            ).format(
                terms_url=settings.MADBOT_TERMS_URL,
                privacy_url=settings.MADBOT_PRIVACY_URL,
            )
        ),
        initial=True,
        required=True,
    )

    def __init__(self, *args, **kwargs):
        """Initialize the form and defines the order of fields."""
        super().__init__(*args, **kwargs)
        field_order = [
            "email",
            "username",
            "first_name",
            "last_name",
            "password1",
            "password2",
            "terms_approved",
        ]
        self.order_fields(field_order)

    def signup(self, request, user):
        """
        Handle the signup process for social accounts.

        This method is called after the form is submitted and validated. It ensures
        that the terms acceptance status is saved to the user model.

        Args:
        ----
            request (HttpRequest): The HTTP request object containing information
                about the current request.
            user (User): The user instance associated with the social account being created.

        Returns:
        -------
            None

        """
        user.terms_approved = self.cleaned_data["terms_approved"]
        user.save()


class CustomSocialSignupForm(SignupForm_socialaccount):
    """
    A custom form for handling social account sign-ups with additional fields.

    This form extends the default social account sign-up form to include fields
    for the user's first name, last name, and a mandatory terms acceptance checkbox.
    It ensures that these details are collected and saved during the social account
    registration process.

    Attributes
    ----------
        first_name (CharField): A field to collect the user's first name.
        last_name (CharField): A field to collect the user's last name.
        terms_approved (BooleanField): A checkbox to confirm acceptance of the terms
            and conditions, as well as the privacy notice.

    """

    first_name = d_forms.CharField(max_length=30, label="First name")
    last_name = d_forms.CharField(max_length=30, label="Last name")
    # ruff: noqa: S308
    terms_approved = d_forms.BooleanField(
        label=mark_safe(  # nosec B308 B703
            _(
                'I accept the <a href="{terms_url}" target="_blank">Terms and Conditions</a> and <a href="{privacy_url}" target="_blank">Privacy Notice</a> '
            ).format(
                terms_url=settings.MADBOT_TERMS_URL,
                privacy_url=settings.MADBOT_PRIVACY_URL,
            )
        ),
        initial=True,
        required=True,
    )

    def signup(self, request, user):
        """
        Handle the signup process for social accounts.

        This method is called after the form is submitted and validated. It ensures
        that the terms acceptance status is saved to the user model.

        Args:
        ----
            request (HttpRequest): The HTTP request object containing information
                about the current request.
            user (User): The user instance associated with the social account being created.

        Returns:
        -------
            None

        """
        user.terms_approved = self.cleaned_data["terms_approved"]
        user.save()
