from madbot_api.core import connector as connectors
from madbot_api.core.connector import ConnectorError
from madbot_api.core.errors import ConnectorException


def check_connection_state(connector, shared_param_data, private_param_data):
    """
    Attempt to establish a connection using provided parameters.

    It handles exceptions related to authentication and connection errors.

    Args:
    ----
        connector: The type of connector being used to establish the connection
            (e.g., 'MySQL', 'PostgreSQL', 'MongoDB').
        shared_param_data: Shared parameters common to all connections (e.g., API keys, base URLs).
        private_param_data: Private parameters specific to the connection
            (e.g., credentials or configuration settings).

    Returns:
    -------
        The connector_instance after attempting to establish the connection.

    Raises:
    ------
        ConnectorException: If an authentication or connection error occurs.

    """
    # You need to pass the shared and private parameters to the connector instance
    combined_params = {**shared_param_data, **private_param_data}

    try:
        # Attempt to establish the connection
        connector_instance = connectors.get_connector_instance(
            connector,
            combined_params,
        )
    except ConnectorError as e:
        raise ConnectorException(error=e)
    except Exception as e:
        if getattr(e, "__context__") == "Failed to authenticate user.":
            raise ConnectorException(
                details_code="invalid_credentials",
                parameter="connection",
            )
        raise ConnectorException(
            details_code="hostname_unreachable",
            parameter="hostname",
            parameter_value=connector.get_instance_name(),
        )
    return connector_instance


def generate_dict_from_paths(paths):
    """
    Convert a list of paths into a dictionary with structured metadata.

    Each path is expected to follow the format:
    "schemas/<source>/<type>/<slug>".
    The function extracts the "source", "type", and "slug" components from the path
    and organizes them into a dictionary, where the keys are the "slug" values
    and the values are dictionaries containing the corresponding "source", "type", and "slug".

    Args:
    ----
        paths (list of str): A list of string paths to process. Each path should
            follow the format "schemas/<source>/<type>/<slug>".

    Returns:
    -------
        dict: A dictionary where each key is a "slug" extracted from the path,
            and the value is another dictionary with the following structure:
            {
                "source": <source>,  # The second part of the path.
                "type": <type>,      # The third part of the path.
                "slug": <slug>,      # The fourth part of the path.
            }

    Raises:
    ------
        ValueError: If a path does not follow the expected format.

    """
    result = {}
    for path in paths:
        parts = path.split("/")
        if len(parts) >= 4:
            source, type_, slug = parts[1], parts[2], parts[3]
            result[slug] = {
                "source": source,
                "type": type_,
                "slug": slug,
            }
    return result
