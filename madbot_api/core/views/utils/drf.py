from oauth2_provider.contrib.rest_framework.authentication import OAuth2Authentication
from oauth2_provider.contrib.rest_framework.permissions import (
    TokenMatchesOASRequirements,
)
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from madbot_api.core.views.utils.exception_handler import custom_get_object_or_404
from madbot_api.core.views.utils.pagination import StandardPagination


class IsApprovedUser(BasePermission):
    """Custom permission class that checks if the user is authenticated and approved before granting access to a view."""

    def has_permission(self, request, view):
        """
        Check if the user has permission to access the view based on authentication and approval status.

        Args:
        ----
            request (HttpRequest): The request object containing user information.
            view (View): The view instance being accessed.

        Returns:
        -------
            bool: True if the user is authenticated and approved, False otherwise.

        """
        return (
            request.user and request.user.is_authenticated and request.user.is_approved
        )


class MadbotViewSet(GenericViewSet):
    """Handle API requests for managing objects with authentication and permissions."""

    pagination_class = StandardPagination
    authentication_classes = [OAuth2Authentication]
    permission_classes = [IsAuthenticated, TokenMatchesOASRequirements, IsApprovedUser]
    required_alternate_scopes = {
        "POST": [["write"]],
        "GET": [["read"]],
        "PUT": [["write"]],
        "DELETE": [["write"]],
        "PATCH": [["write"]],
    }

    def get_object(self, queryset=None):
        """
        Set the `view_mode` in the context dictionary based on the action being performed.

        Returns
        -------
            A dictionary containing the context for the serializer.

        """
        return custom_get_object_or_404(
            model=self.queryset.model,
            context="uri",
            id=self.kwargs.get("pk"),
        )

    def get_serializer_context(self):
        """
        Set the `view_mode` in the context dictionary based on the action being performed.

        :return: The `context` dictionary is being returned.
        """
        context = super().get_serializer_context()
        context["view_mode"] = self.action
        return context
