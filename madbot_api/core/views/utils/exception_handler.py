import logging
import traceback
import uuid

from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import Http404
from django.shortcuts import get_object_or_404 as django_get_object_or_404

# from django.http import JsonResponse
from rest_framework.exceptions import (
    MethodNotAllowed,
    NotAuthenticated,
    PermissionDenied,
)
from rest_framework.response import Response
from rest_framework.views import exception_handler

from madbot_api.core.errors import (
    CustomException,
    ForbiddenException,
    InternalServerErrorException,
    MethodNotAllowedException,
    ObjectNotFoundException,
    UnauthorizedException,
    ValidationException,
)
from madbot_api.core.views.utils import camelcase_to_snakecase


def custom_exception_handler(exc, context):
    """
    Handle exceptions for a REST framework in Python.

    This function handles specific custom exceptions and provides a custom response.

    Args:
    ----
        exc: The raised exception, which could be of any type.
        context: A dictionary containing information about the current request and view.

    Returns:
    -------
        A `Response` object containing the exception data, including `code`, `message`,
        and `details`, with the appropriate HTTP status.

    Raises:
    ------
        This function does not raise exceptions itself but converts raised exceptions into
        custom responses.

    """
    # start a logger
    logger = logging.getLogger("madbot")

    # Call REST framework's default exception handler first
    response = exception_handler(exc, context)

    if isinstance(exc, CustomException) and not response:
        # Custom handling for CustomException
        if (
            exc.details
            and isinstance(exc.details, list)
            and len(exc.details) > 0
            and "parameter" in exc.details[0]
            and exc.details[0].get("parameter") is not None
        ):
            exc.details[0]["parameter"] = exc.details[0]["parameter"].lower()
    elif isinstance(exc, NotAuthenticated):
        message = str(exc)
        if getattr(exc, "auth_header", None):
            auth_header = exc.auth_header.split("error_description=")
            if len(auth_header) > 1:
                message = auth_header[1].split(",")[0].replace('"', "")
        exc = UnauthorizedException(details={"message": message})
    elif isinstance(exc, PermissionDenied):
        exc = ForbiddenException(
            details={
                "parameter": "scope",
                "code": "permission_denied",
                "message": str(exc),
            },
        )
    elif isinstance(exc, MethodNotAllowed):
        exc = MethodNotAllowedException(method=context["request"].method)
    else:
        if settings.DEBUG:
            # show the traceback to the console
            logger.debug(traceback.format_exc())

        id = uuid.uuid4()
        logger.error(
            msg=str(exc),
            extra={"id": id, "traceback": traceback.format_exc()},
        )
        exc = InternalServerErrorException(id=id)
        data = {
            "code": exc.code,
            "message": exc.message,
            "details": exc.details,
        }
        return Response(data, status=exc.status)

    data = {
        "code": exc.code,
        "message": exc.message,
        "details": exc.details,
    }
    logger.warning(data)
    return Response(data, status=exc.status)


def custom_get_object_or_404(model, context=None, parameter=None, **kwargs):
    """
    Retrieve an object based on specified criteria or raise exceptions.

    This function retrieves an object based on specified criteria or raises exceptions
    if the object is not found or if there is a validation error.

    Args:
    ----
        model: The Django model class from which the object is to be retrieved.
        context: Optional; Specifies the context in which the object is being retrieved.
                 Valid options: `header`, `uri`, `query`, or `body`. Defaults to `None`.
        parameter: Optional; Specifies the parameter name used for retrieval. Defaults to the model name in snake_case.
        **kwargs: Additional lookup parameters for retrieving the object.

    Returns:
    -------
        An object of the specified `model` if it exists, based on the lookup criteria.

    Raises:
    ------
        ObjectNotFoundException: If the object is not found in the database.
        ValidationException: If the context is invalid or there is a validation error.
        Exception: If an unknown error occurs.

    Example usage:
    obj = custom_get_object_or_404(MyModel, id=1)
    This will retrieve an object of the `MyModel` class with the id equal to 1.
    If the object is not found, it will raise an `ObjectNotFoundException`.

    """
    if context and context not in ["header", "uri", "query", "body"]:
        raise Exception(
            "The context parameter must be one of the following values: header, uri, query, body",
        )

    try:
        return django_get_object_or_404(model, **kwargs)
    except Http404:
        if context in ["query", "body"]:
            raise ValidationException(
                code=context + "_validation_error",
                details_code="does_not_exist",
                parameter=parameter
                if parameter
                else camelcase_to_snakecase(model.__name__),
                parameter_value=kwargs.get(next(iter(kwargs.keys()))),
            )
        raise ObjectNotFoundException(
            parameter=parameter
            if parameter
            else camelcase_to_snakecase(model.__name__),
            parameter_value=kwargs.get(next(iter(kwargs.keys()))),
        )
    except ValidationError:
        raise ValidationException(
            code=context + "_validation_error" if context else "validation_error",
            details_code="invalid_uuid",
            parameter=parameter
            if parameter
            else camelcase_to_snakecase(model.__name__),
            parameter_value=kwargs.get(next(iter(kwargs.keys()))),
        )
    except Exception as e:
        raise ValidationException(
            code="validation_error",
            parameter=parameter
            if parameter
            else camelcase_to_snakecase(model.__name__),
            details=str(e),
        )
