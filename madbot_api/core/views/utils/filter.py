import django_filters
from django.db.models import Case, IntegerField, Min, Q, Value, When
from django.db.models.functions import Coalesce
from django_filters import rest_framework

from madbot_api.core.errors import ValidationException
from madbot_api.core.models import DataType, MadbotUser, MetadataMapping, NodeType
from madbot_api.core.views.utils.exception_handler import custom_get_object_or_404


# The `ParentFilter` class is a custom Django filter that filters a queryset based on the
# value of a parent object.
class ParentFilter(django_filters.CharFilter):
    """Filter a queryset based on the value of a parent object."""

    def filter(self, queryset, value):
        """
        Filter a queryset based on a parent object value.

        Args:
        ----
            queryset: The queryset to filter based on parent.
            value: The parent object ID or "null" to filter for no parent.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            if value == "null":
                queryset = queryset.filter(parent=None)
            else:
                parent_object = custom_get_object_or_404(
                    model=queryset.model,
                    context="query",
                    parameter="parent",
                    id=value,
                )
                queryset = queryset.filter(parent=parent_object)
        return queryset


# The `TypeFilter` class is a custom Django filter that filters a queryset based on a
# specified `NodeType` object.
class TypeFilter(django_filters.CharFilter):
    """Filter a queryset based on a specified NodeType object."""

    def filter(self, queryset, value):
        """
        Filter a queryset based on a `NodeType` object ID.

        Args:
        ----
            queryset: The queryset to filter based on type.
            value: The ID of the `NodeType` to filter by.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            type_object = custom_get_object_or_404(
                model=NodeType,
                context="query",
                parameter="type",
                id=value,
            )
            queryset = queryset.filter(type=type_object)
        return queryset


# The UserFilter class is a subclass of django_filters.CharFilter.
class UserFilter(django_filters.CharFilter):
    """Filter a queryset based on a User object ID."""

    def filter(self, queryset, value):
        """
        Filter a queryset based on a `User` object ID.

        Args:
        ----
            queryset: The queryset to filter based on user.
            value: The ID of the `User` to filter by.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            user_object = custom_get_object_or_404(
                model=MadbotUser,
                context="query",
                parameter="user",
                id=value,
            )
            queryset = queryset.filter(user=user_object)
        return queryset


class SearchUserFilter(django_filters.CharFilter):
    """Filter a queryset based on part of a username, email, or full name."""

    def filter(self, queryset, value):
        """
        Filter a queryset based on part of a username, email, or full name.

        Args:
        ----
            queryset: The queryset to filter based on user attributes.
            value: A substring of username, email, or full name to search for.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            query = Q()
            # if value is username
            query |= Q(user__username__icontains=value)
            # if value is email
            query |= Q(user__email__icontains=value)
            # if value is fullname
            names = value.split()
            name_query = Q()
            for name in names:
                name_query &= Q(user__first_name__icontains=name) | Q(
                    user__last_name__icontains=name,
                )
            query |= name_query
            queryset = queryset.filter(query)
        return queryset


# The MemberFilter class is a Django FilterSet that filters Members based on
# their user.
class MemberFilter(rest_framework.FilterSet):
    """Filter Members based on user attributes."""

    user = UserFilter(field_name="user")
    search = SearchUserFilter(field_name="search")


# The `FilterArchived` class is a custom Django filter that filters a queryset
# based on a boolean value for the `deleted` field.
class FilterArchived(django_filters.BooleanFilter):
    """Filter a queryset based on the boolean value of the deleted field."""

    def filter_queryset(self, queryset, value):
        """
        Filter a queryset based on the boolean value of the `deleted` field.

        Args:
        ----
            queryset: The queryset to filter based on archived status.
            value: The boolean string ("true" or "false") to filter by.

        Returns:
        -------
            The filtered queryset.

        Raises:
        ------
            ValidationException: If the value is invalid.

        """
        if value is not None:
            try:
                archived = bool(value.lower() == "true")
                queryset = queryset.filter(deleted=archived)
            except ValueError:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="invalid",
                    parameter="archived",
                    parameter_value=value,
                )
        return queryset


# The `FilterUnread` class defines a custom Django filter that filters a
# queryset based on a boolean value for the "unread" field.
class FilterUnread(django_filters.BooleanFilter):
    """Filter a queryset based on the boolean value of the unread field."""

    def filter_queryset(self, queryset, value):
        """
        Filter a queryset based on the boolean value of the `unread` field.

        Args:
        ----
            queryset: The queryset to filter based on unread status.
            value: The boolean string ("true" or "false") to filter by.

        Returns:
        -------
            The filtered queryset.

        Raises:
        ------
            ValidationException: If the value is invalid.

        """
        if value is not None:
            try:
                unread = bool(value.lower() == "true")
                queryset = queryset.filter(unread=unread)
            except ValueError:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="invalid",
                    parameter="unread",
                    parameter_value=value,
                )
        return queryset


class MetadataSearchFilter(django_filters.CharFilter):
    """Filter a queryset based on metadata field criteria."""

    def filter(self, queryset, value):
        """
        Filter a queryset based on metadata field criteria.

        Args:
        ----
            queryset: The queryset to filter based on metadata.
            value: The string value to search for in metadata fields.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            # filter by:
            # 1. the name of the metadata field
            # 2. the name of a metadata from the metadata mapping
            # 3. the description of the metadata field
            # 4. the description of a metadata from the metadata mapping
            metadata_mapping_matches_name = MetadataMapping.objects.filter(
                connector_metadata_field__name__icontains=value,
            )
            metadata_mapping_matches_description = MetadataMapping.objects.filter(
                connector_metadata_field__description__icontains=value,
            )

            queryset = (
                queryset.filter(
                    Q(name__icontains=value)
                    | Q(description__icontains=value)
                    | Q(metadata_mapping__in=metadata_mapping_matches_name)
                    | Q(metadata_mapping__in=metadata_mapping_matches_description)
                    | Q(name="Additional metadata"),
                )
                .distinct()  # remove duplicates
                .annotate(  # rank the results
                    rank=Coalesce(
                        Min(
                            Case(
                                When(name__icontains=value, then=1),
                                When(
                                    metadata_mapping__in=metadata_mapping_matches_name,
                                    then=2,
                                ),
                                When(description__icontains=value, then=3),
                                When(
                                    metadata_mapping__in=metadata_mapping_matches_description,
                                    then=4,
                                ),
                                When(
                                    name="Additional metadata",
                                    then=5,
                                ),
                                default=6,
                                output_field=IntegerField(),
                            ),
                        ),
                        Value(6),
                    ),
                )
                .order_by("rank", "name")
            )
        return queryset


class DataTypesFilter(django_filters.CharFilter):
    """Filter metadata fields by data type name."""

    def filter(self, queryset, value):
        """
        Filter metadata fields by data type name.

        Args:
        ----
            queryset: The queryset to filter based on data types.
            value: The string name of the data type to filter by.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            # filter data types by the requested name
            data_types_matches = DataType.objects.filter(name__icontains=value)
            queryset = queryset.filter(data_types__in=data_types_matches)
        return queryset


class RelatedTypesFilter(django_filters.CharFilter):
    """Filter metadata fields based on related types."""

    def filter(self, queryset, value):
        """
        Filter metadata fields based on related types.

        Args:
        ----
            queryset: The queryset to filter based on related types.
            value: The string indicating type ("data", "sample", or "node").

        Returns:
        -------
            The filtered queryset.

        Raises:
        ------
            ValidationException: If the value is invalid.

        """
        if value is not None and value != "":
            if value == "null":
                # return metadata fields that are not related to any types
                queryset = queryset.filter(
                    data_related=False,
                    sample_related=False,
                    node_related=False,
                    data_association_related=False,
                )
            else:
                # filter metadata fields by the requested type
                query = Q()
                if value == "data":
                    query |= Q(data_related=True)
                elif value == "sample":
                    query |= Q(sample_related=True)
                elif value == "node":
                    query |= Q(node_related=True)
                elif value == "data_association":
                    query |= Q(data_association_related=True)
                else:
                    raise ValidationException(
                        code="query_validation_error",
                        details_code="invalid",
                        parameter="related_types",
                        parameter_value=value,
                    )

                # also include metadata fields that are not related to any types
                query |= Q(
                    data_related=False,
                    sample_related=False,
                    node_related=False,
                    data_association_related=False,
                )

                # filter the queryset and than order by requested related type first
                queryset = queryset.filter(query).order_by(f"-{value}_related", "name")
        return queryset


class SourceFilter(django_filters.CharFilter):
    """Filter metadata fields by source name."""

    def filter(self, queryset, value):
        """
        Filter metadata fields by source name.

        Args:
        ----
            queryset: The queryset to filter based on data types.
            value: The string name of the source to filter by.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None and value != "":
            queryset = queryset.filter(source__iexact=value)
        return queryset


class HiddenFilter(django_filters.BooleanFilter):
    """Filter a queryset based on the boolean value of the hidden field."""

    def filter(self, queryset, value):
        """
        Filter a queryset based on the boolean value of the hidden field.

        Args:
        ----
            queryset: The queryset to filter based on data types.
            value: The boolean string ("true" or "false") to filter by.

        Returns:
        -------
            The filtered queryset.

        """
        if value is not None:
            try:
                hidden = bool(value == "true")
                queryset = queryset.filter(hidden=hidden)
            except ValueError:
                raise ValidationException(
                    code="query_validation_error",
                    details_code="invalid",
                    parameter="hidden",
                    parameter_value=value,
                )
        return queryset
