import os
from logging.handlers import TimedRotatingFileHandler


class MakeFileHandler(TimedRotatingFileHandler):
    """Manage log file creation with automatic directory management."""

    def __init__(self, filename, *args, **kwargs):
        """
        Ensure the directory for the log file exists.

        This class extends the TimedRotatingFileHandler to create any necessary
        directories for the log file before attempting to create the file itself.

        Args:
        ----
            filename (str): The name of the log file. If the directory does not
                            exist, it will be created.
            *args: Variable length argument list for TimedRotatingFileHandler.
            **kwargs: Arbitrary keyword arguments for TimedRotatingFileHandler.

        """
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        super().__init__(filename, *args, **kwargs)
