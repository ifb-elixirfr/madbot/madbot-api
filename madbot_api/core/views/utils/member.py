from madbot_api.core.models import (
    Node,
    NodeMember,
    Submission,
    SubmissionMember,
    Workspace,
    WorkspaceMember,
)

###############################################################################
# Utils function for members
###############################################################################


def workspaces_roles_and_descriptions():
    """
    Return a dictionary containing workspaces' roles and their corresponding descriptions.

    Returns
    -------
        list: A list of dictionaries containing different roles and their descriptions.

    """
    return [
        {
            "role": "owner",
            "description": "full access to the workspace and all of its nodes, "
            "can delete it, manage members and create new nodes",
        },
        {
            "role": "member",
            "description": "full access to the workspace and "
            "to the nodes they are also a member of, "
            "can create new nodes",
        },
    ]


def nodes_roles_and_descriptions():
    """
    Return a dictionary containing nodes' roles and their corresponding descriptions.

    Returns
    -------
        list: A list of dictionaries containing different roles and their descriptions.

    """
    return [
        {
            "role": "owner",
            "description": "full access to the node, "
            "can delete it and can manage members",
        },
        {
            "role": "manager",
            "description": "full access to the node, and can manage members",
        },
        {
            "role": "contributor",
            "description": "full access to the node, but cannot delete it",
        },
        {
            "role": "collaborator",
            "description": "can view the node, but not modify any content",
        },
    ]


def submissions_roles_and_descriptions():
    """
    Return a dictionary containing submissions' roles and their corresponding descriptions.

    Returns
    -------
        list: A list of dictionaries containing different roles and their descriptions.

    """
    return [
        {
            "role": "owner",
            "description": "Full access to the submission, can manage its members, submit and delete it.",
        },
        {
            "role": "manager",
            "description": "Full access to the submission, can manage its members and delete it.",
        },
        {
            "role": "data broker",
            "description": "Full access to the submission, and can submit it.",
        },
        {
            "role": "contributor",
            "description": "Full access to the submission, and can only edit its content.",
        },
        {
            "role": "collaborator",
            "description": "Read-only access to the submission, and can view but not modify any of its content.",
        },
    ]


def count_owners(object_instance):
    """
    Return the count of owners associated with that object.

    Args:
    ----
        object_instance: An instance of either the `Node`, `Workspace`, or `Submission` class.

    Returns:
    -------
        int: The count of owners associated with the `object_instance`.

    Raises:
    ------
        ValueError: If the `object_instance` is of an unsupported type.

    """
    if isinstance(object_instance, Node):
        queryset = NodeMember.objects.filter(node=object_instance, role="owner")
    elif isinstance(object_instance, Workspace):
        queryset = WorkspaceMember.objects.filter(
            workspace=object_instance,
            role="owner",
        )
    elif isinstance(object_instance, Submission):
        queryset = SubmissionMember.objects.filter(
            submission=object_instance,
            role="owner",
        )

    return queryset.count()
