from django.core.exceptions import ValidationError

from madbot_api.core.errors import (
    ForbiddenException,
    ObjectNotFoundException,
    ValidationException,
)
from madbot_api.core.models import (
    Data,
    Node,
    NodeMember,
    Submission,
    SubmissionMember,
    Workspace,
    WorkspaceMember,
)
from madbot_api.core.views.utils import camelcase_to_snakecase
from madbot_api.core.views.utils.exception_handler import custom_get_object_or_404

########################################################################################
# Node Mixin class
########################################################################################


# The `GetNodeMixin` class provides a method to retrieve a `Node` object based on a given
# object ID.
class GetNodeMixin:
    """Mixin for retrieving Node objects and checking membership."""

    def get_node(self, workspace):
        """
        Retrieve a Node object based on the provided workspace.

        This method retrieves the node identified by the 'node_pk' URL parameter
        and checks if the user is a member of the node or the workspace.

        Args:
        ----
            workspace (Workspace): The workspace to which the node belongs.

        Raises:
        ------
            ForbiddenException: If the user is not a member of the node.

        Returns:
        -------
            Node: The retrieved Node object.

        """
        object_id = self.kwargs.get("node_pk")
        node = custom_get_object_or_404(
            model=Node,
            context="uri",
            id=object_id,
            workspace=workspace,
        )

        # Check if the user is member of the node
        if (
            not NodeMember.objects.filter(
                node__id=object_id,
                user=self.request.user,
            ).exists()
            and not WorkspaceMember.objects.filter(
                workspace=node.workspace,
                user=self.request.user,
                role="owner",
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

        return node

    def check_object_belongs_to_node(self, node, obj):
        """
        Check if the specified object belongs to the given node.

        This method raises an ObjectNotFoundException if the object does not belong
        to the node.

        Args:
        ----
            node (Node): The node to check against.
            obj (Object): The object to check for membership.

        Raises:
        ------
            ObjectNotFoundException: If the object does not belong to the node.

        """
        if getattr(obj, "node", None) != node:
            raise ObjectNotFoundException(
                parameter=camelcase_to_snakecase(obj.__class__.__name__),
                parameter_value=getattr(obj, "id", None),
            )


########################################################################################
# Submission Mixin class
########################################################################################


# The `GetSubmissionMixin` class provides a method to retrieve a `Submission` object based on a given
# object ID.
class GetSubmissionMixin:
    """Mixin for retrieving Submission objects and checking membership."""

    def get_submission(self, workspace, action=None):
        """
        Retrieve a Submission object based on the provided workspace.

        This method retrieves the submission identified by the 'submission_pk' URL parameter
        and checks if the user is a member of the submission or the workspace.

        Args:
        ----
            workspace (Workspace): The workspace to which the submission belongs.
            action (str, optional): The action being performed (e.g., 'create', 'update', 'destroy').

        Raises:
        ------
            ForbiddenException: If the user is not a member of the submission or lacks the required permissions.

        Returns:
        -------
            Submission: The retrieved Submission object.

        """
        object_id = self.kwargs.get("submission_pk")
        submission = custom_get_object_or_404(
            model=Submission,
            context="uri",
            id=object_id,
            workspace=workspace,
        )

        is_submission_member = SubmissionMember.objects.filter(
            submission__id=object_id, user=self.request.user
        ).exists()
        is_workspace_owner = WorkspaceMember.objects.filter(
            workspace=submission.workspace, user=self.request.user, role="owner"
        ).exists()

        if not (is_submission_member or is_workspace_owner):
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

        if action in ["create", "update", "partial_update", "destroy"]:
            has_required_role = SubmissionMember.objects.filter(
                submission=submission,
                user=self.request.user,
                role__in=["owner", "data broker", "contributor"],
            ).exists()

            if not has_required_role:
                action_messages = {
                    "create": "The member's role does not allow creation.",
                    "update": "The member's role does not allow update.",
                    "partial_update": "The member's role does not allow update.",
                    "destroy": "The member's role does not allow deletion.",
                }
                raise ForbiddenException(
                    details={
                        "parameter": "submission",
                        "code": "not_allowed",
                        "message": action_messages.get(action),
                    },
                )

        return submission

    def check_object_belongs_to_submission(self, submission, obj):
        """
        Check if the specified object belongs to the given submission.

        This method raises an ObjectNotFoundException if the object does not belong
        to the submission.

        Args:
        ----
            submission (Submission): The submission to check against.
            obj (Object): The object to check for membership.

        Raises:
        ------
            ObjectNotFoundException: If the object does not belong to the submission.

        """
        if getattr(obj, "submission", None) != submission:
            raise ObjectNotFoundException(
                parameter=camelcase_to_snakecase(obj.__class__.__name__),
                parameter_value=getattr(obj, "id", None),
            )


########################################################################################
# Workspace Mixin class
########################################################################################


class WorkspaceHeaderMixin:
    """Mixin for retrieving workspace from request headers."""

    def get_workspace(self):
        """
        Retrieve the workspace from the request header.

        This method checks if the 'X-Workspace' header is provided, validates
        it, and checks if the user is a member of the workspace.

        Raises
        ------
            ValidationException: If the workspace ID is missing or invalid.
            ForbiddenException: If the user is not a member of the workspace.

        Returns
        -------
            Workspace: The retrieved Workspace object.

        """
        workspace_id = self.request.headers.get("X-Workspace")

        # Check that the workspace ID is provided in the request header
        if not workspace_id:
            raise ValidationException(
                code="header_validation_error",
                details_code="required",
                parameter="X-Workspace",
            )

        # Check if the provided workspace ID exists

        try:
            workspace = Workspace.objects.select_related("created_by").get(
                id=workspace_id,
            )
        except ValidationError:
            raise ValidationException(
                code="header_validation_error",
                details_code="invalid_uuid",
                parameter="X-Workspace",
                parameter_value=workspace_id,
            )
        except Workspace.DoesNotExist:
            raise ValidationException(
                code="header_validation_error",
                details_code="does_not_exist",
                parameter="X-Workspace",
                parameter_value=workspace_id,
            )

        # Check if the user is part of the workspace
        if (
            not workspace.workspace_members.select_related("user", "workspace")
            .filter(user=self.request.user)
            .exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )

        return workspace


class BelongsToWorkspaceMixin:
    """Mixin for checking if an object belongs to a workspace."""

    def check_object_belongs_to_workspace(self, workspace, obj):
        """
        Check if the given object belongs to the specified workspace.

        This method raises an ObjectNotFoundException if the object's workspace
        does not match the provided workspace.

        Args:
        ----
            workspace (Workspace): The workspace to check against.
            obj (Object): The object to check.

        Raises:
        ------
            ObjectNotFoundException: If the object does not belong to the workspace.

        """
        if getattr(obj, "workspace", None) != workspace:
            raise ObjectNotFoundException(
                parameter=camelcase_to_snakecase(obj.__class__.__name__),
                parameter_value=getattr(obj, "id", None),
            )


########################################################################################
# Generalization of the mixin class
########################################################################################


# TODO: Use this generalization in member, node and datalink
class GetObjectMixin:
    """A mixin to provide a method for retrieving an object based on the given object ID."""

    def get_the_object(self, model, object_pk, context=None):
        """
        Retrieve an object of the specified model type based on the given object ID.

        This method retrieves the specified object from the database and raises
        a 404 error if the object is not found.

        Args:
        ----
            model (Type[Model]): The model class to retrieve the object from.
            object_pk (Any): The primary key of the object to retrieve.
            context (Optional[str]): Optional context for the retrieval.

        Returns:
        -------
            Any: The retrieved object.

        """
        return custom_get_object_or_404(model=model, context=context, pk=object_pk)


class BelongsToObjectMixin:
    """A mixin to provide a method for checking if an object belongs to another object."""

    def check_object_belongs_to_parent(
        self,
        parent_model,
        child_model,
        parent_name,
        child_name,
        child_pk,
        parent_pk,
    ):
        """
        Check if the child object belongs to the specified parent object.

        Args:
        ----
            parent_model (Type[Model]): The model class of the parent object.
            child_model (Type[Model]): The model class of the child object.
            parent_name (str): The name of the parent object type (e.g., "workspace", "node").
            child_name (str): The name of the child object.
            child_pk (Any): The primary key of the child object.
            parent_pk (Any): The primary key of the parent object.

        """
        parent = custom_get_object_or_404(
            model=parent_model,
            context="uri",
            pk=parent_pk,
        )
        child = custom_get_object_or_404(
            model=child_model,
            context="uri",
            pk=child_pk,
        )

        if getattr(child, parent_name) != parent:
            raise ObjectNotFoundException(
                parameter=child_name,
                parameter_value=child_pk,
            )


class GetGeneralMixin:
    """Mixin for checking membership and retrieving objects based on their type."""

    def check_and_get_object(self, workspace, model, object_pk, context=None):
        """
        Retrieve the object and check user membership based on its type.

        This method retrieves an object of the specified model using the provided
        primary key and workspace. It then checks if the user has the necessary
        membership for the object based on its type.

        Args:
        ----
            workspace (Workspace): The workspace associated with the object.
            model (Type[Model]): The model class of the object to retrieve.
            object_pk (Any): The primary key of the object to retrieve.
            context (Optional[str]): Optional context for retrieving the object.

        Raises:
        ------
            ForbiddenException: If the user is not a member of the object.

        Returns:
        -------
            Any: The retrieved object.

        """
        # Retrieve the object or raise a 404 error
        obj = custom_get_object_or_404(
            model=model,
            context=context,
            pk=object_pk,
            workspace=workspace,
        )

        # Check if the user is a member of the object based on its type
        if isinstance(obj, Node):
            self._check_node_membership(obj)
        elif isinstance(obj, Data):
            self._check_data_access(obj, workspace)
        elif isinstance(obj, Submission):
            self._check_submission_membership(obj)

        return obj

    def _check_node_membership(self, node):
        """
        Check if the user is a member of the specified node.

        Args:
        ----
            node (Node): The node to check for membership.

        Raises:
        ------
            ForbiddenException: If the user is not a member of the node.

        """
        if (
            not NodeMember.objects.filter(node=node, user=self.request.user).exists()
            and not WorkspaceMember.objects.filter(
                workspace=node.workspace,
                user=self.request.user,
                role="owner",
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "node",
                    "code": "not_allowed",
                    "message": "You are not a member of this node.",
                },
            )

    def _check_data_access(self, data, workspace):
        """
        Check if the user is a member of the specified data object.

        Args:
        ----
            data (Data): The data object to check for membership.
            workspace: The workspace associated with the data.

        Raises:
        ------
            ForbiddenException: If the user is not a member of the data object.

        """
        datalinks_node_ids = data.datalinks.all().values_list("node_id", flat=True)
        if (
            not WorkspaceMember.objects.filter(
                workspace=workspace,
                user=self.request.user,
                role="owner",
            ).exists()
            and not NodeMember.objects.filter(
                node__id__in=datalinks_node_ids,
                user=self.request.user,
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "data",
                    "code": "not_allowed",
                    "message": "You do not have access to this data.",
                },
            )

    def _check_submission_membership(self, submission):
        """
        Check if the user is a member of the specified submission.

        Args:
        ----
            submission (Submission): The submission to check for membership.

        Raises:
        ------
            ForbiddenException: If the user is not a member of the submission.

        """
        if (
            not SubmissionMember.objects.filter(
                submission=submission,
                user=self.request.user,
            ).exists()
            and not WorkspaceMember.objects.filter(
                workspace=submission.workspace,
                user=self.request.user,
                role="owner",
            ).exists()
        ):
            raise ForbiddenException(
                details={
                    "parameter": "submission",
                    "code": "not_allowed",
                    "message": "You are not a member of this submission.",
                },
            )

    def _is_workspace_owner(self, workspace):
        """
        Check if the user is the owner of the specified workspace.

        :param workspace: The workspace to check ownership against.
        :return: True if the user is the owner, otherwise False.
        """
        return WorkspaceMember.objects.filter(
            workspace=workspace,
            user=self.request.user,
            role="owner",
        ).exists()
