from copy import copy

from anytree import Node as AnyNode
from django.db.models.query import QuerySet

from madbot_api.core.models import (
    Data,
    DataAssociation,
    DataLink,
    MetadataField,
    Node,
    Sample,
)


def _convert_to_anytree(root, queryset):
    """
    Convert a Django queryset of nodes into an anytree structure with a specified root node.

    Args:
    ----
        root (Node | Sample | Data): The `root` parameter represents the root
        node of the tree structure. It can be a Node, Sample, or Data object.
        queryset (List): Represents the set of ancestors of the root. The queryset
        can be composed of Node, Sample, or Data objects.

    Returns:
    -------
        Anytree: Returns the root node of the tree.

    Example return:
    AnyNode(id=<root_node_id>, name=<root_node_title>, title=<root_node_title>, metadata=<root_node_metadata>)

    """
    tree = {}
    for object in queryset:
        object_name = ""
        if hasattr(object, "title"):
            object_name = object.title
        elif hasattr(object, "name"):
            object.name

        tree[str(object.id)] = AnyNode(
            name=object_name,
            id=str(object.id),
            title=object_name,
            metadata=object.metadata,
        )

    for object in queryset:
        if hasattr(object, "parent_id") and object.parent_id:
            tree[str(object.parent_id)].parent = tree[str(object.id)]
        if hasattr(object, "node_id") and object.node_id:
            tree[str(object.node_id)].parent = tree[str(object.id)]

    return tree[str(root.id)]


def _combine_metadata(
    obj,
    obj_type,
    node,
    metadata_field_filter=None,
    metadata_heritage=None,
    inherited_field=None,
):
    """
    Combine metadata from an object, and its ancestors into a single dictionary representing the metadata heritage.

    Args:
    ----
        obj (Node | Sample | Data | DataAssociation): The `obj` parameter represents
        the object whose metadata is being processed.
        obj_type (str): The `obj_type` parameter represents the type of the object
        node (AnyNode): The anytree node for which the metadata are retrieved.
        The `obj` serves as the starting point for traversing the tree structure.
        metadata_field_filter (Queryset | List | None): A list of metadata fields to
        filter the metadata heritage. If provided, the function will only return
        metadata heritage for the specified metadata fields.
        metadata_heritage (dict): A dictionary containing the metadata heritage of
        the object. The dictionary is updated with metadata from the object and its
        ancestors.
        inherited_field (list): A list of metadata fields that already have been
        inherited.


    Returns:
    -------
        tuple: A tuple containing three elements:
            - metadata_heritage: A dictionary representing the metadata heritage
            of the object. The dictionary keys are the metadata fields, the values
            are lists of metadata values from the object, its ancestors and
            optionally its bound samples.
            - inherited_field: A list of metadata fields that have been inherited.

    Example:
    -------
    (
        {
            "title": [
                {
                    "source": {
                        "type": "node",
                        "id": "<node_id>",
                    },
                    "value": "<title_value>",
                    "inherited": False,
                    "index": 0,
                },
                {
                    "source": {
                        "type": "node",
                        "id": "<ancestor_node_id>",
                    },
                    "value": "<ancestor_title_value>",
                    "inherited": True,
                    "index": 1,
                },
            ],
        },
        ["title"],
        ["<node_id>", "<ancestor_node_id>"],
    )

    """
    # Initialize mutable arguments inside the function
    if metadata_heritage is None:
        metadata_heritage = {}
    if inherited_field is None:
        inherited_field = []

    # Collect metadata from object and ancestors
    if hasattr(node, "metadata"):
        # Filter metadata based on the object type
        # to ensure the metadata field can be associated with the object
        node_metadata = node.metadata
        # node_metadata = node.metadata.filter(**{f"field__{obj_type}_related": True})

        # Apply metadata field filter if provided
        if metadata_field_filter:
            node_metadata = node_metadata.filter(field__in=metadata_field_filter)

        for meta in node_metadata.all():
            # if this metadata object is just a selector, skip it
            if meta.value is None and meta.selected_value is not None:
                continue

            field = meta.field

            # Metadata from the object itself
            if str(obj.id) == str(node.id):
                # set a default value for the field in dictionary in case it does not exists
                metadata_heritage.setdefault(field, set())
                # insert value in metadata heritage
                metadata_heritage[field].add(meta)
            else:
                # Take only the recent inherited metadata
                if field not in inherited_field:
                    # set a default value for the field in dictionary in case it does not exists
                    metadata_heritage.setdefault(field, set())
                    # append value to metadata heritage
                    metadata_heritage[field].add(meta)
                    # leave a trace that we passed here
                    inherited_field.append(field)

        if node.children:
            for child in node.children:
                _combine_metadata(
                    obj,
                    obj_type,
                    child,
                    metadata_field_filter,
                    metadata_heritage,
                    copy(inherited_field),
                )

    return (metadata_heritage, inherited_field)


def node_notary(node, metadata_field_filter=None):
    """
    Manage the metadata heritage for a given node object.

    The metadata heritage includes metadata from the node object itself, and its ancestors.

    Args:
    ----
        node (Node): The node object for which the metadata heritage should be generated.
        metadata_field_filter (QuerySet | List | None, optional): A filter for the metadata fields.
        If provided, only metadata from the specified fields will be included in the heritage.
        Defaults to None.

    Raises:
    ------
        ValueError: If the node parameter is not a Node object.
        ValueError: If the metadata_field_filter parameter is not a QuerySet or list, or contains objects that are not MetadataField.

    Returns:
    -------
        dict: The metadata heritage of the provided node.

    Example:
    -------
    [
        {
            "field": "title",
            "values": [
                {
                    "source": {
                        "type": "node",
                        "id": "<node_id>",
                    },
                    "value": "<title_value>",
                    "inherited": False,
                    "index": 0,
                },
                {
                    "source": {
                        "type": "node",
                        "id": "<ancestor_node_id>",
                    },
                    "value": "<ancestor_title_value>",
                    "inherited": True,
                    "index": 1,
                },
            ],
        },
        ...
    ]

    """
    # Check if the sample is valid
    if not isinstance(node, Node):
        raise ValueError("The node parameter provided is not a Node object.")

    # If a metadata field filter is provided, check that it is a queryset of metadata fields
    if metadata_field_filter:
        if not (
            isinstance(metadata_field_filter, QuerySet)
            or isinstance(metadata_field_filter, list)
        ):
            raise ValueError(
                "The metadata_field_filter parameter provided is not a QuerySet or list object.",
            )
        if not all(isinstance(obj, MetadataField) for obj in metadata_field_filter):
            raise ValueError(
                "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
            )

    # Get all ancestors and the current node
    ancestors = node.get_ancestors(include_self=True).only(
        "id",
        "parent_id",
        "metadata",
    )

    # Create the anytree structure
    tree = _convert_to_anytree(node, ancestors)

    # Return the metadata heritage
    metadata_heritage, _ = _combine_metadata(node, "node", tree, metadata_field_filter)
    return [
        {"field": field, "values": values}
        for field, values in metadata_heritage.items()
    ]


def sample_notary(sample, metadata_field_filter=None):
    """
    Manage the metadata heritage for a given sample object.

    The metadata heritage includes metadata from the sample object itself, its node,
    and the ancestors of its node.

    Args:
    ----
        sample (Sample): The sample object for which the metadata heritage should be generated.
        metadata_field_filter (QuerySet | List | None, optional): A filter for the metadata fields.
        If provided, only metadata from the specified fields will be included in the heritage.
        Defaults to None.

    Raises:
    ------
        ValueError: If the sample parameter is not a Sample object.
        ValueError: If the metadata_field_filter parameter is not a QuerySet or list, or contains objects that are not MetadataField.

    Returns:
    -------
        dict: The metadata heritage of the provided sample.

    Example:
    -------
    [
        {
            "field": "description",
            "values": [
                {
                    "source": {
                        "type": "sample",
                        "id": "<sample_id>",
                    },
                    "value": "<description_value>",
                    "inherited": False,
                    "index": 0,
                },
                {
                    "source": {
                        "type": "node",
                        "id": "<node_id>",
                    },
                    "value": "<node_description_value>",
                    "inherited": True,
                    "index": 1,
                },
                {
                    "source": {
                        "type": "node",
                        "id": "<ancestor_node_id>",
                    },
                    "value": "<ancestor_description_value>",
                    "inherited": True,
                    "index": 2,
                },
            ],
        },
        ...
    ]

    """
    # Check if the sample is valid
    if not isinstance(sample, Sample):
        raise ValueError("The sample parameter provided is not a Sample object.")

    # If a metadata field filter is provided, check that it is a queryset of metadata fields
    if metadata_field_filter:
        if not (
            isinstance(metadata_field_filter, QuerySet)
            or isinstance(metadata_field_filter, list)
        ):
            raise ValueError(
                "The metadata_field_filter parameter provided is not a QuerySet or list object.",
            )
        if not all(isinstance(obj, MetadataField) for obj in metadata_field_filter):
            raise ValueError(
                "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
            )

    # Get the sample's node and its ancestors
    node = sample.node
    ancestors = node.get_ancestors(include_self=True).only(
        "id",
        "parent_id",
        "metadata",
    )

    # Create the anytree structure
    tree = _convert_to_anytree(sample, [sample, *ancestors])

    # Return the metadata heritage
    metadata_heritage, _ = _combine_metadata(
        sample,
        "sample",
        tree,
        metadata_field_filter,
    )
    return [
        {"field": field, "values": values}
        for field, values in metadata_heritage.items()
    ]


def data_notary(data, datalink, metadata_field_filter=None):
    """
    Manage the metadata heritage for a given data object.

    The metadata heritage includes metadata from the data object itself, its node, and the ancestors
    of its node. It also includes metadata from the samples it is bound to (if any)
    and the ancestors nodes of those samples.

    Args:
    ----
        data (Data): The data object for which the metadata heritage should be generated.
        datalink (DataLink): The datalink object associated with the data.
        metadata_field_filter (QuerySet | List | None, optional): A filter for the metadata fields.
        If provided, only metadata from the specified fields will be included in the heritage.
        Defaults to None.

    Raises:
    ------
        ValueError: If the data parameter is not a Data object.
        ValueError: If the metadata_field_filter parameter is not a QuerySet or list, or contains objects that are not MetadataField.

    Returns:
    -------
        dict: The metadata heritage of the provided data.


    Example:
    -------
    [
        {
            "field": "content",
            "values": [
                {
                    "source": {
                        "type": "data",
                        "id": "<data_id>",
                    },
                    "value": "<content_value>",
                    "inherited": False,
                    "index": 0,
                },
                {
                    "source": {
                        "type": "sample",
                        "id": "<sample_id>",
                    },
                    "value": "<sample_content_value>",
                    "inherited": True,
                    "index": 1,
                },
                {
                    "source": {
                        "type": "node",
                        "id": "<node_id>",
                    },
                    "value": "<node_content_value>",
                    "inherited": True,
                    "index": 2,
                },
                {
                    "source": {
                        "type": "node",
                        "id": "<ancestor_node_id>",
                    },
                    "value": "<ancestor_content_value>",
                    "inherited": True,
                    "index": 3,
                },
            ],
        },
        ...
    ]

    """
    # Check if the data and datalink are valid and related
    if not isinstance(data, Data):
        raise ValueError("The data parameter provided is not a Data object.")

    if not isinstance(datalink, DataLink):
        raise ValueError("The datalink parameter provided is not a Datalink object.")

    if datalink.data != data:
        raise ValueError("The data parameter and datalink parameter do not match.")

    # If a metadata field filter is provided, check that it is a queryset of metadata fields
    if metadata_field_filter:
        if not (
            isinstance(metadata_field_filter, QuerySet)
            or isinstance(metadata_field_filter, list)
        ):
            raise ValueError(
                "The metadata_field_filter parameter provided is not a QuerySet or list object.",
            )
        if not all(isinstance(obj, MetadataField) for obj in metadata_field_filter):
            raise ValueError(
                "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
            )

    # Retrieve the node and its ancestors
    node = datalink.node
    ancestors = node.get_ancestors(include_self=True).only(
        "id",
        "parent_id",
        "metadata",
    )

    # Add a node_id property to the data object, this is used to create the anytree structure
    data.node_id = node.id

    # Get all samples that are bound to the data
    bound_samples = Sample.objects.filter(bound_data__data=data)

    # Create the main anytree structure
    tree = _convert_to_anytree(data, set([data, *ancestors]))

    # construct a tree for each bound samples
    sample_trees = []
    for sample in bound_samples:
        ancestors = sample.node.get_ancestors(include_self=True).only(
            "id",
            "parent_id",
            "metadata",
        )
        sample_trees.append(_convert_to_anytree(sample, [sample, *ancestors]))

    # Combine the main tree with the sample trees
    for sample_tree in sample_trees:
        sample_tree.parent = tree

    # Return the metadata heritage
    metadata_heritage, _ = _combine_metadata(data, "data", tree, metadata_field_filter)
    return [
        {"field": field, "values": values}
        for field, values in metadata_heritage.items()
    ]


def data_association_notary(data_association, metadata_field_filter=None):
    """
    Manage the metadata heritage for a given data association object.

    The metadata heritage includes metadata from the node object itself, and its ancestors.

    Args:
    ----
        data_association (DataAssociation): The DataAssociation object for which the metadata heritage should be generated.
        metadata_field_filter (QuerySet | List | None, optional): A filter for the metadata fields.
        If provided, only metadata from the specified fields will be included in the heritage.
        Defaults to None.

    Raises:
    ------
        ValueError: If the node parameter is not a DataAssociation object.
        ValueError: If the metadata_field_filter parameter is not a QuerySet or list, or contains objects that are not MetadataField.

    Returns:
    -------
        dict: The metadata heritage of the provided node.

    Example:
    -------
    [
        {
            "field": "title",
            "values": [
                {
                    "source": {
                        "type": "DataAssociation",
                        "id": "<data_association_id>",
                    },
                    "value": "<title_value>",
                    "inherited": False,
                    "index": 0,
                },
            ],
        },
        ...
    ]

    """
    # Check if the sample is valid
    if not isinstance(data_association, DataAssociation):
        raise ValueError("The node parameter provided is not a Node object.")

    # If a metadata field filter is provided, check that it is a queryset of metadata fields
    if metadata_field_filter:
        if not (
            isinstance(metadata_field_filter, QuerySet)
            or isinstance(metadata_field_filter, list)
        ):
            raise ValueError(
                "The metadata_field_filter parameter provided is not a QuerySet or list object.",
            )
        if not all(isinstance(obj, MetadataField) for obj in metadata_field_filter):
            raise ValueError(
                "The metadata_field_filter parameter provided contains objects that are not MetadataField objects.",
            )

    # Create the anytree structure
    tree = _convert_to_anytree(data_association, [data_association])

    # Return the metadata heritage
    metadata_heritage, _ = _combine_metadata(
        data_association, "data_association", tree, metadata_field_filter
    )
    return [
        {"field": field, "values": values}
        for field, values in metadata_heritage.items()
    ]
