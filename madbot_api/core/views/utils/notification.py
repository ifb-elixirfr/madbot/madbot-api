from notifications.signals import notify

from madbot_api.core.views.notifications import (
    NotificationsSerializer,
    send_notification_socket,
)


def trigger_notification(
    sender,
    recipient,
    verb,
    action_object,
    target,
    actor,
    workspace,
):
    """
    Send a notification to a recipient using the provided parameters.

    Args:
    ----
        sender: The user or entity triggering the notification.
        recipient: The user who will receive the notification.
        verb: A string describing the action or event being notified (e.g., "liked").
        action_object: The object or item affected by the action (e.g., the post that was liked).
        target: The specific item, user, or resource that is the focus of the action.
        actor: The entity or user who performed the action that triggered the notification.
        workspace: The context or description associated with the notification.

    Returns:
    -------
        The notification object sent to the recipient.

    """
    notification = notify.send(
        sender=sender,
        recipient=recipient,
        verb=verb,
        action_object=action_object,
        target=target,
        actor=actor,
        description=workspace,
    )
    notification_serializer = NotificationsSerializer(notification[0][1][0])
    send_notification_socket(
        recipient.username,
        notification_serializer.data,
        target.__class__.__name__,
    )
