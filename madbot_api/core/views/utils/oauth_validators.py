from oauth2_provider.oauth2_validators import OAuth2Validator


class CustomOAuth2Validator(OAuth2Validator):
    """Custom OAuth2 validator for handling additional user claims."""

    oidc_claim_scope = None
    # The line `oidc_claim_scope = None` is defining
    # the OpenID Connect (OIDC) claim scope for the OAuth2Validator class.

    def get_additional_claims(self, request):
        """Return additional claims for a user based on their request."""
        return {
            "given_name": request.user.first_name,
            "family_name": request.user.last_name,
            "name": " ".join([request.user.first_name, request.user.last_name]),
            "preferred_username": request.user.username,
            "email": request.user.email,
        }
