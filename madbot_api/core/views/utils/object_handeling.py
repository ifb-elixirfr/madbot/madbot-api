"""Utility functions for converting objects to dictionaries."""


def object_to_dictionary(obj):
    """
    Create a dictionary representation of its parameters.

    Args:
    ----
        obj: The object from which the dictionary representation will be created.

    Returns:
    -------
        A dictionary containing the parameters of the object.

    Example:
    -------
        Suppose we have an object `obj` with parameters `param1`, `param2`, and `param3`.
        The function `object_to_dictionary(obj)` would return a dictionary like this:

        {
            'param1': value1,
            'param2': value2,
            'param3': value3
        }

    """
    obj_dict = {}
    for param in obj:
        param_dict = param.to_dict()
        obj_dict.update(param_dict)
    return obj_dict
