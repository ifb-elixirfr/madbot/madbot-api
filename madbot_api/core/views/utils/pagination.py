from rest_framework import pagination

# The `StandardPagination` class is a subclass of `PageNumberPagination` that adds
# additional data to the paginated response, including the current page number and
# the total number of pages.


class StandardPagination(pagination.PageNumberPagination):
    """Custom pagination class that defines the pagination behavior for API responses."""

    page_size = 10
    page_size_query_param = "page_size"
    max_page_size = 50
    page_query_param = "p"

    def get_paginated_response(self, data, extra_data=None):
        """
        Add the current page number and total number of pages to the paginated response.

        Args:
        ----
            data: The paginated data to be returned in the response, which can be
                a list of objects or any other data structure that needs pagination.
            extra_data: Optional additional data to include in the response.

        Returns:
        -------
            A response object containing the paginated data, current page number,
            total number of pages, and any extra data provided.

        """
        response = super(StandardPagination, self).get_paginated_response(data)
        if extra_data:
            response.data["extra_data"] = extra_data
        response.data["current_page"] = self.page.number
        response.data["total_pages"] = self.page.paginator.num_pages

        return response
