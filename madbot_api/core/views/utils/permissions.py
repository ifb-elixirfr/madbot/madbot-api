# ###############################################################################
# # Permissions - API
# ###############################################################################


def method_permission_classes(classes):
    """
    Set the permission classes on a view instance.

    This decorator wraps the call and sets the permission classes on the view
    instance that is being called, allowing the normal get_permissions()
    method to function without modification.

    Args:
    ----
        classes: The permission classes to be set on the view instance.

    Returns:
    -------
        A decorator that sets the permission classes on the view instance.

    Usage:
        @method_permission_classes([IsAuthenticated])
        def some_view(self, request):
            ...

    """

    # Instead of setting the permission_classes property on the function,
    # like the built-in decorator does, this decorator wraps the call and
    # sets the permission classes on the view instance that is being called.
    # This way, the normal get_permissions() doesn't need any changes,
    # since that simply relies on self.permission_classes.
    def decorator(func):
        def decorated_func(self, *args, **kwargs):
            # To work with request permissions,
            # we do need to call check_permission() from the decorator,
            # because it's originally called in initial() so before
            # the permission_classes property is patched.
            self.permission_classes = classes
            # this call is needed for request permissions
            self.check_permissions(self.request)
            return func(self, *args, **kwargs)

        return decorated_func

    return decorator
