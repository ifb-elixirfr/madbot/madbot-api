from madbot_api.core.errors import (
    ConnectorException,
    ForbiddenException,
    ObjectNotFoundException,
    SaveException,
    ValidationException,
)


def custom_error_schema_response(exception_type, detail=None):
    """
    Generate a JSON schema for error responses, including an optional detail field.

    Args:
    ----
        exception_type: The type of exception that occurred, expected to have
            `code` and `message` attributes to populate the response schema.
        detail: Optional additional information about the error, can contain
            any properties or data for the error response.

    Returns:
    -------
        A JSON schema for a custom error response.

    """
    base_response_schema = {
        "type": "object",
        "properties": {
            "code": {"type": "string", "minLength": 1},
            "message": {"type": "string", "minLength": 1},
        },
        "example": {
            "code": exception_type.code,
            "message": exception_type.message,
        },
        "required": ["code", "message"],
    }

    if detail is not None:
        base_response_schema["properties"]["detail"] = {
            "type": "object",
            "minLength": 1,
        }
        base_response_schema["example"]["detail"] = detail
        base_response_schema["required"].append("detail")

    return base_response_schema


def cesr_404_object_not_found():
    """
    Return a schema response for a 404 Not Found error.

    Returns
    -------
        A dictionary containing a 404 response schema for an object not found error.

    """
    return {
        (404, "application/json"): custom_error_schema_response(
            ObjectNotFoundException(parameter="<id>", parameter_value="<id>"),
        ),
    }


def cesr_403_forbidden(detail):
    """
    Return a schema response for a 403 Forbidden error.

    Args:
    ----
        detail: Additional details about the forbidden error.

    Returns:
    -------
        A dictionary containing a 403 response schema for a forbidden error.

    """
    return {
        (403, "application/json"): custom_error_schema_response(
            ForbiddenException,
            detail=detail,
        ),
    }


def cesr_400_validation_error(detail):
    """
    Return a schema response for a 400 Validation Error.

    Args:
    ----
        detail: Additional details about the validation error.

    Returns:
    -------
        A dictionary containing a 400 response schema for a validation error.

    """
    return {
        (400, "application/json"): custom_error_schema_response(
            ValidationException,
            detail=detail,
        ),
    }


def cesr_400_query_validation_error(detail):
    """
    Return a schema response for a 400 Query Validation Error.

    Args:
    ----
        detail: Additional details about the query validation error.

    Returns:
    -------
        A dictionary containing a 400 response schema for a query validation error.

    """
    return {
        (400, "application/json"): custom_error_schema_response(
            ValidationException("query_validation_error"),
            detail=detail,
        ),
    }


def cesr_400_body_validation_error(detail):
    """
    Return a schema response for a 400 Body Validation Error.

    Args:
    ----
        detail: Additional details about the body validation error.

    Returns:
    -------
        A dictionary containing a 400 response schema for a body validation error.

    """
    return {
        (400, "application/json"): custom_error_schema_response(
            ValidationException("body_validation_error"),
            detail=detail,
        ),
    }


def cesr_400_uri_validation_error(detail):
    """
    Return a schema response for a 400 URI Validation Error.

    Args:
    ----
        detail: Additional details about the URI validation error.

    Returns:
    -------
        A dictionary containing a 400 response schema for a URI validation error.

    """
    return {
        (400, "application/json"): custom_error_schema_response(
            ValidationException("uri_validation_error"),
            detail=detail,
        ),
    }


def cesr_400_save_validation_error(detail):
    """
    Return a schema response for a 400 Save Validation Error.

    Args:
    ----
        detail: Additional details about the save validation error.

    Returns:
    -------
        A dictionary containing a 400 response schema for a save validation error.

    """
    return {
        (400, "application/json"): custom_error_schema_response(
            SaveException,
            detail=detail,
        ),
    }


def cesr_400_connector_error(detail):
    """
    Return a schema response for a 400 Connector Error.

    Args:
    ----
        detail: Additional details about the connector error.

    Returns:
    -------
        A dictionary containing a 400 response schema for a connector error.

    """
    return {
        (400, "application/json"): custom_error_schema_response(
            ConnectorException,
            detail=detail,
        ),
    }
