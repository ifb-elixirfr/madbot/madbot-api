import requests
from jsonschema import Draft201909Validator, exceptions

# def is_ncbi_taxon(instance):
#     if not isinstance(instance, str):
#         return False
#     response = requests.get(
#         url='https://www.ebi.ac.uk/ols4/api/v2/entities?search="'
#         + instance.split("::")[1]
#         + '"&size=20&lang=en&exactMatch=false&includeObsoleteEntities=false&ontologyId=ncbitaxon'
#     )
#     if response.status_code != 200:
#         return False
#     testValidation = False
#     for item in response.json()["elements"]:
#         if (
#             item["curie"].split(":")[1] == instance.split("::")[0]
#             and item["label"] == instance.split("::")[1]
#         ):
#             testValidation = True
#             break

#     return testValidation


def verify_rorid(rorid):
    """
    Verify if an RORID exists.

    Args:
    ----
        rorid (str): The rorid of the person to verify.

    Returns:
    -------
        bool: True if the given RORID exists.

    """
    headers = {"Accept": "application/json"}
    api_url = f"https://api.ror.org/v2/organizations/{rorid}"

    try:
        response = requests.get(api_url, headers=headers, timeout=10)
        response.raise_for_status()
        return True
    except requests.exceptions.RequestException:
        raise exceptions.ValidationError(
            f"The RORID '{rorid}' did not return a successful response."
        )


def verify_orcid_names(orcid):
    """
    Verify if an ORCID exists.

    Args:
    ----
        orcid (str): The orcid of the person to verify.

    Returns:
    -------
        bool: True if the given ORCID exists.

    """
    headers = {"Accept": "application/json"}
    api_url = f"https://pub.orcid.org/v3.0/{orcid}"

    try:
        response = requests.get(api_url, headers=headers, timeout=10)
        response.raise_for_status()
        return True
    except requests.exceptions.RequestException:
        raise exceptions.ValidationError(
            f"The ORCID '{orcid}' did not return a successful response."
        )


def verify_pubmed_title_via_requests(pmid, expected_title):
    """
    Validate if the title of a PubMed article matches an expected title using the PubMed API via HTTP.

    :param pmid: PubMed ID to fetch.
    :param expected_title: The expected title to compare against.
    :return: True if the titles match, False otherwise.

    This function sends a request to the PubMed API and retrieves the article's title using the given PubMed ID.
    It then compares the fetched title with the provided expected title and returns a boolean indicating
    whether they match or not. If the title cannot be fetched or an error occurs, the function will return False.
    """
    url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi"
    params = {"db": "pubmed", "id": pmid, "retmode": "json"}
    test_validation = False
    try:
        response = requests.get(url, params=params, timeout=10)
        response.raise_for_status()
        data = response.json()
        actual_title = data["result"][pmid]["title"]
        test_validation = actual_title == expected_title
    except Exception:
        test_validation = False

    if test_validation is not True:
        raise exceptions.ValidationError(
            f"The title '{expected_title}' does not match the title associated with pubmedID '{pmid}'",
        )


def verify_doi_title(doi, expected_title):
    """
    Verify if the title of a DOI matches the expected title.

    Args:
    ----
        doi (str): The DOI of the article to verify.
        expected_title (str): The expected title to match against.

    Returns:
    -------
        bool: True if the titles match, False otherwise.

    Raises:
    ------
        None: This function handles exceptions internally and returns appropriate error messages.

    """
    api_url = f"https://api.crossref.org/works/{doi}"

    try:
        response = requests.get(api_url, timeout=10)
        response.raise_for_status()

        data = response.json()
        actual_title = data.get("message", {}).get("title", [""])[0]

        if not actual_title:
            return False

        return actual_title.strip().lower() == expected_title.strip().lower()
    except requests.exceptions.RequestException:
        raise exceptions.ValidationError(
            f"The title '{expected_title}' does not match the title associated with doi '{doi}'",
        )
    except KeyError:
        raise exceptions.ValidationError(
            f"The title '{expected_title}' does not match the title associated with doi '{doi}'",
        )


def cross_validate(self, instance):
    """
    Validate the provided instance by cross-referencing the scientific name and taxon ID with the NCBI Taxonomy ontology.

    This function checks if the "scientificName" and "taxonID" in the instance match with the records in the NCBI Taxonomy ontology
    using the EBI OLS API. It sends a GET request to the API with the scientific name, and if the response status is not 200,
    it raises a ValidationError indicating that the scientific name does not exist. It then checks if any element in the response
    contains the provided taxon ID and scientific name. If no match is found, it raises a ValidationError indicating that the
    scientific name does not match the name associated with the ID.

    Args:
    ----
        self: The instance of the class that includes the schema for validation.
        instance (dict): A dictionary containing the keys "scientificName" and "taxonID".

    Raises:
    ------
        ValidationError: If the response from the API is not 200, or if the scientific name and taxon ID do not match.

    Example:
    -------
        instance = {
            "scientificName": "Homo sapiens",
            "taxonID": "9606"
        }
        cross_validate(instance)  # This will raise a ValidationError if the validation fails.

    """
    # Get the schema reference if it exists
    schema_ref = self.schema.get("$ref")

    if not schema_ref:
        return

    if schema_ref == "/api/schemas/referential/person" and "orcid" in instance:
        verify_orcid_names(instance["orcid"])

    elif schema_ref == "/api/schemas/referential/organization" and "rorid" in instance:
        verify_rorid(instance["rorid"])

    elif (
        schema_ref == "/api/schemas/referential/pubmed"
        and "pubmedID" in instance
        and "title" in instance
    ):
        verify_pubmed_title_via_requests(instance["pubmedID"], instance["title"])

    elif (
        schema_ref == "/api/schemas/referential/doi"
        and "doi" in instance
        and "title" in instance
    ):
        verify_doi_title(instance["doi"], instance["title"])

    elif schema_ref == "/api/schemas/referential/publication-collection":
        for publication in instance:
            if "pubmedID" in publication and "title" in publication:
                verify_pubmed_title_via_requests(
                    publication["pubmedID"], publication["title"]
                )
            elif "doi" in publication and "title" in publication:
                verify_doi_title(publication["doi"], publication["title"])

    elif (
        schema_ref == "/api/schemas/referential/taxon"
        and "scientificName" in instance
        and "taxonID" in instance
    ):
        id = instance["taxonID"]
        scientific_name = instance["scientificName"]
        response = requests.get(
            url='https://www.ebi.ac.uk/ols4/api/v2/entities?search="'
            + scientific_name
            + '"&size=20&lang=en&exactMatch=false&includeObsoleteEntities=false&ontologyId=ncbitaxon',
            timeout=10,
        )
        if response.status_code != 200:
            raise exceptions.ValidationError(
                f"Scientific name '{scientific_name}' does not exist"
            )
        test_validation = False
        for item in response.json()["elements"]:
            if item["curie"].split(":")[1] == id and item["label"] == scientific_name:
                test_validation = True
                break
        if not test_validation:
            raise exceptions.ValidationError(
                f"Scientific name '{scientific_name}' does not match the ID '{id}'"
            )


class MadbotValidator(Draft201909Validator):
    """Custom validator class extending the JSON Schema Draft 2019-09 Validator."""

    def validate(self, instance, _schema=None):
        """
        Validate the given instance against the schema and perform additional checks.

        This method calls the parent class's validate method and performs cross-validation
        for taxonomic data using the cross_validate method.

        Args:
        ----
            instance: The instance to validate against the schema.
            _schema: An optional schema to validate against. If not provided, the class schema is used.

        Raises:
        ------
            ValidationError: If validation fails according to the schema or cross-validation checks.

        """
        super().validate(instance, _schema)

        cross_validate(self, instance)
