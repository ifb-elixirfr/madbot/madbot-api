from rest_framework import serializers
from rest_framework.exceptions import ErrorDetail

from madbot_api.core.errors import ValidationException


class MadbotSerializer(serializers.ModelSerializer):
    """Serializer for validating and serializing data for the Madbot model."""

    def __init__(self, *args, **kwargs):
        """
        Initialize the serializer and validate the supported fields.

        This method checks if certain fields are supported in the `fields` query parameter and
        raises an exception if any unsupported fields are found.

        Args:
        ----
            *args: Positional arguments for the parent serializer.
            **kwargs: Keyword arguments for the parent serializer, including `context` for the request.

        """
        request = kwargs.get("context", {}).get("request")
        str_fields = request.GET.get("fields", "") if request else None
        fields = str_fields.split(",") if str_fields else []

        super(MadbotSerializer, self).__init__(*args, **kwargs)

        unsupported_fields = sorted(
            list(
                set(fields)
                - set(
                    list(getattr(self.Meta, "authorized_extra_fields", {}).keys())
                    + list(self.fields.keys()),
                ),
            ),
        )
        if len(unsupported_fields) != 0:
            raise ValidationException(
                code="query_validation_error",
                details={
                    field_name: [
                        ErrorDetail(
                            "This field is not supported.",
                            code="not_supported",
                        ),
                    ]
                    for field_name in unsupported_fields
                },
            )

        for field_name in set(fields).intersection(
            set(getattr(self.Meta, "authorized_extra_fields", {}).keys()),
        ):
            self.fields[field_name] = self.Meta.authorized_extra_fields[field_name]

    def validate(self, *args, **kwargs):
        """
        Validate the input data against the serializer fields.

        This method checks if the body has any unknown fields and raises a
        ValidationError if unsupported fields are found.

        Args:
        ----
            *args: Positional arguments for validation.
            **kwargs: Keyword arguments for validation.

        Raises:
        ------
            serializers.ValidationError: If unknown fields are found in the input data.

        """
        # check if the body has any unknown fields
        unknown_body_params = sorted(
            list(
                set(self.initial_data)
                - (set(self.fields) - set(getattr(self.Meta, "read_only_fields", []))),
            ),
        )
        if unknown_body_params:
            errors = {
                param_name: [
                    ErrorDetail("This field is not supported.", code="not_supported"),
                ]
                for param_name in unknown_body_params
            }
            raise serializers.ValidationError(errors)
        return super().validate(*args, **kwargs)

    def is_valid(self, raise_exception=True):
        """
        Check if the current object is valid and raises a custom exception if it is not.

        Args:
        ----
            raise_exception (bool): Determines whether an exception should be raised if validation fails.
            Defaults to True.

        Raises:
        ------
            ValidationException: If validation fails and `raise_exception` is True.

        Returns:
        -------
            bool: Indicates whether the object is valid.

        """
        valid = super().is_valid(raise_exception=False)
        if not valid and raise_exception:
            raise ValidationException(
                code="body_validation_error",
                details=self.errors,
                initial_data=self.initial_data,
            )
        return valid


class InteractorSerializerMixin(serializers.ModelSerializer):
    """Mixin for serializing interactor information."""

    def get_interactor_information(self, object):
        """
        Return information about an object, including its class name and ID.

        Args:
        ----
            object: The object to retrieve information from.

        Returns:
        -------
            dict: A dictionary with the object's class name and ID.

        """
        return {
            "object": object.__class__.__name__ if object else None,
            "id": object.id if object else None,
        }
