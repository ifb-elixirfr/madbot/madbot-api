import json


def validate_json(json_data):
    """
    Check if a given string is a valid JSON by attempting to parse it.

    This function attempts to parse the input string as JSON and returns True if it is valid,
    or False otherwise.

    Args:
    ----
        json_data (str or dict): Provide a string representing a JSON object or a dictionary.

    Returns:
    -------
        bool: Return True if the json_data is a valid JSON string, and False otherwise.

    """
    if isinstance(json_data, dict):
        return True

    try:
        json.loads(json_data)
    except ValueError:
        return False
    return True
