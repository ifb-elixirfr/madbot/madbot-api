import os

import hvac
from django.conf import settings
from hvac.exceptions import Forbidden


class MetaSingleton(type):
    """A metaclass for creating singleton instances."""

    __instances = {}

    def __call__(cls, *args, **kwargs):
        """Create or return the singleton instance of the class."""
        if cls not in MetaSingleton.__instances:
            MetaSingleton.__instances[cls] = super(MetaSingleton, cls).__call__(
                *args,
                **kwargs,
            )
        return MetaSingleton.__instances[cls]


class Vault(metaclass=MetaSingleton):
    """Vault client singleton for interacting with Vault secrets."""

    def __init__(self):
        """Initialize the Vault client and authenticate using AppRole credentials."""
        self.vault_client = hvac.Client(url=settings.MADBOT_VAULT_URL)
        self.vault_client.auth.approle.login(
            role_id=settings.MADBOT_VAULT_ROLEID,
            secret_id=settings.MADBOT_VAULT_SECRETID,
        )

    def refresh_authentication(self):
        """Refresh_authentication is used to refresh the authentication credentials."""
        self.vault_client.auth.approle.login(
            role_id=settings.MADBOT_VAULT_ROLEID,
            secret_id=settings.MADBOT_VAULT_SECRETID,
        )

    def _read_secret(self, user, object):
        """
        Read a secret object for a specific user.

        Args:
        ----
            user: Provide the user who is trying to access the secret information.
            object: Provide the object or resource that the user is trying to access.

        Returns:
        -------
            dict: Return the secret data associated with the user and object.

        """
        path = os.path.join(str(user.id), str(object.id))
        self.vault_client.kv.default_kv_version = 1
        value = self.vault_client.secrets.kv.v1.read_secret(
            mount_point=settings.MADBOT_VAULT_MOUNT_POINT,
            path=path,
        )
        return value["data"]

    def read_secret(self, user, object):
        """
        Read secret in vault.

        Args:
        ----
            user: Provide the user for whom the secret is being read.
            object: Provide the object associated with the secret.

        Returns:
        -------
        dict: Return the secret data retrieved from the vault.

        """
        try:
            return self._read_secret(user, object)
        except Forbidden:
            self.refresh_authentication()
            return self._read_secret(user, object)

    def _write_secret(self, user, tool, data):
        """
        Write secret data to Vault using the provided user, tool, and data.

        Args:
        ----
            user: Provide the user for whom the secret data is being written.
            tool: Provide the tool representing the secret data.
            data: Provide a list of dictionaries for the secret data.

        """
        path = os.path.join(str(user.id), str(tool.id))

        # Write the formatted secret data to the Vault
        self.vault_client.secrets.kv.v1.create_or_update_secret(
            mount_point=settings.MADBOT_VAULT_MOUNT_POINT,
            path=path,
            secret=data,
        )

    def write_secret(self, user, credential, data):
        """
        Register secret in vault.

        Args:
        ----
            user: Provide the user for whom the secret is being registered.
            credential: Provide the credential tool associated with the secret.
            data: Provide a list of dictionaries for the secret data.

        """
        try:
            return self._write_secret(user, credential, data)
        except Forbidden:
            self.refresh_authentication()
            return self._write_secret(user, credential, data)

    def _delete_secret(self, user, tool):
        """
        Delete a secret associated with a user and a tool.

        Args:
        ----
            user: Provide the user who wants to delete a secret.
            tool: Provide the identifier of the secret tool that the user wants to delete.

        """
        path = os.path.join(str(user.id), str(tool.id))
        self.vault_client.secrets.kv.v1.delete_secret(
            mount_point=settings.MADBOT_VAULT_MOUNT_POINT,
            path=path,
        )

    def delete_secret(self, user, tool):
        """
        Delete secret in vault.

        Args:
        ----
            user: Provide the user who wants to delete the secret.
            tool: Provide the identifier of the tool associated with the secret.

        """
        try:
            self._delete_secret(user, tool)
        except Forbidden:
            self.refresh_authentication()
            self._delete_secret(user, tool)
