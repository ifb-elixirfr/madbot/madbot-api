from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from madbot_api import VERSION

########################################################################################
# Serializer methods
########################################################################################


class VersionSerializer(serializers.Serializer):
    """Serializer for the version of the Madbot API."""

    version = serializers.CharField(max_length=50)


########################################################################################
# ViewSets class
########################################################################################


class VersionView(APIView):
    """Retrieve the version of the Madbot API."""

    def get(self, request):
        """Return the version of the Madbot API."""
        serializer = VersionSerializer(data={"version": VERSION})
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)
