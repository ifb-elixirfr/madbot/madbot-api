from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema
from rest_framework import filters, serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import Workspace, WorkspaceMember
from madbot_api.core.serializers.user import UserSerializer
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.filter import MemberFilter
from madbot_api.core.views.utils.member import count_owners
from madbot_api.core.views.utils.mixin import BelongsToObjectMixin, GetObjectMixin
from madbot_api.core.views.utils.notification import trigger_notification
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_400_uri_validation_error,
    cesr_403_forbidden,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class WorkspaceMemberSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for WorkspaceMember model."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for WorkspaceMemberSerializer."""

        model = WorkspaceMember
        fields = [
            "id",
            "user",
            "role",
            "workspace",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)

    def to_representation(self, instance):
        """
        Modify the representation of an instance by including user data and excluding the workspace field.

        Args:
        ----
            instance: The model instance being serialized, typically containing
            user information.

        Returns:
        -------
            dict: A dictionary representation of the instance, including the
            serialized user data and excluding the workspace field.

        """
        representation = super().to_representation(instance)
        user = UserSerializer(instance.user).data
        representation["user"] = user
        # Exclude workspace field from the representation
        representation.pop("workspace", None)
        return representation


class WorkspaceMemberSerializerUpdate(WorkspaceMemberSerializer):
    """Serializer for updating WorkspaceMember data."""

    role = serializers.ChoiceField(
        required=False,
        choices=WorkspaceMember.ROLE_WORKSPACE_MEMBER,
    )

    class Meta:
        """Meta class for WorkspaceMemberSerializerUpdate."""

        model = WorkspaceMember
        fields = [
            "id",
            "user",
            "workspace",
            "role",
            "created_time",
            "created_by",
            "last_edited_time",
            "last_edited_by",
        ]
        read_only_fields = [
            "id",
            "user",
            "workspace",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]


########################################################################################
# ViewSets class
########################################################################################


class WorkspaceMemberViewSet(MadbotViewSet, GetObjectMixin, BelongsToObjectMixin):
    """ViewSet for managing workspace members."""

    serializer_class = WorkspaceMemberSerializer
    queryset = WorkspaceMember.objects.all().order_by("user__username")

    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_class = MemberFilter

    @extend_schema(
        responses={
            200: WorkspaceMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "workspace",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            ),
            **cesr_404_object_not_found(),
        },
    )
    def list(self, request, workspace_pk=None):
        """Retrieve the list of workspace members for a specific workspace."""
        # try to get the workspace
        self.get_the_object(Workspace, workspace_pk, "uri")
        try:
            WorkspaceMember.objects.get(
                workspace__pk=workspace_pk,
                user=request.user.id,
            )
        # check if the user is a member of the workspace
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )

        serializer = WorkspaceMemberSerializer(
            self.filter_queryset(
                self.get_queryset().filter(workspace__pk=workspace_pk),
            ),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            201: WorkspaceMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter_name>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "user",
                    "code": "does_not_exist",
                    "message": '"<parameter_value>" does not exist.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "role",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "required",
                    "message": "This field is required.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You must be owner of the workspace to add a new member.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You can only add new members with a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "user",
                    "code": "not_allowed",
                    "message": "User is already a member of this workspace.",
                },
            ),
            **cesr_404_object_not_found(),
        },
    )
    def create(self, request, workspace_pk=None, *args, **kwargs):
        """Create a new workspace member object."""
        # get the workspace
        workspace = self.get_the_object(Workspace, workspace_pk, "uri")
        try:
            current_member = WorkspaceMember.objects.get(
                workspace__pk=workspace_pk,
                user=request.user.id,
            )

        # check if the user is a member of the workspace
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )
        # check if the user is owner the workspace
        if current_member.role != "owner":
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You must be owner of the workspace to add a new member.",
                },
            )

        # Check the body data
        serializer = WorkspaceMemberSerializer(
            data={**request.data, "workspace": workspace.id},
        )
        serializer.is_valid()

        # check if the user is trying to add a member with a role above his
        if (
            request.data.get("role", None) == "owner"
            and current_member.role == "member"
        ):
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "You can only add new members with a"
                    " role below or equal to yours.",
                },
            )

        # Create the WorkspaceMember with the associated workspace
        member = serializer.save(created_by=request.user, last_edited_by=request.user)

        # trigger the notification and send notification
        workspace_members = workspace.workspace_members.all().exclude(user=request.user)
        for workspace_member in workspace_members:
            trigger_notification(
                sender=request.user,
                recipient=workspace_member.user,
                verb="created",
                action_object=member,
                target=workspace,
                actor=request.user,
                workspace=workspace.id,
            )

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: WorkspaceMemberSerializer,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "role",
                    "code": "invalid_choice",
                    "message": '"<parameter_value>" is not a valid choice.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You must be owner of the workspace to edit other members.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "The role of the workspace member cannot be changed because they are the last owner of the current workspace.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You can only change a member to a role below or equal to yours.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You cannot promote yourself.",
                },
            ),
            **cesr_404_object_not_found(),
        },
    )
    def update(
        self,
        request,
        pk,
        workspace_pk=None,
    ):
        """Update a workspace member item."""
        # get the workspace
        workspace = self.get_the_object(Workspace, workspace_pk, "uri")
        # get the member to update
        item = self.get_object()

        try:
            current_member = WorkspaceMember.objects.get(
                workspace__pk=workspace_pk,
                user=request.user.id,
            )
        # check if the user is a member of the workspace
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )

        self.check_object_belongs_to_parent(
            Workspace,
            WorkspaceMember,
            "workspace",
            "workspace_member",
            pk,
            workspace_pk,
        )
        # check is the user is trying to promote themselves
        if (
            item.user.id == request.user.id
            and request.data.get("role", None) == "owner"
            and current_member.role == "member"
        ):
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You cannot promote yourself.",
                },
            )

        # check if the user has the right to modify members
        if current_member.role != "owner":
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You must be owner of the workspace to edit other members.",
                },
            )

        # check is the user is trying to update the last owner
        if item.role == "owner" and count_owners(workspace) == 1:
            raise ForbiddenException(
                details={
                    "parameter": "role",
                    "code": "not_allowed",
                    "message": "The role of the workspace member cannot be changed"
                    " because they are the last owner of the current workspace.",
                },
            )

        # check if the user is trying to demote a member that has a role above his
        if item.role == "owner" and current_member.role == "member":
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You can only change a member with a"
                    " role equal to yours.",
                },
            )

        serializer = WorkspaceMemberSerializerUpdate(item, data=request.data)
        serializer.is_valid()
        workspace_member = serializer.save(last_edited_by=request.user)

        # trigger the notification
        trigger_notification(
            sender=request.user,
            recipient=item.user,
            verb="changed",
            action_object=workspace_member,
            target=workspace,
            actor=request.user,
            workspace=workspace.id,
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_400_uri_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "invalid",
                    "message": '"<parameter_value>" is not a valid UUID.',
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You must be owner of the workspace to remove another member.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because they are the last owner of the current workspace.",
                },
            ),
            **cesr_403_forbidden(
                detail={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You can't remove a workspace member with a role above yours.",
                },
            ),
            **cesr_404_object_not_found(),
        },
    )
    def destroy(
        self,
        request,
        pk,
        workspace_pk=None,
    ):
        """Delete a workspace member item."""
        # get the workspace
        workspace = self.get_the_object(Workspace, workspace_pk, "uri")
        # get the member to delete
        item = self.get_object()
        try:
            current_member = WorkspaceMember.objects.get(
                workspace__pk=workspace_pk,
                user=request.user,
            )
        # check if the user is a member of the node
        except ObjectDoesNotExist:
            raise ForbiddenException(
                details={
                    "parameter": "workspace",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )
        self.check_object_belongs_to_parent(
            Workspace,
            WorkspaceMember,
            "workspace",
            "workspace_member",
            pk,
            workspace_pk,
        )

        # check if the user has the right to modify members
        if current_member.role != "owner" and str(current_member.id) != pk:
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You must be owner of the workspace to remove another member.",
                },
            )
        # check if the user is trying to remove a member that has a role above his
        if item.role == "owner" and current_member.role == "member":
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You can't remove a workspace member with a role above yours.",
                },
            )
        # check is the user is trying to remove the last owner
        if item.role == "owner" and count_owners(workspace) == 1:
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "This member cannot be removed because"
                    " they are the last owner of the current workspace.",
                },
            )
        item.delete()

        # trigger the notification
        trigger_notification(
            sender=request.user,
            recipient=item.user,
            verb="suppressed",
            action_object=item,
            target=workspace,
            actor=request.user,
            workspace=workspace.id,
        )
        return Response(status=status.HTTP_204_NO_CONTENT)
