from drf_spectacular.utils import extend_schema
from rest_framework import serializers, status
from rest_framework.response import Response

from madbot_api.core.errors import ForbiddenException
from madbot_api.core.models import Workspace, WorkspaceMember
from madbot_api.core.views.utils.drf import MadbotViewSet
from madbot_api.core.views.utils.schema_response import (
    cesr_400_body_validation_error,
    cesr_404_object_not_found,
)
from madbot_api.core.views.utils.serializer import (
    InteractorSerializerMixin,
    MadbotSerializer,
)

########################################################################################
# Serializer methods
########################################################################################


class WorkspaceSerializer(MadbotSerializer, InteractorSerializerMixin):
    """Serializer for handling workspace data, including creator and editor information."""

    created_by = serializers.SerializerMethodField()
    last_edited_by = serializers.SerializerMethodField()

    class Meta:
        """Meta class for WorkspaceSerializer."""

        model = Workspace
        fields = [
            "id",
            "name",
            "description",
            "slug",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]
        read_only_fields = [
            "id",
            "slug",
            "created_by",
            "created_time",
            "last_edited_by",
            "last_edited_time",
        ]

    def get_created_by(self, obj):
        """
        Return the interactor information of the object's creator.

        Args:
        ----
            obj: The object containing the creator information.

        Returns:
        -------
            dict: The interactor information of the creator.

        """
        return self.get_interactor_information(obj.created_by)

    def get_last_edited_by(self, obj):
        """
        Return information about the last interactor who edited the object.

        Args:
        ----
            obj: The object containing the last editor information.

        Returns:
        -------
            dict: The interactor information of the last editor.

        """
        return self.get_interactor_information(obj.last_edited_by)


# ########################################################################################
# # ViewSets class
# ########################################################################################


class WorkspaceViewSet(MadbotViewSet):
    """ViewSet for managing workspaces."""

    queryset = Workspace.objects.all().order_by("name")
    serializer_class = WorkspaceSerializer

    def get_queryset(self):
        """
        Retrieve the workspaces for the current user.

        This method filters the workspaces to return only those where the
        current user is a member.

        Returns
        -------
            QuerySet: A queryset of workspaces associated with the current user.

        """
        # Only return workspaces where the current user is a member
        return Workspace.objects.filter(workspace_members__user=self.request.user)

    @extend_schema(
        responses={
            200: WorkspaceSerializer,
        },
    )
    def list(self, request, *args, **kwargs):
        """Retrieve the list of workspace."""
        serializer = self.get_serializer(
            self.get_queryset(),
            many=True,
        )
        return self.get_paginated_response(self.paginate_queryset(serializer.data))

    @extend_schema(
        responses={
            201: WorkspaceSerializer,
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "name",
                        "code": "required",
                        "message": "This field is required",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
        },
    )
    def create(self, request, *args, **kwargs):
        """Create a new workspace object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()
        serializer.save(created_by=request.user, last_edited_by=request.user)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @extend_schema(
        responses={
            200: WorkspaceSerializer,
            **cesr_400_body_validation_error(
                [
                    {
                        "parameter": "name",
                        "code": "required",
                        "message": "This field is required",
                    },
                ],
            ),
            **cesr_400_body_validation_error(
                detail={
                    "parameter": "<parameter>",
                    "code": "not_supported",
                    "message": "This field is not supported.",
                },
            ),
            **cesr_404_object_not_found(),
        },
    )
    def update(self, request, pk):
        """Update a workspace item."""
        item = self.get_object()
        serializer = self.get_serializer(item, data=request.data)
        serializer.is_valid()
        member = WorkspaceMember.objects.filter(workspace=item, user=request.user)
        # Check if the user is part of the workspace
        if not member.exists():
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )
        # Check if the user's role allows update
        if not member.first().role == "owner":
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "The member's role does not allow update.",
                },
            )
        serializer.save(last_edited_by=request.user)
        updated_item = self.get_object()
        updated_serializer = self.get_serializer(updated_item)
        return Response(updated_serializer.data, status=status.HTTP_200_OK)

    @extend_schema(
        responses={
            204: None,
            **cesr_404_object_not_found(),
        },
    )
    def destroy(self, request, pk):
        """Delete a workspace item."""
        item = self.get_object()
        member = WorkspaceMember.objects.filter(workspace=item, user=request.user)
        # Check if the user is part of the workspace
        if not member.exists():
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "You are not a member of this workspace.",
                },
            )
        # Check if the user's role allows deletion
        if not member.first().role == "owner":
            raise ForbiddenException(
                details={
                    "parameter": "workspace_member",
                    "code": "not_allowed",
                    "message": "The member's role does not allow deletion.",
                },
            )
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
