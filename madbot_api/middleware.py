from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from django.contrib.auth.models import AnonymousUser
from oauth2_provider.models import AccessToken


@database_sync_to_async
def get_user(token_key):
    """
    Retrieve the user associated with the given token key.

    Args:
    ----
        token_key (str): The token key to look up.

    Returns:
    -------
        User: The user associated with the token if valid; otherwise, an AnonymousUser.

    """
    try:
        token = AccessToken.objects.get(token=token_key)
        if token.is_valid():
            return token.user
        return AnonymousUser()
    except Exception:
        return AnonymousUser()


class TokenAuthMiddleware(BaseMiddleware):
    """
    Middleware that authenticates users based on a token from the request cookies.

    This middleware extracts an authorization token from the request cookies,
    retrieves the corresponding user, and adds the user to the ASGI scope.
    """

    def __init__(self, app):
        """
        Initialize the TokenAuthMiddleware.

        Args:
        ----
            app: The ASGI application to wrap with this middleware.

        """
        # Store the ASGI application we were passed
        self.app = app

    async def __call__(self, scope, receive, send):
        """
        Process the ASGI scope, adding the user to the scope based on the token.

        Args:
        ----
            scope: The ASGI scope for the request.
            receive: A callable to receive events.
            send: A callable to send events.

        Returns:
        -------
            The response from the wrapped application.

        """
        headers = dict(scope["headers"])
        try:
            token_key = (
                dict(
                    (
                        x.strip().split("=")
                        for x in headers[b"cookie"].decode().split(";")
                    ),
                )
            ).get("AuthorizationToken", None)
        except ValueError:
            token_key = None

        scope["user"] = (
            AnonymousUser() if token_key is None else await get_user(token_key)
        )

        return await self.app(scope, receive, send)


# @user_passes_test(lambda u: u.is_superuser)
# def flower_redirect(request):
#     """Redirect to the flower."""
#     if request.user.is_superuser:
#         return redirect(f"http://{settings.FLOWER_HOST}:{settings.FLOWER_PORT}")
#     return HttpResponseForbidden(
#         "You don't have permission to access Flower. Superuser status is required."
#     )
