from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import include, path, re_path
from django.views.generic import TemplateView
from rest_framework import routers

from madbot_api import settings
from madbot_api.core.url_api import urlpatterns as api_url_patterns
from madbot_api.core.views import schemas, users

router = routers.DefaultRouter()
urlpatterns = [
    path("api/", include(api_url_patterns)),
    path("admin/", admin.site.urls),
    path("o/", include("oauth2_provider.urls", namespace="oauth2_provider")),
    path("invitations/", include("invitations.urls", namespace="invitations")),
    # path(
    #     "accounts/login/",
    #     LoginView.as_view(template_name="registration/login.html"),
    #     name="login",
    # ),
    path("accounts/profile/", users.profile, name="users-profile"),
    path("accounts/logout/", LogoutView.as_view(), name="logout"),
    path(
        "accounts/awaiting-approval/",
        TemplateView.as_view(template_name="account/awaiting_approval.html"),
        name="awaiting_approval",
    ),
    path("accounts/", include("allauth.urls")),
    re_path(
        r"^api/schemas/(?P<source>[^/]+)(?:/(?P<slug>[^/]+))?$",
        schemas.CoreSchemasView.as_view(),
        name="schemas",
    ),
    re_path(
        r"^api/schemas/connectors/(?P<source>[^/]+)(?:/(?P<slug>[^/]+))?$",
        schemas.ConnectorSchemasView.as_view(),
        name="schemas_connectors",
    ),
    path("health-check/", include("health_check.urls")),
]

# Check if Silk is installed before including its URLs
if settings.SILKY_PYTHON_PROFILER:
    urlpatterns += [path("silk/", include("silk.urls", namespace="silk"))]
