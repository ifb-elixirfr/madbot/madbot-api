#!/bin/bash

if ! which vault >/dev/null 2>&1; then
    echo "Please install vault using \`micromamba install -c conda-forge vault=1.8.2\`"
    exit 1
fi

first_run=false
if [ ! -d ".vault" ]; then
    first_run=true
    mkdir -p .vault
    cat <<EOL > .vault/config.hcl
listener "tcp" {
  address = "127.0.0.1:8200"
  tls_disable = 1
}
storage "file" {
path = "./.vault/data"
}
api_addr      = "http://127.0.0.1:8200"
ui            = false
EOL
fi

vault server -config=.vault/config.hcl &
export VAULT_ADDR='http://127.0.0.1:8200'
sleep 5

if [ "$first_run" = true ]; then
    vault operator init -key-shares=1 -key-threshold=1 -format=json > .vault/init.json
    jq -r ".unseal_keys_b64[0]" .vault/init.json > .vault/unseal.key
    jq -r ".root_token" .vault/init.json > .vault/root.token
    # rm .vault/init.json
fi

# unseal vault
vault operator unseal $(cat .vault/unseal.key) > /dev/null

if [ "$first_run" = true ]; then
    # login
    vault login $(cat .vault/root.token) > /dev/null

    # create kv for madbot
    vault secrets enable -path=madbot kv

    # create policy to kv access
    cat <<EOL > .vault/madbot_app_policy.hcl
path "madbot/*" {
  capabilities = ["create", "update", "read", "delete", "list"]
}
EOL
    vault policy write madbot_app .vault/madbot_app_policy.hcl

    # activate approle authentication
    vault auth enable approle

    # create approle for madbot
    vault write auth/approle/role/madbot secret_id_ttl=0 secret_id_num_uses=0 token_num_uses=0 token_max_ttl=1h token_ttl=1h policies=madbot_app
    role_id=$(vault read -format=json auth/approle/role/madbot/role-id | jq -r '.data.role_id')
    secret_id=$(vault write -f auth/approle/role/madbot/secret-id -format=json | jq -r '.data.secret_id')
    
    echo -e "\n\nVault initialized successfully"
    echo "Please use the following settings in your MADBOT .env file :"
    echo "MADBOT_VAULT_URL=$VAULT_ADDR"
    echo "MADBOT_VAULT_MOUNT_POINT=madbot"
    echo "MADBOT_VAULT_ROLEID=$role_id"
    echo "MADBOT_VAULT_SECRETID=$secret_id"
fi

wait